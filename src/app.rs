pub mod data;
pub mod events;
pub mod gui;
pub mod print;
pub mod strings;

use crate::{
    app::{
        data::{
            app_directories::AppDirectories,
            app_midi::AppMidi,
            app_nodes::AppNodes,
            app_options::AppOptions,
            app_threads::AppThreads,
            app_time::AppTime,
            app_vulkano::AppVulkano,
        },
        events::{
            gui_events_buffer::event_recreate_image,
            gui_events_layer::event_layer_remove,
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            // gui_events_storage::StorageEvent,
            gui_events_window::event_window_create,
            window_events::{
                event_window_key,
                event_window_resize,
            },
            GuiEvent,
        },
        gui::layout_gui,
        print::print_info,
        strings::APP_NAME,
    },
    constants::{
        SWAPCHAIN_FORMAT,
        WINDOW_MAIN_HEIGHT,
        WINDOW_MAIN_WIDTH,
        WINDOW_SINK_HEIGHT,
        WINDOW_SINK_WIDTH,
    },
    impls::files::{
        file_source_added,
        texture_received,
    },
    nodes::{
        control::update_control_nodes,
        filter::update_filter_nodes,
        sinks::{
            window::render_sink_windows,
            update_sink_nodes,
        },
        sources::{
            SourceNode,
            update_source_nodes,
        },
        NodeFamily,
    },
};
#[cfg(unix)]
use crate::{
    app::{
        data::app_pipewire::AppPipewire,
        events::gui_events_pipewire::event_pipewire_frame,
    },
    impls::pipewire::do_roundtrip,
};
use egui_winit_vulkano::{
    Gui,
    GuiConfig,
    RenderResources,
};
use std::time::Instant;
use vulkano::VulkanError;
use vulkano_util::{
    renderer::VulkanoWindowRenderer,
    window::{
        WindowDescriptor,
        WindowResizeConstraints,
    },
};
use winit::{
    application::ApplicationHandler,
    event::{
        ElementState,
        // Event,
        MouseButton,
        WindowEvent,
    },
    event_loop::{
        ActiveEventLoop,
        // EventLoop,
        EventLoopProxy,
    },
    window::WindowId,
};

// ----------------------------------------------------------------------------

/// App for creating art.
// #[derive(Debug, Clone)]
pub struct
    PwVideomixApp
{
    pub directories: AppDirectories,
    pub midi: AppMidi,
    pub nodes: AppNodes,
    pub options: AppOptions,
    #[cfg(unix)]
    pub pipewire: AppPipewire,
    pub proxy: Option<EventLoopProxy<GuiEvent>>,
    pub threads: AppThreads,
    pub time: AppTime,
    pub vulkano: AppVulkano,
}

impl
    Default
for
    PwVideomixApp
{
    fn
        default()
    ->
        Self
    {
        Self {
            directories:
                AppDirectories::default(),
            midi:
                AppMidi::default(),
            nodes:
                AppNodes::default(),
            options:
                AppOptions::default(),
            #[cfg(unix)]
            pipewire:
                AppPipewire::default(),
            proxy:
                None,
            threads:
                AppThreads::default(),
            time:
                AppTime::default(),
            vulkano:
                AppVulkano::default(),
        }
    }
}

impl
    ApplicationHandler<GuiEvent>
for
    PwVideomixApp
{
    // fn user_event(&mut self, event_loop: &ActiveEventLoop, user_event: MyUserEvent) {
    //     // Handle user event.
    // }
    //
    // fn resumed(&mut self, event_loop: &ActiveEventLoop) {
    //     // Your application got resumed.
    // }
    //
    // fn window_event(&mut self, event_loop: &ActiveEventLoop, window_id: WindowId, event: WindowEvent) {
    //     // Handle window event.
    // }
    //
    // fn device_event(&mut self, event_loop: &ActiveEventLoop, device_id: DeviceId, event: DeviceEvent) {
    //     // Handle device event.
    // }
    //
    // fn about_to_wait(&mut self, event_loop: &ActiveEventLoop) {
    //     self.window.request_redraw();
    //     self.counter += 1;
    // }
    fn
        resumed(
            &mut self,
            event_loop: &ActiveEventLoop,
        )
    {
        self
            .vulkano
            .windows
            .create_window(
                event_loop,
                &self
                    .vulkano
                    .context,
                &WindowDescriptor {
                    title: String::from(APP_NAME),
                    width: WINDOW_MAIN_WIDTH,
                    height: WINDOW_MAIN_HEIGHT,
                    resize_constraints: WindowResizeConstraints {
                        min_width: 64.0,
                        min_height: 64.0,
                        ..Default::default()
                    },
                    resizable: true,
                    ..Default::default()
                },
                |ci| {
                    ci.image_format = SWAPCHAIN_FORMAT;
                    ci.min_image_count = ci
                        .min_image_count
                        .max(
                            2,
                        );
                },
            );
        // Create gui as main render pass:
        // (no overlay means it clears the image each frame).
        let
            renderer
        :
            &mut VulkanoWindowRenderer
        =
            self
                .vulkano
                .windows
                .get_primary_renderer_mut()
                .unwrap();

        self
            .vulkano
            .gui_main
        =
            Some(
                Gui::new(
                    event_loop,
                    renderer
                        .surface(),
                    renderer
                        .graphics_queue(),
                    renderer
                        .swapchain_format(),
                    GuiConfig {
                        // allow_srgb_render_target: true,
                        ..Default::default()
                    },
                ),
            );

        // let window_main: WindowId = renderer
        //     .window()
        //     .id();

        // Print info:
        print_info(
            &self
                .vulkano
                .context,
            event_loop,
            &renderer
                .swapchain_format(),
        );
    }

    fn about_to_wait(
        &mut self,
        _event_loop: &ActiveEventLoop,
    ) {
        // println!("main events cleared");
        self
            .time
            .updated
        =
            Instant::now();
        // println!(
        //     "updated: {:?}\ntime: {:?}",
        //     self.time.updated,
        //     self.time.time,
        // );

    // Update pipewire:
        #[cfg(unix)]
        do_roundtrip(
            &self
                .pipewire
                .pipewire_loop
                .core,
            self
                .pipewire
                .pipewire_loop
                .mainloop
                .clone(),
        );

        match
            self
                .vulkano
                .gui_main
                .as_mut()
        {
            None => (),
            Some(gui) => {
                let render_resources: RenderResources = gui
                    .render_resources();
                // Reading receivers from threads:
                // Handle dropped or opened files:
                // Receive Vec<u8> image data from texture thread.
                // "Only" one picture per frame???
                match
                    self
                        .threads
                        .texture_channel
                        .receiver
                        .try_recv()
                {
                    Ok(texture_message) => {
                        texture_received(
                            self
                                .vulkano
                                .command_buffer_allocator
                                .clone(),
                            self
                                .nodes
                                .map_source_to_node
                                .clone(),
                            render_resources
                                .clone(),
                            texture_message,
                        );
                    },
                    Err(_) => (),
                };
            // Update control nodes:
                update_control_nodes(
                    self
                        .nodes
                        .map_control_to_node
                        .clone(),
                    self
                        .nodes
                        .map_filter_to_node
                        .clone(),
                    self
                        .nodes
                        .map_sink_to_node
                        .clone(),
                    self
                        .nodes
                        .map_source_to_node
                        .clone(),
                    render_resources
                        .clone(),
                );
        // Update source nodes:
                update_source_nodes(
                    self
                        .vulkano
                        .command_buffer_allocator
                        .clone(),
                    self
                        .vulkano
                        .descriptor_set_allocator
                        .clone(),
                    self
                        .nodes
                        .map_source_to_node
                        .clone(),
                    render_resources
                        .clone(),
                    #[cfg(feature = "gstreamer")]
                    self
                        .time
                        .updated
                        .clone(),
                );
        // Update filter nodes:
                update_filter_nodes(
                    self
                        .vulkano
                        .command_buffer_allocator
                        .clone(),
                    self
                        .vulkano
                        .descriptor_set_allocator
                        .clone(),
                    self
                        .time
                        .avg_fps,
                    self
                        .nodes
                        .map_filter_to_node
                        .clone(),
                    render_resources
                        .clone(),
                );
            // Update sink nodes:
                update_sink_nodes(
                    self
                        .vulkano
                        .command_buffer_allocator
                        .clone(),
                    #[cfg(unix)]
                    self
                        .pipewire
                        .map_node_to_stream
                        .clone(),
                    self
                        .nodes
                        .map_sink_to_node
                        .clone(),
                    self
                        .vulkano
                        .map_window_to_gui
                        .clone(),
                    render_resources
                        .clone(),
                    &self
                        .vulkano
                        .windows,
                );
            },
        };


    // Request redraws:
        // temporarily always redraw every sink window until moved to specific places???
        for
            (
                _window_id,
                renderer,
            )
        in
            self
                .vulkano
                .windows
                .iter_mut()
        {
            renderer
                .window()
                .request_redraw();
        };

        // renderer
        //     .window()
        //     .request_redraw();
    }

    fn
        window_event(
            &mut self,
            event_loop: &ActiveEventLoop,
            window_id: WindowId,
            event: WindowEvent,
        )
    {
        let event_loop_proxy: EventLoopProxy<GuiEvent> = match
            self
                .proxy
                .clone()
        {
            None => panic!("!! error: EventLoopProxy not found"),
            Some(proxy) => proxy,
        };
        match
            self
                .vulkano
                .windows
                .primary_window_id()
        {
            None => println!("!! error: no primary window id found"),
            Some(main_window_id) => match
                self
                    .vulkano
                    .windows
                    .get_renderer_mut(
                        window_id,
                    )
            {
                None => println!("!! error: renderer not found for: {window_id:?}"),
                Some(renderer) => match
                    self
                        .vulkano
                        .gui_main
                        .as_mut()

                {
                    None => println!("!! error: gui not found for: {window_id:?}"),
                    Some(gui) => {
                        // Update Egui integration so the UI works!
                        let
                            _pass_events_to_game
                        :
                            bool
                        =
                            !gui
                                .update(
                                    &event,
                                );

                        if
                            main_window_id
                            ==
                            window_id
                        {
                // Main panel window:
                            match
                                event
                            {
                            // Redraw:
                                WindowEvent::RedrawRequested => {
                                    // Update time and FPS:
                                    // really updating???
                                    self
                                        .time
                                        .update();
                                    // Create ui:
                                    gui
                                        .immediate_ui(
                                            |gui_immediate| {
                                                layout_gui(
                                                    self
                                                        .time
                                                        .avg_fps,
                                                    self
                                                        .vulkano
                                                        .command_buffer_allocator
                                                        .clone(),
                                                    gui_immediate
                                                        .context(),
                                                    self
                                                        .vulkano
                                                        .descriptor_set_allocator
                                                        .clone(),
                                                    event_loop_proxy,
                                                    self
                                                        .nodes
                                                        .map_control_to_node
                                                        .clone(),
                                                    #[cfg(unix)]
                                                    self
                                                        .pipewire
                                                        .map_id_to_camera
                                                        .clone(),
                                                    self
                                                        .nodes
                                                        .map_filter_to_node
                                                        .clone(),
                                                    #[cfg(unix)]
                                                    self
                                                        .pipewire
                                                        .map_node_to_stream
                                                        .clone(),
                                                    self
                                                        .nodes
                                                        .map_sink_to_node
                                                        .clone(),
                                                    self
                                                        .nodes
                                                        .map_source_to_node
                                                        .clone(),
                                                    self
                                                        .midi
                                                        .midi_node_rc
                                                        .clone(),
                                                    self
                                                        .options
                                                        .gui
                                                        .clone(),
                                                    #[cfg(unix)]
                                                    self
                                                        .pipewire
                                                        .pipewire_loop
                                                        .core
                                                        .clone(),
                                                    gui_immediate
                                                        .render_resources(),
                                                    self
                                                        .threads
                                                        .texture_channel
                                                        .sender
                                                        .clone(),
                                                );
                                            }
                                        );
                                    // Render gui
                                    match
                                        renderer
                                            .acquire(
                                                None,
                                                |_| {},
                                            )
                                    {
                                        Err(
                                            VulkanError::OutOfDate
                                        ) => {
                                            renderer
                                                .resize();
                                            println!(
                                                "!! error: {:?}\n!! recreate surface of resized window",
                                                VulkanError::OutOfDate,
                                            );
                                        },
                                        Err(error) => {
                                            panic!("!! error: {error}");
                                        },
                                        Ok(before_future) => {
                                            // Acquire swapchain future
                                            let after_future = gui
                                                .draw_on_image(
                                                    before_future,
                                                    renderer
                                                        .swapchain_image_view(),
                                                );
                                            // Present swapchain:
                                            renderer
                                                .present(
                                                    after_future,
                                                    true,
                                                );
                                        },
                                    };
                                },
                            // Close requested:
                                WindowEvent::CloseRequested => {
                                    event_loop
                                        .exit();
                                },
                            // Dropped files:
                                WindowEvent::DroppedFile(path) => {
                                    println!("-> file dropped: {path:?}");
                                    file_source_added(
                                        self
                                            .nodes
                                            .map_source_to_node
                                            .clone(),
                                        vec!(path),
                                        gui
                                            .render_resources()
                                            .clone(),
                                        self
                                            .threads
                                            .texture_channel
                                            .sender
                                            .clone(),
                                    );
                                },
                            // Keyboard input:
                                WindowEvent::KeyboardInput { device_id: _, event, is_synthetic: _ } => {
                                    event_window_key(
                                        event,
                                        self
                                            .nodes
                                            .map_sink_to_node
                                            .clone(),
                                        self
                                            .vulkano
                                            .map_window_to_sink
                                            .clone(),
                                        window_id,
                                    );
                                },
                            // Resized:
                                WindowEvent::Resized(_) => {
                                    renderer
                                        .resize();
                                },
                            // Scale factor changed:
                                WindowEvent::ScaleFactorChanged { .. } => {
                                    renderer
                                        .resize();
                                },
                            // Loop destroyed:
                                WindowEvent::Destroyed => {
                                    // Close MIDI connections and remove ports.
                                    match
                                        self
                                            .midi
                                            .midi_connection_rc
                                            .replace(None)
                                    {
                                        None => (),
                                        Some(connection) => {
                                            println!("ii disconnect from MIDI source");
                                            connection
                                                .input
                                                .close();
                                            // Switch off all LEDs!!!
                                            connection
                                                .output
                                                .close();
                                        },
                                    };

                                    // Quit pipewire mainloop.
                                    #[cfg(unix)]
                                    {
                                        println!(".. quitting PipeWire");
                                        self
                                            .pipewire
                                            .pipewire_loop
                                            .mainloop
                                            .quit();
                                    }
                                    // #[cfg(unix)]
                                    // Deinit pipewire???
                                    println!(".. quitting");
                                    println!("\n.. done");
                                },
                                // Event::DeviceEvent { device_id, event } => (),
                                // Event::NewEvents(_) => (),
                                // Event::RedrawEventsCleared => (),
                                // Event::Resumed => (),
                                // Event::Suspended => (),
                                _ => (),
                            };
                // Sink window:
                        } else {
                            match
                                event
                            {
                            // Redraw:
                                WindowEvent::RedrawRequested => {
                                    render_sink_windows(
                                        self
                                            .vulkano
                                            .descriptor_set_allocator
                                            .clone(),
                                        self
                                            .nodes
                                            .map_sink_to_node
                                            .clone(),
                                        self
                                            .vulkano
                                            .map_window_to_gui
                                            .clone(),
                                        self
                                            .vulkano
                                            .map_window_to_sink
                                            .clone(),
                                        renderer,
                                        window_id,
                                    );
                                },
                            // Close requested:
                                WindowEvent::CloseRequested => match
                                    self
                                        .vulkano
                                        .map_window_to_sink
                                        .borrow()
                                        .get(&window_id)
                                {
                                    None => (),
                                    Some(sink_id) => {
                                        event_loop_proxy
                                            .send_event(
                                                GuiEvent::ChangeNode(
                                                    NodeEvent {
                                                        id: sink_id.to_owned(),
                                                        event_class: NodeEventClass::Remove,
                                                        family: NodeFamily::Sink,
                                                    }
                                                )
                                            )
                                            .ok();
                                    },
                                },
                            // Mouse input:
                                #[allow(deprecated)]
                                WindowEvent::MouseInput { device_id: _, state, button} => match
                                    state
                                {
                                    ElementState::Released => (),
                                    ElementState::Pressed => {
                                        match
                                            button
                                        {
                                            MouseButton::Middle => {
                                                println!("-- toggle fullscreen test");
                                            },
                                            _ => (),
                                        };
                                    },
                                },
                            // Keyboard input:
                                WindowEvent::KeyboardInput { device_id: _, event, is_synthetic: _ } => {
                                    event_window_key(
                                        event,
                                        self
                                            .nodes
                                            .map_sink_to_node
                                            .clone(),
                                        self
                                            .vulkano
                                            .map_window_to_sink
                                            .clone(),
                                        window_id,
                                    );
                                },
                            // Resized:
                                WindowEvent::Resized(_) => {
                                    event_window_resize(
                                        self
                                            .nodes
                                            .map_sink_to_node
                                            .clone(),
                                        self
                                            .vulkano
                                            .map_window_to_sink
                                            .clone(),
                                        renderer,
                                        window_id,
                                    );
                                },
                            // Scale factor changed:
                                WindowEvent::ScaleFactorChanged { .. } => {
                                    renderer
                                        .resize();
                                },
                            // Sink window closed:
                                WindowEvent::Destroyed => {
                                },
                                _ => (),
                            };
                        };
                    },
                },
            },
        };
    }

// GuiEvent
    fn user_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        user_event: GuiEvent,
    ) {
        match
            user_event
        {
        // Handle dropped or opened files:
            // GuiEvent::AddFiles(files_added) => {
            //     file_source_added(
            //         files_added,
            //         picture_channel.sender.clone(),
            //     );
            // },
        // Remove one input layer from multiple sources of a filter node.
            GuiEvent::LayerRemove((filter_id, layer_id)) => {
                event_layer_remove(
                    filter_id,
                    layer_id,
                    self
                        .nodes
                        .map_filter_to_node
                        .clone(),
                );
            },
        // Node events (select, deselect, remove):
            GuiEvent::ChangeNode(node_event) => {
                let event_loop_proxy: EventLoopProxy<GuiEvent> = match
                    self
                        .proxy
                        .clone()
                {
                    None => panic!("!! error: EventLoopProxy not found"),
                    Some(proxy) => proxy,
                };
                node_event
                    .event_change_node(
                        event_loop_proxy
                            .clone(),
                        self
                            .nodes
                            .map_control_to_node
                            .clone(),
                        self
                            .nodes
                            .map_filter_to_node
                            .clone(),
                        #[cfg(unix)]
                        self
                            .pipewire
                            .map_node_to_stream
                            .clone(),
                        self
                            .nodes
                            .map_sink_to_node
                            .clone(),
                        self
                            .nodes
                            .map_source_to_node
                            .clone(),
                        self
                            .vulkano
                            .map_window_to_gui
                            .clone(),
                        self
                            .options
                            .gui
                            .clone(),
                    );
            },
        // Selected node to open a side panel with more options:
            // usage???
            GuiEvent::SelectedNode => {
                println!("!! gui event received: selected node");
            },
        // MIDI:
            GuiEvent::Midi(midi_event) => {
                let event_loop_proxy: EventLoopProxy<GuiEvent> = match
                    self
                        .proxy
                        .clone()
                {
                    None => panic!("!! error: EventLoopProxy not found"),
                    Some(proxy) => proxy,
                };
                midi_event
                    .event_received(
                        event_loop_proxy,
                        self
                            .nodes
                            .map_control_to_node
                            .clone(),
                        self
                            .nodes
                            .map_filter_to_node
                            .clone(),
                        self
                            .nodes
                            .map_sink_to_node
                            .clone(),
                        self
                            .nodes
                            .map_source_to_node
                            .clone(),
                        self
                            .midi
                            .midi_connection_rc
                            .clone(),
                        self
                            .midi
                            .midi_node_rc
                            .clone(),
                        self
                            .options
                            .gui
                            .clone(),
                    );
            },
        // Pipewire frame received:
            #[cfg(unix)]
            GuiEvent::PipewireFrame((picture_buffer, source_id)) => {
                match
                    self
                        .nodes
                        .map_source_to_node
                        .borrow()
                        .map
                        .get(&source_id)
                {
                    None => (),
                    Some(SourceNode::PipewireInput(pw_input_node)) => {
                        event_pipewire_frame(
                            self
                                .vulkano
                                .command_buffer_allocator
                                .clone(),
                            pw_input_node
                                .scene
                                .image_storage
                                .clone(),
                            picture_buffer,
                            self
                                .vulkano
                                .gui_main
                                .as_mut()
                                .unwrap()
                                .render_resources()
                                .clone(),
                        );
                    },
                    Some(_) => (),
                };
            },
        // Size of the output image has changed:
            // Create a new image.
            GuiEvent::RecreateImage((node_family, node_id, size)) => {
                event_recreate_image(
                    self
                        .nodes
                        .map_filter_to_node
                        .clone(),
                    self
                        .nodes
                        .map_source_to_node
                        .clone(),
                    node_family,
                    node_id,
                    self
                        .vulkano
                        .gui_main
                        .as_mut()
                        .unwrap()
                        .render_resources(),
                    size,
                );
            },
        // Save or load storage event:
            GuiEvent::Storage(storage_event) => {
                let event_loop_proxy: EventLoopProxy<GuiEvent> = match
                    self
                        .proxy
                        .clone()
                {
                    None => panic!("!! error: EventLoopProxy not found"),
                    Some(proxy) => proxy,
                };
                storage_event
                    .event_storage(
                        event_loop_proxy,
                        self
                            .nodes
                            .map_control_to_node
                            .clone(),
                        self
                            .nodes
                            .map_filter_to_node
                            .clone(),
                        #[cfg(unix)]
                        self
                            .pipewire
                            .map_node_to_stream
                            .clone(),
                        self
                            .nodes
                            .map_sink_to_node
                            .clone(),
                        self
                            .nodes
                            .map_source_to_node
                            .clone(),
                        // self
                        //     .vulkano
                        //     .map_window_to_gui
                        //     .clone(),
                        // self
                        //     .vulkano
                        //     .map_window_to_sink
                        //     .clone(),
                        self
                            .options
                            .gui
                            .clone(),
                        self
                            .vulkano
                            .gui_main
                            .as_mut()
                            .unwrap()
                            .render_resources()
                            .clone(),
                    );
            },
        // Button "Output > Window" has been pressed:
            GuiEvent::WindowCreate => {
                // Create sink window:
                let window_id: WindowId = self
                    .vulkano
                    .windows
                    .create_window(
                        event_loop,
                        &self
                            .vulkano
                            .context,
                        &WindowDescriptor {
                            title: String::from("pw-videomix - sink window"),
                            width: WINDOW_SINK_WIDTH,
                            height: WINDOW_SINK_HEIGHT,
                            resizable: true,
                            resize_constraints: WindowResizeConstraints {
                                min_width: 64.0,
                                min_height: 64.0,
                                ..Default::default()
                            },
                            ..WindowDescriptor::default()
                        },
                        |create_info| {
                            create_info.image_format = SWAPCHAIN_FORMAT;
                            create_info.min_image_count = create_info
                                .min_image_count
                                .max(
                                    2,
                                );
                        },
                    );
                let renderer: &mut VulkanoWindowRenderer = self
                    .vulkano
                    .windows
                    .get_renderer_mut(
                        window_id,
                    )
                    .unwrap();

                // Create sink window:
                event_window_create(
                    // &self
                    //     .vulkano
                    //     .context,
                    event_loop,
                    self
                        .nodes
                        .map_sink_to_node
                        .clone(),
                    self
                        .vulkano
                        .map_window_to_gui
                        .clone(),
                    self
                        .vulkano
                        .map_window_to_sink
                        .clone(),
                    renderer,
                    window_id,
                    // self
                    //     .vulkano
                    //     .windows,
                );
            },
        // Remove sink window:
            GuiEvent::WindowRemove(window_id) => {

                self
                    .vulkano
                    .windows
                    .remove_renderer(
                        window_id,
                    );
                self
                    .vulkano
                    .map_window_to_sink
                    .borrow_mut()
                    .remove(&window_id);
            },

        };
    }

}

impl PwVideomixApp {
    /// Updates times and dt at the end of each frame.
    pub fn update_time(
        &mut self,
    ) {
        // Each second, update average fps & reset frame count & dt sum.
        self
            .time
            .update();
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
