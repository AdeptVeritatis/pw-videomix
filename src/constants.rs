
use egui_winit_vulkano::egui::{Color32, Vec2};
use std::{
    f32::consts::FRAC_PI_2,
    ops::RangeInclusive,
};
use vulkano::format::Format;

// ----------------------------------------------------------------------------
// Gui:
// ----------------------------------------------------------------------------

pub const IMAGE_FORMAT: Format = Format::R8G8B8A8_UNORM;
// pub const IMAGE_FORMAT: Format = Format::B8G8R8A8_UNORM;
pub const SWAPCHAIN_FORMAT: Format = Format::B8G8R8A8_UNORM;
// pub const SWAPCHAIN_FORMAT: Format = Format::R8G8B8A8_SRGB;
pub const FILTER_NODE_SIZE: f32 = 160.0;
pub const MENU_SPACING: f32 = 16.0;
pub const PANEL_ADD_SPACING_TITLE: f32 = 24.0;
pub const PANEL_ADD_SPACING_BUTTONS: f32 = 8.0;
pub const PANEL_ADD_WIDTH: f32 = 160.0;
pub const PANEL_NODE_SPACING_SMALL: f32 = 7.0;
pub const PANEL_NODE_SPACING_TITLE: f32 = 24.0;
pub const PANEL_NODE_GRID_WIDTH_MIN: f32 = 50.0;
pub const PICTURE_PREVIEW_SIZE: f32 = 160.0;
pub const PROJECT_DIRECTORY: &str = "projects/";
pub const PROJECT_PATH: &str = "projects/example.json";
pub const TEXT_CATEGORY_SIZE: f32 = 16.0;
pub const WINDOW_MAIN_HEIGHT: f32 = 720.0; //480.0;
pub const WINDOW_MAIN_WIDTH: f32 = 1280.0; //640.0;
pub const WINDOW_NODE_SIZE: Vec2 = Vec2::new(160.0, 160.0); // 320.0

// ----------------------------------------------------------------------------
// Nodes:
// ----------------------------------------------------------------------------

pub const FPS_DEFAULT: u32 = 6;
pub const STANDARD_NODE_BACKGROUND: Color32 = Color32::DARK_GRAY;
pub const STANDARD_NODE_OUTPUT_HEIGHT: f32 = 480.0;
pub const STANDARD_NODE_OUTPUT_WIDTH: f32 = 480.0;
pub const STANDARD_NODE_SIZE: f32 = 160.0;
pub const NODE_OUTPUT_HEIGHT: f32 = 480.0; // still needed???
pub const NODE_OUTPUT_WIDTH: f32 = 480.0; // still needed???

// ----------------------------------------------------------------------------
// Nodes - connector:
// ----------------------------------------------------------------------------

// If rounding >= half the size, connectors are circles instead of rectangles.
// Different colors for different connector states???
pub const CONNECTOR_COLOR: Color32 = Color32::DARK_GRAY;
pub const CONNECTOR_ROUNDING: f32 = 0.0;
pub const CONNECTOR_SIZE: f32 = 5.0;
pub const CONNECTOR_STIFFNESS: f32 = 64.0;
pub const CONNECTOR_WIDTH: f32 = 1.0; // line width
// What about offsets for exact positioning of the connections???

// ----------------------------------------------------------------------------
// Nodes - fps:
// ----------------------------------------------------------------------------

pub const NODE_FPS_DECIMALS: usize = 2;
// pub const NODE_FPS_OFFSET: f32 = 1.0;
pub const NODE_FPS_RANGE: RangeInclusive<f32> = RangeInclusive::new(0.01, 240.0);
pub const NODE_FPS_SPEED: f32 = 0.01;

// ----------------------------------------------------------------------------
// Nodes - layer:
// ----------------------------------------------------------------------------

pub const NODE_LAYER_CENTER_DECIMALS: usize = 3;
pub const NODE_LAYER_CENTER_OFFSET: f32 = 0.0;
pub const NODE_LAYER_CENTER_RANGE: RangeInclusive<f32> = RangeInclusive::new(-1.0, 1.0);
pub const NODE_LAYER_CENTER_SPEED: f32 = 0.001;
pub const NODE_LAYER_MIX_DECIMALS: usize = 3;
pub const NODE_LAYER_MIX_OFFSET: f32 = 0.0; // or 1.0???
pub const NODE_LAYER_MIX_RANGE: RangeInclusive<f32> = RangeInclusive::new(0.0, 1.0);
pub const NODE_LAYER_MIX_SPEED: f32 = 0.001;
pub const NODE_LAYER_SCALE_DECIMALS: usize = 2;
pub const NODE_LAYER_SCALE_OFFSET: f32 = 1.0;
pub const NODE_LAYER_SCALE_RANGE: RangeInclusive<f32> = RangeInclusive::new(0.01, 10.0);
pub const NODE_LAYER_SCALE_SPEED: f32 = 0.01; // 0.001

// ----------------------------------------------------------------------------
// Nodes - midi:
// ----------------------------------------------------------------------------

pub const MIDI_EVENT_CONTROLLER: u8 = 176; // knobs, rotary encoder, slider and modulation wheels (coarse)
pub const MIDI_EVENT_KEY_NOTE_ON: u8 = 144; // piano keys
pub const MIDI_EVENT_KEY_NOTE_OFF: u8 = 128; // piano keys
pub const MIDI_EVENT_PAD_NOTE_ON: u8 = 153; // pads
pub const MIDI_EVENT_PAD_NOTE_OFF: u8 = 137; // pads
pub const MIDI_EVENT_PAD_AFTERTOUCH: u8 = 169; // pads
pub const MIDI_EVENT_PITCH_BEND: u8 = 224; // pitch bend (higher value range)

// ----------------------------------------------------------------------------
//
// Nodes - Control
//
// ----------------------------------------------------------------------------

pub const CONTROL_NODE_SIZE: f32 = 60.0;
pub const CONTROL_VALUE_DEFAULT: f32 = 0.0;

// ----------------------------------------------------------------------------
// Nodes - trigger:
// ----------------------------------------------------------------------------

pub const TRIGGER_DEFAULT_FRAME_COUNT: usize = 1;
pub const TRIGGER_PRECISION_DECIMALS: usize = 0;
pub const TRIGGER_PRECISION_RANGE: RangeInclusive<u32> = RangeInclusive::new(0, 6);
pub const TRIGGER_PRECISION_SPEED: f32 = 0.1; // ??? 1.0 ??? but not used!!! ???

// ----------------------------------------------------------------------------
//
// Nodes - Filter
//
// ----------------------------------------------------------------------------

pub const FILTER_MOVE_DECIMALS: usize = 3;
pub const FILTER_MOVE_OFFSET_MESH: f32 = 0.0;
pub const FILTER_MOVE_OFFSET_TEX: f32 = 0.5;
pub const FILTER_MOVE_RANGE_MESH: RangeInclusive<f32> = RangeInclusive::new(-1.0, 1.0);
pub const FILTER_MOVE_RANGE_TEX: RangeInclusive<f32> = RangeInclusive::new(0.0, 1.0);
pub const FILTER_MOVE_SPEED: f32 = 0.001;

// ----------------------------------------------------------------------------
// Nodes - color mixer:
// ----------------------------------------------------------------------------

pub const COLOR_MIXER_DECIMALS: usize = 3;
pub const COLOR_MIXER_OFFSET: f32 = 1.0;
pub const COLOR_MIXER_RANGE: RangeInclusive<f32> = RangeInclusive::new(-2.0, 2.0);
pub const COLOR_MIXER_SPEED: f32 = 0.002;

// ----------------------------------------------------------------------------
// Nodes - color rotator:
// ----------------------------------------------------------------------------

pub const COLOR_ROTATOR_ANGLE_DECIMALS: usize = 4;
pub const COLOR_ROTATOR_ANGLE_OFFSET: f32 = 0.0;
pub const COLOR_ROTATOR_ANGLE_RANGE: RangeInclusive<f32> = RangeInclusive::new(-2.0, 2.0); // Larger than needed to loop the value.
pub const COLOR_ROTATOR_ANGLE_SPEED: f32 = 0.0001;
pub const COLOR_ROTATOR_BOUND_DECIMALS: usize = 3;
pub const COLOR_ROTATOR_BOUND_HIGH_OFFSET: f32 = 1.0;
pub const COLOR_ROTATOR_BOUND_LOW_OFFSET: f32 = 0.0;
pub const COLOR_ROTATOR_BOUND_RANGE: RangeInclusive<f32> = RangeInclusive::new(-2.0, 2.0);
pub const COLOR_ROTATOR_BOUND_SPEED: f32 = 0.001;
pub const COLOR_ROTATOR_GAIN_DECIMALS: usize = 3;
pub const COLOR_ROTATOR_GAIN_OFFSET: f32 = 1.0;
pub const COLOR_ROTATOR_GAIN_RANGE: RangeInclusive<f32> = RangeInclusive::new(0.0, 2.0);
pub const COLOR_ROTATOR_GAIN_SPEED: f32 = 0.001;
pub const COLOR_ROTATOR_SPEED_DECIMALS: usize = 4;
pub const COLOR_ROTATOR_SPEED_OFFSET: f32 = 0.0;
pub const COLOR_ROTATOR_SPEED_RANGE: RangeInclusive<f32> = RangeInclusive::new(-0.25, 0.25);
pub const COLOR_ROTATOR_SPEED_SPEED: f32 = 0.0001;

// ----------------------------------------------------------------------------
// Nodes - fader:
// ----------------------------------------------------------------------------

pub const FADER_TIME_OFFSET: f32 = 1000.0;
pub const FADER_TIME_RANGE: RangeInclusive<f32> = RangeInclusive::new(0.0, 3599999.0);
pub const FADER_TIME_SPEED: f32 = 1.0;

// ----------------------------------------------------------------------------
// Nodes - kaleidoscope:
// ----------------------------------------------------------------------------

// pub const KALEIDOSCOPE_COLOR_ACTIVE: Color32 = Color32::BLACK;
pub const KALEIDOSCOPE_COLOR_INACTIVE: Color32 = Color32::GRAY;

pub const KALEIDOSCOPE_ANGLE_DECIMALS: usize = 3;
pub const KALEIDOSCOPE_ANGLE_OFFSET: f32 = 0.0; // 0.5; // std::f32::consts::FRAC_PI_2, // 90°
pub const KALEIDOSCOPE_ANGLE_RANGE: RangeInclusive<f32> = RangeInclusive::new(-0.9 * FRAC_PI_2, 0.9 * FRAC_PI_2);
pub const KALEIDOSCOPE_ANGLE_SPEED: f32 = 0.001;
pub const KALEIDOSCOPE_AXIS_DECIMALS: usize = 3;
pub const KALEIDOSCOPE_AXIS_OFFSET: f32 = 1.0;
pub const KALEIDOSCOPE_AXIS_RANGE: RangeInclusive<f32> = RangeInclusive::new(0.01, 1.0); // 0.001
pub const KALEIDOSCOPE_AXIS_SPEED: f32 = 0.001;
// axis_range 0.01 -> (2*100 + 2) * (2*100 + 2) -> 40804
// axis_range 0.001 -> (2*1000 + 2) * (2*1000 + 2) -> 4008004
// pub const KALEIDOSCOPE_TILES_MAX: u32 = 40804;

// ----------------------------------------------------------------------------
// Nodes - mandala:
// ----------------------------------------------------------------------------

pub const MANDALA_ANGLE_DECIMALS: usize = 1;
pub const MANDALA_ANGLE_OFFSET: f32 = 0.0;
pub const MANDALA_ANGLE_RANGE: RangeInclusive<f32> = RangeInclusive::new(-360.0, 360.0);
pub const MANDALA_ANGLE_SPEED: f32 = 0.1;
pub const MANDALA_REPETITIONS_DECIMALS: usize = 0;
pub const MANDALA_REPETITIONS_OFFSET: u32 = 6;
pub const MANDALA_REPETITIONS_RANGE: RangeInclusive<u32> = RangeInclusive::new(2, 360);
pub const MANDALA_REPETITIONS_SPEED: f32 = 0.1; // ??? 1.0 ??? but not used!!! ???
pub const MANDALA_ROTATE_PRESET: bool = false;
pub const MANDALA_ROTATION_SPEED_DECIMALS: usize = 5;
pub const MANDALA_ROTATION_SPEED_OFFSET: f32 = 0.0;
pub const MANDALA_ROTATION_SPEED_RANGE: RangeInclusive<f32> = RangeInclusive::new(-1.0, 1.0);
pub const MANDALA_ROTATION_SPEED_SPEED: f32 = 0.00001;
pub const MANDALA_SIZE_DECIMALS: usize = 3;
pub const MANDALA_SIZE_OFFSET: f32 = 1.0;
pub const MANDALA_SIZE_RANGE: RangeInclusive<f32> = RangeInclusive::new(0.01, 10.0);
pub const MANDALA_SIZE_SPEED: f32 = 0.001;

// ----------------------------------------------------------------------------
// Nodes - mixer:
// ----------------------------------------------------------------------------

pub const MIXER_MAX_INPUTS: u32 = 64;

// ----------------------------------------------------------------------------
//
// Nodes - Sinks
//
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// Nodes - encoder:
// ----------------------------------------------------------------------------

pub const ENCODER_FILE_STRING: &str = "video";
pub const ENCODER_FPS: f32 = 60.0;
// pub const ENCODER_SINK_HEIGHT: f32 = 180.0;
// pub const ENCODER_SINK_WIDTH: f32 = 240.0;

// ----------------------------------------------------------------------------
// Nodes - monitor:
// ----------------------------------------------------------------------------

pub const MONITOR_SINK_HEIGHT: f32 = 180.0;
pub const MONITOR_SINK_WIDTH: f32 = 240.0;

// ----------------------------------------------------------------------------
// Nodes - snapshot:
// ----------------------------------------------------------------------------

pub const SNAPSHOT_FILE_STRING: &str = "pic";
pub const SNAPSHOT_SERIES_MAX: u32 = 9999;

pub const SNAPSHOT_DECIMALS: usize = 0;
pub const SNAPSHOT_SPEED: f32 = 0.1;
pub const SNAPSHOT_FACTOR: u32 = 10;

pub const SNAPSHOT_COUNTER_OFFSET: u32 = 0;
pub const SNAPSHOT_COUNTER_RANGE: RangeInclusive<u32> = RangeInclusive::new(0, SNAPSHOT_SERIES_MAX);

pub const SNAPSHOT_FRAME_OFFSET: u32 = 1;
pub const SNAPSHOT_FRAME_RANGE: RangeInclusive<u32> = RangeInclusive::new(1, 36000); // up to 10 min @ 60 fps

pub const SNAPSHOT_QTY_OFFSET: u32 = 2;
pub const SNAPSHOT_QTY_RANGE: RangeInclusive<u32> = RangeInclusive::new(1, SNAPSHOT_SERIES_MAX);

pub const SNAPSHOT_TIMESPAN_OFFSET: u32 = 1000;
pub const SNAPSHOT_TIMESPAN_RANGE: RangeInclusive<f32> = RangeInclusive::new(1.0, 600000.0); // up to 10 min @ 60 fps

// ----------------------------------------------------------------------------
// Nodes - window:
// ----------------------------------------------------------------------------

pub const WINDOW_SINK_HEIGHT: f32 = 480.0;
pub const WINDOW_SINK_WIDTH: f32 = 480.0; //640.0;
pub const WINDOW_SINK_SIZE_MAX: u32 = 10000;

// ----------------------------------------------------------------------------
//
// Nodes - Sources
//
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// Nodes - pipewire input:
// ----------------------------------------------------------------------------

pub const PIPEWIRE_INPUT_SIZE_HEIGHT: f32 = 480.0; //1080.0;
pub const PIPEWIRE_INPUT_SIZE_WIDTH: f32 = 640.0; //1920.0;
pub const PIPEWIRE_OUTPUT_SIZE_HEIGHT: f32 = 480.0;
pub const PIPEWIRE_OUTPUT_SIZE_WIDTH: f32 = 480.0;

// ----------------------------------------------------------------------------
// Nodes - stack:
// ----------------------------------------------------------------------------

pub const STACK_FPS_OFFSET: f32 = 1.0;
