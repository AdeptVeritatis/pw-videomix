
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            bind::{
                elements::filter_label::{
                    FilterMixRR,
                    FilterMixRG,
                    FilterMixRB,
                    FilterMixGR,
                    FilterMixGG,
                    FilterMixGB,
                    FilterMixBR,
                    FilterMixBG,
                    FilterMixBB,
                },
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        NODE_OUTPUT_HEIGHT,
        NODE_OUTPUT_WIDTH,
        PANEL_NODE_SPACING_SMALL,
    },
    impls::{
        midir::{
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        // serde::definitions::{
        //     Pos2Def,
        //     Vec2Def,
        // },
    },
    nodes::{
        connections::InputConnection,
        filter::FilterNode,
        NodeClass,
        NodeFamily,
        draw_node_connect_single,
        draw_node_header,
        draw_node_options,
        draw_node_output,
    },
    impls::vulkano::scenes::{
        copy::copy_connection_scene::ConnectionScene,
        render::color_mixer_scene::{
            ColorMixerMatrix,
            ColorMixerScene,
        },
    },
};
use egui_winit_vulkano::{
    egui::{
        Grid,
        Pos2,
        Ui,
        Vec2,
    },
    RenderResources,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ColorMixerValues {
    // Size of the canvas, the color mixer is projected on.
    // #[serde(serialize_with = "Vec2Def", skip_deserializing)]
    // #[serde(with = "Vec2Def")]
    #[serde(skip)]
    pub output_size: Vec2,
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    // #[serde(with = "Pos2Def")]
    #[serde(skip)]
    pub pos_out: Pos2,
    // Source, the input the filter is connected to.
    #[serde(skip_deserializing)]
    pub source: Option<InputConnection>,
    // State: has no state, is always mixing

    // Color mixer specific settings.
    pub mixer_matrix: ColorMixerMatrix,
}

impl Default for ColorMixerValues {
    fn default() -> Self {
        Self {
            output_size: Vec2::new(NODE_OUTPUT_WIDTH, NODE_OUTPUT_HEIGHT),
            pos_out: Pos2::default(),
            source: None,

            mixer_matrix: ColorMixerMatrix::default(),
        }
    }

}

// ----------------------------------------------------------------------------

#[derive(Serialize, Debug, Clone)]
pub struct ColorMixerNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    #[serde(skip_serializing)]
    pub scene: ColorMixerScene,
    pub values: ColorMixerValues,
    #[serde(skip_serializing)]
    pub updated: Instant,
}

// Constructors and helpers:
impl ColorMixerNode {
    pub fn new(
        filter_id: String,
        resources: RenderResources,
    ) -> Self {
        let scene: ColorMixerScene = ColorMixerScene::new(
            resources,
        );
        Self {
            class: NodeClass::ColorMixer,
            family: NodeFamily::Filter,
            id: filter_id.clone(),
            name: filter_id.clone(),
            scene,
            values: ColorMixerValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a ColorMixerNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
    // Header:
        self.name = draw_node_header(
            self.class.clone(),
            self.id.clone(),
            self.name.clone(),
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Options:
        draw_node_options(
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Input:
        // Show the connect widget.
        // Get new connection or use old.
        ui_inner.separator();
        self.values.source = draw_node_connect_single(
            Rc::clone(&map_filter_to_node),
            Rc::clone(&map_source_to_node),
            self.class.clone(),
            self.family.clone(),
            self.id.clone(),
            self.name.clone(),
            self.values.source.clone(),
            ui_area,
            ui_inner,
        );
    // Output:
        ui_inner.separator();
        ui_inner.vertical_centered(|ui_source| {
            self.values.output_size = draw_node_output(
                event_loop_proxy.clone(),
                self.family.clone(),
                self.id.clone(),
                self.values.output_size,
                ui_source,
            );
            self.values.pos_out = ui_source.min_rect().max;
        });
    }

    /// The side panel of a MandalaNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
    // Options:
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        Grid::new(
            "color mixer",
        )
            .show(
                ui,
                |ui_grid| {
                // Red:
                    ui_grid
                        .label(
                            "red:",
                        );
                    ui_grid
                        .end_row();
                    self.values.mixer_matrix.mix_rr = FilterMixRR::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_rr,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.mixer_matrix.mix_rg = FilterMixRG::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_rg,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.mixer_matrix.mix_rb = FilterMixRB::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_rb,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                // Green:
                    ui_grid
                        .label(
                            "green:",
                        );
                    ui_grid
                        .end_row();
                    self.values.mixer_matrix.mix_gr = FilterMixGR::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_gr,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid.end_row();
                    self.values.mixer_matrix.mix_gg = FilterMixGG::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_gg,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.mixer_matrix.mix_gb = FilterMixGB::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_gb,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                // Blue:
                    ui_grid
                        .label(
                            "blue:",
                        );
                    ui_grid
                        .end_row();
                    self.values.mixer_matrix.mix_br = FilterMixBR::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_br,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.mixer_matrix.mix_bg = FilterMixBG::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_bg,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.mixer_matrix.mix_bb = FilterMixBB::show(
                        self
                            .values
                            .mixer_matrix
                            .mix_bb,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                }
            );
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
    }

    pub fn control_bind(
        &self,
        bind: String,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "mix_rr" => {
                node.values.mixer_matrix.mix_rr = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            "mix_rg" => {
                node.values.mixer_matrix.mix_rg = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            "mix_rb" => {
                node.values.mixer_matrix.mix_rb = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            "mix_gr" => {
                node.values.mixer_matrix.mix_gr = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            "mix_gg" => {
                node.values.mixer_matrix.mix_gg = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            "mix_gb" => {
                node.values.mixer_matrix.mix_gb = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            "mix_br" => {
                node.values.mixer_matrix.mix_br = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            "mix_bg" => {
                node.values.mixer_matrix.mix_bg = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            "mix_bb" => {
                node.values.mixer_matrix.mix_bb = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorMixer(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> FilterNode {
        // Using location[0], location[1], ..., because we know the data structure.
        let values: ColorMixerValues = self
            .values
            .clone();
        let mut mixer_matrix: ColorMixerMatrix = values
            .mixer_matrix;

        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();

        match
            location[0]
                .as_str()
        {
            "values" => match
                location[1]
                    .as_str()
            {
                "mixer_matrix" => match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    // f32_from-2.0_fillrange_default1.0#64_fillrange_to2.0
                    "mix_rr" => {
                        mixer_matrix.mix_rr = FilterMixRR::change_midi(
                            mixer_matrix.mix_rr,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mix_rg" => {
                        mixer_matrix.mix_rg = FilterMixRG::change_midi(
                            mixer_matrix.mix_rg,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mix_rb" => {
                        mixer_matrix.mix_rb = FilterMixRB::change_midi(
                            mixer_matrix.mix_rb,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mix_gr" => {
                        mixer_matrix.mix_gr = FilterMixGR::change_midi(
                            mixer_matrix.mix_gr,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mix_gg" => {
                        mixer_matrix.mix_gg = FilterMixGG::change_midi(
                            mixer_matrix.mix_gg,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mix_gb" => {
                        mixer_matrix.mix_gb = FilterMixGB::change_midi(
                            mixer_matrix.mix_gb,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mix_br" => {
                        mixer_matrix.mix_br = FilterMixBR::change_midi(
                            mixer_matrix.mix_br,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mix_bg" => {
                        mixer_matrix.mix_bg = FilterMixBG::change_midi(
                            mixer_matrix.mix_bg,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mix_bb" => {
                        mixer_matrix.mix_bb = FilterMixBB::change_midi(
                            mixer_matrix.mix_bb,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    _ => (),
                },
                _ => (),
            },
            _ => (),
        };
        FilterNode::ColorMixer(
            Self {
                values: ColorMixerValues {
                    mixer_matrix,
                    ..values
                },
                ..self.to_owned()
            }
        )
    }
}

// Helpers:
impl ColorMixerNode {
    // Update storage image.
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        resources: RenderResources,
    ) -> Self {
        let mut values: ColorMixerValues = self.values.clone();

        match
            values
                .source
        {
            None => self,
            Some(mut connection) => {
                // Update ConnectionScene.
                let scene: ConnectionScene = connection.scene
                    .clone()
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone()
                    );
                connection = InputConnection {
                    scene,
                    ..connection
                };
                // Get image from InputConnection.
                match
                    // is image connection updated?
                    connection
                        .scene
                        .image_sink
                        .clone()
                {
                    None => {
                        println!("!! has source but no scene");
                    },
                    Some(image_input) => {
                        // Render input to storage image.
                        let image_output = self.scene.image_storage.clone();

                        let extent = image_output.image().extent();
                        let viewport: Viewport = Viewport {
                            offset: [
                                0.0,
                                0.0,
                            ],
                            extent: [extent[0] as f32, extent[1] as f32],
                            depth_range: 0.0..=1.0,
                        };

                        self.scene.render(
                            command_buffer_allocator,
                            descriptor_set_allocator,
                            image_input,
                            image_output,
                            values.mixer_matrix,
                            resources,
                            viewport,
                        );
                    },
                };
                // image_sink doesn't need to be stored again.???
                // Store updated values.
                values = ColorMixerValues {
                    source: Some(connection),
                    ..self.values.clone()
                };
                Self{
                    values,
                    updated: Instant::now(),
                    ..self
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------


