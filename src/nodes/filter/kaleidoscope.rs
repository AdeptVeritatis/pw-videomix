
// WallpaperNode instead of Kaleidoscope, because it is the wallpaper group!!!

pub mod p1;
pub mod p6m;
pub mod pmm;

use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            bind::{
                elements::filter_label::{
                    FilterMeshPositionX,
                    FilterMeshPositionY,
                    FilterTexAngle,
                    FilterTexAxisA,
                    FilterTexAxisB,
                },
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        FILTER_MOVE_OFFSET_MESH,
        KALEIDOSCOPE_ANGLE_OFFSET,
        KALEIDOSCOPE_AXIS_OFFSET,
        NODE_OUTPUT_HEIGHT,
        NODE_OUTPUT_WIDTH,
        PANEL_NODE_SPACING_SMALL,
    },
    impls::{
        midir::{
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        serde::definitions::{
            Pos2Def,
            // Vec2Def,
        },
        vulkano::{
            scenes::{
                copy::copy_connection_scene::ConnectionScene,
                render::kaleidoscope_scene::KaleidoscopeScene,
            },
            vertices::TextureVertex,
        },
    },
    nodes::{
        connections::InputConnection,
        filter::{
            kaleidoscope::{
                p1::mesh_p1,
                p6m::mesh_p6m,
                pmm::mesh_pmm,
            },
            FilterNode,
        },
        NodeClass,
        NodeFamily,
        NodeState,
        draw_node_connect_single,
        draw_node_header,
        draw_node_options,
        draw_node_output,
        draw_node_state,
    },
};
use egui_winit_vulkano::{
    egui::{
        Color32,
        ComboBox,
        Grid,
        Pos2,
        RichText,
        Ui,
        Vec2,
    },
    RenderResources,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    fmt,
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct KaleidoscopeValues {
    // Size of the canvas, the kaleidoscope is projected on.
    // #[serde(with = "Vec2Def")]
    #[serde(skip)]
    pub output_size: Vec2,
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    // #[serde(with = "Pos2Def")]
    #[serde(skip)]
    pub pos_out: Pos2,
    // pub sinks: Vec<FilterSink>,
    // Source, the input the filter is connected to.
    #[serde(skip_deserializing)]
    pub source: Option<InputConnection>,
    // State: play, pause, stop
    pub state: NodeState,

    // Kaleidoscope specific settings.
    // Mesh.
    #[serde(with = "Pos2Def")]
    pub mesh_position: Pos2,
    pub mesh_rotate: bool,
    pub mesh_rotation: f32,
    pub mesh_rotation_speed: f32,
    pub mesh_size: f32,
    // Texture UV mapping.
    pub tex_angle: f32,
    pub tex_axis_a: f32,
    pub tex_axis_b: f32,
    pub tex_lattice: KaleidoscopeLattice,
    #[serde(with = "Pos2Def")]
    pub tex_position: Pos2,
    pub tex_rotate: bool,
    pub tex_rotation: f32,
    pub tex_rotation_preset: f32,
    pub tex_rotation_speed: f32,
    pub tex_side: KaleidoscopeSide,
    pub tex_symmetry: KaleidoscopeSymmetry,
}

impl Default for KaleidoscopeValues {
    fn default() -> Self {
        Self {
            output_size: Vec2::new(NODE_OUTPUT_WIDTH, NODE_OUTPUT_HEIGHT),
            pos_out: Pos2::default(),
            source: None,
            state: NodeState::Play,

            mesh_position: Pos2::new(FILTER_MOVE_OFFSET_MESH, FILTER_MOVE_OFFSET_MESH),
            mesh_rotate: false,
            mesh_rotation: 0.0,
            mesh_rotation_speed: 0.0,
            mesh_size: 1.0,

            tex_angle: KALEIDOSCOPE_ANGLE_OFFSET,
            tex_axis_a: KALEIDOSCOPE_AXIS_OFFSET,
            tex_axis_b: KALEIDOSCOPE_AXIS_OFFSET,
            tex_lattice: KaleidoscopeLattice::Hexagonal,
            tex_position: Pos2::new(0.5, 0.5),
            tex_rotate: false,
            tex_rotation: 0.0,
            tex_rotation_preset: 0.0,
            tex_rotation_speed: 0.0,
            tex_side: KaleidoscopeSide::Left,
            tex_symmetry: KaleidoscopeSymmetry::P6m,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct KaleidoscopeMesh {
    pub indices: Vec<u32>,
    pub mesh_rotation: f32,
    pub tex_rotation: f32,
    pub vertices: Vec<TextureVertex>,
    pub viewport: Viewport,
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum KaleidoscopeLattice {
    // for wallpaper group, classified by the 5 Bravais lattices
    // https://en.wikipedia.org/wiki/List_of_planar_symmetry_groups
    Square,
    Oblique, // parallelogrammatic
    Hexagonal, // equilateral triangular
    Rectangular, // centered rhombic
    Rhombic, // centered rectangular
}

impl fmt::Display for KaleidoscopeLattice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Square => write!(f, "square"),
            Self::Oblique => write!(f, "oblique"),
            Self::Hexagonal => write!(f, "hexagonal"),
            Self::Rectangular => write!(f, "rectangular"),
            Self::Rhombic => write!(f, "rhombic"),
        }
    }
}
// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum KaleidoscopeSymmetry {
    // See: https://en.wikipedia.org/wiki/Wallpaper_group
    P1,
    P2,
    Pm,
    Pg,
    Cm,
    Pmm,
    Pmg,
    Pgg,
    Cmm,
    P4,
    P4m,
    P4g,
    P3,
    P3m1,
    P31m,
    P6,
    P6m,
}

impl fmt::Display for KaleidoscopeSymmetry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::P1 => write!(f, "p1 (∘1)"),
            Self::P2 => write!(f, "p2 (2222)"),
            Self::Pm => write!(f, "pm (∗∗)"),
            Self::Pg => write!(f, "pg (××)"),
            Self::Cm => write!(f, "cm (∗×)"),
            Self::Pmm => write!(f, "pmm (∗2222)"),
            Self::Pmg => write!(f, "pmg (22∗)"),
            Self::Pgg => write!(f, "pgg (22×)"),
            Self::Cmm => write!(f, "cmm (2∗22)"),
            Self::P4 => write!(f, "p4 (442)"),
            Self::P4m => write!(f, "p4m (∗422)"),
            Self::P4g => write!(f, "p4g (4∗2)"),
            Self::P3 => write!(f, "p3 (333)"),
            Self::P3m1 => write!(f, "p3m1 (∗333)"),
            Self::P31m => write!(f, "p31m (3∗3)"),
            Self::P6 => write!(f, "p6 (632)"),
            Self::P6m => write!(f, "p6m (∗632)"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum KaleidoscopeSide {
    Left,
    Right,
}

impl fmt::Display for KaleidoscopeSide {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Left => write!(f, "left"),
            Self::Right => write!(f, "right"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Debug, Clone)]
pub struct KaleidoscopeNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    #[serde(skip_serializing)]
    pub scene: KaleidoscopeScene,
    pub values: KaleidoscopeValues,
    #[serde(skip_serializing)]
    pub updated: Instant,
}

// Constructors and draw:
impl KaleidoscopeNode {
    pub fn new(
        filter_id: String,
        resources: RenderResources,
    ) -> Self {
        let scene: KaleidoscopeScene = KaleidoscopeScene::new(
            resources,
        );
        Self {
            class: NodeClass::Kaleidoscope,
            family: NodeFamily::Filter,
            id: filter_id.clone(),
            name: filter_id.clone(),
            scene,
            values: KaleidoscopeValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a KaleidoscopeNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
    // Header:
        self.name = draw_node_header(
            self.class.clone(),
            self.id.clone(),
            self.name.clone(),
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Options:
        ui_inner.separator();
        Grid::new("Options").show(ui_inner, |ui_grid| {
            self.values.state = draw_node_state(self.values.state.clone(), ui_grid);
        });
        draw_node_options(
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Input:
        // Show the connect widget.
        // Get new connection or use old.
        ui_inner.separator();
        self.values.source = draw_node_connect_single(
            Rc::clone(&map_filter_to_node),
            Rc::clone(&map_source_to_node),
            self.class.clone(),
            self.family.clone(),
            self.id.clone(),
            self.name.clone(),
            self.values.source.clone(),
            ui_area,
            ui_inner,
        );
    // Output:
        ui_inner.separator();
        ui_inner.vertical_centered(|ui_source| {
            self.values.output_size = draw_node_output(
                event_loop_proxy.clone(),
                self.family.clone(),
                self.id.clone(),
                self.values.output_size,
                ui_source,
            );
            self.values.pos_out = ui_source.min_rect().max;
        });
    }

    /// The side panel of a KaleidoscopeNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("experimental").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("not complete").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("expect tiling\nand mapping errors").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
    // Options:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        Grid::new("Header").show(ui, |ui_grid| {
            // ui_grid.label("size:");
            // ui_grid.add(
            //     DragValue::new(&mut mesh_size)
            //         .fixed_decimals(3)
            //         .clamp_range(RangeInclusive::new(0.01, 10.0))
            //         .speed(0.001),
            // );
            // ui_grid.end_row();
            self.values.mesh_position.x = FilterMeshPositionX::show(
                self
                    .values
                    .mesh_position
                    .x,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            self.values.mesh_position.y = FilterMeshPositionY::show(
                self
                    .values
                    .mesh_position
                    .y,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            ui_grid
                .end_row();
            // ui_grid.label("rotate:");
            // ui_grid.checkbox(&mut mesh_rotate, "");
            // ui_grid.label("speed:");
            // ui_grid.add(
            //     DragValue::new(&mut mesh_rotation_speed)
            //         .fixed_decimals(5)
            //         .clamp_range(RangeInclusive::new(-1.0, 1.0))
            //         .speed(0.000005),
            // );
        });
    // Texture UV mapping:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label("mapping:");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ComboBox::from_id_salt(format!("ComboBox {} lattice", self.id))
            .selected_text(
                self
                    .values
                    .tex_lattice
                    .to_string(),
            )
            .wrap()
            .show_ui(ui, |ui_combo| {
                for shape_lattice in [
                    KaleidoscopeLattice::Square,
                    KaleidoscopeLattice::Oblique,
                    KaleidoscopeLattice::Hexagonal,
                    KaleidoscopeLattice::Rectangular,
                    KaleidoscopeLattice::Rhombic,
                ] {
                    ui_combo.selectable_value(
                        &mut self
                            .values
                            .tex_lattice,
                        shape_lattice
                            .clone(),
                        shape_lattice
                            .to_string(),
                    );
                }
            });
        ComboBox::from_id_salt(format!("ComboBox {} symmetry", self.id))
            .selected_text(
                self
                    .values
                    .tex_symmetry
                    .to_string(),
            )
            .wrap()
            .show_ui(ui, |ui_combo| {
                let combobox_selection: Vec<KaleidoscopeSymmetry> = match
                    self
                        .values
                        .tex_lattice
                {
                    KaleidoscopeLattice::Square => vec!(
                        KaleidoscopeSymmetry::P1,
                        KaleidoscopeSymmetry::Pmm,
                    ),
                    KaleidoscopeLattice::Oblique => vec!(
                        KaleidoscopeSymmetry::P1,

                    ),
                    KaleidoscopeLattice::Hexagonal => vec!(
                        KaleidoscopeSymmetry::P1,
                        KaleidoscopeSymmetry::P6m,
                    ),
                    KaleidoscopeLattice::Rectangular => vec!(
                        KaleidoscopeSymmetry::P1,
                        KaleidoscopeSymmetry::Pmm,
                    ),
                    KaleidoscopeLattice::Rhombic => vec!(
                        KaleidoscopeSymmetry::P1,
                    ),
                };

                for
                    shape_symmetry
                in
                    combobox_selection
                {
                    ui_combo.selectable_value(
                        &mut self
                            .values
                            .tex_symmetry,
                        shape_symmetry
                            .clone(),
                        shape_symmetry
                            .to_string(),
                    );
                }
            });
        // ComboBox::from_id_source(format!("ComboBox {} side", self.id))
        //     .selected_text(format!("{tex_side}"))
        //     .wrap(true)
        //     .show_ui(ui, |ui_combo| {
        //         for side in [KaleidoscopeSide::Left, KaleidoscopeSide::Right] {
        //             ui_combo.selectable_value(
        //                 &mut tex_side,
        //                 side.clone(),
        //                 format!("{side}"),
        //             );
        //         }
        //     });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        Grid::new(
            "Mapping",
        )
            .show(
                ui,
                |ui_grid| {
                    self.values.tex_angle = FilterTexAngle::show(
                        self
                            .values
                            .tex_angle,
                        self
                            .values
                            .tex_lattice,
                        self
                            .values
                            .tex_symmetry,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.tex_axis_a = FilterTexAxisA::show(
                        self
                            .values
                            .tex_axis_a,
                        self
                            .values
                            .tex_lattice,
                        self
                            .values
                            .tex_symmetry,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.tex_axis_b = FilterTexAxisB::show(
                        self
                            .values
                            .tex_axis_b,
                        self
                            .values
                            .tex_lattice,
                        self
                            .values
                            .tex_symmetry,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();

                    // ui_grid.label("move h:");
                    // ui_grid.add(
                    //     DragValue::new(&mut tex_position_x)
                    //         .fixed_decimals(3)
                    //         .clamp_range(RangeInclusive::new(0.0, 1.0))
                    //         .speed(0.001),
                    // );
                    // ui_grid.label("move v:");
                    // ui_grid.add(
                    //     DragValue::new(&mut tex_position_y)
                    //         .fixed_decimals(3)
                    //         .clamp_range(RangeInclusive::new(0.0, 1.0))
                    //         .speed(0.001),
                    // );
                    // ui_grid.end_row();
                    // ui_grid.label("angle:");
                    // // Change value when rotating??? No, because it is already part of shape???
                    // ui_grid.add(
                    //     DragValue::new(&mut tex_rotation_preset)
                    //         .fixed_decimals(1)
                    //         .clamp_range(RangeInclusive::new(-360.0, 360.0))
                    //         .speed(0.05),
                    // );
                    // // ui_grid.label("°");
                    // ui_grid.end_row();
                    // ui_grid.label("rotate:");
                    // ui_grid.checkbox(&mut tex_rotate, "");
                    // ui_grid.label("speed:");
                    // ui_grid.add(
                    //     DragValue::new(&mut tex_rotation_speed)
                    //         .fixed_decimals(5)
                    //         .clamp_range(RangeInclusive::new(-1.0, 1.0))
                    //         .speed(0.000005),
                    // );
                    // ui_grid.end_row();
                    // ui_grid.label("");
                    // ui_grid.label("");
                    // ui_grid.label("@60fps:");
                    // ui_grid.label(format!(
                    //     "{:.2} s",
                    //     match
                    //         tex_rotation_speed
                    //     {
                    //         speed if speed == 0.0 => 0.0,
                    //         speed => speed.recip().abs() / 60.0,
                    //         // speed => speed.recip().abs() / 60.0 / repetitions as f32, // why repetitions???
                    //     },
                    // ));
                },
            );
        ui.add_space(PANEL_NODE_SPACING_SMALL);
    }

    pub fn control_bind(
        &self,
        bind: String,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "mesh_position_x" => {
                node.values.mesh_position.x = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Kaleidoscope(
                            node,
                        ),
                    );
            },
            "mesh_position_y" => {
                node.values.mesh_position.y = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Kaleidoscope(
                            node,
                        ),
                    );
            },
            "tex_angle" => {
                node.values.tex_angle = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Kaleidoscope(
                            node,
                        ),
                    );
            },
            "tex_axis_a" => {
                node.values.tex_axis_a = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Kaleidoscope(
                            node,
                        ),
                    );
            },
            "tex_axis_b" => {
                node.values.tex_axis_b = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Kaleidoscope(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> FilterNode {
        let mut values: KaleidoscopeValues = self
            .values
            .clone();
        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "mesh_position_x" => {
                        values.mesh_position.x = FilterMeshPositionX::change_midi(
                            values
                                .mesh_position
                                .x,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mesh_position_y" => {
                        values.mesh_position.y = FilterMeshPositionY::change_midi(
                            values
                                .mesh_position
                                .y,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "tex_angle" => {
                        values.tex_angle = FilterTexAngle::change_midi(
                            values
                                .tex_angle,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "tex_axis_a" => {
                        values.tex_axis_a = FilterTexAxisA::change_midi(
                            values
                                .tex_axis_a,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "tex_axis_b" => {
                        values.tex_axis_b = FilterTexAxisB::change_midi(
                            values
                                .tex_axis_b,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        FilterNode::Kaleidoscope(
            Self {
                values,
                ..self
                    .to_owned()
            }
        )
    }

}

// Helpers:
impl KaleidoscopeNode {
    fn kaleidoscope_mesh(
        self,
    ) -> KaleidoscopeMesh {
        let values: KaleidoscopeValues = self.values.clone();

        let extent: [f32; 2] = [self.values.output_size.x, self.values.output_size.y];
        let viewport: Viewport = Viewport {
            offset: [
                0.0,
                0.0,
            ],
            extent,
            depth_range: 0.0..=1.0,
        };

        match
            values
                .tex_symmetry
        {
            KaleidoscopeSymmetry::P1 => {
                mesh_p1(
                    self.values,
                    viewport,
                )
            },
            // KaleidoscopeSymmetry::P2 => {},
            // KaleidoscopeSymmetry::Pm => {},
            // KaleidoscopeSymmetry::Pg => {},
            // KaleidoscopeSymmetry::Cm => {},
            KaleidoscopeSymmetry::Pmm => {
                mesh_pmm(
                    self.values,
                    viewport,
                )
            },
            // KaleidoscopeSymmetry::Pmg => {},
            // KaleidoscopeSymmetry::Pgg => {},
            // KaleidoscopeSymmetry::Cmm => {},
            // KaleidoscopeSymmetry::P4 => {},
            // KaleidoscopeSymmetry::P4m => {},
            // KaleidoscopeSymmetry::P4g => {},
            // KaleidoscopeSymmetry::P3 => {},
            // KaleidoscopeSymmetry::P3m1 => {},
            // KaleidoscopeSymmetry::P31m => {},
            // KaleidoscopeSymmetry::P6 => {},
            KaleidoscopeSymmetry::P6m => {
                mesh_p6m(
                    self.values,
                    viewport,
                )
            },
            symmetry => {
                // println!("!! symmetry not implemented: {symmetry}");
                // KaleidoscopeMesh {
                //     ...
                // }
                panic!("!! symmetry not implemented: {symmetry}");
            },
        }
    }

    // Update storage image.
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        resources: RenderResources,
    ) -> Self {
        let mut values: KaleidoscopeValues = self.values.clone();

        match
            values
                .source
        {
            None => self,
            Some(mut connection) => {
                // Update ConnectionScene.
                let scene: ConnectionScene = connection.scene
                    .clone()
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone()
                    );
                connection = InputConnection {
                    scene,
                    ..connection
                };
                // Get image from InputConnection.
                let (mesh_rotation, tex_rotation) = match
                    // is image connection updated?
                    connection
                        .scene
                        .image_sink
                        .clone()
                {
                    None => {
                        println!("!! has source but no scene");
                        (self.values.mesh_rotation, self.values.tex_rotation)
                    },
                    Some(image_input) => {
                        // Render input to storage image.
                        match
                            self
                                .clone()
                                .values
                                .state
                        {
                            NodeState::Stop => (self.values.mesh_rotation, self.values.tex_rotation),
                            NodeState::Pause => (self.values.mesh_rotation, self.values.tex_rotation),
                            NodeState::Play => {
                                let image_output = self.scene.image_storage.clone();
                                let kaleidoscope_mesh: KaleidoscopeMesh = self.clone().kaleidoscope_mesh();

                                self.scene.render(
                                    command_buffer_allocator,
                                    descriptor_set_allocator,
                                    image_input,
                                    image_output,
                                    kaleidoscope_mesh.clone(),
                                    resources,
                                );
                                (kaleidoscope_mesh.mesh_rotation, kaleidoscope_mesh.tex_rotation)
                            },
                        }
                    },
                };
                // image_sink doesn't need to be stored again.???
                // Store updated values.
                values = KaleidoscopeValues {
                    mesh_rotation,
                    tex_rotation,
                    source: Some(connection),
                    ..self.values.clone()
                };
                Self {
                    values,
                    updated: Instant::now(),
                    ..self
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
