#![allow(clippy::identity_op)]

use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            bind::{
                elements::filter_label::{
                    FilterCenterX,
                    FilterCenterY,
                    FilterMixFactor,
                    FilterScaleFactor,
                },
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        NODE_OUTPUT_HEIGHT,
        NODE_OUTPUT_WIDTH,
        PANEL_NODE_SPACING_SMALL,
    },
    impls::{
        midir::{
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        // serde::definitions::{
        //     // Pos2Def,
        //     Vec2Def,
        // },
        vulkano::{
            scenes::{
                copy::copy_connection_scene::ConnectionScene,
                render::mixer_scene::MixerScene,
            },
            vertices::LayerVertex,
        },
    },
    nodes::{
        connections::InputConnection,
        filter::{
            FilterMesh,
            FilterNode,
        },
        NodeClass,
        NodeFamily,
        NodeLayer,
        draw_node_connect_multi,
        draw_node_header,
        draw_node_options,
        draw_node_output,
    },
};
use egui_winit_vulkano::{
    egui::{
        Color32,
        Grid,
        Pos2,
        RichText,
        Ui,
        Vec2,
    },
    RenderResources,
};
use serde::{
    Deserialize,
    Serialize,
};
use std::{
    cell::RefCell,
    collections::BTreeMap,
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    buffer::{
        Buffer,
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    image::view::ImageView,
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MixerValues {
    // Size of the canvas, the mixer is projected on.
    // #[serde(with = "Vec2Def")]
    #[serde(skip)]
    pub output_size: Vec2,
    // Position of the output source connector from the node.
    // Gets updated during drawing.

    // #[serde(with = "Pos2Def")]
    #[serde(skip)]
    pub pos_out: Pos2,
    // pub sinks: Vec<FilterSink>,
    // Sources, the inputs the filter is connected to.
    #[serde(skip)]
    pub sources: BTreeMap<String, NodeLayer>,
    // State: has no state, is always mixing
}

impl Default for MixerValues {
    fn default() -> Self {
        Self {
            output_size: Vec2::new(NODE_OUTPUT_WIDTH, NODE_OUTPUT_HEIGHT),
            pos_out: Pos2::default(),
            sources: BTreeMap::new(),
        }
    }
}

// ----------------------------------------------------------------------------

/// A node to mix sources into one sink
#[derive(Serialize, Debug, Clone)]
pub struct MixerNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    #[serde(skip_serializing)]
    pub scene: MixerScene,
    pub values: MixerValues,
    #[serde(skip_serializing)]
    pub updated: Instant,
}

// Constructors and draw:
impl MixerNode {
    pub fn new(
        filter_id: String,
        resources: RenderResources,
    ) -> Self {
        let scene: MixerScene = MixerScene::new(
            resources,
        );
        Self {
            class: NodeClass::Mixer,
            family: NodeFamily::Filter,
            id: filter_id.clone(),
            name: filter_id.clone(),
            scene,
            values: MixerValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a MixerNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
    // Header:
        self.name = draw_node_header(
            self.class.clone(),
            self.id.clone(),
            self.name.clone(),
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Options:
        draw_node_options(
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Input:
        // Show the connect widget.
        // Get new connection or use old.
        ui_inner.separator();
        self.values.sources = draw_node_connect_multi(
            event_loop_proxy.clone(),
            Rc::clone(&map_filter_to_node),
            Rc::clone(&map_source_to_node),
            self.class.clone(),
            self.family.clone(),
            self.id.clone(),
            self.name.clone(),
            self.values.sources.clone(),
            ui_area,
            ui_inner,
        );
    // Output:
        ui_inner.separator();
        ui_inner.vertical_centered(|ui_source| {
            self.values.output_size = draw_node_output(
                event_loop_proxy.clone(),
                self.family.clone(),
                self.id.clone(),
                self.values.output_size,
                ui_source,
            );
            self.values.pos_out = ui_source.min_rect().max;
        });
    }

    /// The side panel of a MixerNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        // ui.label("select add|max mixing");
        ui.label(RichText::new("use 'mix'").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("to blend in source").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        let mut sources: BTreeMap<String, NodeLayer> = self
            .values
            .sources
            .to_owned();
        if sources.is_empty() {
            ui.separator();
            ui.add_space(PANEL_NODE_SPACING_SMALL);
            ui.label(RichText::new("no input").color(Color32::RED));
            ui.add_space(PANEL_NODE_SPACING_SMALL);
            ui.label(RichText::new("!   add a node   !").color(Color32::RED));
            // ui.add_space(PANEL_NODE_SPACING_SMALL);
        }
        sources = sources
            .iter()
            .map(|(source_id, source)| {
                let mut source = source
                    .to_owned();

        // Header for single layer:
                ui.separator();
                ui.add_space(PANEL_NODE_SPACING_SMALL);
                ui.label(source.connection.source_id.clone());
                ui.small(source.connection.source_name.clone());
                ui.add_space(PANEL_NODE_SPACING_SMALL);
        // Values from single layer:
                Grid::new(self.id.clone())
                    .show(ui, |ui_layer| {
                        source.mix_factor = FilterMixFactor::show(
                            source
                                .mix_factor,
                            map_control_to_node
                                .clone(),
                            midi_node_rc
                                .clone(),
                            self
                                .id
                                .clone(),
                            source_id
                                .clone(),
                            ui_layer,
                        );
                        source.scale_factor = FilterScaleFactor::show(
                            source
                                .scale_factor,
                            map_control_to_node
                                .clone(),
                            midi_node_rc
                                .clone(),
                            self
                                .id
                                .clone(),
                            source_id
                                .clone(),
                            ui_layer,
                        );
                        ui_layer.end_row();
                        source.center.x = FilterCenterX::show(
                            source
                                .center
                                .x,
                            map_control_to_node
                                .clone(),
                            midi_node_rc
                                .clone(),
                            self
                                .id
                                .clone(),
                            source_id
                                .clone(),
                            ui_layer,
                        );
                        source.center.y = FilterCenterY::show(
                            source
                                .center
                                .y,
                            map_control_to_node
                                .clone(),
                            midi_node_rc
                                .clone(),
                            self
                                .id
                                .clone(),
                            source_id
                                .clone(),
                            ui_layer,
                        );
                    });
            // Store changed values from single layer:
                (
                    source_id
                        .to_owned(),
                    source,
                )
            })
            .collect();
        ui.add_space(PANEL_NODE_SPACING_SMALL);

        self.values.sources = sources;
    }

    pub fn control_bind(
        &self,
        bind: String,
        location: Vec<String>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        let mut sources = node
            .values
            .sources;

        match
            location[0]
                .as_str()
        {
            "values" => match
                location[1] // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    .as_str()
            {
                "sources" => match
                    sources
                        .get(&location[2]) // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                {
                    None => (),
                    Some(node_layer) => {
                        let mut node_layer: NodeLayer = node_layer
                            .to_owned();

                        match
                            bind
                                .as_str()
                        {
                            "center_x" => {
                                node_layer.center.x = result;
                                sources
                                    .insert(
                                        location[2].clone(),
                                        node_layer
                                            .to_owned(),
                                    );
                                node.values = MixerValues {
                                    sources,
                                    ..node
                                        .values
                                };
                                map_filter_to_node
                                    .borrow_mut()
                                    .map
                                    .insert(
                                        node_id,
                                        FilterNode::Mixer(
                                            node,
                                        ),
                                    );
                            },
                            "center_y" => {
                                node_layer.center.y = result;
                                sources
                                    .insert(
                                        location[2].clone(),
                                        node_layer
                                            .to_owned(),
                                    );
                                node.values = MixerValues {
                                    sources,
                                    ..node
                                        .values
                                };
                                map_filter_to_node
                                    .borrow_mut()
                                    .map
                                    .insert(
                                        node_id,
                                        FilterNode::Mixer(
                                            node,
                                        ),
                                    );
                            },
                            "mix_factor" => {
                                node_layer.mix_factor = result;
                                sources
                                    .insert(
                                        location[2].clone(),
                                        node_layer
                                            .to_owned(),
                                    );
                                node.values = MixerValues {
                                    sources,
                                    ..node
                                        .values
                                };
                                map_filter_to_node
                                    .borrow_mut()
                                    .map
                                    .insert(
                                        node_id,
                                        FilterNode::Mixer(
                                            node,
                                        ),
                                    );
                            },
                            "scale_factor" => {
                                node_layer.scale_factor = result;
                                sources
                                    .insert(
                                        location[2].clone(),
                                        node_layer
                                            .to_owned(),
                                    );
                                node.values = MixerValues {
                                    sources,
                                    ..node
                                        .values
                                };
                                map_filter_to_node
                                    .borrow_mut()
                                    .map
                                    .insert(
                                        node_id,
                                        FilterNode::Mixer(
                                            node,
                                        ),
                                    );
                            },
                            bind => println!("!! error: unknown binding {bind}"),
                        };
                    },
                },
                _ => (),
            },
            _ => (),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> FilterNode {
        let values: MixerValues = self
            .values
            .clone();
        let mut sources = values.sources;
        // Using location[0], location[1], ..., because we know the data structure.
        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => match
                location[1] // use .first() and .next()!!!
                    .as_str()
            {
                "sources" => match
                    sources
                        .get(&location[2])
                {
                    None => (),
                    Some(node_layer) => {
                        let mut node_layer: NodeLayer = node_layer
                            .to_owned();
                        match
                            midi_bind
                                .bind
                                .key
                                .as_str()
                        {
                            "center_x" => {
                                node_layer.center.x = FilterCenterX::change_midi(
                                    node_layer.center.x,
                                    controller_class,
                                    midi_event,
                                    speed,
                                );
                            },
                            "center_y" => {
                                node_layer.center.y = FilterCenterY::change_midi(
                                    node_layer.center.y,
                                    controller_class,
                                    midi_event,
                                    speed,
                                );
                            },
                            "mix_factor" => {
                                node_layer.mix_factor = FilterMixFactor::change_midi(
                                    node_layer.mix_factor,
                                    controller_class,
                                    midi_event,
                                    speed,
                                );
                            },
                            "scale_factor" => {
                                node_layer.scale_factor = FilterScaleFactor::change_midi(
                                    node_layer.scale_factor,
                                    controller_class,
                                    midi_event,
                                    speed,
                                );
                            },
                            _ => (),
                        };
                        sources
                            .insert(
                                location[2].clone(),
                                node_layer
                                    .to_owned(),
                            );
                    },
                },
                _ => (),
            },
            _ => (),
        };
        FilterNode::Mixer(
            Self {
                values: MixerValues {
                    sources,
                    ..values
                },
                ..self.to_owned()
            }
        )
    }

}

// Helpers:
impl MixerNode {
    // maybe use as general function or just as template???
    fn mixer_mesh(
        self,
    ) -> FilterMesh {
        let mut vertices: Vec<LayerVertex> = vec!(
        );
        let mut indices: Vec<u32> = Vec::new();

        // let source_num = self.values.sources.len();
        for (tex_i, (_layer_id, node_layer)) in self.values.sources.iter().enumerate() {
            let extent: Vec2 = node_layer.connection.source_extent;
            let scale_factor: f32 = node_layer.scale_factor;
            let center: Pos2 = node_layer.center;

            let aspect_ratio_layer: f32 = extent.x / extent.y;
            let aspect_ratio_output: f32 = self.values.output_size.x / self.values.output_size.y;

            let (pos_x_l, pos_x_r, pos_y_t, pos_y_b): (f32, f32, f32, f32) = match
                aspect_ratio_layer
            {
                // Width bound:
                aspect_ratio if aspect_ratio >= aspect_ratio_output => {
                    (
                        2.0 * center.x - scale_factor,
                        2.0 * center.x + scale_factor,
                        - 2.0 * center.y - scale_factor * aspect_ratio.recip() * aspect_ratio_output,
                        - 2.0 * center.y + scale_factor * aspect_ratio.recip() * aspect_ratio_output,
                    )

                },
                // Height bound:
                aspect_ratio => {
                    (
                        2.0 * center.x - scale_factor * aspect_ratio * aspect_ratio_output.recip(),
                        2.0 * center.x + scale_factor * aspect_ratio * aspect_ratio_output.recip(),
                        - 2.0 * center.y - scale_factor,
                        - 2.0 * center.y + scale_factor,
                    )
                },
            };

            vertices.push(
                // 0 - TL
                LayerVertex {
                    position: [pos_x_l, pos_y_t], // values from mixer layer
                    tex_coords: [0.0, 0.0], // values from mixer layer
                    tex_i: tex_i as u32,
                },
            );
            vertices.push(
                // 1 - TR
                LayerVertex {
                    position: [pos_x_r, pos_y_t],
                    tex_coords: [1.0, 0.0],
                    tex_i: tex_i as u32,
                },
            );
            vertices.push(
                // 2 - BR
                LayerVertex {
                    position: [pos_x_r, pos_y_b],
                    tex_coords: [1.0, 1.0],
                    tex_i: tex_i as u32,
                },
            );
            vertices.push(
                // 3 - BL
                LayerVertex {
                    position: [pos_x_l, pos_y_b],
                    tex_coords: [0.0, 1.0],
                    tex_i: tex_i as u32,
                },
            );

            // Next 6 indices for next 4 vertices.
            indices.push(tex_i as u32 * 4 + 0);
            indices.push(tex_i as u32 * 4 + 1);
            indices.push(tex_i as u32 * 4 + 2);
            indices.push(tex_i as u32 * 4 + 2);
            indices.push(tex_i as u32 * 4 + 3);
            indices.push(tex_i as u32 * 4 + 0);
        };
        let extent: [f32; 2] = [self.values.output_size.x, self.values.output_size.y];
        let viewport: Viewport = Viewport {
            offset: [
                0.0,
                0.0,
            ],
            extent,
            depth_range: 0.0..=1.0,
        };
        FilterMesh {
            indices,
            vertices,
            viewport,
        }
    }

    // Update storage image.
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        resources: RenderResources,
    ) -> Self {
        let mut values: MixerValues = self.values.clone();
        let mut sources: BTreeMap<String, NodeLayer> = values.sources;
        let mut image_inputs: Vec<Arc<ImageView>> = Vec::new();

        let mut mixer_scene: MixerScene = self.scene.clone();
        let mut mix_values: Vec<f32> = Vec::new();

        sources = sources
            .iter()
            .map(|(layer_id, node_layer)| {
                let mut connection = node_layer.connection.clone();
                let mut scene: ConnectionScene = connection.scene;

                // Update ConnectionScene.
                scene = scene
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone()
                    );
                // Get image from InputConnection.
                match
                    // is image connection updated?
                    scene
                        .image_sink
                        .clone()
                {
                    None => {
                        println!("!! scene has no image sink");
                    },
                    Some(image_input) => {
                        image_inputs.push(
                            image_input,
                        );
                        mix_values
                            .push(node_layer.mix_factor);
                    },
                };
                connection = InputConnection {
                    scene,
                    ..connection
                };
                (
                    layer_id.to_owned(),
                    NodeLayer {
                        connection,
                        ..node_layer.to_owned()
                    },
                )
        })
        .collect();

        if mix_values.is_empty() {
            mix_values.push(1.0);
        }
        // Create new buffer.
        // Take a buffer from a pool???
        let buffer_mix: Subbuffer<[f32]> = Buffer::from_iter(
            resources.memory_allocator.clone(),
            BufferCreateInfo {
                usage: BufferUsage::STORAGE_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            mix_values,
        )
        .unwrap();
        mixer_scene = MixerScene {
            buffer_mix,
            ..mixer_scene
        };

        values = MixerValues {
            sources,
            ..values
        };
        let mixer_node = Self{ // ???
            scene: mixer_scene,
            values,
            updated: Instant::now(),
            ..self
        };
        // Render input to storage image.
        let image_output = mixer_node.scene.image_storage.clone();
        let mixer_mesh: FilterMesh = mixer_node.clone().mixer_mesh(); // not self but newly updated

        if !image_inputs.is_empty() {
            mixer_node.scene.render(
                command_buffer_allocator,
                descriptor_set_allocator,
                image_inputs,
                image_output,
                mixer_mesh,
                resources,
            );
        };
        mixer_node
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
