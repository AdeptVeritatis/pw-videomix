#![allow(clippy::identity_op)]

use crate::{
    constants::{
        KALEIDOSCOPE_ANGLE_OFFSET,
        // KALEIDOSCOPE_TILES_MAX,
    },
    impls::vulkano::vertices::TextureVertex,
    nodes::filter::kaleidoscope::{
        KaleidoscopeLattice,
        KaleidoscopeMesh,
        KaleidoscopeValues,
    },
};
use glam::{
    Mat2,
    Vec2,
};
use vulkano::pipeline::graphics::viewport::Viewport;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn mesh_p1(
    values: KaleidoscopeValues,
    viewport: Viewport,
) -> KaleidoscopeMesh {
// Wallpaper group.
    // Square, oblique, hexagonal, rectangular, rhombic.
    let tex_lattice: KaleidoscopeLattice = values
        .tex_lattice;

// Lattice position and orientation.
    let vec_center: Vec2 = Vec2::new(
        values
            .mesh_position
            .x,
        - values
            .mesh_position
            .y,
    );
    let mut vec_height: Vec2 = Vec2::NEG_Y;
    let mut vec_width: Vec2 = Vec2::X;

// Lattice angles.
    let tex_angle: f32 = match
        tex_lattice
    {
        KaleidoscopeLattice::Oblique
        |
        KaleidoscopeLattice::Rhombic
        => values
            .tex_angle,
        _ => KALEIDOSCOPE_ANGLE_OFFSET,
    };
    let mat_angle: Mat2 = Mat2::from_angle(
        tex_angle,
    );
    vec_height = mat_angle * vec_height;

// Lattice size.
    let tex_axis_a: f32 = values.tex_axis_a;
    let tex_axis_b: f32 = match
        tex_lattice
    {
        KaleidoscopeLattice::Oblique
        |
        KaleidoscopeLattice::Rectangular
        => values
            .tex_axis_b,
        _ => tex_axis_a,
    };
    vec_height = tex_axis_b * vec_height;
    vec_width = tex_axis_a * vec_width;

    // No rotation, as it makes stuff complicated.
    // Use edge detection. If 2/3/4 specific edges are </> border -> break ???

    // let mesh_rotation: f32 = values
    //     .mesh_rotation;
    // let mat_rot: Mat2 = Mat2::from_angle(
    //     mesh_rotation,
    // );
    // vec_height = mat_rot * vec_height;
    // vec_width = mat_rot * vec_width;

    // let tex_rotation: f32 = values
    //     .tex_rotation;

    // let mesh_size: f32 = values.mesh_size;

    // Adjust coordinates from [-1.0..1.0] to [0.0..1.0] // deprecated???

    // dbg!(vec_center);
    // dbg!(vec_height);
    // dbg!(vec_width);
    // dbg!(tex_axis_a);
    // dbg!(tex_axis_b);

// Create vertices and indices for triangles.
    let mut indices: Vec<u32> = Vec::new();
    let mut vertices: Vec<TextureVertex> = Vec::new();

    let mut tiles: u32 = 0; // use vertices.len() (-4 or indices before vertices)???

    // Create 4 quadrants.
    let mut vec_start: Vec2 = vec_center;

    // Top left:
    while
        vec_start
            .y
        >=
        -1.0
    {
        vec_start = vec_start + vec_height;
        let mut vec_line: Vec2  = vec_start;
        while
            vec_line
                .x
            >=
            -1.0
        {
            vec_line = vec_line - vec_width;
            // Create 4 vertices and 6 indices.
            (
                indices,
                vertices,
            ) = create_mesh(
                indices,
                vec_height,
                vec_line,
                vec_width,
                vertices,
                tiles,
            );
            tiles += 1;
            // if
            //     tiles
            //     >
            //     KALEIDOSCOPE_TILES_MAX
            // {
            //     dbg!(tiles);
            //     println!("break from inner loop - top left");
            //     break
            // };
        };
        // if
        //     tiles
        //     >
        //     KALEIDOSCOPE_TILES_MAX
        // {
        //     dbg!(tiles);
        //     println!("break from outer loop - top left");
        //     break
        // };
    };
    // Top right:
    vec_start = vec_center;
    while
        vec_start
            .y
        >=
        -1.0
    {
        vec_start = vec_start + vec_height;
        let mut vec_line: Vec2  = vec_start;
        while
            vec_line
                .x
            <=
            1.0
        {
            // Create 4 vertices and 6 indices.
            (
                indices,
                vertices,
            ) = create_mesh(
                indices,
                vec_height,
                vec_line,
                vec_width,
                vertices,
                tiles,
            );
            tiles += 1;

            vec_line = vec_line + vec_width;
            // if
            //     tiles
            //     >
            //     KALEIDOSCOPE_TILES_MAX
            // {
            //     dbg!(tiles);
            //     println!("break from inner loop - top right");
            //     break
            // };
        };
        // if
        //     tiles
        //     >
        //     KALEIDOSCOPE_TILES_MAX
        // {
        //     dbg!(tiles);
        //     println!("break from outer loop - top right");
        //     break
        // };
    };
    // Bottom left:
    vec_start = vec_center;
    while
        vec_start
            .y
        <=
        1.0
    {
        let mut vec_line: Vec2  = vec_start;
        while
            vec_line
                .x
            >=
            -1.0
        {
            vec_line = vec_line - vec_width;

            // Create 4 vertices and 6 indices.
            (
                indices,
                vertices,
            ) = create_mesh(
                indices,
                vec_height,
                vec_line,
                vec_width,
                vertices,
                tiles,
            );
            tiles += 1;
            // if
            //     tiles
            //     >
            //     KALEIDOSCOPE_TILES_MAX
            // {
            //     dbg!(tiles);
            //     println!("break from inner loop - bottom left");
            //     break
            // };
        };
        vec_start = vec_start - vec_height;
        // if
        //     tiles
        //     >
        //     KALEIDOSCOPE_TILES_MAX
        // {
        //     dbg!(tiles);
        //     println!("break from outer loop - bottom left");
        //     break
        // };
    };
    // Bottom right:
    vec_start = vec_center;
    while
        vec_start
            .y
        <=
        1.0
    {
        let mut vec_line: Vec2  = vec_start;
        while
            vec_line
                .x
            <=
            1.0
        {
            // Create 4 vertices and 6 indices.
            (
                indices,
                vertices,
            ) = create_mesh(
                indices,
                vec_height,
                vec_line,
                vec_width,
                vertices,
                tiles,
            );
            tiles += 1;

            vec_line = vec_line + vec_width;
            // if
            //     tiles
            //     >
            //     KALEIDOSCOPE_TILES_MAX
            // {
            //     dbg!(tiles);
            //     println!("break from inner loop - bottom right");
            //     break
            // };
        };
        vec_start = vec_start - vec_height;
        // if
        //     tiles
        //     >
        //     KALEIDOSCOPE_TILES_MAX
        // {
        //     dbg!(tiles);
        //     println!("break from outer loop - bottom right");
        //     break
        // };
    };
    // dbg!(tiles);

    KaleidoscopeMesh {
        indices,
        mesh_rotation: values
            .mesh_rotation,
        tex_rotation: values
            .tex_rotation,
        vertices,
        viewport,
    }
}

// ----------------------------------------------------------------------------

fn create_mesh(
    mut indices: Vec<u32>,
    vec_height: Vec2,
    vec_line: Vec2,
    vec_width: Vec2,
    mut vertices: Vec<TextureVertex>,
    tiles: u32,
) -> (
    Vec<u32>,
    Vec<TextureVertex>,
) {
    vertices
        .push(
            // TL
            TextureVertex {
                position: vec_line
                    .to_array(),
                tex_coords: [0.0, 0.0], // calculate!!!
            },
        );
    vertices
        .push(
            // TR
            TextureVertex {
                position: (
                    vec_line + vec_width
                )
                    .to_array(),
                tex_coords: [1.0, 0.0], // calculate!!!
            },
        );
    vertices
        .push(
            // BR
            TextureVertex {
                position: (
                    vec_line + vec_width - vec_height
                )
                    .to_array(),
                tex_coords: [1.0, 1.0], // calculate!!!
            },
        );
    vertices
        .push(
            // BL
            TextureVertex {
                position: (
                    vec_line - vec_height
                )
                    .to_array(),
                tex_coords: [0.0, 1.0], // calculate!!!
            },
        );

    indices
        .push(0 + 4 * tiles);
    indices
        .push(3 + 4 * tiles);
    indices
        .push(1 + 4 * tiles);

    indices
        .push(1 + 4 * tiles);
    indices
        .push(3 + 4 * tiles);
    indices
        .push(2 + 4 * tiles);

    (
        indices,
        vertices,
    )
}
