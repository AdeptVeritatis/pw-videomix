#![allow(clippy::identity_op)]

use crate::{
    impls::vulkano::vertices::TextureVertex,
    nodes::filter::kaleidoscope::{
        KaleidoscopeLattice,
        KaleidoscopeMesh,
        KaleidoscopeValues,
    },
};
use egui_winit_vulkano::egui::Pos2;
use vulkano::pipeline::graphics::viewport::Viewport;

pub fn mesh_pmm(
    values: KaleidoscopeValues,
    viewport: Viewport,
) -> KaleidoscopeMesh {
    // Wallpaper group.
    // Square and rectangular.
// Mesh values:
    // let mesh_center: Pos2 = values.mesh_position;
    let mesh_center: Pos2 = Pos2::new(
        values.mesh_position.x - 1.0,
        - values.mesh_position.y - 1.0,
    );
    let mesh_rotation: f32 = values.mesh_rotation;
    // let mesh_size: f32 = values.mesh_size;

// Texture values:
    let tex_lattice: KaleidoscopeLattice = values.tex_lattice;
    // let tex_angle: f32 = values.tex_angle;
    let tex_axis_a: f32 = values.tex_axis_a;
    let tex_axis_b: f32 = match
        tex_lattice
    {
        KaleidoscopeLattice::Rectangular => values.tex_axis_b,
        _ => tex_axis_a,
    };

    // Adjust coordinates from [-1.0..1.0] to [0.0..1.0]
    let tex_rotation: f32 = values.tex_rotation;

// Create vertices and indices for triangles.
    let repetitions_a: u32 = tex_axis_a.recip().ceil() as u32 + 1; //???
    let repetitions_b: u32 = tex_axis_b.recip().ceil() as u32 * 2; //???

    let mut indices: Vec<u32> = Vec::new();
    let mut vertices: Vec<TextureVertex> = Vec::new();

    // All rows:
    for
        i_b
    in
        0..=repetitions_b
    {
        // Make one 'for i_a in 0..=rep' for all. Change vertices accordingly.???
        // Middle line:
        // Top line of first row is equal to bottom line. Ignore middle line on first row.
        if
            i_b > 0
        {
            let prev_b = i_b - 1;
            for
                i_a
            in
                0..=repetitions_a
            {
                // First element in row:
                if
                    i_a > 0
                {
                    let prev_a = i_a - 1;
                    // Middle line, middle vertex:
                    vertices.push(
                        // TR
                        TextureVertex {
                            position: [
                                mesh_center.x + tex_axis_a * (prev_a * 2 + 1) as f32,
                                mesh_center.y + tex_axis_b * (prev_b * 2 + 1) as f32,
                            ],
                            tex_coords: [1.0, 0.0],
                        },
                    );

                    // (position in block; previous columns; previous rows)
                    indices.push(0 + 2 * prev_a + (1 + 2 * repetitions_a) * prev_b);
                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * prev_b);
                    indices.push(0 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);

                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * prev_b);
                    indices.push(0 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);

                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * prev_b);
                    indices.push(2 + 2 * prev_a + (1 + 2 * repetitions_a) * prev_b);
                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);

                    indices.push(2 + 2 * prev_a + (1 + 2 * repetitions_a) * prev_b);
                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                    indices.push(2 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                }
                // Middle line, right vertex:
                // Left vertex equals right vertex in first column.
                vertices.push(
                    // TL
                    TextureVertex {
                        position: [
                            mesh_center.x + tex_axis_a * (i_a * 2) as f32,
                            mesh_center.y + tex_axis_b * (prev_b * 2 + 1) as f32,
                        ],
                        tex_coords: [0.0, 0.0],
                    },
                );

            }
        }
        // Bottom line:
        // Is top line in first round.
        for
            i_a
        in
            0..=repetitions_a
        {
            // First element in row:
            if
                i_a > 0
            {
                let prev_a = i_a - 1;
                let next_b = i_b + 1;
                // Bottom line, middle vertex:
                vertices.push(
                    // BR
                    TextureVertex {
                        position: [
                            mesh_center.x + tex_axis_a * (prev_a * 2 + 1) as f32,
                            mesh_center.y + tex_axis_b * (i_b * 2) as f32,
                        ],
                        tex_coords: [1.0, 1.0],
                    },
                );

                if
                    i_b > 0
                {
                    // (position in block; previous columns; previous rows)
                    indices.push(0 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                    indices.push(0 + 2 * prev_a + (1 + 2 * repetitions_a) * next_b);

                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                    indices.push(0 + 2 * prev_a + (1 + 2 * repetitions_a) * next_b);
                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * next_b);

                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                    indices.push(2 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * next_b);

                    indices.push(2 + 2 * prev_a + (1 + 2 * repetitions_a) * i_b);
                    indices.push(1 + 2 * prev_a + (1 + 2 * repetitions_a) * next_b);
                    indices.push(2 + 2 * prev_a + (1 + 2 * repetitions_a) * next_b);
                }
            }
            // Bottom line, right vertex:
            // Left vertex equals right vertex in first column.
            vertices.push(
                // BL
                TextureVertex {
                    position: [
                        mesh_center.x + tex_axis_a * (i_a * 2) as f32,
                        mesh_center.y + tex_axis_b * (i_b * 2) as f32,
                    ],
                    tex_coords: [0.0, 1.0],
                },
            );

        }
    }

    KaleidoscopeMesh {
        indices,
        mesh_rotation,
        tex_rotation,
        vertices,
        viewport,
    }
}
