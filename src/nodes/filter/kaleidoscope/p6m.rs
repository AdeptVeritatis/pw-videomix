#![allow(clippy::identity_op)]

use crate::{
    impls::vulkano::vertices::TextureVertex,
    nodes::filter::kaleidoscope::{
        KaleidoscopeMesh,
        KaleidoscopeValues,
    },
};
use egui_winit_vulkano::egui::Pos2;
use vulkano::pipeline::graphics::viewport::Viewport;

pub fn mesh_p6m(
    values: KaleidoscopeValues,
    viewport: Viewport,
) -> KaleidoscopeMesh {
    // Wallpaper group.
    // Hexagonal.

    let sqrt3: f32 = 3.0_f32.sqrt();
// Mesh values:
    // let mesh_center: Pos2 = values.mesh_position;
    let mesh_center: Pos2 = Pos2::new(
        values.mesh_position.x - 1.0,
        - values.mesh_position.y - 1.0,
    );
    let mesh_rotation: f32 = values.mesh_rotation;
    // let mesh_size: f32 = values.mesh_size;

// Texture values:
    // let tex_angle: f32 = values.tex_angle;
    let tex_axis: f32 = values.tex_axis_a;
    let tex_axis_half: f32 = tex_axis / 2.0;
    let tex_axis_quarter: f32 = tex_axis / 4.0;
    let tex_height_double: f32 = tex_axis * sqrt3;
    let tex_height: f32 = tex_height_double / 2.0;
    let tex_height_long: f32 = tex_height_double / 3.0;
    let tex_height_half: f32 = tex_height_double / 4.0;
    let tex_height_short: f32 = tex_height_long / 2.0;
    // Adjust coordinates from [-1.0..1.0] to [0.0..1.0]
    let tex_rotation: f32 = values.tex_rotation;

// Create vertices and indices for triangles.
    let repetitions_elements: u32 = (tex_axis.recip() * 2.0).ceil() as u32 + 2; // +2 for better mobility
    let repetitions_rows: u32 = (tex_axis.recip() * sqrt3 * 1.5).ceil() as u32 + 1;

    let mut indices: Vec<u32> = Vec::new();
    let mut vertices: Vec<TextureVertex> = Vec::new();

    // All rows:
    for
        i_row
    in
        0..=repetitions_rows
    {
        let diff_y: f32 = mesh_center.y + tex_height * i_row as f32;
        let diff_row: f32 = i_row as f32 / 2.0 % 1.0;
        let diff_index: u32 = match
            i_row % 2
        {
            0 => {
                6 * repetitions_elements
            },
            1 => {
                6 * repetitions_elements - 6
            },
            _ => 0,
        };
        for
            i_element
        in
            0..=repetitions_elements
        {
            let diff_x: f32 = mesh_center.x + tex_axis * (i_element as f32 - diff_row);
        // 6 vertices in total per repetition.
        // First line. 2 vertices.
            // Is also used at the end of a row and in last row.
            vertices.push(
                // BL
                TextureVertex {
                    position: [
                        diff_x,
                        diff_y,
                    ],
                    tex_coords: [0.0, 1.0],
                },
            );
            // Is also used in last row, but not at the end of a row.
            if
                i_element
                < repetitions_elements
            {
                vertices.push(
                    // BR
                    TextureVertex {
                        position: [
                            diff_x + tex_axis_half,
                            diff_y,
                        ],
                        tex_coords: [1.0, 1.0],
                    },
                );
            }
            if
                i_row
                < repetitions_rows
            {
        // Second line. 1 vertex.
                // Only used within a row.
                if
                    i_element
                    < repetitions_elements
                {
                    vertices.push(
                        // TR
                        TextureVertex {
                            position: [
                                diff_x + tex_axis_half,
                                diff_y + tex_height_short,
                            ],
                            tex_coords: [1.0, 0.0],
                        },
                    );
                }
        // Third line. 2 vertices.
                // Is also used at the end of a row but not in the last row.
                vertices.push(
                    // BR
                    TextureVertex {
                        position: [
                            diff_x - tex_axis_quarter,
                            diff_y + tex_height_half,
                        ],
                        tex_coords: [1.0, 1.0],
                    },
                );
                if
                    i_element
                    < repetitions_elements
                {
                // Rest is only used within a row.
                    vertices.push(
                        // BR
                        TextureVertex {
                            position: [
                                diff_x + tex_axis_quarter,
                                diff_y + tex_height_half,
                            ],
                            tex_coords: [1.0, 1.0],
                        },
                    );
        // Fourth line. 1 vertex.
                    vertices.push(
                        // TR
                        TextureVertex {
                            position: [
                                diff_x,
                                diff_y + tex_height_long,
                            ],
                            tex_coords: [1.0, 0.0],
                        },
                    );

        // 36 indices for 12 triangles with 6*n + 2 vertices per row.
                    let index_offset: u32 = 6 * i_element + (6 * repetitions_elements + 2) * i_row;
                    let index_border_right: u32 = match
                        i_element + 1 == repetitions_elements
                    {
                        // At border.
                        true => 7 + index_offset,
                        // Within body.
                        false => 9 + index_offset,
                    };

                    indices.push(0 + index_offset);
                    indices.push(3 + index_offset);
                    indices.push(5 + index_offset);

                    indices.push(0 + index_offset);
                    indices.push(5 + index_offset);
                    indices.push(4 + index_offset);

                    indices.push(0 + index_offset);
                    indices.push(4 + index_offset);
                    indices.push(2 + index_offset);

                    indices.push(0 + index_offset);
                    indices.push(2 + index_offset);
                    indices.push(1 + index_offset);

                    indices.push(6 + index_offset);
                    indices.push(1 + index_offset);
                    indices.push(2 + index_offset);

                    indices.push(6 + index_offset);
                    indices.push(2 + index_offset);
                    indices.push(index_border_right);

                    if
                        i_row + 1
                        < repetitions_rows // ???
                    {
                        if
                            i_row % 2 == 0
                            || i_element > 0
                        {
                            indices.push(2 + index_offset + diff_index);
                            indices.push(5 + index_offset);
                            indices.push(3 + index_offset);

                            indices.push(2 + index_offset + diff_index);
                            indices.push(3 + index_offset + diff_index);
                            indices.push(5 + index_offset);
                        }

                        indices.push(8 + index_offset + diff_index);
                        indices.push(5 + index_offset);
                        indices.push(3 + index_offset + diff_index);

                        indices.push(8 + index_offset + diff_index);
                        indices.push(4 + index_offset);
                        indices.push(5 + index_offset);

                        indices.push(8 + index_offset + diff_index);
                        indices.push(2 + index_offset);
                        indices.push(4 + index_offset);

                        indices.push(8 + index_offset + diff_index);
                        indices.push(index_border_right);
                        indices.push(2 + index_offset);
                    }
                }
            }
        }
    }

    KaleidoscopeMesh {
        indices,
        mesh_rotation,
        tex_rotation,
        vertices,
        viewport,
    }
}

