
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            bind::{
                elements::filter_label::{
                    FilterTimeHold,
                    FilterTimeMix,
                },
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        FADER_TIME_OFFSET,
        NODE_OUTPUT_HEIGHT,
        NODE_OUTPUT_WIDTH,
        PANEL_NODE_SPACING_SMALL,
    },
    impls::{
        midir::{
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        // serde::definitions::{
        //     // Pos2Def,
        //     Vec2Def,
        // },
        vulkano::scenes::{
            copy::copy_connection_scene::ConnectionScene,
            render::fader_scene::FaderScene,
        },
    },
    nodes::{
        connections::InputConnection,
        filter::FilterNode,
        NodeClass,
        NodeFamily,
        NodeLayer,
        NodeState,
        draw_node_connect_multi,
        draw_node_header,
        draw_node_options,
        draw_node_output,
        draw_node_state,
    },
};
use egui_winit_vulkano::{
    egui::{
        ComboBox,
        Grid,
        Pos2,
        Ui,
        Vec2,
    },
    RenderResources,
};
use serde::{
    Deserialize,
    Serialize,
};
use std::{
    cell::RefCell,
    collections::BTreeMap,
    fmt,
    rc::Rc,
    sync::Arc,
    time::{
        Duration,
        Instant,
    },
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum FaderClass {
    Looped,
    Mirrored,
    Stacked,
}

impl fmt::Display for FaderClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match
            *self
        {
            Self::Looped => write!(f, "looped"),
            Self::Mirrored => write!(f, "mirrored"),
            Self::Stacked => write!(f, "stacked"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum FaderDirection {
    Down,
    Up,
}

impl fmt::Display for FaderDirection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match
            *self
        {
            Self::Down => write!(f, "down"),
            Self::Up => write!(f, "up"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum FaderPhase {
    Hold(usize),
    Mix([usize; 2]),
}

impl fmt::Display for FaderPhase {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match
            *self
        {
            Self::Hold(_) => write!(f, "hold"),
            Self::Mix(_) => write!(f, "mix"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum FaderTransition {
    Linear,
    Sine,
}

impl fmt::Display for FaderTransition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match
            *self
        {
            Self::Linear => write!(f, "linear"),
            Self::Sine => write!(f, "sine"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct FaderValues {
    // Size of the canvas, the fader is projected on.
    // #[serde(with = "Vec2Def")]
    #[serde(skip)]
    pub output_size: Vec2,
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    // #[serde(with = "Pos2Def")]
    #[serde(skip)]
    pub pos_out: Pos2,
    // pub sinks: Vec<FilterSink>,
    // Inputs of the filter.
    #[serde(skip)]
    pub sources: BTreeMap<String, NodeLayer>,
    // State: play, pause, stop
    pub state: NodeState,

    // Fader specific values:
    pub class: FaderClass,
    // Direction of mix:
    pub direction: FaderDirection,
    // Mixing count of current frame for mixing phase.
    pub mix_count: u32,
    // Mixing step difference for mixing phase.
    pub mix_step: f32,
    // Current mixing value for mixing phase.
    pub mix_value: f32,
    // FaderPhase or stage or state.
    pub phase: FaderPhase,
    // Timespan hold in ms:
    pub time_hold: u32,
    // Timespan mix in ms:
    pub time_mix: u32,
    // Timestamp from last phase change:
    #[serde(skip, default = "Instant::now")]
    pub time_stamp: Instant,
    // FaderTransition for transition style type.
    pub transition: FaderTransition,
}

impl Default for FaderValues {
    fn default() -> Self {
        Self {
            output_size: Vec2::new(NODE_OUTPUT_WIDTH, NODE_OUTPUT_HEIGHT),
            pos_out: Pos2::default(),
            sources: BTreeMap::new(),
            state: NodeState::Play,

            class: FaderClass::Looped,
            direction: FaderDirection::Down,
            mix_count: 0,
            mix_step: 0.1,
            mix_value: 1.0,
            phase: FaderPhase::Hold(0),
            time_hold: FADER_TIME_OFFSET as u32,
            time_mix: FADER_TIME_OFFSET as u32,
            time_stamp: Instant::now(),
            transition: FaderTransition::Sine,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Debug, Clone)]
pub struct FaderNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    #[serde(skip_serializing)]
    pub scene: FaderScene,
    pub values: FaderValues,
    #[serde(skip_serializing)]
    pub updated: Instant,
}

// constructors and draw:
impl FaderNode {
    pub fn new(
        filter_id: String,
        resources: RenderResources,
    ) -> Self {
        let scene: FaderScene = FaderScene::new(
            resources,
        );
        Self {
            class: NodeClass::Fader,
            family: NodeFamily::Filter,
            id: filter_id.clone(),
            name: filter_id.clone(),
            scene,
            values: FaderValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a FaderNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
    // Header:
        self.name = draw_node_header(
            self.class.clone(),
            self.id.clone(),
            self.name.clone(),
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Options:
        ui_inner.separator();
        Grid::new("Options").show(ui_inner, |ui_grid| {
            self.values.state = draw_node_state(self.values.state.clone(), ui_grid);
        });
        draw_node_options(
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Input:
        // Show the connect widget.
        // Get new connection or use old.
        ui_inner.separator();
        self.values.sources = draw_node_connect_multi(
            event_loop_proxy.clone(),
            Rc::clone(&map_filter_to_node),
            Rc::clone(&map_source_to_node),
            self.class.clone(),
            self.family.clone(),
            self.id.clone(),
            self.name.clone(),
            self.values.sources.clone(),
            ui_area,
            ui_inner,
        );
    // Output:
        ui_inner.separator();
        ui_inner.vertical_centered(|ui_source| {
            self.values.output_size = draw_node_output(
                event_loop_proxy.clone(),
                self.family.clone(),
                self.id.clone(),
                self.values.output_size,
                ui_source,
            );
            self.values.pos_out = ui_source.min_rect().max;
        });
    }

    /// The side panel of a FaderNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        Grid::new("comboboxes").show(ui, |ui_grid| {
            ui_grid.label("class:");
            ComboBox::from_id_salt(format!("ComboBox {} class", self.id))
                .selected_text(
                    self
                        .values
                        .class
                        .to_string()
                )
                // .width(100.0)
                // .wrap()
                .show_ui(ui_grid, |ui_combo| {
                    for
                        fader_class
                    in
                        [
                            FaderClass::Looped,
                            FaderClass::Mirrored,
                            FaderClass::Stacked,
                        ]
                    {
                        ui_combo
                            .selectable_value(
                                &mut self
                                    .values
                                    .class,
                                fader_class
                                    .clone(),
                                fader_class
                                    .to_string(),
                            );
                    }
                });
            ui_grid.end_row();
            ui_grid.label("direction:");
            ComboBox::from_id_salt(format!("ComboBox {} direction", self.id))
                .selected_text(
                    self
                        .values
                        .direction
                        .to_string()
                )
                // .wrap()
                .show_ui(ui_grid, |ui_combo| {
                    for fader_dir in [
                        FaderDirection::Down,
                        FaderDirection::Up,
                    ] {
                        ui_combo.selectable_value(
                            &mut self
                                .values
                                .direction,
                            fader_dir
                                .clone(),
                            fader_dir
                                .to_string(),
                        );
                    }
                });
            ui_grid.end_row();
            ui_grid.label("phase:");
            ComboBox::from_id_salt(format!("ComboBox {} phase", self.id))
                .selected_text(
                    self
                        .values
                        .phase
                        .to_string()
                )
                // .wrap()
                .show_ui(ui_grid, |ui_combo| {
                    for fader_phase in [
                        FaderPhase::Hold(0), // reset or use known values?
                        FaderPhase::Mix([0, 0]),
                    ] {
                        ui_combo.selectable_value(
                            &mut self.values.phase,
                            fader_phase
                                .clone(),
                            fader_phase
                                .to_string(),
                        );
                    }
                });
            ui_grid.end_row();
        });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label("timespan:");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        Grid::new("timespan").show(ui, |ui_grid| {
            self.values.time_hold = FilterTimeHold::show(
                self
                    .values
                    .time_hold,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            ui_grid.end_row();
            match
                self
                    .values
                    .class
            {
                FaderClass::Looped | FaderClass::Mirrored => {
                    self.values.time_mix = FilterTimeMix::show(
                        self
                            .values
                            .time_mix,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid.end_row();
                    ui_grid.label("transition:");
                    ComboBox::from_id_salt(format!("ComboBox {} transition", self.id))
                        .selected_text(
                            self
                                .values
                                .transition
                                .to_string()
                        )
                        // .width(100.0)
                        // .wrap()
                        .show_ui(ui_grid, |ui_combo| {
                            for fader_transition in [
                                FaderTransition::Linear,
                                FaderTransition::Sine,
                            ] {
                                ui_combo.selectable_value(
                                    &mut self
                                        .values
                                        .transition,
                                    fader_transition
                                        .clone(),
                                    fader_transition
                                        .to_string(),
                                );
                            }
                        });
                    ui_grid.end_row();
                },
                FaderClass::Stacked => (),
            };
        });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
    }

    pub fn control_bind(
        &self,
        bind: String,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "time_hold" => {
                node.values.time_hold = result as u32;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Fader(
                            node,
                        ),
                    );
            },
            "time_mix" => {
                node.values.time_mix = result as u32;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Fader(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> FilterNode {
        let mut values: FaderValues = self
            .values
            .clone();
        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "time_hold" => {
                        values.time_hold = FilterTimeHold::change_midi(
                            values.time_hold,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "time_mix" => {
                        values.time_mix = FilterTimeMix::change_midi(
                            values.time_mix,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        FilterNode::Fader(
            Self {
                values,
                ..self
                    .to_owned()
            }
        )
    }

}

// Helpers:
impl FaderNode {
    // Update storage image.
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        fps: f32,
        resources: RenderResources,
    ) -> Self {
        let mut values: FaderValues = self.values.clone();
        let mut direction: FaderDirection = values.direction;
        let mut mix_count: u32 = values.mix_count;
        let mut mix_step: f32 = values.mix_step;
        let mut mix_value: f32 = values.mix_value;
        let mut phase: FaderPhase = values.phase;
        let mut time_stamp: Instant = values.time_stamp;

        let time_now = Instant::now();
        // println!("{:?}", time_stamp + Duration::from_millis(values.time_hold));
        // println!("{:?}", time_now);
        // println!("{:?}", time_stamp);

        // move to a function???
        match
            phase
        {
            FaderPhase::Mix(pair) => {
                match
                    time_stamp + Duration::from_millis(values.time_mix as u64)
                    > time_now
                {
                    // Time has NOT passed already.
                    true => {
                        // render updated mix into image_storage
                        mix_count += 1;
                        mix_step = 1000.0 * (values.time_mix as f32).recip() * fps.round().recip();
                        mix_value = self
                            .clone()
                            .fader_mix(
                                mix_count,
                                mix_step,
                            );
                        self
                            .clone()
                            .render_mix(
                                command_buffer_allocator,
                                descriptor_set_allocator,
                                mix_value,
                                pair[0],
                                pair[1],
                                resources,
                            );
                        // mix_count += 1;
                    },
                    // Time reached, next phase.
                    false => {
                        time_stamp = time_now;
                        let next: usize = pair[1];
                        phase = FaderPhase::Hold(next);
                        // println!("ii new hold {phase} {next}");
                        // render new hold into image_storage
                        values = self
                            .clone()
                            .render_hold(
                                command_buffer_allocator,
                                descriptor_set_allocator,
                                next,
                                resources,
                            );
                    },
                };
            },
            FaderPhase::Hold(prev) => {
                match
                    time_stamp + Duration::from_millis(values.time_hold as u64)
                    > time_now
                {
                    // Time has NOT passed already.
                    true => {
                        // Update for non-static sources.
                        values = self
                            .clone()
                            .render_hold(
                                command_buffer_allocator,
                                descriptor_set_allocator,
                                prev,
                                resources,
                            );
                    },
                    // Time reached, next phase.
                    false => {
                        // if stacked -> hold[next], else -> mix[[1], next]
                        let mut next: usize = prev;
                        match
                            values
                                .sources
                                .len()
                        {
                            0 => {
                                // println!("ii no connection");
                            },
                            1 => {
                                // println!("ii only one connection");
                                // Update for non-static sources.
                                values = self
                                    .clone()
                                    .render_hold(
                                        command_buffer_allocator,
                                        descriptor_set_allocator,
                                        prev,
                                        resources,
                                    );
                            },
                            len => {
                                time_stamp = time_now;
                                match
                                    values
                                        .class
                                {
                                    FaderClass::Looped => {
                                        match
                                            direction
                                        {
                                            FaderDirection::Down => {
                                                // (prev + 1) % len // drifting???
                                                next += 1;
                                                if
                                                    next >= len
                                                {
                                                    next = 0;
                                                }
                                            },
                                            FaderDirection::Up => {
                                                if
                                                    prev == 0
                                                {
                                                    next = len;
                                                }
                                                next -= 1;
                                            },
                                        };
                                        phase = FaderPhase::Mix([prev, next]);
                                        // println!("ii next mix {phase} {prev} {next}");
                                        // render mix
                                        // use function???
                                        mix_count = 0;
                                        mix_step = 1000.0 * (values.time_mix as f32).recip() * fps.round().recip();
                                        mix_value = 1.0;
                                        self
                                            .clone()
                                            .render_mix(
                                                command_buffer_allocator,
                                                descriptor_set_allocator,
                                                mix_value,
                                                prev,
                                                next,
                                                resources,
                                            );
                                    },
                                    FaderClass::Mirrored => {
                                        match
                                            direction
                                        {
                                            FaderDirection::Down => {
                                                match
                                                    prev + 1
                                                    >= len
                                                {
                                                    true => {
                                                        direction = FaderDirection::Up;
                                                        next -= 1;
                                                    },
                                                    false => {
                                                        next += 1;
                                                    },
                                                };
                                            },
                                            FaderDirection::Up => {
                                                match
                                                    prev == 0
                                                {
                                                    true => {
                                                        direction = FaderDirection::Down;
                                                        next += 1;
                                                    },
                                                    false => {
                                                        next -= 1;
                                                    },
                                                };
                                            },
                                        };
                                        phase = FaderPhase::Mix([prev, next]);
                                        // println!("ii next mix {phase} {prev} {next}");
                                        // render mix
                                        mix_count = 0;
                                        mix_step = 1000.0 * (values.time_mix as f32).recip() * fps.round().recip();
                                        mix_value = 1.0;
                                        self
                                            .clone()
                                            .render_mix(
                                                command_buffer_allocator,
                                                descriptor_set_allocator,
                                                mix_value,
                                                prev,
                                                next,
                                                resources,
                                            );
                                    },
                                    FaderClass::Stacked => {
                                        match
                                            direction
                                        {
                                            FaderDirection::Down => {
                                                next += 1;
                                                if
                                                    next >= len
                                                {
                                                    next = 0;
                                                }
                                            },
                                            FaderDirection::Up => {
                                                if
                                                    prev == 0
                                                {
                                                    next = len;
                                                }
                                                next -= 1;
                                            },
                                        };
                                        phase = FaderPhase::Hold(next);
                                        // println!("ii next frame {phase} {next}");
                                        // render next frame
                                        values = self
                                            .clone()
                                            .render_hold(
                                                command_buffer_allocator,
                                                descriptor_set_allocator,
                                                next,
                                                resources,
                                            );
                                    },
                                };
                            },
                        };
                    },
                };
            },
        };

        values = FaderValues {
            direction,
            mix_count,
            mix_step,
            mix_value,
            phase,
            time_stamp,
            ..values
        };
        Self {
            values,
            updated: Instant::now(),
            ..self
        }
    }

    fn fader_mix(
        self,
        mix_count: u32,
        mix_step: f32,
    ) -> f32 {
        let values = self.values;
        // let mut mix_value: f32 = values.mix_value;

        match
            values
                .transition
        {
            FaderTransition::Linear => {
                let mut mix_value: f32 = 1.0 - mix_step * (mix_count as f32);
                // mix_value -= mix_step;
                if mix_value < 0.0 {
                    mix_value = 0.0;
                }
                mix_value
            },
            FaderTransition::Sine => {
                let mut mix_value: f32 = ((std::f32::consts::PI * mix_step * (mix_count as f32)).cos() + 1.0) / 2.0;
                if mix_value < 0.0 {
                    mix_value = 0.0;
                }
                mix_value
            },
        }
    }

}

// Render:
impl FaderNode {
    // Render hold phase:
    pub fn render_hold(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        next: usize,
        resources: RenderResources,
    ) -> FaderValues {
        let values: FaderValues = self.values;
        let mut sources: BTreeMap<String, NodeLayer> = values.sources.clone();

        match
            sources
                .iter()
                .nth(next)
        {
            None => (),
            Some((layer_id, layer)) => {
                let mut connection: InputConnection = layer.connection.clone();
                let mut scene: ConnectionScene = connection.scene;

                // Update ConnectionScene.
                scene = scene
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone()
                    );
                match
                    layer
                        .connection
                        .scene
                        .image_sink
                        .to_owned()
                {
                    None => (),
                    Some(image_input) => {
                        // Render input to storage image.
                        self.scene.render_hold(
                            command_buffer_allocator,
                            descriptor_set_allocator,
                            image_input,
                            resources,
                        );
                    },
                };
                connection = InputConnection {
                    scene,
                    ..connection
                };

                sources
                    .insert(
                        layer_id.to_owned(),
                        NodeLayer {
                            connection,
                            ..layer.to_owned()
                        },
                    );
            },
        };
        FaderValues {
            sources,
            ..values
        }
    }

    // Render mix phase:
    pub fn render_mix(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        mix: f32,
        next: usize,
        prev: usize,
        resources: RenderResources,
    ) -> FaderValues {
        let values: FaderValues = self.values;
        let mut sources: BTreeMap<String, NodeLayer> = values.sources.clone();

        match
            sources
                .clone()
                .iter()
                .nth(prev)
        {
            None => (),
            Some((prev_id, prev_layer)) => {
                let mut prev_connection: InputConnection = prev_layer.connection.clone();
                let mut prev_scene: ConnectionScene = prev_connection.scene;

                // Update ConnectionScene.
                prev_scene = prev_scene
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone()
                    );
                match
                    prev_scene
                        .image_sink
                        .to_owned()
                {
                    None => (),
                    Some(image_prev) => {

                        match
                            sources
                                .iter()
                                .nth(next)
                        {
                            None => (),
                            Some((next_id, next_layer)) => {
                                let mut next_connection: InputConnection = next_layer.connection.clone();
                                let mut next_scene: ConnectionScene = next_connection.scene;

                                // Update ConnectionScene.
                                next_scene = next_scene
                                    .update(
                                        command_buffer_allocator
                                            .clone(),
                                        resources
                                            .clone()
                                    );
                                match
                                    next_scene
                                        .image_sink
                                        .to_owned()
                                {
                                    None => (),
                                    Some(image_next) => {
                                        let extent: [f32; 2] = [values.output_size.x, values.output_size.y];
                                        let viewport: Viewport = Viewport {
                                            offset: [
                                                0.0,
                                                0.0,
                                            ],
                                            extent,
                                            depth_range: 0.0..=1.0,
                                        };

                                        // Render input to storage image.
                                        self.scene.render_mix(
                                            command_buffer_allocator,
                                            descriptor_set_allocator,
                                            image_next,
                                            image_prev,
                                            mix,
                                            resources,
                                            viewport,
                                        );
                                    },
                                };
                                next_connection = InputConnection {
                                    scene: next_scene,
                                    ..next_connection
                                };

                                sources
                                    .insert(
                                        next_id.to_owned(),
                                        NodeLayer {
                                            connection: next_connection,
                                            ..next_layer.to_owned()
                                        },
                                    );
                            },
                        };
                    },
                };
                prev_connection = InputConnection {
                    scene: prev_scene,
                    ..prev_connection
                };

                sources
                    .insert(
                        prev_id.to_owned(),
                        NodeLayer {
                            connection: prev_connection,
                            ..prev_layer.to_owned()
                        },
                    );
            },
        };
        FaderValues {
            sources,
            ..values
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
