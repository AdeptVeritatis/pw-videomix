
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            bind::{
                elements::filter_label::{
                    FilterAngle,
                    FilterBoundHigh,
                    FilterBoundLow,
                    FilterGainB,
                    FilterGainG,
                    FilterGainR,
                    FilterRotSpeed,
                },
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        COLOR_ROTATOR_ANGLE_OFFSET,
        COLOR_ROTATOR_BOUND_HIGH_OFFSET,
        COLOR_ROTATOR_BOUND_LOW_OFFSET,
        COLOR_ROTATOR_GAIN_OFFSET,
        COLOR_ROTATOR_SPEED_OFFSET,
        NODE_OUTPUT_HEIGHT,
        NODE_OUTPUT_WIDTH,
        PANEL_NODE_SPACING_SMALL,
    },
    impls::{
        midir::{
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        // serde::definitions::{
        //     // Pos2Def,
        //     Vec2Def,
        // },
        vulkano::scenes::{
            copy::copy_connection_scene::ConnectionScene,
            render::color_mixer_scene::{
                ColorMixerMatrix,
                ColorMixerScene,
            },
        },
    },
    nodes::{
        connections::InputConnection,
        filter::FilterNode,
        NodeClass,
        NodeFamily,
        draw_node_connect_single,
        draw_node_header,
        draw_node_options,
        draw_node_output,
    },
};
use egui_winit_vulkano::{
    egui::{
        Grid,
        Pos2,
        Ui,
        Vec2,
    },
    RenderResources,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ColorRotatorValues {
    // Size of the canvas, the color rotator is projected on.
    // #[serde(with = "Vec2Def")]
    #[serde(skip)]
    pub output_size: Vec2,
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    // #[serde(with = "Pos2Def")]
    #[serde(skip)]
    pub pos_out: Pos2,
    // pub sinks: Vec<FilterSink>,
    // Source, the input the filter is connected to.
    #[serde(skip_deserializing)]
    pub source: Option<InputConnection>,
    // State: has no state, is always mixing

    // Color rotator specific settings.
    pub angle: f32,
    pub bound_high: f32,
    pub bound_low: f32,
    pub gain_r: f32,
    pub gain_g: f32,
    pub gain_b: f32,
    pub rot_speed: f32,
}

impl Default for ColorRotatorValues {
    fn default() -> Self {
        Self {
            output_size: Vec2::new(NODE_OUTPUT_WIDTH, NODE_OUTPUT_HEIGHT),
            pos_out: Pos2::default(),
            source: None,

            angle: COLOR_ROTATOR_ANGLE_OFFSET,
            bound_high: COLOR_ROTATOR_BOUND_HIGH_OFFSET,
            bound_low: COLOR_ROTATOR_BOUND_LOW_OFFSET,
            gain_r: COLOR_ROTATOR_GAIN_OFFSET,
            gain_g: COLOR_ROTATOR_GAIN_OFFSET,
            gain_b: COLOR_ROTATOR_GAIN_OFFSET,
            rot_speed: COLOR_ROTATOR_SPEED_OFFSET,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Debug, Clone)]
pub struct ColorRotatorNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    #[serde(skip_serializing)]
    pub scene: ColorMixerScene,
    pub values: ColorRotatorValues,
    #[serde(skip_serializing)]
    pub updated: Instant,
}

// Constructors and helpers:
impl ColorRotatorNode {
    pub fn new(
        filter_id: String,
        resources: RenderResources,
    ) -> Self {
        let scene: ColorMixerScene = ColorMixerScene::new(
            resources,
        );
        Self {
            class: NodeClass::ColorRotator,
            family: NodeFamily::Filter,
            id: filter_id.clone(),
            name: filter_id.clone(),
            scene,
            values: ColorRotatorValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a ColorRotatorNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
    // Header:
        self.name = draw_node_header(
            self.class.clone(),
            self.id.clone(),
            self.name.clone(),
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Options:
        draw_node_options(
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Input:
        // Show the connect widget.
        // Get new connection or use old.
        ui_inner.separator();
        self.values.source = draw_node_connect_single(
            Rc::clone(&map_filter_to_node),
            Rc::clone(&map_source_to_node),
            self.class.clone(),
            self.family.clone(),
            self.id.clone(),
            self.name.clone(),
            self.values.source.clone(),
            ui_area,
            ui_inner,
        );
    // Output:
        ui_inner.separator();
        ui_inner.vertical_centered(|ui_source| {
            self.values.output_size = draw_node_output(
                event_loop_proxy.clone(),
                self.family.clone(),
                self.id.clone(),
                self.values.output_size,
                ui_source,
            );
            self.values.pos_out = ui_source.min_rect().max;
        });
    }

    /// The side panel of a ColorRotatorNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
    // Options:
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        Grid::new(
            "color rotator",
        )
            .show(
                ui,
                |ui_grid| {
                    self.values.angle = FilterAngle::show(
                        self
                            .values
                            .angle,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.rot_speed = FilterRotSpeed::show(
                        self
                            .values
                            .rot_speed,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.bound_high = FilterBoundHigh::show(
                        self
                            .values
                            .bound_high,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.bound_low = FilterBoundLow::show(
                        self
                            .values
                            .bound_low,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.gain_r = FilterGainR::show(
                        self
                            .values
                            .gain_r,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.gain_g = FilterGainG::show(
                        self
                            .values
                            .gain_g,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.gain_b = FilterGainB::show(
                        self
                            .values
                            .gain_b,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                },
            );
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );

    }

    pub fn control_bind(
        &self,
        bind: String,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "angle" => {
                node.values.angle = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorRotator(
                            node,
                        ),
                    );
            },
            "bound_high" => {
                node.values.bound_high = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorRotator(
                            node,
                        ),
                    );
            },
            "bound_low" => {
                node.values.bound_low = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorRotator(
                            node,
                        ),
                    );
            },
            "gain_r" => {
                node.values.gain_r = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorRotator(
                            node,
                        ),
                    );
            },
            "gain_g" => {
                node.values.gain_g = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorRotator(
                            node,
                        ),
                    );
            },
            "gain_b" => {
                node.values.gain_b = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorRotator(
                            node,
                        ),
                    );
            },
            "rot_speed" => {
                node.values.rot_speed = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::ColorRotator(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> FilterNode {
        let mut values: ColorRotatorValues = self
            .values
            .clone();

        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "angle" => {
                        values.angle = FilterAngle::change_midi(
                            values
                                .angle,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "bound_high" => {
                        values.bound_high = FilterBoundHigh::change_midi(
                            values
                                .bound_high,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "bound_low" => {
                        values.bound_low = FilterBoundLow::change_midi(
                            values
                                .bound_low,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "gain_r" => {
                        values.gain_r = FilterGainR::change_midi(
                            values
                                .gain_r,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "gain_g" => {
                        values.gain_g = FilterGainG::change_midi(
                            values
                                .gain_g,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "gain_b" => {
                        values.gain_b = FilterGainB::change_midi(
                            values
                                .gain_b,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "rot_speed" => {
                        values.rot_speed = FilterRotSpeed::change_midi(
                            values
                                .rot_speed,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        FilterNode::ColorRotator(
            Self {
                values,
                ..self
                    .to_owned()
            }
        )
    }

}

// Helpers:
impl ColorRotatorNode {
    // Update storage image.
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        resources: RenderResources,
    ) -> Self {
        let mut values: ColorRotatorValues = self.values.clone();
        let mut angle: f32 = values.angle;

        match
            values
                .source
        {
            None => self,
            Some(mut connection) => {
                // Update ConnectionScene.
                let scene: ConnectionScene = connection.scene
                    .clone()
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone()
                    );
                connection = InputConnection {
                    scene,
                    ..connection
                };
                // Get image from InputConnection.
                match
                    // is image connection updated?
                    connection
                        .scene
                        .image_sink
                        .clone()
                {
                    None => {
                        println!("!! has source but no scene");
                    },
                    Some(image_input) => {
                        // Render input to storage image.
                        let image_output = self.scene.image_storage.clone();

                        let extent = image_output.image().extent();
                        let viewport: Viewport = Viewport {
                            offset: [
                                0.0,
                                0.0,
                            ],
                            extent: [extent[0] as f32, extent[1] as f32],
                            depth_range: 0.0..=1.0,
                        };


                        let bound_low: f32 = values.bound_low;
                        let diff: f32 = values.bound_high - bound_low;
                        // find a better solution???
                        let mix_param_0: f32 = ((((angle - 4.0/3.0) * std::f32::consts::PI).cos() + 1.0 ) / 2.0).powi(2) * diff + bound_low;
                        let mix_param_1: f32 = ((((angle - 2.0/3.0) * std::f32::consts::PI).cos() + 1.0 ) / 2.0).powi(2) * diff + bound_low;
                        let mix_param_2: f32 = ((((angle) * std::f32::consts::PI).cos() + 1.0 ) / 2.0).powi(2) * diff + bound_low;
                        let mix_param_3: f32 = ((((angle + 2.0/3.0) * std::f32::consts::PI).cos() + 1.0 ) / 2.0).powi(2) * diff + bound_low;
                        let mix_param_4: f32 = ((((angle + 4.0/3.0) * std::f32::consts::PI).cos() + 1.0 ) / 2.0).powi(2) * diff + bound_low;

                        let mixer_matrix: ColorMixerMatrix = ColorMixerMatrix {
                            mix_rr: mix_param_2 * values.gain_r,
                            mix_rg: mix_param_3 * values.gain_r,
                            mix_rb: mix_param_4 * values.gain_r,
                            mix_gr: mix_param_1 * values.gain_g,
                            mix_gg: mix_param_2 * values.gain_g,
                            mix_gb: mix_param_3 * values.gain_g,
                            mix_br: mix_param_0 * values.gain_b,
                            mix_bg: mix_param_1 * values.gain_b,
                            mix_bb: mix_param_2 * values.gain_b,
                        };

                        // Prepare next round:
                        angle = match
                            angle
                            +
                            values
                                .rot_speed
                        {
                            angle if
                                angle
                                <
                                -1.0
                            => angle + 2.0,
                            angle if
                                angle
                                >
                                1.0
                            => angle - 2.0,
                            angle => angle,
                        };

                        self.scene.render(
                            command_buffer_allocator,
                            descriptor_set_allocator,
                            image_input,
                            image_output,
                            mixer_matrix,
                            resources,
                            viewport,
                        );
                    },
                };
                // image_sink doesn't need to be stored again.???
                // Store updated values.
                values = ColorRotatorValues {
                    angle,
                    source: Some(connection),
                    ..self.values.clone()
                };
                Self{
                    values,
                    updated: Instant::now(),
                    ..self
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
