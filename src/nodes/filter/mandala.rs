
// RosetteNode instead of Mandala because it is the rosette group???

use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            bind::{
                elements::filter_label::{
                    FilterMeshPositionX,
                    FilterMeshPositionY,
                    FilterMeshRotate,
                    FilterMeshRotationSpeed,
                    FilterMeshSize,
                    FilterRepetitions,
                    FilterTexPositionX,
                    FilterTexPositionY,
                    FilterTexRotate,
                    FilterTexRotationPreset,
                    FilterTexRotationSpeed,
                    FilterTexSize,
                },
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        MANDALA_ANGLE_OFFSET,
        FILTER_MOVE_OFFSET_MESH,
        FILTER_MOVE_OFFSET_TEX,
        MANDALA_REPETITIONS_OFFSET,
        MANDALA_ROTATE_PRESET,
        MANDALA_ROTATION_SPEED_OFFSET,
        MANDALA_SIZE_OFFSET,
        NODE_OUTPUT_HEIGHT,
        NODE_OUTPUT_WIDTH,
        PANEL_NODE_SPACING_SMALL,
    },
    impls::{
        midir::{
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        serde::definitions::{
            Pos2Def,
            // Vec2Def,
        },
        vulkano::{
            scenes::{
                copy::copy_connection_scene::ConnectionScene,
                render::mandala_scene::MandalaScene,
            },
            vertices::TextureVertex,
        },
    },
    nodes::{
        connections::InputConnection,
        filter::FilterNode,
        NodeClass,
        NodeFamily,
        NodeState,
        draw_node_connect_single,
        draw_node_header,
        draw_node_options,
        draw_node_output,
        draw_node_state,
    },
};
use egui_winit_vulkano::{
    egui::{
        ComboBox,
        Grid,
        Pos2,
        Ui,
        Vec2,
    },
    RenderResources,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    fmt,
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MandalaValues {
    // Size of the canvas, the mandala is projected on.
    // #[serde(with = "Vec2Def")]
    #[serde(skip)]
    pub output_size: Vec2,
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    // #[serde(with = "Pos2Def")]
    #[serde(skip)]
    pub pos_out: Pos2,
    // pub sinks: Vec<FilterSink>,
    // Source, the input the filter is connected to.
    #[serde(skip_deserializing)]
    pub source: Option<InputConnection>,
    // State: play, pause, stop
    pub state: NodeState,

    // Mandala specific settings.
    pub repetitions: u32,
    // Mesh.
    #[serde(with = "Pos2Def")]
    pub mesh_position: Pos2,
    pub mesh_rotate: bool,
    pub mesh_rotation: f32,
    pub mesh_rotation_speed: f32,
    pub mesh_size: f32,
    // Texture UV mapping.
    pub tex_mapping: MandalaMapping,
    #[serde(with = "Pos2Def")]
    pub tex_position: Pos2,
    pub tex_rotate: bool,
    pub tex_rotation: f32,
    pub tex_rotation_preset: f32,
    pub tex_rotation_speed: f32,
    pub tex_side: MandalaSide,
    pub tex_size: f32,
}

impl Default for MandalaValues {
    fn default() -> Self {
        Self {
            output_size: Vec2::new(NODE_OUTPUT_WIDTH, NODE_OUTPUT_HEIGHT),
            pos_out: Pos2::default(),
            source: None,
            state: NodeState::Play,

            repetitions: MANDALA_REPETITIONS_OFFSET,

            mesh_position: Pos2::new(FILTER_MOVE_OFFSET_MESH, FILTER_MOVE_OFFSET_MESH),
            mesh_rotate: false,
            mesh_rotation: MANDALA_ANGLE_OFFSET,
            mesh_rotation_speed: MANDALA_ROTATION_SPEED_OFFSET,
            mesh_size: MANDALA_SIZE_OFFSET,

            tex_mapping: MandalaMapping::Flat,
            tex_position: Pos2::new(FILTER_MOVE_OFFSET_TEX, FILTER_MOVE_OFFSET_TEX),
            tex_rotate: MANDALA_ROTATE_PRESET,
            tex_rotation: MANDALA_ANGLE_OFFSET,
            tex_rotation_preset: MANDALA_ANGLE_OFFSET,
            tex_rotation_speed: MANDALA_ROTATION_SPEED_OFFSET,
            tex_side: MandalaSide::Left,
            tex_size: MANDALA_SIZE_OFFSET,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct MandalaMesh {
    pub indices: Vec<u32>,
    pub mesh_rotation: f32,
    pub tex_rotation: f32,
    pub vertices: Vec<TextureVertex>,
    pub viewport: Viewport,
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum MandalaMapping {
    // just throwing names around.
    // Clean,
    // Deformed,
    // Even,
    Flat,
    Flow,
    // Morphing,
    // None,
    // Plain,
    // Solid,
    Warped,
}

impl fmt::Display for MandalaMapping {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Flat => write!(f, "flat"),
            Self::Flow => write!(f, "flow"),
            Self::Warped => write!(f, "warped"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum MandalaSide {
    Left,
    Right,
}

impl fmt::Display for MandalaSide {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Left => write!(f, "left"),
            Self::Right => write!(f, "right"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Debug, Clone)]
pub struct MandalaNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    #[serde(skip_serializing)]
    pub scene: MandalaScene,
    pub values: MandalaValues,
    #[serde(skip_serializing)]
    pub updated: Instant,
}

// Constructors and draw:
impl MandalaNode {
    pub fn new(
        filter_id: String,
        resources: RenderResources,
    ) -> Self {
        let scene: MandalaScene = MandalaScene::new(
            resources,
        );
        Self {
            class: NodeClass::Mandala,
            family: NodeFamily::Filter,
            id: filter_id.clone(),
            name: filter_id.clone(),
            scene,
            values: MandalaValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a MandalaNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
    // Header:
        self.name = draw_node_header(
            self.class.clone(),
            self.id.clone(),
            self.name.clone(),
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Options:
        ui_inner.separator();
        Grid::new("Options").show(ui_inner, |ui_grid| {
            self.values.state = draw_node_state(self.values.state.clone(), ui_grid);
        });
        draw_node_options(
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Input:
        // Show the connect widget.
        // Get new connection or use old.
        ui_inner.separator();
        self.values.source = draw_node_connect_single(
            Rc::clone(&map_filter_to_node),
            Rc::clone(&map_source_to_node),
            self.class.clone(),
            self.family.clone(),
            self.id.clone(),
            self.name.clone(),
            self.values.source.clone(),
            ui_area,
            ui_inner,
        );
    // Output:
        ui_inner.separator();
        ui_inner.vertical_centered(|ui_source| {
            self.values.output_size = draw_node_output(
                event_loop_proxy.clone(),
                self.family.clone(),
                self.id.clone(),
                self.values.output_size,
                ui_source,
            );
            self.values.pos_out = ui_source.min_rect().max;
        });
    }

    /// The side panel of a MandalaNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
    // Options:
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        Grid::new("Header")
            .show(
                ui,
                |ui_grid| {
                    self.values.repetitions = FilterRepetitions::show(
                        self
                            .values
                            .repetitions,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.mesh_size = FilterMeshSize::show(
                        self
                            .values
                            .mesh_size,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.mesh_position.x = FilterMeshPositionX::show(
                        self
                            .values
                            .mesh_position
                            .x,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    self.values.mesh_position.y = FilterMeshPositionY::show(
                        self
                            .values
                            .mesh_position
                            .y,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                    self.values.mesh_rotate = FilterMeshRotate::show(
                        self
                            .values
                            .mesh_rotate,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    self.values.mesh_rotation_speed = FilterMeshRotationSpeed::show(
                        self
                            .values
                            .mesh_rotation_speed,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                },
            );
    // Texture UV mapping:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label("mapping:");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ComboBox::from_id_salt(format!("ComboBox {} mapping", self.id))
            .selected_text(
                self
                    .values
                    .tex_mapping
                    .to_string(),
            )
            .wrap()
            .show_ui(ui, |ui_combo| {
                for shape_mapping in [
                    MandalaMapping::Flat,
                    MandalaMapping::Flow,
                    MandalaMapping::Warped,
                ] {
                    ui_combo.selectable_value(
                        &mut self.values.tex_mapping,
                        shape_mapping
                            .clone(),
                        shape_mapping
                            .to_string(),
                    );
                }
            });
        ComboBox::from_id_salt(format!("ComboBox {} side", self.id))
            .selected_text(
                self
                    .values
                    .tex_side
                    .to_string(),
            )
            .wrap()
            .show_ui(ui, |ui_combo| {
                for side in [MandalaSide::Left, MandalaSide::Right] {
                    ui_combo.selectable_value(
                        &mut self.values.tex_side,
                        side
                            .clone(),
                        side
                            .to_string(),
                    );
                }
            });
        Grid::new("Mapping").show(ui, |ui_grid| {
            self.values.tex_size = FilterTexSize::show(
                self
                    .values
                    .tex_size,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            ui_grid
                .end_row();
            self.values.tex_position.x = FilterTexPositionX::show(
                self
                    .values
                    .tex_position
                    .x,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            self.values.tex_position.y = FilterTexPositionY::show(
                self
                    .values
                    .tex_position
                    .y,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            ui_grid
                .end_row();
            self.values.tex_rotation_preset = FilterTexRotationPreset::show(
                self
                    .values
                    .tex_rotation_preset,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            ui_grid
                .end_row();
            self.values.tex_rotate = FilterTexRotate::show(
                self
                    .values
                    .tex_rotate,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            self.values.tex_rotation_speed = FilterTexRotationSpeed::show(
                self
                    .values
                    .tex_rotation_speed,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            ui_grid
                .end_row();
            ui_grid.label("");
            ui_grid.label("");
            ui_grid.label("@60fps:");
            ui_grid.label(format!(
                "{:.2} s",
                match
                    self
                        .values
                        .tex_rotation_speed
                {
                    speed if speed == 0.0 => 0.0,
                    speed => speed.recip().abs() / 60.0,
                    // speed => speed.recip().abs() / 60.0 / repetitions as f32, // why repetitions???
                },
            ));
        });
    }

    pub fn control_bind(
        &self,
        bind: String,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "mesh_position_x" => {
                node.values.mesh_position.x = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "mesh_position_y" => {
                node.values.mesh_position.y = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "mesh_rotation_speed" => {
                node.values.mesh_rotation_speed = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "mesh_size" => {
                node.values.mesh_size = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "repetitions" => {
                node.values.repetitions = result as u32;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "tex_position_x" => {
                node.values.tex_position.x = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "tex_position_y" => {
                node.values.tex_position.y = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "tex_rotation_preset" => {
                node.values.tex_rotation_preset = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "tex_rotation_speed" => {
                node.values.tex_rotation_speed = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            "tex_size" => {
                node.values.tex_size = result;
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        FilterNode::Mandala(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> FilterNode {
        // Using location[0], location[1], ..., because we know the data structure.
        let mut values: MandalaValues = self
            .values
            .clone();

        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "mesh_position_x" => {
                        // f32_from-1.0_fillrange_default0.0#64_fillrange_to1.0
                        values.mesh_position.x = FilterMeshPositionX::change_midi(
                            values
                                .mesh_position
                                .x,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mesh_position_y" => {
                        // f32_from-1.0_fillrange_default0.0#64_fillrange_to1.0
                        values.mesh_position.y = FilterMeshPositionY::change_midi(
                            values
                                .mesh_position
                                .y,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mesh_rotate" => {
                        values.mesh_rotate = FilterMeshRotate::change_midi(
                            values
                                .mesh_rotate,
                        );
                    },
                    "mesh_rotation_speed" => {
                        // f32_from#0_singlespeed_default0.0#64_singlespeed_to#127
                        values.mesh_rotation_speed = FilterMeshRotationSpeed::change_midi(
                            values
                                .mesh_rotation_speed,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "mesh_size" => {
                        // f32_from0.01_fillrange_default1.0#64_fillrange_to10.0
                        values.mesh_size = FilterMeshSize::change_midi(
                            values
                                .mesh_size,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "repetitions" => {
                        // u32_from2_singlestep_default6_singlestep_#64_fillrange_to360
                        values.repetitions = FilterRepetitions::change_midi(
                            values
                                .repetitions,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "tex_position_x" => {
                        values.tex_position.x = FilterTexPositionX::change_midi(
                            values
                                .tex_position
                                .x,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "tex_position_y" => {
                        values.tex_position.y = FilterTexPositionY::change_midi(
                            values
                                .tex_position
                                .y,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "tex_rotate" => {
                        values.tex_rotate = FilterTexRotate::change_midi(
                            values
                                .tex_rotate,
                        );
                    },
                    "tex_rotation_preset" => {
                        values.tex_rotation_preset = FilterTexRotationPreset::change_midi(
                            values
                                .tex_rotation_preset,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "tex_rotation_speed" => {
                        values.tex_rotation_speed = FilterTexRotationSpeed::change_midi(
                            values
                                .tex_rotation_speed,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "tex_size" => {
                        values.tex_size = FilterTexSize::change_midi(
                            values
                                .tex_size,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        FilterNode::Mandala(
            Self {
                values,
                ..self
                    .to_owned()
            }
        )
    }
}

// Helpers:
impl MandalaNode {
    fn mandala_mesh(
        self,
    ) -> MandalaMesh {
        // Calculate distances and angles from values of node.
        let values: MandalaValues = self.values.clone();
        let repetitions: u32 = values.repetitions;
        // Angle in radians for a single triangle.
        // One repetition needs two triangles mirrored (360°/2) or (2pi/2).
        let repeat_rad: f32 = std::f32::consts::PI / repetitions as f32;

        // Mesh values:
        let mesh_size: f32 = values.mesh_size;
        let mesh_center: Pos2 = values.mesh_position;
        let mut mesh_rotation: f32 = values.mesh_rotation;

        // Texture values:
        let tex_size: f32 = (2.0 * values.tex_size).recip();
        // Adjust coordinates from [-1.0..1.0] to [0.0..1.0]
        let tex_center: Pos2 = Pos2::new(
            - values.tex_position.x,
            values.tex_position.y,
        );
        let tex_rotation_preset: f32 = values.tex_rotation_preset;
        let mut tex_rotation: f32 = values.tex_rotation;
        let tex_mirror: f32 = match
            values.tex_side
        {
            MandalaSide::Left => 1.0,
            MandalaSide::Right => - 1.0,
        };

        // Create vertices and indices for triangles.
        let mut vertices: Vec<TextureVertex> = vec!(
            // 0 - Center
            TextureVertex {
                position: [
                    2.0 * mesh_center.x,
                    - 2.0 * mesh_center.y,
                ],
                tex_coords: [
                    tex_center.x,
                    tex_center.y,
                ],
            },
        );
        let mut indices: Vec<u32> = Vec::new();
        // Two vertices, which are the left corner of two neighboring triangles.
        // The left corner of the next triangle is the right corner of this triangle.
        // Indices can target to vertices being created only in next iteration.
        for i_repeat in 0..repetitions {
            // Two triangles build a pair.
            // Right side is mirrored

            // Doesn't change for a triangle pair:
            // let tex_rotation_rad: f32 = (tex_rotation + tex_rotation_preset / 360.0).to_radians();
            let tex_rotation_rad: f32 = 2.0 * std::f32::consts::PI * (tex_rotation + tex_rotation_preset / 360.0);

            // 0 - left side, 1 - right side
            for i_mirror in 0..2 {
                let i_element: u32 = 2 * i_repeat + i_mirror;

            // Mesh.
                let mesh_rotation_rad: f32 = {
                    // repeat_rad * i_element as f32 - mesh_rotation.to_radians()
                    repeat_rad * i_element as f32 - std::f32::consts::TAU * mesh_rotation
                };
                // Distances from center point.
                let mesh_distance_x: f32 = mesh_size * mesh_rotation_rad.sin();
                let mesh_distance_y: f32 = mesh_size * mesh_rotation_rad.cos();
                // Position.
                let position: [f32; 2] = [
                    2.0 * mesh_center.x - mesh_distance_x,
                    - 2.0 * mesh_center.y - mesh_distance_y,
                ];

            // Texture.
                let tex_coords: [f32; 2] = match
                    values
                        .tex_mapping
                        .clone()
                {
                    MandalaMapping::Flat => {
                        // Distances from center point.
                        let tex_distance: Vec2 = match
                            i_mirror
                        {
                            0 => {
                                Vec2::new(
                                    tex_size * tex_rotation_rad.sin(),
                                    tex_size * tex_rotation_rad.cos(),
                                )
                            },
                            _ => {
                                Vec2::new(
                                    tex_size * (tex_rotation_rad + repeat_rad).sin(),
                                    tex_size * (tex_rotation_rad + repeat_rad).cos(),
                                )
                            },
                        };
                        [
                            tex_center.x - tex_distance.x * tex_mirror,
                            tex_center.y - tex_distance.y,
                        ]
                    },
                    MandalaMapping::Flow => {
                        // Distances from center point.
                        let tex_distance: Vec2 = match
                            i_mirror
                        {
                            0 => {
                                Vec2::new(
                                    tex_size * (tex_rotation_rad / repetitions as f32).sin(),
                                    tex_size * (tex_rotation_rad / repetitions as f32).cos(),
                                )
                            },
                            _ => {
                                Vec2::new(
                                    tex_size * ((tex_rotation_rad + repeat_rad) / repetitions as f32).sin(),
                                    tex_size * ((tex_rotation_rad + repeat_rad) / repetitions as f32).cos(),
                                )
                            },
                        };
                        [
                            tex_center.x - tex_distance.x * tex_mirror,
                            tex_center.y - tex_distance.y,
                        ]
                    },
                    MandalaMapping::Warped => {
                        // Distances from center point.
                        let tex_distance: Vec2 = match
                            i_mirror
                        {
                            0 => {
                                Vec2::new(
                                    tex_size * tex_rotation_rad.sin(),
                                    tex_size * tex_rotation_rad.cos(),
                                )
                            },
                            _ => {
                                Vec2::new(
                                    tex_size * (tex_rotation_rad + std::f32::consts::FRAC_PI_2).sin(),
                                    tex_size * (tex_rotation_rad + std::f32::consts::FRAC_PI_2).cos(),
                                )
                            },
                        };
                        [
                            tex_center.x - tex_distance.x * tex_mirror,
                            tex_center.y - tex_distance.y,
                        ]

                    },
                };

            // Vertices.
                vertices.push(
                    TextureVertex {
                        position,
                        tex_coords,
                    },
                );
            // Indices elements.
                // Start at left from center top, the last of the vertices.
                let index = match
                    i_element
                {
                    0 => 2 * repetitions,
                    _ => i_element,
                };
                // Write indices.
                indices.push(0);
                indices.push(index);
                indices.push(i_element + 1);

            }
            // Prepare next rotation.
            // Add one rotation speed to it.
            // Why does rotation speed depend on repetitions???
            mesh_rotation = match
                values.mesh_rotate
            {
                true => (mesh_rotation + values.mesh_rotation_speed / repetitions as f32) % 1.0,
                false => mesh_rotation,
            };
            // Texture UV mapping values:
            tex_rotation = match
                values.tex_rotate
            {
                true => (tex_rotation + values.tex_rotation_speed / repetitions as f32) % 1.0,
                false => tex_rotation,
            };
        };

        let extent: [f32; 2] = [self.values.output_size.x, self.values.output_size.y];
        let viewport: Viewport = Viewport {
            offset: [
                0.0,
                0.0,
            ],
            extent,
            depth_range: 0.0..=1.0,
        };
        MandalaMesh {
            indices,
            mesh_rotation,
            tex_rotation,
            vertices,
            viewport,
        }
    }

    // Update storage image.
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        resources: RenderResources,
    ) -> Self {
        let mut values: MandalaValues = self.values.clone();

        match
            values
                .source
        {
            None => self,
            Some(mut connection) => {
                // Update ConnectionScene.
                let scene: ConnectionScene = connection.scene
                    .clone()
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone()
                    );
                connection = InputConnection {
                    scene,
                    ..connection
                };
                // Get image from InputConnection.
                let (mesh_rotation, tex_rotation) = match
                    // is image connection updated?
                    connection
                        .scene
                        .image_sink
                        .clone()
                {
                    None => {
                        println!("!! has source but no scene");
                        (self.values.mesh_rotation, self.values.tex_rotation)
                    },
                    Some(image_input) => {
                        // Render input to storage image.
                        match
                            self
                                .clone()
                                .values
                                .state
                        {
                            NodeState::Stop => (self.values.mesh_rotation, self.values.tex_rotation),
                            NodeState::Pause => (self.values.mesh_rotation, self.values.tex_rotation),
                            NodeState::Play => {
                                let image_output = self.scene.image_storage.clone();
                                let mandala_mesh: MandalaMesh = self.clone().mandala_mesh();

                                self.scene.render(
                                    command_buffer_allocator,
                                    descriptor_set_allocator,
                                    image_input,
                                    image_output,
                                    mandala_mesh.clone(),
                                    resources,
                                );
                                (mandala_mesh.mesh_rotation, mandala_mesh.tex_rotation)
                            },
                        }
                    },
                };
                // image_sink doesn't need to be stored again.???
                // Store updated values.
                values = MandalaValues {
                    mesh_rotation,
                    tex_rotation,
                    source: Some(connection),
                    ..self.values.clone()
                };
                Self {
                    values,
                    updated: Instant::now(),
                    ..self
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// idea:
// Option for radius with x, y and diagonally bounds.
// let mesh_radius: f32 = match
//     output_aspect_ratio
// {
//     // Landscape / wide and square:
//     // ratio if ratio >= 1. => output_rect.height() * mesh_size / 2.0,
//     // Portrait / high:
//     // _ => output_rect.width() * mesh_size / 2.0,
// };
