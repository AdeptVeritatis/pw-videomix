pub mod function_generator;
pub mod number;
pub mod trigger;
pub mod trigonometry;

use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
        gui::{
            bind::BindValues,
            options::GuiOptions,
        },
    },
    constants::CONTROL_NODE_SIZE,
    nodes::{
        control::{
            function_generator::FunctionGeneratorNode,
            number::NumberNode,
            trigger::TriggerNode,
            trigonometry::TrigonometryNode,
        },
        filter::FilterNode,
        sinks::SinkNode,
        sources::SourceNode,
        NodeFamily,
    },
};
#[cfg(feature = "gstreamer")]
use crate::nodes::sinks::video_out::EncoderNode;
use egui_winit_vulkano::{
    egui::{
        Context,
        Sense,
        Ui,
        Vec2,
        Window,
    },
    RenderResources,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    fmt,
    rc::Rc,
};
// use vulkano::pipeline::graphics::viewport::Viewport;
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ControlInput {
    pub class: ControlInputClass,
    pub input: Option<String>, // a control node
    pub value: Option<f32>,
}

impl Default for ControlInput {
    fn default() -> Self {
        Self {
            class: ControlInputClass::Zero,
            input: None,
            value: None,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum ControlInputClass {
    Input,
    One,
    Pi,
    Zero,
}

impl fmt::Display for ControlInputClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Input => write!(f, "Input"),
            Self::One => write!(f, "1"),
            Self::Pi => write!(f, "Pi"),
            Self::Zero => write!(f, "0"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum ControlNode {
    None,
    FunctionGenerator(FunctionGeneratorNode),
    /// Add or multiply a number to input.
    Number(NumberNode),
    Trigger(TriggerNode),
    Trigonometry(TrigonometryNode),
}

impl fmt::Display for ControlNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::None => write!(f, "none"),
            Self::FunctionGenerator(_) => write!(f, "Function Generator"),
            Self::Number(_) => write!(f, "Number"),
            Self::Trigger(_) => write!(f, "Trigger"),
            Self::Trigonometry(_) => write!(f, "Trigonometry"),
        }
    }
}

/// ## Constructors
impl ControlNode {
    pub fn function_generator(
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        resources: RenderResources,
    ) {
        // Get next available control ID.
        let control_id: String = get_next_control_id(
            map_control_to_node
                .clone()
        );
        // Create FunctionGeneratorNode.
        let function_generator_node: FunctionGeneratorNode = FunctionGeneratorNode::new(
            control_id
                .clone(),
            resources,
        );
        // Create ControlNode::FunctionGenerator from FunctionGeneratorNode.
        let control_node: Self = Self::FunctionGenerator(
            function_generator_node,
        );
        // Info.
        println!("-> create ControlNode \"{control_id}\" ( {control_node} )");
        // Save ControlNode to map.
        map_control_to_node
            .borrow_mut()
            .map
            .insert(
                control_id,
                control_node,
            );
    }

    pub fn number(
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        resources: RenderResources,
    ) {
        // Get next available control ID.
        let control_id: String = get_next_control_id(
            map_control_to_node
                .clone()
        );
        // Create NumberNode.
        let number_node: NumberNode = NumberNode::new(
            control_id
                .clone(),
            resources,
        );
        // Create ControlNode::Number from NumberNode.
        let control_node: Self = Self::Number(
            number_node,
        );
        // Info.
        println!("-> create ControlNode \"{control_id}\" ( {control_node} )");
        // Save ControlNode to map.
        map_control_to_node
            .borrow_mut()
            .map
            .insert(
                control_id,
                control_node,
            );
    }

    pub fn trigger(
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        resources: RenderResources,
    ) {
        // Get next available control ID.
        let control_id: String = get_next_control_id(
            map_control_to_node
                .clone()
        );
        // Create TriggerNode.
        let trigger_node: TriggerNode = TriggerNode::new(
            control_id
                .clone(),
            resources,
        );
        // Create ControlNode::Trigger from TriggerNode.
        let control_node: Self = Self::Trigger(
            trigger_node,
        );
        // Info.
        println!("-> create ControlNode \"{control_id}\" ( {control_node} )");
        // Save ControlNode to map.
        map_control_to_node
            .borrow_mut()
            .map
            .insert(
                control_id,
                control_node,
            );
    }

    pub fn trigonometry(
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        resources: RenderResources,
    ) {
        // Get next available control ID.
        let control_id: String = get_next_control_id(
            map_control_to_node
                .clone()
        );
        // Create TrigonometryNode.
        let trigonometry_node: TrigonometryNode = TrigonometryNode::new(
            control_id
                .clone(),
            resources,
        );
        // Create ControlNode::Trigonometry from TrigonometryNode.
        let control_node: Self = Self::Trigonometry(
            trigonometry_node,
        );
        // Info.
        println!("-> create ControlNode \"{control_id}\" ( {control_node} )");
        // Save ControlNode to map.
        map_control_to_node
            .borrow_mut()
            .map
            .insert(
                control_id,
                control_node,
            );
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn draw_control_nodes(
    context_egui: Context,
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    map_control_to_node: Rc<RefCell<ControlToNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
    ui_area: &mut Ui,
) {
    let mut control_values: HashMap<String, ControlNode> = HashMap::new();

    for
        (
            control_id,
            control_node,
        )
    in
        map_control_to_node
            .borrow()
            .map
            .iter()
    {
        let mut control_node = control_node
            .to_owned();

        // ui_area.label(format!("{} ( {:?} )", control_id, control_node));
        // Draw control nodes:
        match
            Window::new(
                control_id
                    .clone(),
            )
                .fixed_size(
                    Vec2::new(
                        CONTROL_NODE_SIZE,
                        CONTROL_NODE_SIZE,
                    ),
                )
                // .default_size(
                //     Vec2::new(
                //         CONTROL_NODE_SIZE,
                //         CONTROL_NODE_SIZE,
                //     ),
                // )
                .resizable(
                    false,
                )
                .title_bar(
                    false,
                )
                .show(
                    &context_egui,
                    |ui_control| {
                        ui_control
                            .vertical_centered(
                                |ui_inner| {
                                    // Control node content:
                                    control_node = match
                                        control_node
                                            .clone()
                                    {
                                        ControlNode::None => ControlNode::None,
                                        ControlNode::FunctionGenerator(function_generator_node) => {
                                            function_generator_node
                                                .clone()
                                                .draw(
                                                    // event_loop_proxy
                                                    //     .clone(),
                                                    // map_control_to_node
                                                    //     .clone(),
                                                    // options_gui
                                                    //     .clone(),
                                                    // ui_area,
                                                    ui_inner,
                                                );
                                            if
                                                options_gui
                                                    .borrow()
                                                    .debug_output
                                            {
                                                ui_area
                                                    .label(
                                                        format!("{control_id} ( {function_generator_node:?} )")
                                                    );
                                            };
                                            ControlNode::FunctionGenerator(function_generator_node)
                                        },
                                        ControlNode::Number(number_node) => {
                                            number_node
                                                .clone()
                                                .draw(
                                                    // event_loop_proxy
                                                    //     .clone(),
                                                    // map_control_to_node
                                                    //     .clone(),
                                                    // options_gui
                                                    //     .clone(),
                                                    // ui_area,
                                                    ui_inner,
                                                );
                                            if
                                                options_gui
                                                    .borrow()
                                                    .debug_output
                                            {
                                                ui_area
                                                    .label(
                                                        format!("{control_id} ( {number_node:?} )")
                                                    );
                                            };
                                            ControlNode::Number(number_node)
                                        },
                                        ControlNode::Trigger(trigger_node) => {
                                            trigger_node
                                                .clone()
                                                .draw(
                                                    // event_loop_proxy
                                                    //     .clone(),
                                                    // map_control_to_node
                                                    //     .clone(),
                                                    // options_gui
                                                    //     .clone(),
                                                    // ui_area,
                                                    ui_inner,
                                                );
                                            if
                                                options_gui
                                                    .borrow()
                                                    .debug_output
                                            {
                                                ui_area
                                                    .label(
                                                        format!("{control_id} ( {trigger_node:?} )")
                                                    );
                                            };
                                            ControlNode::Trigger(trigger_node)
                                        },
                                        ControlNode::Trigonometry(trigonometry_node) => {
                                            trigonometry_node
                                                .clone()
                                                .draw(
                                                    // event_loop_proxy
                                                    //     .clone(),
                                                    // map_control_to_node
                                                    //     .clone(),
                                                    // options_gui
                                                    //     .clone(),
                                                    // ui_area,
                                                    ui_inner,
                                                );
                                            if
                                                options_gui
                                                    .borrow()
                                                    .debug_output
                                            {
                                                ui_area
                                                    .label(
                                                        format!("{control_id} ( {trigonometry_node:?} )")
                                                    );
                                            };
                                            ControlNode::Trigonometry(trigonometry_node)
                                        },
                                    };
                                },
                            )
                    },
                )
        {
            None => (),
            Some(inner_response) => {
                if
                    inner_response
                        .response
                        .interact(
                            Sense::click(),
                        )
                        .clicked()
                {
                    event_loop_proxy
                        .send_event(
                            GuiEvent::ChangeNode(
                                NodeEvent {
                                    family: NodeFamily::Control,
                                    id: control_id
                                        .to_owned(),
                                    event_class: NodeEventClass::Select,
                                }
                            )
                        )
                        .ok();
                };
                inner_response
                    .response
                    .context_menu(
                        |ui_context| {
                            if
                                ui_context
                                    .button(
                                        "Remove ControlNode"
                                    )
                                    .clicked()
                            {
                                ui_context
                                    .close_menu();
                                event_loop_proxy
                                    .send_event(
                                        GuiEvent::ChangeNode(
                                            NodeEvent {
                                                family: NodeFamily::Control,
                                                id: control_id
                                                    .to_owned(),
                                                event_class: NodeEventClass::Remove,
                                            },
                                        ),
                                    )
                                    .ok();
                            };
                        },
                    );
            },
        };
        control_values
            .insert(
                control_id
                    .to_owned(),
                control_node,
            );
    };

    for
        (
            control_id,
            control_node,
        )
    in
        control_values
    {
        map_control_to_node
            .borrow_mut()
            .map
            .insert(
                control_id,
                control_node,
            );
    };
}

// ----------------------------------------------------------------------------

pub fn update_control_nodes(
    map_control_to_node: Rc<RefCell<ControlToNode>>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    resources: RenderResources,
) {
    let control_nodes = map_control_to_node
        .borrow()
        .to_owned();
    // race condition???
    for
        control_node
    in
        control_nodes
            .map
            .values()
    {
        match
            control_node
        {
            ControlNode::None => (),
            ControlNode::FunctionGenerator(node) => {
                update_bound_values(
                    node
                        .connected
                        .clone(),
                    map_control_to_node
                        .clone(),
                    map_filter_to_node
                        .clone(),
                    map_sink_to_node
                        .clone(),
                    map_source_to_node
                        .clone(),
                    #[cfg(feature = "gstreamer")]
                    resources
                        .clone(),
                    node
                        .values
                        .result,
                );
            },
            ControlNode::Number(node) => {
                update_bound_values(
                    node
                        .connected
                        .clone(),
                    map_control_to_node
                        .clone(),
                    map_filter_to_node
                        .clone(),
                    map_sink_to_node
                        .clone(),
                    map_source_to_node
                        .clone(),
                    #[cfg(feature = "gstreamer")]
                    resources
                        .clone(),
                    node
                        .values
                        .result,
                );
            },
            ControlNode::Trigger(node) => {
                update_bound_bool(
                    node
                        .connected
                        .clone(),
                    map_sink_to_node
                        .clone(),
                    #[cfg(feature = "gstreamer")]
                    resources
                        .clone(),
                    node
                        .values
                        .result,
                );
            },
            ControlNode::Trigonometry(node) => {
                update_bound_values(
                    node
                        .connected
                        .clone(),
                    map_control_to_node
                        .clone(),
                    map_filter_to_node
                        .clone(),
                    map_sink_to_node
                        .clone(),
                    map_source_to_node
                        .clone(),
                    #[cfg(feature = "gstreamer")]
                    resources
                        .clone(),
                    node
                        .values
                        .result,
                );
            },
        };
    };

    let control_nodes = map_control_to_node
        .borrow()
        .to_owned();
    for
        (
            control_id,
            control_node,
        )
    in
        control_nodes
            .map
            .clone()
    {
        match
            control_node
                .clone()
        {
            ControlNode::None => (),
            ControlNode::FunctionGenerator(mut function_generator_node) => {
                function_generator_node
                    .update(
                        map_control_to_node
                            .clone(),
                        resources
                            .clone(),
                    );

                map_control_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        control_id,
                        ControlNode::FunctionGenerator(function_generator_node),
                    );
            },
            ControlNode::Number(mut number_node) => {
                number_node
                    .update(
                        map_control_to_node
                            .clone(),
                        resources
                            .clone(),
                    );

                map_control_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        control_id,
                        ControlNode::Number(number_node),
                    );
            },
            ControlNode::Trigger(mut trigger_node) => {
                trigger_node
                    .update(
                        map_control_to_node
                            .clone(),
                        resources
                            .clone(),
                    );

                map_control_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        control_id,
                        ControlNode::Trigger(trigger_node),
                    );
            },
            ControlNode::Trigonometry(mut trigonometry_node) => {
                trigonometry_node
                    .update(
                        map_control_to_node
                            .clone(),
                        resources
                            .clone(),
                    );

                map_control_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        control_id,
                        ControlNode::Trigonometry(trigonometry_node),
                    );
            },
        };
    };
}

// ----------------------------------------------------------------------------
// Functions - binds:
// ----------------------------------------------------------------------------

fn update_bound_bool(
    binds: Vec<BindValues>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    #[cfg(feature = "gstreamer")]
    resources: RenderResources,
    _result: bool,
) {
    for
        connect
    in
        binds
    {
        match
            connect
                .node_family
        {
            NodeFamily::None => (),
            NodeFamily::Control => (),
            NodeFamily::Filter => (),
            NodeFamily::Sink => {
                let list = map_sink_to_node
                    .borrow()
                    .map
                    .to_owned();
                match
                    list
                        .get(
                            &connect
                                .node_id,
                        )
                {
                    None => (),
                    #[cfg(feature = "gstreamer")]
                    Some(SinkNode::Encoder(node)) => {

                        // result is f32 not bool!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // need a separate structure!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // create a trigger node, which creates clicks from f32!!!!!!!!!!!!!!!!!!!!

                        // ??????????????????????????????????????????????
                        let mut node: EncoderNode = node
                            .to_owned();
                        node
                            .control_bind( // control_bind_bool ???
                                connect
                                    .key,
                                resources
                                    .clone(),
                            );
                        map_sink_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                connect
                                    .node_id,
                                SinkNode::Encoder(
                                    node,
                                ),
                            );
                    },
                    // Some(SinkNode::Snapshot(node)) => {
                    //     node
                    //         .control_bind(
                    //             connect
                    //                 .key,
                    //             map_sink_to_node
                    //                 .clone(),
                    //             connect
                    //                 .node_id,
                    //             result,
                    //         );
                    // },
                    Some(_) => (),
                };
            },
            NodeFamily::Source => (),
        };
    };
}

fn update_bound_values(
    binds: Vec<BindValues>,
    map_control_to_node: Rc<RefCell<ControlToNode>>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    #[cfg(feature = "gstreamer")]
    resources: RenderResources,
    result: f32,
) {
    for
        connect
    in
        binds
    {
        match
            connect
                .node_family
        {
            NodeFamily::None => (),
            NodeFamily::Control => {
                // reload again????????????
                let list = map_control_to_node
                    .borrow()
                    .to_owned();

                match
                    list
                        .map
                        .get(
                            &connect
                                .node_id,
                        )
                {
                    None => (),
                    Some(ControlNode::None) => (),
                    Some(ControlNode::FunctionGenerator(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_control_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(ControlNode::Number(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_control_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(ControlNode::Trigger(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_control_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(ControlNode::Trigonometry(_)) => {
                        // node
                        //     .control_bind(
                        //         connect
                        //             .key,
                        //         map_control_to_node
                        //             .clone(),
                        //         connect
                        //             .node_id,
                        //         number_node
                        //             .values
                        //             .result,
                        //     );
                    },
                };
            },
            NodeFamily::Filter => {
                let list = map_filter_to_node
                    .borrow()
                    .to_owned();
                match
                    list
                        .map
                        .get(
                            &connect
                                .node_id,
                        )
                {
                    None => (),
                    Some(FilterNode::ColorMixer(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_filter_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(FilterNode::ColorRotator(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_filter_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(FilterNode::Fader(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_filter_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(FilterNode::Kaleidoscope(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_filter_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(FilterNode::Mandala(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_filter_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(FilterNode::Mixer(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                connect
                                    .location,
                                map_filter_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(_) => (),
                };
            },
            NodeFamily::Sink => {
                let list = map_sink_to_node
                    .borrow()
                    .map
                    .to_owned();
                match
                    list
                        .get(
                            &connect
                                .node_id,
                        )
                {
                    None => (),
                    #[cfg(feature = "gstreamer")]
                    Some(SinkNode::Encoder(node)) => {

                        // result is f32 not bool!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // need a separate structure!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // create a trigger node, which creates clicks from f32!!!!!!!!!!!!!!!!!!!!

                        // ??????????????????????????????????????????????
                        let mut node: EncoderNode = node
                            .to_owned();
                        node
                            .control_bind(
                                connect
                                    .key,
                                resources
                                    .clone(),
                            );
                        map_sink_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                connect
                                    .node_id,
                                SinkNode::Encoder(
                                    node,
                                ),
                            );
                    },
                    Some(SinkNode::Snapshot(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_sink_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(_) => (),
                };
            },
            NodeFamily::Source => {
                let list = map_source_to_node
                    .borrow()
                    .map
                    .to_owned();
                match
                    list
                        .get(
                            &connect
                                .node_id,
                        )
                {
                    None => (),
                    Some(SourceNode::Stack(node)) => {
                        node
                            .control_bind(
                                connect
                                    .key,
                                map_source_to_node
                                    .clone(),
                                connect
                                    .node_id,
                                result,
                            );
                    },
                    Some(_) => (),
                };
            },
        };
    };
}

// ----------------------------------------------------------------------------
// Functions - helpers:
// ----------------------------------------------------------------------------

fn get_next_control_id(
    map_control_to_node: Rc<RefCell<ControlToNode>>,
) -> String {
    let control_to_node: ControlToNode = map_control_to_node
        .borrow()
        .to_owned();

    let mut control_idx: usize = 1;
    loop {
        match
            control_idx
        {
            idx if
                !control_to_node
                    .map
                    .contains_key(
                        &format!("Control {idx}"),
                    )
            => break,
            idx if
                idx > 100
            => break,
            _ => control_idx += 1,
        }
    }
    format!("Control {control_idx}")
}
