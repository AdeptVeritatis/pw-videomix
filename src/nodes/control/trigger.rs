
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        // events::{
        //     // gui_events_node::NodeEvent,
        //     GuiEvent,
        // },
        gui::{
            bind::{
                elements::control_label:: {
                    ControlPrecision,
                    ControlValue,
                },
                midi_bind::MidiBind,
                BindValues,
            },
            // options::GuiOptions,
        },
    },
    constants::{
        // CONTROL_VALUE_DEFAULT,
        PANEL_NODE_SPACING_SMALL,
        TRIGGER_DEFAULT_FRAME_COUNT,
    },
    impls::midir::{
        MidiControllerClass,
        MidiMessage,
        MidiNode,
    },
    nodes::{
        control::{
            ControlInput,
            ControlInputClass,
            ControlNode,
        },
        NodeClass,
        NodeFamily,
    },
};
use egui_winit_vulkano::{
    egui::{
        Color32,
        ComboBox,
        Grid,
        RichText,
        Ui,
    },
    RenderResources,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    f32::consts::{
        PI,
        // TAU,
    },
    fmt,
    rc::Rc,
    // time::{
        // Duration,
        // Instant,
    // },
};
// use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum TriggerClass {
    EveryNthFrame,
    FlankDown,
    FlankUp,
    Value,
}

impl fmt::Display for TriggerClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::EveryNthFrame => write!(f, "Every Nth Frame"),
            Self::FlankDown => write!(f, "Flank down"),
            Self::FlankUp => write!(f, "Flank up"),
            Self::Value => write!(f, "Value"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TriggerValues {
    // // Position of the output source connector from the node.
    // // Gets updated during drawing.
    // pub pos_out: Pos2,
    // // pub sinks: Vec<FilterSink>,
    // // Source, the input the filter is connected to.
    // pub source: Option<InputConnection>,
    // // State: play, pause, stop
    // pub state: NodeState,

    pub value: f32,
    // pub frames: usize,
    pub precision: u32, // usize,
    pub previous: f32,
    pub flank_active: bool,
    pub frame_count: usize,
    pub input: ControlInput,
    pub class: TriggerClass,
    pub result: bool,
}

impl Default for TriggerValues {
    fn default() -> Self {
        Self {
            // pos_out: Pos2::default(),
            // source: None,
            // state: NodeState::Play,

            value: 1.0, // ???
            // frames: 1,
            precision: 3,
            previous: 0.0,
            flank_active: false,
            frame_count: TRIGGER_DEFAULT_FRAME_COUNT,
            class: TriggerClass::Value,
            input: ControlInput::default(),
            result: false,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TriggerNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,

    pub values: TriggerValues,
    pub connected: Vec<BindValues>,
}

// Constructors and helpers:
impl TriggerNode {
    pub fn new(
        control_id: String,
        _resources: RenderResources,
    ) -> Self {
        Self {
            class: NodeClass::Trigger,
            family: NodeFamily::Control,
            id: control_id
                .clone(),
            name: control_id
                .clone(),
            values: TriggerValues::default(),
            connected: Vec::new(),
        }
    }

    /// Draw a TriggerNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        // _event_loop_proxy: EventLoopProxy<GuiEvent>,
        // _map_control_to_node: Rc<RefCell<ControlToNode>>,
        // _options_gui: Rc<RefCell<GuiOptions>>,
        // _ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
        let factor: f32 = match
            self
                .values
                .precision
        {
            0 => 1.0,
            1 => 10.0,
            2 => 100.0,
            3 => 1000.0,
            _ => 10000.0,
        };

        ui_inner
            .label(
                self
                    .name
                    .clone(),
            );
        ui_inner
            .separator();
        ui_inner
            .small(
                match
                    self
                        .values
                        .input
                        .class
                {
                    ControlInputClass::Input => match
                        self
                            .values
                            .input
                            .value
                            .to_owned()
                    {
                        None => String::from("∅"),
                        Some(value) => {
                            ( ( value * factor ).round() / factor )
                                .to_string()
                        },
                    },
                    ControlInputClass::One => String::from("1"),
                    ControlInputClass::Pi => String::from("π"),
                    ControlInputClass::Zero => String::from("0"),
                }
            );
        ui_inner
            .small(
                match
                    self
                        .values
                        .class
                {
                    TriggerClass::EveryNthFrame => String::from("frequency"),
                    TriggerClass::FlankDown => String::from("flank down"),
                    TriggerClass::FlankUp => String::from("flank up"),
                    TriggerClass::Value => String::from("value"),
                }
            );

        ui_inner
            .small(
                match
                    self
                        .values
                        .class
                {
                    TriggerClass::EveryNthFrame => match
                        self
                            .values
                            // .frames,
                            .input
                            .value
                    {
                        None => String::from("no frame"),
                        Some(input) => match
                            input
                                .floor()
                                .abs()
                        {
                            0.0 => String::from("no frame"),
                            1.0 => String::from("every frame"),
                            2.0 => String::from("2nd frame"),
                            3.0 => String::from("3rd frame"),
                            num => {
                                format!(
                                    "{num}th frame",
                                )
                            },
                        },
                    },
                    TriggerClass::FlankDown => String::from("\\_"),
                    TriggerClass::FlankUp => String::from("_/"),
                    TriggerClass::Value => {
                        (
                            (
                                self
                                    .values
                                    .value
                                *
                                factor
                            )
                                .floor()
                                /
                                factor
                        )
                            .to_string()
                    },
                },
            );
        ui_inner
            .label(
                self
                    .values
                    .result
                    .to_string()
            );
    }

    /// The side panel of a TriggerNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    // ) -> Self {
    ) {
        let mut values: TriggerValues = self
            .values
            .to_owned();

    // Options:
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ui
            .label(
                "Input:",
            );
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ui
            .label(
                RichText::new(
                    "Input sets threshold.",
                )
                .color(
                    Color32::RED,
                ),
            );
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ComboBox::from_id_salt(
            format!(
                "ComboBox {} input",
                self
                    .id,
            )
        )
            .selected_text(
                values
                    .input
                    .class
                    .to_string(),
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    for
                        input
                    in
                        [
                            ControlInputClass::Input,
                            ControlInputClass::Zero,
                            ControlInputClass::One,
                            ControlInputClass::Pi,
                        ]
                    {
                        ui_combo
                            .selectable_value(
                                &mut values
                                    .input
                                    .class,
                                input
                                    .clone(),
                                input
                                    .to_string(),
                            );
                    }
                }
            );
        match
            values
                .input
                .class
        {
            ControlInputClass::Input => {

                ui
                    .add_space(
                        PANEL_NODE_SPACING_SMALL,
                    );
                ui
                    .label(
                        "Select Input:"
                    );
                ui
                    .add_space(
                        PANEL_NODE_SPACING_SMALL,
                    );
                ComboBox::from_id_salt(
                    format!(
                        "ComboBox {} select input",
                        self
                            .id,
                    )
                )
                    .selected_text(
                        match
                            values
                                .input
                                .input
                                .clone()
                        {
                            None => String::from("none"),
                            Some(input) => input,
                        }
                    )
                    .wrap()
                    .show_ui(
                        ui,
                        |ui_combo| {
                            let mut list: Vec<String> = Vec::new();
                            list
                                .push(
                                    String::from(
                                        "none",
                                    ),
                                );
                            for
                                node
                            in
                                map_control_to_node
                                    .borrow()
                                    .map
                                    .keys()
                            {
                                // check for feedback loops?
                                list
                                    .push(
                                        node
                                            .to_owned(),
                                    );
                            };

                            for
                                input
                            in
                                list
                            {
                                ui_combo
                                    .selectable_value(
                                        &mut values
                                            .input
                                            .input,
                                        Some(
                                            input
                                                .to_owned(),
                                        ),
                                        input,
                                    );
                            };
                        },
                    );
            },
            _ => (),
        };

        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label("Trigger Type:");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ComboBox::from_id_salt(
            format!(
                "ComboBox {} triggertype",
                self
                    .id,
            )
        )
            .selected_text(
                values
                    .class
                    .to_string(),
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    for
                        trigger_class
                    in
                        [
                            TriggerClass::EveryNthFrame,
                            TriggerClass::FlankDown,
                            TriggerClass::FlankUp,
                            TriggerClass::Value,
                        ]
                    {
                        ui_combo
                            .selectable_value(
                                &mut values
                                    .class,
                                trigger_class
                                    .clone(),
                                trigger_class
                                    .to_string(),
                            );
                    }
                });
        ui.add_space(PANEL_NODE_SPACING_SMALL);

        match
            self
                .values
                .class
        {
            TriggerClass::Value
            => {
                ui.separator();
                ui.add_space(PANEL_NODE_SPACING_SMALL);
                ui
                    .label(
                        RichText::new(
                            "Value sets treshold.",
                        )
                        .color(
                            Color32::RED,
                        ),
                    );
                ui
                    .add_space(
                        PANEL_NODE_SPACING_SMALL,
                    );

                Grid::new(
                    "Header",
                )
                    .show(
                        ui,
                        |ui_grid| {
                            values.value = ControlValue::show(
                                values
                                    .value,
                                map_control_to_node
                                    .clone(),
                                midi_node_rc
                                    .clone(),
                                self
                                    .id
                                    .clone(),
                                ui_grid,
                            );
                            ui_grid
                                .end_row();
                            values.precision = ControlPrecision::show(
                                values
                                    .precision,
                                map_control_to_node
                                    .clone(),
                                midi_node_rc
                                    .clone(),
                                self
                                    .id
                                    .clone(),
                                ui_grid,
                            );
                        },
                    );
                ui
                    .add_space(
                        PANEL_NODE_SPACING_SMALL,
                    );
            },
            _ => (),
        };

        self.values = values;
    }

    pub fn control_bind(
        &self,
        bind: String,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "value" => {
                // update here too ???
                // node
                //     .update(
                //         map_control_to_node
                //             .clone(),
                //         resources
                //             .clone(),
                //     );
                node.values.value = result;
                map_control_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        ControlNode::Trigger(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> ControlNode {
        let mut values: TriggerValues = self
            .values
            .to_owned();

        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "precision" => {
                        values.precision = ControlPrecision::change_midi(
                            values
                                .precision,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "value" => {
                        values.value = ControlValue::change_midi(
                            values
                                .value,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        ControlNode::Trigger(
            Self {
                values,
                ..self
                    .to_owned()
            },
        )
    }
}
// Helpers:
impl TriggerNode {
    pub fn update(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        _resources: RenderResources,
    ) {
        // Reset input value.
        self.values.input.value = None;

        // Get input value, class and calculate output
        let num_in: f32 = match
            self
                .values
                .input
                .class
        {
            ControlInputClass::Input => {
                match
                    self
                        .values
                        .input
                        .input
                        .clone()
                {
                    None => (),
                    Some(input) => match
                        map_control_to_node
                            .borrow()
                            .map
                            .get(
                                &input,
                            )
                    {
                        None => (),
                        Some(control_node) => match
                            control_node
                        {
                            ControlNode::None => (),
                            ControlNode::Trigger(_) => (),
                            ControlNode::FunctionGenerator(function_generator_node) => {
                                self.values.input.value = Some(
                                    function_generator_node
                                        .values
                                        .result,
                                );
                            },
                            ControlNode::Number(number_node) => {
                                self.values.input.value = Some(
                                    number_node
                                        .values
                                        .result,
                                );
                            },
                            ControlNode::Trigonometry(trigonometry_node) => {
                                self.values.input.value = Some(
                                    trigonometry_node
                                        .values
                                        .result,
                                );
                            },
                        },
                    },
                };
                match
                    self
                        .values
                        .input
                        .value
                        .to_owned()
                {
                    None => 0.0,
                    Some(value) => value,
                }
            },
            ControlInputClass::One => 1.0,
            ControlInputClass::Pi => PI,
            ControlInputClass::Zero => 0.0,
        };

        let factor: f32 = match
            self
                .values
                .precision
        {
            0 => 1.0,
            1 => 10.0,
            2 => 100.0,
            3 => 1000.0,
            _ => 10000.0,
        };

        self.values.result = match
            self
                .values
                .class
        {
            TriggerClass::EveryNthFrame => match
                self
                    .values
                    .input
                    .value
            {
                None => false,
                Some(input) => match
                    input
                        .floor()
                        .abs()
                    as
                        usize
                {
                    0 => false,
                    value => {
                        match
                            self
                                .values
                                .frame_count
                        {
                            count if
                                count
                                >=
                                value
                            => {
                                self.values.frame_count = TRIGGER_DEFAULT_FRAME_COUNT;
                                true
                            },
                            _ => {
                                self.values.frame_count += 1;
                                false
                            },
                        }
                    },
                },
            },
            TriggerClass::FlankDown => match
                num_in
            {
                num_in if
                    num_in
                    <
                    self
                        .values
                        .previous
                => match
                    self
                        .values
                        .flank_active
                {
                    true => false,
                    false => {
                        self.values.flank_active = true;
                        true
                    },
                },
                _ => {
                    if
                        self
                            .values
                            .flank_active
                    {
                        self.values.flank_active = false;
                    };
                    false
                },
            },
            TriggerClass::FlankUp => match
                num_in
            {
                num_in if
                    num_in
                    >
                    self
                        .values
                        .previous
                => match
                    self
                        .values
                        .flank_active
                {
                    true => false,
                    false => {
                        self.values.flank_active = true;
                        true
                    },
                },
                _ => {
                    if
                        self
                            .values
                            .flank_active
                    {
                        self.values.flank_active = false;
                    };
                    false
                },
            },
            TriggerClass::Value => {
                (
                    num_in
                    *
                    factor
                )
                    .round()
                ==
                (
                    self
                        .values
                        .value
                    *
                    factor
                )
                    .floor()
            },
        };

        self.values.previous = num_in;
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
