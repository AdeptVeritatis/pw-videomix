
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        // events::{
        //     // gui_events_node::NodeEvent,
        //     GuiEvent,
        // },
        gui::{
            bind::BindValues,
            // options::GuiOptions,
        },
    },
    constants::{
        CONTROL_VALUE_DEFAULT,
        PANEL_NODE_SPACING_SMALL,
    },
    // impls::midir::{
    //     MidiControllerClass,
    //     MidiMessage,
    //     MidiNode,
    // },
    nodes::{
        control::{
            ControlInput,
            ControlInputClass,
            ControlNode,
        },
        NodeClass,
        NodeFamily,
    },
};
use egui_winit_vulkano::{
    egui::{
        ComboBox,
        // Grid,
        Ui,
    },
    RenderResources,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    f32::consts::PI,
    fmt,
    rc::Rc,
};
// use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum TrigonometryClass {
    Cosine,
    Sine,
}

impl fmt::Display for TrigonometryClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Cosine => write!(f, "Cosine"),
            Self::Sine => write!(f, "Sine"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TrigonometryValues {
    // // Position of the output source connector from the node.
    // // Gets updated during drawing.
    // pub pos_out: Pos2,
    // // pub sinks: Vec<FilterSink>,
    // // Source, the input the filter is connected to.
    // pub source: Option<InputConnection>,
    // // State: play, pause, stop
    // pub state: NodeState,

    // pub value: f32,
    pub input: ControlInput,
    pub trigonometry: TrigonometryClass,
    pub result: f32, // better f64 ???
}

impl Default for TrigonometryValues {
    fn default() -> Self {
        Self {
            // pos_out: Pos2::default(),
            // source: None,
            // state: NodeState::Play,

            // value: CONTROL_VALUE_DEFAULT,
            trigonometry: TrigonometryClass::Cosine,
            input: ControlInput::default(),
            result: CONTROL_VALUE_DEFAULT,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TrigonometryNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,

    pub values: TrigonometryValues,
    pub connected: Vec<BindValues>,
}

// Constructors and helpers:
impl TrigonometryNode {
    pub fn new(
        control_id: String,
        _resources: RenderResources,
    ) -> Self {
        Self {
            class: NodeClass::Trigonometry,
            family: NodeFamily::Control,
            id: control_id
                .clone(),
            name: control_id
                .clone(),
            values: TrigonometryValues::default(),
            connected: Vec::new(),
        }
    }

    /// Draw a NumberNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        // _event_loop_proxy: EventLoopProxy<GuiEvent>,
        // _map_control_to_node: Rc<RefCell<ControlToNode>>,
        // _options_gui: Rc<RefCell<GuiOptions>>,
        // _ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
        ui_inner
            .label(
                self
                    .name
                    .clone(),
            );
        ui_inner
            .separator();
        ui_inner
            .small(
                format!(
                    "{}  {}  {}",
                    match
                        self
                            .values
                            .trigonometry
                    {
                        TrigonometryClass::Cosine => "cos  (",
                        TrigonometryClass::Sine => "sin  (",
                    },
                    match
                        self
                            .values
                            .input
                            .class
                    {
                        ControlInputClass::Input => match
                            self
                                .values
                                .input
                                .value
                                .to_owned()
                        {
                            None => String::from("∅"),
                            Some(value) => {
                                ( ( value * 100.0 ).round() / 100.0 )
                                    .to_string()
                            },
                        },
                        ControlInputClass::One => String::from("1"),
                        ControlInputClass::Pi => String::from("π"),
                        ControlInputClass::Zero => String::from("0"),
                    },
                    ")"
                ),
            );
        ui_inner
            .label(
                ( ( self.values.result * 1000.0 ).round() / 1000.0 )
                    .to_string()
            );
    }

    /// The side panel of a TrigonometryNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        // midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
        let mut values: TrigonometryValues = self
            .values
            .to_owned();

    // Options:
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ui
            .label(
                "Input:",
            );
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ComboBox::from_id_salt(
            format!(
                "ComboBox {} input",
                self
                    .id,
            )
        )
            .selected_text(
                values
                    .input
                    .class
                    .to_string(),
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    for
                        input
                    in
                        [
                            ControlInputClass::Input,
                            ControlInputClass::Zero,
                            ControlInputClass::One,
                            ControlInputClass::Pi,
                        ]
                    {
                        ui_combo
                            .selectable_value(
                                &mut values
                                    .input
                                    .class,
                                input
                                    .clone(),
                                input
                                    .to_string(),
                            );
                    };
                },
            );
        match
            values
                .input
                .class
        {
            ControlInputClass::Input => {

                ui
                    .add_space(
                        PANEL_NODE_SPACING_SMALL,
                    );
                ui
                    .label(
                        "Select Input:"
                    );
                ui
                    .add_space(
                        PANEL_NODE_SPACING_SMALL,
                    );
                ComboBox::from_id_salt(
                    format!(
                        "ComboBox {} select input",
                        self
                            .id,
                    )
                )
                    .selected_text(
                        match
                            values
                                .input
                                .input
                                .clone()
                        {
                            None => String::from("none"),
                            Some(input) => input,
                        }
                    )
                    .wrap()
                    .show_ui(
                        ui,
                        |ui_combo| {
                            let mut list: Vec<String> = Vec::new();
                            list
                                .push(
                                    String::from(
                                        "none",
                                    ),
                                );
                            for
                                node
                            in
                                map_control_to_node
                                    .borrow()
                                    .map
                                    .keys()
                            {
                                // check for feedback loops?
                                list
                                    .push(
                                        node
                                            .to_owned(),
                                    );
                            };

                            for
                                input
                            in
                                list
                            {
                                ui_combo
                                    .selectable_value(
                                        &mut values
                                            .input
                                            .input,
                                        Some(
                                            input
                                                .to_owned(),
                                        ),
                                        input,
                                    );
                            };
                        },
                    );
            },
            _ => (),
        };

        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ui
            .separator();
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ui
            .label(
                "Calculation:",
            );
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ComboBox::from_id_salt(
            format!(
                "ComboBox {} trigonometry",
                self
                    .id,
            )
        )
            .selected_text(
                values
                    .trigonometry
                    .to_string(),
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    for
                        trigonometry
                    in
                        [
                            TrigonometryClass::Cosine,
                            TrigonometryClass::Sine,
                        ]
                    {
                        ui_combo
                            .selectable_value(
                                &mut values
                                    .trigonometry,
                                trigonometry
                                    .clone(),
                                trigonometry
                                    .to_string(),
                            );
                    }
                });
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );

        self.values = values;
    }

    // pub fn control_bind(
    //     &self,
    //     bind: String,
    //     map_control_to_node: Rc<RefCell<ControlToNode>>,
    //     node_id: String,
    //     result: f32,
    // ) {
    //     let mut node = self
    //         .to_owned();
    //     match
    //         bind
    //             .as_str()
    //     {
    //         "value" => {
    //             // update here too ???
    //             // node
    //             //     .update(
    //             //         map_control_to_node
    //             //             .clone(),
    //             //         resources
    //             //             .clone(),
    //             //     );
    //             node.values.value = result;
    //             map_control_to_node
    //                 .borrow_mut()
    //                 .insert(
    //                     node_id,
    //                     ControlNode::Trigonometry(
    //                         node,
    //                     ),
    //                 );
    //         },
    //         bind => println!("!! error: unknown binding {bind}"),
    //     };
    // }

    // pub fn received_midi(
    //     &self,
    //     midi_bind: &MidiBind,
    //     midi_event: MidiMessage,
    //     speed: f32,
    // ) -> ControlNode {
    //     let mut values: TrigonometryValues = self
    //         .values
    //         .to_owned();
    //
    //     let controller_class: MidiControllerClass = midi_bind
    //         .controller_class
    //         .clone();
    //     let location: Vec<String> = midi_bind
    //         .label_bind
    //         .location
    //         .clone();
    //     match
    //         location[0]
    //             .as_str()
    //     {
    //         "values" => {
    //             match
    //                 midi_bind
    //                     .label_bind
    //                     .key
    //                     .as_str()
    //             {
    //                 "value" => {
    //                     values.value = ControlValue::change_midi(
    //                         values
    //                             .value,
    //                         controller_class,
    //                         midi_event,
    //                         speed,
    //                     );
    //                 },
    //                 _ => (),
    //             };
    //         },
    //         _ => (),
    //     };
    //     ControlNode::Trigonometry(
    //         Self {
    //             values,
    //             ..self
    //                 .to_owned()
    //         },
    //     )
    // }
}
// Helpers:
impl TrigonometryNode {
    pub fn update(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        _resources: RenderResources,
    ) {
        // Reset input value.
        self.values.input.value = None;

        // Get input value, class and calculate output
        let num_in: f32 = match
            self
                .values
                .input
                .class
        {
            ControlInputClass::Input => {
                match
                    self
                        .values
                        .input
                        .input
                        .clone()
                {
                    None => (),
                    Some(input) => match
                        map_control_to_node
                            .borrow()
                            .map
                            .get(
                                &input,
                            )
                    {
                        None => (),
                        Some(control_node) => match
                            control_node
                        {
                            ControlNode::None => (),
                            ControlNode::Trigger(_) => (),
                            ControlNode::FunctionGenerator(function_generator_node) => {
                                self.values.input.value = Some(
                                    function_generator_node
                                        .values
                                        .result,
                                );
                            },
                            ControlNode::Number(number_node) => {
                                self.values.input.value = Some(
                                    number_node
                                        .values
                                        .result,
                                );
                            },
                            ControlNode::Trigonometry(trigonometry_node) => {
                                self.values.input.value = Some(
                                    trigonometry_node
                                        .values
                                        .result,
                                );
                            },
                        },
                    },
                };
                match
                    self
                        .values
                        .input
                        .value
                        .to_owned()
                {
                    None => 0.0,
                    Some(value) => value,
                }
            },
            ControlInputClass::One => 1.0,
            ControlInputClass::Pi => PI,
            ControlInputClass::Zero => 0.0,
        };
        self.values.result = match
            self
                .values
                .trigonometry
        {
            TrigonometryClass::Cosine => num_in.cos(),
            TrigonometryClass::Sine => num_in.sin(),
        };
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
