
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        // events::{
        //     // gui_events_node::NodeEvent,
        //     GuiEvent,
        // },
        gui::{
            bind::{
                elements::control_label::ControlValue,
                midi_bind::MidiBind,
                BindValues,
            },
            // options::GuiOptions,
        },
    },
    constants::{
        CONTROL_VALUE_DEFAULT,
        PANEL_NODE_SPACING_SMALL,
    },
    impls::midir::{
        MidiControllerClass,
        MidiMessage,
        MidiNode,
    },
    nodes::{
        control::{
            ControlInput,
            ControlInputClass,
            ControlNode,
        },
        NodeClass,
        NodeFamily,
    },
};
use egui_winit_vulkano::{
    egui::{
        ComboBox,
        Grid,
        Ui,
    },
    RenderResources,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    f32::consts::PI,
    fmt,
    rc::Rc,
};
// use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum CalculationClass {
    Add,
    Divide,
    Multiply,
    Subtract,
}

impl fmt::Display for CalculationClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Add => write!(f, "add"),
            Self::Divide => write!(f, "divide"),
            Self::Multiply => write!(f, "multiply"),
            Self::Subtract => write!(f, "subtract"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct NumberValues {
    // // Position of the output source connector from the node.
    // // Gets updated during drawing.
    // pub pos_out: Pos2,
    // // pub sinks: Vec<FilterSink>,
    // // Source, the input the filter is connected to.
    // pub source: Option<InputConnection>,
    // // State: play, pause, stop
    // pub state: NodeState,

    pub value: f32,
    pub input: ControlInput,
    pub calculation: CalculationClass,
    pub result: f32, // better f64 ???
}

impl Default for NumberValues {
    fn default() -> Self {
        Self {
            // pos_out: Pos2::default(),
            // source: None,
            // state: NodeState::Play,

            value: CONTROL_VALUE_DEFAULT,
            calculation: CalculationClass::Add,
            input: ControlInput::default(),
            result: CONTROL_VALUE_DEFAULT,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct NumberNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,

    pub values: NumberValues,
    pub connected: Vec<BindValues>,
}

// Constructors and helpers:
impl NumberNode {
    pub fn new(
        control_id: String,
        _resources: RenderResources,
    ) -> Self {
        Self {
            class: NodeClass::Number,
            family: NodeFamily::Control,
            id: control_id
                .clone(),
            name: control_id
                .clone(),
            values: NumberValues::default(),
            connected: Vec::new(),
        }
    }

    /// Draw a NumberNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        // _event_loop_proxy: EventLoopProxy<GuiEvent>,
        // _map_control_to_node: Rc<RefCell<ControlToNode>>,
        // _options_gui: Rc<RefCell<GuiOptions>>,
        // _ui_area: &mut Ui,
        ui_inner: &mut Ui,
    ) {
        ui_inner
            .label(
                self
                    .name
                    .clone(),
            );
        ui_inner
            .separator();
        ui_inner
            .small(
                format!(
                    "{}  {}  {}",
                    match
                        self
                            .values
                            .input
                            .class
                    {
                        ControlInputClass::Input => match
                            self
                                .values
                                .input
                                .value
                                .to_owned()
                        {
                            None => String::from("∅"),
                            Some(value) => {
                                ( ( value * 100.0 ).round() / 100.0 )
                                    .to_string()
                            },
                        },
                        ControlInputClass::One => String::from("1"),
                        ControlInputClass::Pi => String::from("π"),
                        ControlInputClass::Zero => String::from("0"),
                    },
                    match
                        self
                            .values
                            .calculation
                    {
                        CalculationClass::Add => "+",
                        CalculationClass::Divide => "/",
                        CalculationClass::Multiply => "*",
                        CalculationClass::Subtract => "-",
                    },
                    ( self.values.value * 100.0 ).round() / 100.0,
                ),
            );
        ui_inner
            .label(
                ( ( self.values.result * 1000.0 ).round() / 1000.0 )
                    .to_string()
            );
    }

    /// The side panel of a NumberNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    // ) -> Self {
    ) {
        let mut values: NumberValues = self
            .values
            .to_owned();

    // Options:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label("Input:");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ComboBox::from_id_salt(
            format!(
                "ComboBox {} input",
                self
                    .id,
            )
        )
            .selected_text(
                values
                    .input
                    .class
                    .to_string(),
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    for
                        input
                    in
                        [
                            ControlInputClass::Input,
                            ControlInputClass::Zero,
                            ControlInputClass::One,
                            ControlInputClass::Pi,
                        ]
                    {
                        ui_combo
                            .selectable_value(
                                &mut values
                                    .input
                                    .class,
                                input
                                    .clone(),
                                input
                                    .to_string(),
                            );
                    }
                }
            );
        match
            values
                .input
                .class
        {
            ControlInputClass::Input => {

                ui
                    .add_space(
                        PANEL_NODE_SPACING_SMALL,
                    );
                ui
                    .label(
                        "Select Input:"
                    );
                ui
                    .add_space(
                        PANEL_NODE_SPACING_SMALL,
                    );
                ComboBox::from_id_salt(
                    format!(
                        "ComboBox {} select input",
                        self
                            .id,
                    )
                )
                    .selected_text(
                        match
                            values
                                .input
                                .input
                                .clone()
                        {
                            None => String::from("none"),
                            Some(input) => input,
                        }
                    )
                    .wrap()
                    .show_ui(
                        ui,
                        |ui_combo| {
                            let mut list: Vec<String> = Vec::new();
                            list
                                .push(
                                    String::from(
                                        "none",
                                    ),
                                );
                            for
                                node
                            in
                                map_control_to_node
                                    .borrow()
                                    .map
                                    .keys()
                            {
                                // check for feedback loops?
                                list
                                    .push(
                                        node
                                            .to_owned(),
                                    );
                            };

                            for
                                input
                            in
                                list
                            {
                                ui_combo
                                    .selectable_value(
                                        &mut values
                                            .input
                                            .input,
                                        Some(
                                            input
                                                .to_owned(),
                                        ),
                                        input,
                                    );
                            };
                        },
                    );
            },
            _ => (),
        };

        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label("Calculation:");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ComboBox::from_id_salt(
            format!(
                "ComboBox {} calculation",
                self
                    .id,
            )
        )
            .selected_text(
                values
                    .calculation
                    .to_string(),
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    for
                        calculation
                    in
                        [
                            CalculationClass::Add,
                            CalculationClass::Divide,
                            CalculationClass::Multiply,
                            CalculationClass::Subtract,
                        ]
                    {
                        ui_combo
                            .selectable_value(
                                &mut values
                                    .calculation,
                                calculation
                                    .clone(),
                                calculation
                                    .to_string(),
                            );
                    }
                });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        Grid::new(
            "Header",
        )
            .show(
                ui,
                |ui_grid| {
                    values.value = ControlValue::show(
                        values
                            .value,
                        map_control_to_node
                            .clone(),
                        midi_node_rc
                            .clone(),
                        self
                            .id
                            .clone(),
                        ui_grid,
                    );
                    ui_grid
                        .end_row();
                },
            );
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );

        self.values = values;
    }

    pub fn control_bind(
        &self,
        bind: String,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "value" => {
                // update here too ???
                // node
                //     .update(
                //         map_control_to_node
                //             .clone(),
                //         resources
                //             .clone(),
                //     );
                node.values.value = result;
                map_control_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        ControlNode::Number(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> ControlNode {
        let mut values: NumberValues = self
            .values
            .to_owned();

        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "value" => {
                        values.value = ControlValue::change_midi(
                            values
                                .value,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        ControlNode::Number(
            Self {
                values,
                ..self
                    .to_owned()
            }
        )
    }
}
// Helpers:
impl NumberNode {
    pub fn update(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        _resources: RenderResources,
    ) {
        // Reset input value.
        self.values.input.value = None;

        // Get input value, class and calculate output
        let num_in: f32 = match
            self
                .values
                .input
                .class
        {
            ControlInputClass::Input => {
                match
                    self
                        .values
                        .input
                        .input
                        .clone()
                {
                    None => (),
                    Some(input) => match
                        map_control_to_node
                            .borrow()
                            .map
                            .get(
                                &input,
                            )
                    {
                        None => (),
                        Some(control_node) => match
                            control_node
                        {
                            ControlNode::None => (),
                            ControlNode::Trigger(_) => (),
                            ControlNode::FunctionGenerator(function_generator_node) => {
                                self.values.input.value = Some(
                                    function_generator_node
                                        .values
                                        .result,
                                );
                            },
                            ControlNode::Number(number_node) => {
                                self.values.input.value = Some(
                                    number_node
                                        .values
                                        .result,
                                );
                            },
                            ControlNode::Trigonometry(trigonometry_node) => {
                                self.values.input.value = Some(
                                    trigonometry_node
                                        .values
                                        .result,
                                );
                            },
                        },
                    },
                };
                match
                    self
                        .values
                        .input
                        .value
                        .to_owned()
                {
                    None => 0.0,
                    Some(value) => value,
                }
            },
            ControlInputClass::One => 1.0,
            ControlInputClass::Pi => PI,
            ControlInputClass::Zero => 0.0,
        };
        self.values.result = match
            self
                .values
                .calculation
        {
            CalculationClass::Add => num_in + self.values.value,
            CalculationClass::Divide => {
                match
                    self
                        .values
                        .value
                {
                    value if
                        value
                        ==
                        0.0
                    => 0.0,
                    _
                    => num_in / self.values.value,
                }
            },
            CalculationClass::Multiply => num_in * self.values.value,
            CalculationClass::Subtract => num_in - self.values.value,
        };
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
