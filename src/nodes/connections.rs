
use crate::{
    app::data::app_nodes::{
        FilterToNode,
        SourceToNode,
    },
    constants::{
        CONNECTOR_COLOR,
        CONNECTOR_ROUNDING,
        CONNECTOR_SIZE,
        CONNECTOR_STIFFNESS,
        CONNECTOR_WIDTH,
    },
    impls::{
        // serde::definitions::{
        //     Pos2Def,
        //     Vec2Def,
        // },
        vulkano::scenes::copy::copy_connection_scene::ConnectionScene,
    },
    nodes::{
        filter::FilterNode,
        sources::SourceNode,
        NodeClass,
        NodeFamily,
        NodeValues,
    },
};
use egui_winit_vulkano::{
    egui::{
        epaint::CubicBezierShape,
        Color32,
        Pos2,
        Rect,
        Stroke,
        Ui,
        Vec2,
    },
    Gui,
};
use serde::Serialize;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
    sync::Arc,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    image::view::ImageView,
};
use winit::window::WindowId;

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct OutputConnection {
    pub id: String,
    pub name: String,
    pub pos: Pos2,
    pub source_id: String,
}

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct InputConnectionConstructor {
    pub node_class:
        NodeClass,
    pub node_family:
        NodeFamily,
    pub node_id:
        String,
    pub node_name:
        String,
    pub node_values:
        NodeValues,
}

impl
    Default
for
    InputConnectionConstructor
{
    fn
        default
    () ->
        Self
    {
        Self {
            node_class: NodeClass::None,
            node_family: NodeFamily::None,
            node_id: String::new(),
            node_name: String::new(),
            node_values: NodeValues::default(),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Debug, Clone)]
pub struct InputConnection {
    #[serde(skip_serializing)]
    pub scene: ConnectionScene,
    #[serde(skip_serializing)]
    pub update_wanted: bool,

    pub source_class: NodeClass,
    pub source_family: NodeFamily,
    pub source_id: String,
    pub source_name: String,
    // #[serde(with = "Vec2Def")]
    #[serde(skip_serializing)]
    pub source_extent: Vec2, // updated to create new input image!!!
    #[serde(skip_serializing)]
    pub source_pos: Option<Pos2>,

    pub sink_class: NodeClass, // where updated???
    pub sink_family: NodeFamily, // where updated???
    pub sink_id: String,
    pub sink_name: String,
    // #[serde(with = "Vec2Def")]
    #[serde(skip_serializing)]
    pub sink_extent: Vec2, // where updated???
    // #[serde(with = "Pos2Def")]
    #[serde(skip_serializing)]
    pub sink_pos: Pos2,
}

// Constructors and draw:
impl InputConnection {
    pub fn new(
        constructor: InputConnectionConstructor,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
    ) -> Self {
        let image_source: Arc<ImageView> = match
            constructor
                .node_values
                .source_family
        {
            NodeFamily::None => panic!("!! not a source node"),
            NodeFamily::Control => panic!("!! not a source node"),
            NodeFamily::Sink => panic!("!! not a source node"),
            NodeFamily::Filter => {
                match
                    map_filter_to_node
                        .borrow()
                        .map
                        .get(
                            &constructor
                                .node_values
                                .source_id,
                        )
                {
                    None => panic!("!! not a filter node"),
                    Some(filter_node) => {
                        match
                            filter_node
                                .clone()
                        {
                            FilterNode::None => panic!("!! not a valid filter node"),
                            FilterNode::ColorMixer(color_mixer_node) => {
                                color_mixer_node.scene.image_storage
                            },
                            FilterNode::ColorRotator(color_rotator_node) => {
                                color_rotator_node.scene.image_storage
                            },
                            FilterNode::Fader(fader_node) => {
                                fader_node.scene.image_storage
                            },
                            FilterNode::Kaleidoscope(kaleidoscope_node) => {
                                kaleidoscope_node.scene.image_storage
                            },
                            FilterNode::Mandala(mandala_node) => {
                                mandala_node.scene.image_storage
                            },
                            FilterNode::Mixer(mixer_node) => {
                                mixer_node.scene.image_storage
                            },
                        }
                    },
                }
            },
            NodeFamily::Source => {
                match
                    map_source_to_node
                        .borrow()
                        .map
                        .get(
                            &constructor
                                .node_values
                                .source_id,
                        )
                {
                    None => panic!("!! not a source node"),
                    Some(source_node) => {
                        match
                            source_node
                                .clone()
                        {
                            SourceNode::None => panic!("!! not a valid source node"),
                            SourceNode::Picture(picture_node) => {
                                picture_node.scene.image_storage
                            },
                            #[cfg(unix)]
                            SourceNode::PipewireInput(pw_input_node) => {
                                pw_input_node.scene.image_storage
                            },
                            // #[cfg(feature = "gstreamer")]
                            // SourceNode::PwInputGst(pw_input_gst_node) => {
                            //     pw_input_gst_node.scene.unwrap().image_storage
                            // },
                            SourceNode::Stack(stack_node) => {
                                stack_node.scene.image_storage
                            },
                            #[cfg(feature = "gstreamer")]
                            SourceNode::Video(video_node) => {
                                video_node.scene.unwrap().image_storage
                            },
                        }
                    },
                }
            },
        };
        println!(
            "<> connect \"{}\" to \"{}\"",
            constructor
                .node_name,
            constructor
                .node_values
                .source_id,
        );


        let source_extent: Vec2 = Vec2::new(image_source.image().extent()[0] as f32, image_source.image().extent()[1] as f32);
        let scene = ConnectionScene {
            image_sink: None,
            image_source,
        };
        Self {
            scene,
            // Always update it for the first time.
            update_wanted: true,

            source_class: constructor.node_values.source_class,
            source_family: constructor.node_values.source_family,
            source_id: constructor.node_values.source_id,
            source_name: constructor.node_values.source_name,
            source_extent, // updated to create new input image!!!
            source_pos: Some(Pos2::default()),

            sink_class: constructor.node_class,
            sink_family: constructor.node_family,
            sink_id: constructor.node_id.clone(),
            sink_name: constructor.node_id,
            sink_extent: Vec2::default(), // never updated???????????
            sink_pos: Pos2::default(),
        }
    }

    /// Draws a connection between two nodes on the main panel.
    /// Includes the Bezier curve and two port connectors.
    pub fn draw(
        &self,
        ui_area: &mut Ui,
    ) {
        // Draw bezier line:
        let source_x: f32 = self.source_pos.unwrap().x + 6.0 + CONNECTOR_SIZE * 0.5;
        let source_y: f32 = self.source_pos.unwrap().y - 5.0;
        let sink_x: f32 = self.sink_pos.x - 6.0 - CONNECTOR_SIZE * 0.5;
        let sink_y: f32 = self.sink_pos.y + 8.0;
        let connector_stiffness: f32 =
            match Pos2::distance(Pos2::new(source_x, source_y), Pos2::new(sink_x, sink_y)) {
                dist if dist < 2.0 * CONNECTOR_STIFFNESS => dist / 2.0,
                _ => CONNECTOR_STIFFNESS,
            };
        let shape = CubicBezierShape::from_points_stroke(
            [
                Pos2::new(source_x, source_y),
                // Rectangle from exchanged coordinates:
                // Pos2::new(sink_x, source_y),
                // Pos2::new(source_x, sink_y),
                // Meeting halfway:
                // Pos2::new((source_x + sink_x) * 0.5, source_y),
                // Pos2::new((source_x + sink_x) * 0.5, sink_y),
                // With stiff/rigid ends:
                Pos2::new(source_x + connector_stiffness, source_y),
                Pos2::new(sink_x - connector_stiffness, sink_y),
                Pos2::new(sink_x, sink_y),
            ],
            false,
            Color32::TRANSPARENT,
            Stroke::new(CONNECTOR_WIDTH, CONNECTOR_COLOR),
        );
        ui_area.painter().rect_filled(
            Rect::from_center_size(
                Pos2::new(source_x, source_y),
                Vec2::new(CONNECTOR_SIZE, CONNECTOR_SIZE),
            ),
            CONNECTOR_ROUNDING,
            CONNECTOR_COLOR,
        );
        ui_area.painter().rect_filled(
            Rect::from_center_size(
                Pos2::new(sink_x, sink_y),
                Vec2::new(CONNECTOR_SIZE, CONNECTOR_SIZE),
            ),
            CONNECTOR_ROUNDING,
            CONNECTOR_COLOR,
        );
        ui_area.painter().add(shape);
    }
}

// Helpers:
impl InputConnection {
    pub fn update_window(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
        window_id: WindowId,
    ) -> Self {
        // Lags one frame behind, when updated here.
        // Is updated at draw() of sink.
        // let pos_source = self.get_source_pos(
        //     Rc::clone(&map_filter_to_node),
        //     Rc::clone(&map_source_to_node),
        // );
        let scene: ConnectionScene = match
            // Check, if update needed.
            self
                .clone()
                .update_wanted
        {
            // No update wanted. Return scene.
            false => {
                self
                    .clone()
                    .scene
            },
            // InputConnection changed, TargetImageView needs an update:
            true => {
                // println!("-- update connection from \"{}\" to \"{}\"", self.id, self.sink_id);
                let gui_sink = match
                    map_window_to_gui
                        .borrow_mut()
                        .remove(&window_id)
                {
                    None => panic!("!! gui not found"),
                    Some(gui) => gui,
                };
                let resources = gui_sink.render_resources();
                // Update ConnectionScene.
                let scene = self
                    .clone()
                    .scene
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone(),
                    );
                map_window_to_gui.borrow_mut().insert(window_id, gui_sink);
                scene
            },
        };
        let update_wanted: bool = match
            self.source_family.clone()
        {
            NodeFamily::None => false,
            NodeFamily::Control => false,
            NodeFamily::Sink => false,
            NodeFamily::Source => true, //false, // only for still pictures???
            NodeFamily::Filter => true,
        };
        Self {
            scene,
            update_wanted,
            ..self.to_owned()
        }
    }

    /// Get the port connector position of the source node.
    /// Updated every frame.
    /// Bool is false, when source is not found (anymore).
    pub fn get_source_update(
        &self,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
    ) -> (Option<Pos2>, Vec2) {
    // ) -> (Pos2, bool, Vec2) {
        let empty: (Option<Pos2>, Vec2) = (None, Vec2::default());
        match
            self
                .clone()
                .source_family
        {
            NodeFamily::None => empty,
            NodeFamily::Control => empty,
            NodeFamily::Sink => empty,
            NodeFamily::Source => {
                match
                    map_source_to_node
                        .borrow()
                        .map
                        .get(&self.source_id)
                {
                    None => empty,
                    Some(SourceNode::None) => empty,
                    Some(SourceNode::Picture(picture_node)) => {
                        let extent: [u32; 3] = picture_node.scene.image_storage.image().extent();
                        (
                            Some(picture_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                    #[cfg(unix)]
                    Some(SourceNode::PipewireInput(pw_input_node)) => {
                        let extent: [u32; 3] = pw_input_node.scene.image_storage.image().extent();
                        (
                            Some(pw_input_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                    // #[cfg(feature = "gstreamer")]
                    // Some(SourceNode::PwInputGst(pw_output_gst_node)) => {
                    //     let extent: [u32; 3] = pw_output_gst_node.scene.clone().unwrap().image_storage.image().extent();
                    //     (
                    //         Some(pw_output_gst_node.values.pos_out.to_owned()),
                    //         Vec2::new(extent[0] as f32, extent[1] as f32),
                    //     )
                    // },
                    Some(SourceNode::Stack(stack_node)) => {
                        let extent: [u32; 3] = stack_node.scene.image_storage.image().extent();
                        (
                            Some(stack_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                    #[cfg(feature = "gstreamer")]
                    Some(SourceNode::Video(video_node)) => {
                        let extent: [u32; 3] = video_node.scene.clone().unwrap().image_storage.image().extent();
                        (
                            Some(video_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                }
            },
            NodeFamily::Filter => {
                match
                    map_filter_to_node
                        .borrow()
                        .map
                        .get(&self.source_id)
                {
                    None => empty,
                    Some(FilterNode::None) => empty,
                    Some(FilterNode::ColorMixer(color_mixer_node)) => {
                        let extent: [u32; 3] = color_mixer_node.scene.image_storage.image().extent();
                        (
                            Some(color_mixer_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                    Some(FilterNode::ColorRotator(color_rotator_node)) => {
                        let extent: [u32; 3] = color_rotator_node.scene.image_storage.image().extent();
                        (
                            Some(color_rotator_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                    Some(FilterNode::Fader(fader_node)) => {
                        let extent: [u32; 3] = fader_node.scene.image_storage.image().extent();
                        (
                            Some(fader_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                    Some(FilterNode::Kaleidoscope(kaleidoscope_node)) => {
                        let extent: [u32; 3] = kaleidoscope_node.scene.image_storage.image().extent();
                        (
                            Some(kaleidoscope_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                    Some(FilterNode::Mandala(mandala_node)) => {
                        let extent: [u32; 3] = mandala_node.scene.image_storage.image().extent();
                        (
                            Some(mandala_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                    Some(FilterNode::Mixer(mixer_node)) => {
                        let extent: [u32; 3] = mixer_node.scene.image_storage.image().extent();
                        (
                            Some(mixer_node.values.pos_out.to_owned()),
                            Vec2::new(extent[0] as f32, extent[1] as f32),
                        )
                    },
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
