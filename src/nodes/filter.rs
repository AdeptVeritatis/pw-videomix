pub mod color_mixer;
pub mod color_rotator;
pub mod fader;
pub mod kaleidoscope;
pub mod mandala;
pub mod mixer;

use crate::{
    app::{
        data::app_nodes::{
            FilterToNode,
            SourceToNode,
        },
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
        gui::options::GuiOptions,
    },
    constants::FILTER_NODE_SIZE,
    impls::vulkano::vertices::LayerVertex,
    nodes::{
        filter::{
            color_mixer::ColorMixerNode,
            color_rotator::ColorRotatorNode,
            fader::FaderNode,
            kaleidoscope::KaleidoscopeNode,
            mandala::MandalaNode,
            mixer::MixerNode,
        },
        NodeFamily,
    },
};
use egui_winit_vulkano::{
    egui::{
        Context,
        Sense,
        Ui,
        Vec2,
        Window,
    },
    RenderResources,
};
use serde::Serialize;
use std::{
    cell::RefCell,
    collections::HashMap,
    fmt,
    rc::Rc,
    sync::Arc,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMesh {
    pub indices: Vec<u32>,
    pub vertices: Vec<LayerVertex>,
    pub viewport: Viewport,
}

// ----------------------------------------------------------------------------

/// Filters - nodes with sink inputs and source outputs.

// ----------------------------------------------------------------------------

/// Available filter nodes.
// #[derive(Serialize, Deserialize, Debug, Clone)]
#[derive(Serialize, Debug, Clone)]
pub enum FilterNode {
    /// Color mixer filter.
    ColorMixer(ColorMixerNode),

    /// Color rotator filter.
    ColorRotator(ColorRotatorNode),

    /// Fades or blends between sources.
    Fader(FaderNode), // Slideshow

    /// Kaleidoscope filter.
    Kaleidoscope(KaleidoscopeNode),

    /// Mandala filter.
    Mandala(MandalaNode),

    /// Mixes different sources.
    Mixer(MixerNode),

    /// No filter as placeholder.
    None,
    // Noop,

}

impl fmt::Display for FilterNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::None => write!(f, "none"),
            // Self::Noop => write!(f, "Noop"),
            Self::ColorMixer(_) => write!(f, "ColorMixer"),
            Self::ColorRotator(_) => write!(f, "ColorRotator"),
            Self::Fader(_) => write!(f, "Fader"),
            Self::Kaleidoscope(_) => write!(f, "Kaleidoscope"),
            Self::Mandala(_) => write!(f, "Mandala"),
            Self::Mixer(_) => write!(f, "Mixer"),
        }
    }
}

/// ## Constructors
impl FilterNode {
    pub fn color_mixer(
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        resources: RenderResources,
    ) {
        // Get next available filter ID.
        let filter_id: String = get_next_filter_id(Rc::clone(&map_filter_to_node));
        // Create ColorMixerNode.
        let color_mixer_node: ColorMixerNode = ColorMixerNode::new(
            filter_id.clone(),
            resources,
        );
        // Create FilterNode::ColorMixer from ColorMixerNode.
        let filter_node: FilterNode = Self::ColorMixer(color_mixer_node);
        // Info.
        println!("-> create FilterNode \"{filter_id}\" ( {filter_node} )");
        // Save FilterNode to map.
        map_filter_to_node
            .borrow_mut()
            .map
            .insert(
                filter_id,
                filter_node,
            );
    }

    pub fn color_rotator(
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        resources: RenderResources,
    ) {
        // Get next available filter ID.
        let filter_id: String = get_next_filter_id(Rc::clone(&map_filter_to_node));
        // Create ColorRotatorNode.
        let color_rotator_node: ColorRotatorNode = ColorRotatorNode::new(
            filter_id.clone(),
            resources,
        );
        // Create FilterNode::ColorRotator from ColorRotatorNode.
        let filter_node: FilterNode = Self::ColorRotator(color_rotator_node);
        // Info.
        println!("-> create FilterNode \"{filter_id}\" ( {filter_node} )");
        // Save FilterNode to map.
        map_filter_to_node
            .borrow_mut()
            .map
            .insert(
                filter_id,
                filter_node,
            );
    }

    pub fn fader(
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        resources: RenderResources,
    ) {
        // Get next available filter ID.
        let filter_id: String = get_next_filter_id(Rc::clone(&map_filter_to_node));
        // Create FaderNode.
        let fader_node: FaderNode = FaderNode::new(
            filter_id.clone(),
            resources,
        );
        // Create FilterNode::Fader from FaderNode.
        let filter_node: FilterNode = Self::Fader(fader_node);
        // Info.
        println!("-> create FilterNode \"{filter_id}\" ( {filter_node} )");
        // Save FilterNode to map.
        map_filter_to_node
            .borrow_mut()
            .map
            .insert(
                filter_id,
                filter_node,
            );
    }

    pub fn kaleidoscope(
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        resources: RenderResources,
    ) {
        // Get next available filter ID.
        let filter_id: String = get_next_filter_id(Rc::clone(&map_filter_to_node));
        // Create KaleidoscopeNode.
        let kaleidoscope_node: KaleidoscopeNode = KaleidoscopeNode::new(
            filter_id.clone(),
            resources,
        );
        // Create FilterNode::Kaleidoscope from KaleidoscopeNode.
        let filter_node: FilterNode = Self::Kaleidoscope(kaleidoscope_node);
        // Info.
        println!("-> create FilterNode \"{filter_id}\" ( {filter_node} )");
        // Save FilterNode to map.
        map_filter_to_node
            .borrow_mut()
            .map
            .insert(
                filter_id,
                filter_node,
            );
    }

    pub fn mandala(
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        resources: RenderResources,
    ) {
        // Get next available filter ID.
        let filter_id: String = get_next_filter_id(Rc::clone(&map_filter_to_node));
        // Create MandalaNode.
        let mandala_node: MandalaNode = MandalaNode::new(
            filter_id.clone(),
            resources,
        );
        // Create FilterNode::Mandala from MandalaNode.
        let filter_node: FilterNode = Self::Mandala(mandala_node);
        // Info.
        println!("-> create FilterNode \"{filter_id}\" ( {filter_node} )");
        // Save FilterNode to map.
        map_filter_to_node
            .borrow_mut()
            .map
            .insert(
                filter_id,
                filter_node,
            );
    }

    pub fn mixer(
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        resources: RenderResources,
    ) {
        // Get next available filter ID.
        let filter_id: String = get_next_filter_id(Rc::clone(&map_filter_to_node));
        // Create MixerNode.
        let mixer_node: MixerNode = MixerNode::new(
            filter_id.clone(),
            resources,
        );
        // Create FilterNode::Mixer from MixerNode.
        let filter_node: FilterNode = Self::Mixer(mixer_node);
        // Info.
        println!("-> create FilterNode \"{filter_id}\" ( {filter_node} )");
        // Save FilterNode to map.
        map_filter_to_node
            .borrow_mut()
            .map
            .insert(
                filter_id,
                filter_node,
            );
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn draw_filter_nodes(
    context_egui: Context,
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
    ui_area: &mut Ui,
) {
    let mut filter_values: HashMap<String, FilterNode> = HashMap::new();

    for
        (
            filter_id,
            filter_node,
        )
    in
        map_filter_to_node
            .borrow()
            .map
            .iter()
    {
        let mut filter_node = filter_node
            .to_owned();
        // ui_area.label(format!("{} ( {:?} )", filter_id, filter_node));
        // Draw filter nodes:
        match
            Window::new(
                filter_id
                    .clone()
            )
                .default_size(
                    Vec2::new(
                        FILTER_NODE_SIZE,
                        FILTER_NODE_SIZE,
                    )
                )
                .resizable(
                    false,
                )
                .title_bar(
                    false,
                )
                .show(
                    &context_egui,
                    |ui_filter| {
                        ui_filter
                            .vertical_centered(
                                |ui_inner| {
                                    // Filter node content:
                                    filter_node = match
                                        filter_node
                                            .clone()
                                    {
                                        FilterNode::None => {
                                            FilterNode::None
                                        },
                                        FilterNode::ColorMixer(mut node) => {
                                            node
                                                .draw(
                                                    event_loop_proxy.clone(),
                                                    Rc::clone(&map_filter_to_node),
                                                    Rc::clone(&map_source_to_node),
                                                    Rc::clone(&options_gui),
                                                    ui_area,
                                                    ui_inner,
                                                );
                                            if options_gui.borrow().debug_output {
                                                ui_area.label(format!("{filter_id} ( {node:?} )"));
                                            }
                                            FilterNode::ColorMixer(node)
                                        },
                                        FilterNode::ColorRotator(mut node) => {
                                            node
                                                .draw(
                                                    event_loop_proxy.clone(),
                                                    Rc::clone(&map_filter_to_node),
                                                    Rc::clone(&map_source_to_node),
                                                    Rc::clone(&options_gui),
                                                    ui_area,
                                                    ui_inner,
                                                );
                                            if options_gui.borrow().debug_output {
                                                ui_area.label(format!("{filter_id} ( {node:?} )"));
                                            }
                                            FilterNode::ColorRotator(node)
                                        },
                                        FilterNode::Fader(mut node) => {
                                            node
                                                .draw(
                                                    event_loop_proxy.clone(),
                                                    Rc::clone(&map_filter_to_node),
                                                    Rc::clone(&map_source_to_node),
                                                    Rc::clone(&options_gui),
                                                    ui_area,
                                                    ui_inner,
                                                );
                                            if options_gui.borrow().debug_output {
                                                ui_area.label(format!("{filter_id} ( {node:?} )"));
                                            }
                                            FilterNode::Fader(node)
                                        },
                                        FilterNode::Kaleidoscope(mut node) => {
                                            node
                                                .draw(
                                                    event_loop_proxy.clone(),
                                                    Rc::clone(&map_filter_to_node),
                                                    Rc::clone(&map_source_to_node),
                                                    Rc::clone(&options_gui),
                                                    ui_area,
                                                    ui_inner,
                                                );
                                            if options_gui.borrow().debug_output {
                                                ui_area.label(format!("{filter_id} ( {node:?} )"));
                                            }
                                            FilterNode::Kaleidoscope(node)
                                        },
                                        FilterNode::Mandala(mut node) => {
                                            node
                                                .draw(
                                                    event_loop_proxy.clone(),
                                                    Rc::clone(&map_filter_to_node),
                                                    Rc::clone(&map_source_to_node),
                                                    Rc::clone(&options_gui),
                                                    ui_area,
                                                    ui_inner,
                                                );
                                            if options_gui.borrow().debug_output {
                                                ui_area.label(format!("{filter_id} ( {node:?} )"));
                                            }
                                            FilterNode::Mandala(node)
                                        },
                                        FilterNode::Mixer(mut node) => {
                                            node
                                                .draw(
                                                    event_loop_proxy.clone(),
                                                    Rc::clone(&map_filter_to_node),
                                                    Rc::clone(&map_source_to_node),
                                                    Rc::clone(&options_gui),
                                                    ui_area,
                                                    ui_inner,
                                                );
                                            if options_gui.borrow().debug_output {
                                                ui_area.label(format!("{filter_id} ( {node:?} )"));
                                            }
                                            FilterNode::Mixer(node)
                                        },
                                    };
                                },
                            );
                    },
                )
        {
            None => (),
            Some(inner_response) => {
                if
                    inner_response
                        .response
                        .interact(
                            Sense::click(),
                        )
                        .clicked()
                {
                    event_loop_proxy
                        .send_event(
                            GuiEvent::ChangeNode(
                                NodeEvent {
                                    family: NodeFamily::Filter,
                                    id: filter_id.to_owned(),
                                    event_class: NodeEventClass::Select,
                                }
                            )
                        )
                        .ok();
                };
                inner_response
                    .response
                    .context_menu(
                        |ui_context| {
                            if
                                ui_context
                                    .button(
                                        "Remove FilterNode",
                                    )
                                    .clicked()
                            {
                                ui_context
                                    .close_menu();
                                event_loop_proxy
                                    .send_event(
                                        GuiEvent::ChangeNode(
                                            NodeEvent {
                                                family: NodeFamily::Filter,
                                                id: filter_id
                                                    .to_owned(),
                                                event_class: NodeEventClass::Remove,
                                            },
                                        ),
                                    )
                                    .ok();
                            };
                        },
                    );
            },
        };
        filter_values
            .insert(
                filter_id
                    .to_owned(),
                filter_node,
            );
    };

    for
        (
            filter_id,
            filter_node,
        )
    in
        filter_values
    {
        map_filter_to_node
            .borrow_mut()
            .map
            .insert(
                filter_id,
                filter_node,
            );
    };
}

// ----------------------------------------------------------------------------

pub fn update_filter_nodes(
    command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
    descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
    fps: f32,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    resources: RenderResources,
) {
    let mut map_id_to_filter: HashMap<String, FilterNode> = HashMap::new();
    for (filter_id, filter_node) in map_filter_to_node.borrow().map.iter() {
        let filter_node: FilterNode = match
            filter_node
                .clone()
        {
            FilterNode::None => FilterNode::None,
            FilterNode::ColorMixer(mut color_mixer_node) => {
                color_mixer_node = color_mixer_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        descriptor_set_allocator
                            .clone(),
                        resources.clone(),
                    );
                FilterNode::ColorMixer(color_mixer_node)
            },
            FilterNode::ColorRotator(mut color_rotator_node) => {
                color_rotator_node = color_rotator_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        descriptor_set_allocator
                            .clone(),
                        resources.clone(),
                    );
                FilterNode::ColorRotator(color_rotator_node)
            },
            FilterNode::Fader(mut fader_node) => {
                fader_node = fader_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        descriptor_set_allocator
                            .clone(),
                        fps,
                        resources.clone(),
                    );
                FilterNode::Fader(fader_node)
            },
            FilterNode::Kaleidoscope(mut kaleidoscope_node) => {
                kaleidoscope_node = kaleidoscope_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        descriptor_set_allocator
                            .clone(),
                        resources
                            .clone(),
                    );
                FilterNode::Kaleidoscope(kaleidoscope_node)
            },
            FilterNode::Mandala(mut mandala_node) => {
                mandala_node = mandala_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        descriptor_set_allocator
                            .clone(),
                        resources.clone(),
                    );
                FilterNode::Mandala(mandala_node)
            },
            FilterNode::Mixer(mut mixer_node) => {
                mixer_node = mixer_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        descriptor_set_allocator
                            .clone(),
                        resources.clone(),
                    );
                FilterNode::Mixer(mixer_node)
            },
        };
        map_id_to_filter
            .insert(
                filter_id.to_owned(),
                filter_node,
            );
    };
    // Store nodes:
    for (filter_id, filter_node) in map_id_to_filter {
        map_filter_to_node
            .borrow_mut()
            .map
            .insert(
                filter_id,
                filter_node,
            );
    };
}

// ----------------------------------------------------------------------------
// Functons - helpers:
// ----------------------------------------------------------------------------

fn get_next_filter_id(
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
) -> String {
    let hashmap_filter_to_node: HashMap<String, FilterNode> = map_filter_to_node
        .borrow()
        .map
        .to_owned();
    let mut filter_idx: usize = 1;
    loop {
        match filter_idx {
            idx if !hashmap_filter_to_node
                .contains_key(
                    &format!("Filter {idx}"),
                ) => break,
            idx if idx > 100 => break,
            _ => filter_idx += 1,
        }
    }
    format!("Filter {filter_idx}")
}
