pub mod picture_in;
pub mod picture_stack_in;
#[cfg(unix)]
pub mod pipewire_in;
// #[cfg(feature = "gstreamer")]
// pub mod pipewire_in_gst;
#[cfg(feature = "gstreamer")]
pub mod video_in;

use crate::{
    app::{
        data::app_nodes::SourceToNode,
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
        gui::options::GuiOptions,
    },
    constants::{
        PICTURE_PREVIEW_SIZE,
        STANDARD_NODE_SIZE,
    },
    impls::files::TextureMessage,
    nodes::{
        sources::{
            picture_in::PictureNode,
            picture_stack_in::StackNode,
        },
        NodeFamily,
    },
};
#[cfg(feature = "gstreamer")]
use crate::{
    impls::files::FilePath,
    nodes::sources::{
        // pw_input_gst::PwInputGstNode,
        video_in::VideoNode,
    },
};
#[cfg(unix)]
use crate::{
    impls::pipewire::PipewireStream,
    nodes::sources::pipewire_in::PipewireInputNode,
};
use egui_winit_vulkano::{
    egui::{
        Align,
        Context,
        Layout,
        Sense,
        Ui,
        Vec2,
        Window,
    },
    RenderResources,
};
#[cfg(unix)]
use pipewire::core::Core;
use std::{
    cell::RefCell,
    collections::HashMap,
    fmt,
    rc::Rc,
    sync::Arc,
};
#[cfg(feature = "gstreamer")]
use std::time::Instant;
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

/// Sources - nodes with only source outputs.

// ----------------------------------------------------------------------------

/// Available source nodes.
#[derive(Debug, Clone)]
pub enum SourceNode {
    /// No source as placeholder.
    None,

    /// Loads a picture into a node.
    Picture(PictureNode),

    /// Loads input from PipeWire into a node.
    #[cfg(unix)]
    PipewireInput(PipewireInputNode),

    // /// Loads input from PipeWire into a node.
    // #[cfg(feature = "gstreamer")]
    // PwInputGst(PwInputGstNode),

    /// Loads multiple pictures into one node.
    Stack(StackNode),

    #[cfg(feature = "gstreamer")]
    Video(VideoNode),
}

// Use NodeClass values???
impl fmt::Display for SourceNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::None => write!(f, "none"),
            Self::Picture(_) => write!(f, "Picture"),
            #[cfg(unix)]
            Self::PipewireInput(_) => write!(f, "PipeWire Input"),
            // #[cfg(feature = "gstreamer")]
            // Self::PwInputGst(_) => write!(f, "PipeWire Input"),
            Self::Stack(_) => write!(f, "Stack"),
            #[cfg(feature = "gstreamer")]
            Self::Video(_) => write!(f, "Video"),
        }
    }
}

/// ## Constructors
impl SourceNode {
    /// Creates a PictureNode in a SourceNode and stores it.
    pub fn picture(
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        message: TextureMessage,
        resources: RenderResources,
    ) {
        let source_id: String = get_next_source_id(Rc::clone(&map_source_to_node));
        let picture_node: PictureNode = PictureNode::new(
            command_buffer_allocator,
            source_id.clone(),
            message,
            resources,
        );
        let source_node: Self = Self::Picture(picture_node.clone());

        println!("-> create SourceNode \"{source_id}\" ( {source_node}: {} )", picture_node.name);

        map_source_to_node
            .borrow_mut()
            .map
            .insert(
                source_id,
                source_node,
            );
    }

    /// Creates a PwInputNode in a SourceNode and stores it.
    #[cfg(unix)]
    pub fn pw_input(
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        pw_core: Core,
        resources: RenderResources,
    ) {
        let source_id: String = get_next_source_id(Rc::clone(&map_source_to_node));
        let pw_input_node: PipewireInputNode = PipewireInputNode::new(
            event_loop_proxy,
            map_node_to_stream,
            pw_core,
            resources,
            source_id.clone(),
        );
        let source_node: Self = Self::PipewireInput(pw_input_node.clone());

        println!("-> create SourceNode \"{source_id}\" ( {source_node}: {} )", pw_input_node.name);

        map_source_to_node
            .borrow_mut()
            .map
            .insert(
                source_id,
                source_node,
            );
    }

    // #[cfg(feature = "gstreamer")]
    // pub fn pw_input_gst(
    //     map_source_to_node: Rc<RefCell<HashMap<String, SourceNode>>>,
    // ) -> String {
    //     let source_id: String = get_next_source_id(Rc::clone(&map_source_to_node));
    //     let pw_input_gst_node: PwInputGstNode = PwInputGstNode::new(
    //         source_id.clone(),
    //     );
    //     let source_node: Self = Self::PwInputGst(pw_input_gst_node.clone());
    //
    //     println!("-> create SourceNode \"{source_id}\" ( {source_node}: {} )", pw_input_gst_node.name);
    //
    //     map_source_to_node
    //         .borrow_mut()
    //         .insert(
    //             source_id.clone(),
    //             source_node,
    //         );
    //     source_id
    // }

    pub fn stack(
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        resources: RenderResources,
    ) -> String {
        let source_id: String = get_next_source_id(Rc::clone(&map_source_to_node));
        let stack_node: StackNode = StackNode::new(
            resources,
            source_id.clone(),
        );
        let source_node: Self = Self::Stack(stack_node.clone());

        println!("-> create SourceNode \"{source_id}\" ( {source_node}: {} )", stack_node.name);

        map_source_to_node
            .borrow_mut()
            .map
            .insert(
                source_id.clone(),
                source_node,
            );
        source_id
    }

    #[cfg(feature = "gstreamer")]
    pub fn video(
        file_path: FilePath,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
    ) -> String {
        let source_id: String = get_next_source_id(Rc::clone(&map_source_to_node));
        let video_node: VideoNode = VideoNode::new(
            source_id.clone(),
            file_path,
        );
        let source_node: Self = Self::Video(video_node.clone());

        println!("-> create SourceNode \"{source_id}\" ( {source_node}: {} )", video_node.name);

        map_source_to_node
            .borrow_mut()
            .map
            .insert(
                source_id.clone(),
                source_node,
            );
        source_id
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn draw_source_nodes(
    context: Context,
    descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
    ui: &mut Ui,
) {
    let mut map_id_to_source: HashMap<String, SourceNode> = HashMap::new();

    for (source_id, source_node) in map_source_to_node.borrow().map.iter() {
        let source_node: SourceNode = match
            source_node.clone()
        {
            SourceNode::None => SourceNode::None,
            SourceNode::Picture(mut node) => {
                if options_gui.borrow().debug_output {
                    ui.label(format!("Picture {}", node.name));
                }
                // move rest into function or upwards???
                match
                    Window::new(node.id.clone())
                        .default_size(Vec2::new(PICTURE_PREVIEW_SIZE, PICTURE_PREVIEW_SIZE))
                        .resizable(true)
                        .title_bar(false)
                        // .frame(Frame::canvas(&Style::default()).fill(Color32::BLACK))
                        .show(&context, |ui_pic| {
                            // To test different layouts!!!
                            ui_pic.with_layout(Layout::top_down(Align::Center), |ui_inner| {
                                node
                                    .draw(
                                        descriptor_set_allocator
                                            .clone(),
                                        event_loop_proxy.clone(),
                                        ui_inner,
                                    );
                            });
                        })
                {
                    None => (),
                    Some(inner_response) => {
                        if
                            inner_response
                                .response
                                .interact(
                                    Sense::click(),
                                )
                                .clicked()
                        {
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: NodeFamily::Source,
                                            id: source_id
                                                .to_owned(),
                                            event_class: NodeEventClass::Select,
                                        }
                                    )
                                )
                                .ok();
                        };
                        inner_response
                            .response
                            .context_menu(
                                |ui_context| {
                                    if
                                        ui_context
                                            .button(
                                                "Remove PictureNode",
                                            )
                                            .clicked()
                                    {
                                        ui_context
                                            .close_menu();
                                        event_loop_proxy
                                            .send_event(
                                                GuiEvent::ChangeNode(
                                                    NodeEvent {
                                                        family: NodeFamily::Source,
                                                        id: node
                                                            .id
                                                            .clone(),
                                                        event_class: NodeEventClass::Remove,
                                                    }
                                                )
                                            )
                                            .ok();
                                    }
                                }
                            );
                    },
                };
                SourceNode::Picture(node)
            },
            #[cfg(unix)]
            SourceNode::PipewireInput(mut node) => {
                // move rest into function or upwards???
                match
                    Window::new(node.id.clone())
                        .default_size(Vec2::new(STANDARD_NODE_SIZE, STANDARD_NODE_SIZE))
                        .resizable(true)
                        .title_bar(false)
                        // .frame(Frame::canvas(&Style::default()).fill(Color32::BLACK))
                        .show(&context, |ui_pwinput| {
                            ui_pwinput.vertical_centered(|ui_inner| {
                                node
                                    .draw(
                                        options_gui.clone(),
                                        ui_inner,
                                    );
                            });
                        })
                {
                    None => (),
                    Some(inner_response) => {
                        if
                            inner_response
                                .response
                                .interact(
                                    Sense::click(),
                                )
                                .clicked()
                        {
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: NodeFamily::Source,
                                            id: source_id.to_owned(),
                                            event_class: NodeEventClass::Select,
                                        }
                                    )
                                )
                                .ok();
                        };
                        inner_response.response.context_menu(|ui_context| {
                            if ui_context.button("Remove PipeWire Input").clicked() {
                                ui_context.close_menu();
                                event_loop_proxy
                                    .send_event(
                                        GuiEvent::ChangeNode(
                                            NodeEvent {
                                                family: NodeFamily::Source,
                                                id: node.id.clone(),
                                                event_class: NodeEventClass::Remove,
                                            }
                                        )
                                    )
                                    .ok();
                            }
                        });
                    },
                };
                if options_gui.borrow().debug_output {
                    ui.label(format!("PipeWireInput {}", node.name));
                }
                SourceNode::PipewireInput(node)
            },
            // #[cfg(feature = "gstreamer")]
            // SourceNode::PwInputGst(mut pw_input_gst_node) => {
            //     if options_gui.borrow().debug_output {
            //         ui.label(format!("PipeWire Input {}", pw_input_gst_node.name));
            //     }
            //     // move rest into function or upwards???
            //     match
            //         Window::new(pw_input_gst_node.id.clone())
            //             .default_size(Vec2::new(STANDARD_NODE_SIZE, STANDARD_NODE_SIZE))
            //             .resizable(true)
            //             .title_bar(false)
            //             // .frame(Frame::canvas(&Style::default()).fill(Color32::BLACK))
            //             .show(&context, |ui_stack| {
            //                 ui_stack.vertical_centered(|ui_inner| {
            //                     pw_input_gst_node = pw_input_gst_node
            //                         .to_owned()
            //                         .draw(
            //                             // event_loop_proxy.clone(),
            //                             // options_gui.clone(),
            //                             ui_inner,
            //                         );
            //                 });
            //             })
            //     {
            //         None => (),
            //         Some(inner_response) => {
            //             if inner_response.response.clicked() {
            //                 event_loop_proxy
            //                     .send_event(
            //                         GuiEvent::ChangeNode(
            //                             NodeEvent {
            //                                 family: NodeFamily::Source,
            //                                 id: source_id.to_owned(),
            //                                 event_class: NodeEventClass::Select,
            //                             }
            //                         )
            //                     )
            //                     .ok();
            //             }
            //             inner_response.response.context_menu(|ui_context| {
            //                 if ui_context.button("Remove PwInputGstNode").clicked() {
            //                     ui_context.close_menu();
            //                     event_loop_proxy
            //                         .send_event(
            //                             GuiEvent::ChangeNode(
            //                                 NodeEvent {
            //                                     family: NodeFamily::Source,
            //                                     id: pw_input_gst_node.id.clone(),
            //                                     event_class: NodeEventClass::Remove,
            //                                 }
            //                             )
            //                         )
            //                         .ok();
            //                 }
            //             });
            //         },
            //     };
            //     SourceNode::PwInputGst(pw_input_gst_node)
            // },
            SourceNode::Stack(mut node) => {
                if options_gui.borrow().debug_output {
                    ui.label(format!("Stack {}", node.name));
                }
                // move rest into function or upwards???
                match
                    Window::new(node.id.clone())
                        .default_size(Vec2::new(STANDARD_NODE_SIZE, STANDARD_NODE_SIZE))
                        .resizable(true)
                        .title_bar(false)
                        // .frame(Frame::canvas(&Style::default()).fill(Color32::BLACK))
                        .show(&context, |ui_stack| {
                            ui_stack.vertical_centered(|ui_inner| {
                                node
                                    .draw(
                                        event_loop_proxy.clone(),
                                        options_gui.clone(),
                                        ui_inner,
                                    );
                            });
                        })
                {
                    None => (),
                    Some(inner_response) => {
                        if
                            inner_response
                                .response
                                .interact(
                                    Sense::click(),
                                )
                                .clicked()
                        {
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: NodeFamily::Source,
                                            id: source_id.to_owned(),
                                            event_class: NodeEventClass::Select,
                                        }
                                    )
                                )
                                .ok();
                        };
                        inner_response.response.context_menu(|ui_context| {
                            if ui_context.button("Remove StackNode").clicked() {
                                ui_context.close_menu();
                                event_loop_proxy
                                    .send_event(
                                        GuiEvent::ChangeNode(
                                            NodeEvent {
                                                family: NodeFamily::Source,
                                                id: node.id.clone(),
                                                event_class: NodeEventClass::Remove,
                                            }
                                        )
                                    )
                                    .ok();
                            }
                        });
                    },
                };
                SourceNode::Stack(node)
            },
            #[cfg(feature = "gstreamer")]
            SourceNode::Video(mut node) => {
                if options_gui.borrow().debug_output {
                    ui.label(format!("Video {}", node.name));
                }
                // move rest into function or upwards???
                match
                    Window::new(node.id.clone())
                        .default_size(Vec2::new(STANDARD_NODE_SIZE, STANDARD_NODE_SIZE))
                        .resizable(true)
                        .title_bar(false)
                        // .frame(Frame::canvas(&Style::default()).fill(Color32::BLACK))
                        .show(&context, |ui_stack| {
                            ui_stack.vertical_centered(|ui_inner| {
                                node
                                    .draw(
                                        // event_loop_proxy.clone(),
                                        // options_gui.clone(),
                                        ui_inner,
                                    );
                            });
                        })
                {
                    None => (),
                    Some(inner_response) => {
                        if
                            inner_response
                                .response
                                .interact(
                                    Sense::click(),
                                )
                                .clicked()
                        {
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: NodeFamily::Source,
                                            id: source_id.to_owned(),
                                            event_class: NodeEventClass::Select,
                                        }
                                    )
                                )
                                .ok();
                        };
                        inner_response.response.context_menu(|ui_context| {
                            if ui_context.button("Remove VideoNode").clicked() {
                                ui_context.close_menu();
                                event_loop_proxy
                                    .send_event(
                                        GuiEvent::ChangeNode(
                                            NodeEvent {
                                                family: NodeFamily::Source,
                                                id: node.id.clone(),
                                                event_class: NodeEventClass::Remove,
                                            }
                                        )
                                    )
                                    .ok();
                            }
                        });
                    },
                };
                SourceNode::Video(node)
            },
        };
        map_id_to_source
            .insert(
                source_id.to_owned(),
                source_node,
            );
    };
    // Store value from node interaction.
    for (source_id, source_node) in map_id_to_source {
        map_source_to_node
            .borrow_mut()
            .map
            .insert(
                source_id,
                source_node,
            );
    };
}

// ----------------------------------------------------------------------------

pub fn update_source_nodes(
    command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
    descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    resources: RenderResources,
    #[cfg(feature = "gstreamer")]
    updated_app: Instant,
) {
    let mut map_id_to_source: HashMap<String, SourceNode> = HashMap::new();
    for (source_id, source_node) in map_source_to_node.borrow().map.iter() {
        let source_node: SourceNode = match
            source_node
                .clone()
        {
            // SourceNode::PipewireInput(mut pw_input_node) => {
            //     pw_input_node = pw_input_node
            //         .update(
            //             resources.clone(),
            //         );
            //     SourceNode::PipewireInput(pw_input_node)
            // },
            // #[cfg(feature = "gstreamer")]
            // SourceNode::PwInputGst(mut pw_input_gst_node) => {
            //     pw_input_gst_node = pw_input_gst_node
            //         .update(
            //             resources.clone(),
            //             updated_app.clone(),
            //         );
            //     SourceNode::PwInputGst(pw_input_gst_node)
            // },
            SourceNode::Stack(mut stack_node) => {
                stack_node = stack_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        descriptor_set_allocator
                            .clone(),
                        resources.clone(),
                    );
                SourceNode::Stack(stack_node)
            },
            #[cfg(feature = "gstreamer")]
            SourceNode::Video(mut video_node) => {
                video_node = video_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources.clone(),
                        updated_app.clone(),
                    );
                SourceNode::Video(video_node)
            },
            source_node => source_node,
        };
        map_id_to_source
            .insert(
                source_id.to_owned(),
                source_node,
            );
    };
    // Store nodes:
    for (source_id, source_node) in map_id_to_source {
        map_source_to_node
            .borrow_mut()
            .map
            .insert(
                source_id,
                source_node,
            );
    };
}

// ----------------------------------------------------------------------------
// Functions - helpers:
// ----------------------------------------------------------------------------

fn get_next_source_id(
    map_source_to_node: Rc<RefCell<SourceToNode>>,
) -> String {
    let hashmap_source_to_node: HashMap<String, SourceNode> = map_source_to_node
        .borrow()
        .map
        .to_owned();
    let mut source_idx: usize = 1;
    loop {
        match
            source_idx
        {
            idx if !hashmap_source_to_node
                .contains_key(
                    &format!("Source {idx}"),
                ) => break,
            idx if idx > 100 => break,
            _ => source_idx += 1,
        }
    }
    format!("Source {source_idx}")
}
