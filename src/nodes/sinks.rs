pub mod monitor;
pub mod picture_out;
#[cfg(unix)]
pub mod pipewire_out;
// #[cfg(feature = "gstreamer")]
// pub mod pipewire_out_gst;
#[cfg(feature = "gstreamer")]
pub mod video_out;
pub mod window;

use crate::{
    app::{
        data::app_nodes::{
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::options::GuiOptions,
        print::PrintClass,
    },
    // impls::midir::MidiNode,
    nodes::sinks::{
        monitor::MonitorNode,
        picture_out::SnapshotNode,
        window::{
            WindowNode,
            get_window_name,
        },
    },
};
#[cfg(feature = "gstreamer")]
use crate::nodes::sinks::{
    video_out::EncoderNode,
    // pw_output_gst::{
    //     PwOutputGstNode,
    //     PwOutputGstValues,
    // },
};
#[cfg(unix)]
use crate::{
    impls::pipewire::PipewireStream,
    nodes::sinks::pipewire_out::PwOutputNode,
};
use egui_winit_vulkano::{
    egui::{
        Context,
        Ui,
        Vec2,
    },
    Gui,
    RenderResources,
};
#[cfg(unix)]
use pipewire::core::Core;
use std::{
    cell::RefCell,
    collections::HashMap,
    fmt,
    rc::Rc,
    sync::Arc,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
};
use vulkano_util::{
    // renderer::VulkanoWindowRenderer,
    window::VulkanoWindows,
};
use winit::{
    dpi::LogicalSize,
    event_loop::EventLoopProxy,
    window::{
        Fullscreen,
        WindowId,
    },
};

// ----------------------------------------------------------------------------

/// Sinks - nodes with only sink inputs.

// ----------------------------------------------------------------------------

/// Available sink nodes.
#[derive(Debug, Clone)]
pub enum SinkNode {
    /// No sink as placeholder.
    None,

    /// Encodes content and writes it into a video file to the disk.
    #[cfg(feature = "gstreamer")]
    Encoder(EncoderNode),

    /// Displays content in a node on the main panel.
    Monitor(MonitorNode),

    /// Writes content to a PipeWire output source.
    #[cfg(unix)]
    PipewireOutput(PwOutputNode),

    /// Writes content to a PipeWire output source.
    // #[cfg(feature = "gstreamer")]
    // PwOutputGst(PwOutputGstNode),

    /// Saves snapshots to the disk.
    Snapshot(SnapshotNode),

    /// Displays content in a new window.
    Window(WindowNode),
}

impl fmt::Display for SinkNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::None => write!(f, "none"),
            #[cfg(feature = "gstreamer")]
            Self::Encoder(_) => write!(f, "Encoder"),
            Self::Monitor(_) => write!(f, "Monitor"),
            #[cfg(unix)]
            Self::PipewireOutput(_) => write!(f, "PwOutput"),
            // #[cfg(feature = "gstreamer")]
            // Self::PwOutputGst(_) => write!(f, "PwOutput"),
            Self::Snapshot(_) => write!(f, "Snapshot"),
            Self::Window(_) => write!(f, "Window"),
        }
    }
}

/// ## Constructors
impl SinkNode {
    #[cfg(feature = "gstreamer")]
    pub fn encoder(
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        // resources: RenderResources,
    ) {
        let sink_id: String = get_next_sink_id(Rc::clone(&map_sink_to_node));
        let encoder_node: EncoderNode = EncoderNode::new(
            sink_id.clone(),
            // resources,
        );
        let sink_node: Self = Self::Encoder(encoder_node);

        PrintClass::CreateSinkNode(sink_node.clone()).print();

        map_sink_to_node
            .borrow_mut()
            .map
            .insert(
                sink_id,
                sink_node,
            );
    }

    pub fn monitor(
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        resources: RenderResources,
    ) {
        let sink_id: String = get_next_sink_id(Rc::clone(&map_sink_to_node));
        let monitor_node: MonitorNode = MonitorNode::new(
            sink_id.clone(),
            resources,
        );
        let sink_node: Self = Self::Monitor(monitor_node);

        PrintClass::CreateSinkNode(sink_node.clone()).print();

        map_sink_to_node
            .borrow_mut()
            .map
            .insert(
                sink_id,
                sink_node,
            );
    }

    #[cfg(unix)]
    pub fn pw_output(
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        pw_core: Core,
        resources: RenderResources,
    ) {
        let sink_id: String = get_next_sink_id(Rc::clone(&map_sink_to_node));
        let pw_output_node: PwOutputNode = PwOutputNode::new(
            event_loop_proxy,
            map_node_to_stream,
            pw_core,
            resources,
            sink_id.clone(),
        );
        let sink_node: Self = Self::PipewireOutput(pw_output_node);

        PrintClass::CreateSinkNode(sink_node.clone()).print();

        map_sink_to_node
            .borrow_mut()
            .map
            .insert(
                sink_id,
                sink_node,
            );
    }

    // #[cfg(feature = "gstreamer")]
    // pub fn pw_output_gst(
    //     map_sink_to_node: Rc<RefCell<SinkToNode>>,
    //     // resources: RenderResources,
    // ) {
    //     let sink_id: String = get_next_sink_id(Rc::clone(&map_sink_to_node));
    //     let pw_output_gst_node: PwOutputGstNode = PwOutputGstNode::new(
    //         sink_id.clone(),
    //         // resources,
    //     );
    //     let sink_node: Self = Self::PwOutputGst(pw_output_gst_node);
    //
    //     PrintClass::CreateSinkNode(sink_node.clone()).print();
    //
    //     map_sink_to_node
    //         .borrow_mut()
    //         .map
    //         .insert(
    //             sink_id,
    //             sink_node,
    //         );
    // }

    pub fn snapshot(
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        // resources: RenderResources,
    ) {
        let sink_id: String = get_next_sink_id(Rc::clone(&map_sink_to_node));
        let snapshot_node: SnapshotNode = SnapshotNode::new(
            sink_id.clone(),
            // resources,
        );
        let sink_node: Self = Self::Snapshot(snapshot_node);

        PrintClass::CreateSinkNode(sink_node.clone()).print();

        map_sink_to_node
            .borrow_mut()
            .map
            .insert(
                sink_id,
                sink_node,
            );

    }

    pub fn window(
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        resources: RenderResources,
        window_id: WindowId,
    ) -> String {
        let sink_name: String = get_window_name(window_id);
        let sink_id: String = get_next_sink_id(Rc::clone(&map_sink_to_node));
        let window_node: WindowNode = WindowNode::new(
            sink_id.clone(),
            sink_name.clone(),
            resources,
            window_id,
        );
        let sink_node: Self = Self::Window(window_node);

        PrintClass::CreateSinkNode(sink_node.clone()).print();

        // Store WindowNode:
        map_sink_to_node
            .borrow_mut()
            .map
            .insert(
                sink_id.clone(),
                sink_node,
            );
        // name
        sink_id
    }

}

// other stuff
impl SinkNode {
    pub fn get_id(
        self,
    ) -> String {
        match self {
            Self::None => String::from("none"),
            #[cfg(feature = "gstreamer")]
            Self::Encoder(encoder_node) => {
                encoder_node
                    .to_owned()
                    .id
            },
            Self::Monitor(monitor_node) => {
                monitor_node
                    .to_owned()
                    .id
            },
            #[cfg(unix)]
            Self::PipewireOutput(pw_output_node) => {
                pw_output_node
                    .to_owned()
                    .id
            },
            // #[cfg(feature = "gstreamer")]
            // Self::PwOutputGst(pw_output_gst_node) => {
            //     pw_output_gst_node
            //         .to_owned()
            //         .id
            // },
            Self::Snapshot(snapshot_node) => {
                snapshot_node
                    .to_owned()
                    .id
            },
            Self::Window(window_node) => {
                window_node
                    .to_owned()
                    .id
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn draw_sink_nodes(
    // Context for the nodes:
    command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
    context: Context,
    descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    // midi_node_rc: Rc<RefCell<MidiNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
    resources: RenderResources,
    // Ui for labels and connections on the panel:
    ui_area: &mut Ui,
) {
    let mut map_id_to_sink: HashMap<String, SinkNode> = HashMap::new();

    for (sink_id, sink_node) in map_sink_to_node.borrow().map.iter() {
        let sink_node: SinkNode = match
            sink_node
                .to_owned()
        {
            SinkNode::None => SinkNode::None,
            #[cfg(feature = "gstreamer")]
            SinkNode::Encoder(mut node) => {
                if options_gui.borrow().debug_output {
                    ui_area.label(format!("Encoder {}", node.name)); // name not set yet????????????
                }
                node
                    .draw(
                        context.clone(),
                        event_loop_proxy.clone(),
                        Rc::clone(&map_filter_to_node),
                        Rc::clone(&map_source_to_node),
                        Rc::clone(&options_gui),
                        ui_area,
                    );
                // Return new values.
                SinkNode::Encoder(
                    node,
                )
            },
            SinkNode::Monitor(mut node) => {
                if options_gui.borrow().debug_output {
                    ui_area.label(format!("Monitor {}", node.name)); // name not set yet????????????
                }
                node
                    .draw(
                        context
                            .clone(),
                        descriptor_set_allocator
                            .clone(),
                        event_loop_proxy
                            .clone(),
                        map_filter_to_node
                            .clone(),
                        map_source_to_node
                            .clone(),
                        ui_area,
                    );
                // Return new values.
                SinkNode::Monitor(
                    node,
                )
            },
            #[cfg(unix)]
            SinkNode::PipewireOutput(mut node) => {
                if options_gui.borrow().debug_output {
                    ui_area.label(format!("PwOutput {}", node.name)); // name not set yet????????????
                }
                node
                    .draw(
                        context.clone(),
                        event_loop_proxy.clone(),
                        Rc::clone(&map_filter_to_node),
                        Rc::clone(&map_source_to_node),
                        Rc::clone(&options_gui),
                        ui_area,
                    );
                // Return new values.
                SinkNode::PipewireOutput(
                    node,
                )
            },
            // #[cfg(feature = "gstreamer")]
            // SinkNode::PwOutputGst(pw_output_gst_node) => {
            //     if options_gui.borrow().debug_output {
            //         ui_area.label(format!("PwOutput {}", pw_output_gst_node.name)); // name not set yet????????????
            //     }
            //     let values: PwOutputGstValues = pw_output_gst_node
            //         .to_owned()
            //         .draw(
            //             context.clone(),
            //             event_loop_proxy.clone(),
            //             Rc::clone(&map_filter_to_node),
            //             Rc::clone(&map_source_to_node),
            //             Rc::clone(&options_gui),
            //             ui_area,
            //         );
            //     // Return new values.
            //     SinkNode::PwOutputGst(
            //         PwOutputGstNode {
            //             values,
            //             ..pw_output_gst_node.to_owned()
            //         }
            //     )
            // },
            SinkNode::Snapshot(mut node) => {
                if options_gui.borrow().debug_output {
                    ui_area
                        .label(
                            format!(
                                "Snapshot {}",
                                node
                                    .name,
                            ),
                        );
                };
                node
                    .draw(
                        command_buffer_allocator
                            .clone(),
                        context
                            .clone(),
                        event_loop_proxy
                            .clone(),
                        map_filter_to_node
                            .clone(),
                        map_source_to_node
                            .clone(),
                        // Rc::clone(&midi_node_rc),
                        options_gui
                            .clone(),
                        resources
                            .clone(),
                        ui_area,
                    );
                // Return new values.
                SinkNode::Snapshot(
                    node,
                )
            },
            SinkNode::Window(mut node) => {
                if options_gui.borrow().debug_output {
                    ui_area.label(format!("Window \"{}\" ( {} )", node.id, node.name,));
                }
                node
                    .draw(
                        context.clone(),
                        event_loop_proxy.clone(),
                        Rc::clone(&map_filter_to_node),
                        Rc::clone(&map_source_to_node),
                        Rc::clone(&options_gui),
                        ui_area,
                    );
                // Return new values.
                SinkNode::Window(
                    node,
                )
            },
        };
        map_id_to_sink.insert(
            sink_id.to_owned(),
            sink_node,
        );
    };
    // Store values from node interaction.
    for (sink_id, sink_node) in map_id_to_sink {
        map_sink_to_node
            .borrow_mut()
            .map
            .insert(
                sink_id,
                sink_node,
            );
    };
}

// ----------------------------------------------------------------------------

pub fn update_sink_nodes(
    command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
    #[cfg(unix)]
    map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
    resources: RenderResources,
    windows: &VulkanoWindows,
) {
    let mut map_id_to_sink: HashMap<String, SinkNode> = HashMap::new();
    for (sink_id, sink_node) in map_sink_to_node.borrow().map.iter() {
        let sink_node: SinkNode = match
            sink_node.to_owned()
        {
            SinkNode::None => SinkNode::None,
            // Update EncoderNode:
            #[cfg(feature = "gstreamer")]
            SinkNode::Encoder(mut encoder_node) => {
                encoder_node = encoder_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone(),
                    );
                SinkNode::Encoder(encoder_node)
            },
            // Update MonitorNode:
            SinkNode::Monitor(mut monitor_node) => {
                monitor_node = monitor_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone(),
                    );
                SinkNode::Monitor(monitor_node)
            },
            // Update PwOutputNode:
            #[cfg(unix)]
            SinkNode::PipewireOutput(mut pw_output_node) => {
                pw_output_node = pw_output_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        map_node_to_stream
                            .clone(),
                        resources
                            .clone(),
                    );
                SinkNode::PipewireOutput(pw_output_node)
            },
            // // Update PwOutputGstNode:
            // #[cfg(feature = "gstreamer")]
            // SinkNode::PwOutputGst(mut pw_output_gst_node) => {
            //     pw_output_gst_node = pw_output_gst_node.update(
            //         resources.clone(),
            //     );
            //     SinkNode::PwOutputGst(pw_output_gst_node)
            // },
            // Update SnapshotNode:
            SinkNode::Snapshot(mut snapshot_node) => {
                snapshot_node = snapshot_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone(),
                    );
                SinkNode::Snapshot(snapshot_node)
            },
            // Update WindowNode:
            SinkNode::Window(mut window_node) => {
                let window_id: WindowId = window_node.window_id;
                // Change window dimensions.
                match
                    windows
                        .get_window(
                            window_id,
                        )
                {
                    None => (),
                    Some(window) => {
                        // let window = renderer.window();
                        // Set window size:
                        let window_size: Vec2 = window_node.values.size;
                        let _ = window
                            .request_inner_size(
                                LogicalSize::new(
                                    window_size[0],
                                    window_size[1],
                                ),
                            );
                        // Set fullscreen:
                        match
                            window_node
                                .clone()
                                .values
                                .fullscreen
                        {
                            true => {
                                window.set_fullscreen(Some(Fullscreen::Borderless(
                                    window.current_monitor(),
                                )));
                            },
                            false => {
                                window.set_fullscreen(None);
                            },
                        };
                    },
                };
                // Update target image.
                window_node = window_node
                    .update(
                        command_buffer_allocator
                            .clone(),
                        map_window_to_gui
                            .clone(),
                        window_id,
                    );
                SinkNode::Window(window_node)
            },
        };
        map_id_to_sink
            .insert(
                sink_id.to_owned(),
                sink_node,
            );
    };
    // Store nodes.
    for (sink_id, sink_node) in map_id_to_sink {
        map_sink_to_node
            .borrow_mut()
            .map
            .insert(
                sink_id,
                sink_node,
            );
    };

}

// ----------------------------------------------------------------------------

fn get_next_sink_id(
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
) -> String {
    let hashmap_sink_to_node: HashMap<String, SinkNode> = map_sink_to_node
        .borrow()
        .map
        .to_owned();
    let mut sink_idx: usize = 1;
    loop {
        match sink_idx {
            idx if !hashmap_sink_to_node
                .contains_key(
                    &format!("Sink {idx}"),
                ) => break,
            idx if idx > 100 => break,
            _ => sink_idx += 1,
        }
    }
    format!("Sink {sink_idx}")
}
