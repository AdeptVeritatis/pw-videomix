#![allow(clippy::derivable_impls)]

use crate::{
    app::{
        events::GuiEvent,
        gui::options::GuiOptions,
    },
    constants::PANEL_NODE_SPACING_SMALL,
    impls::{
        pipewire::{
            PipewireCamera,
            PipewireStream,
        },
        vulkano::scenes::copy::pw_input_scene::PipewireInputScene,
    },
    nodes::{
        NodeClass,
        NodeFamily,
        draw_node_header,
        draw_node_options,
    },
};
use egui_winit_vulkano::{
    egui::{
        Color32,
        Pos2,
        RichText,
        Ui,
    },
    RenderResources,
};
use pipewire::core::Core;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
    time::Instant,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct PipewireInputValues {
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    pub pos_out: Pos2,

    // Target of pipewire stream.
    pub target: Option<u32>,
}

impl Default for PipewireInputValues {
    fn default() -> Self {
        Self {
            pos_out: Pos2::default(),

            target: None,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct PipewireInputNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    pub scene: PipewireInputScene,
    pub values: PipewireInputValues,
    pub updated: Instant,
}

// Constructors and draw:
impl PipewireInputNode {
    pub fn new(
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        pw_core: Core,
        resources: RenderResources,
        source_id: String,
    ) -> Self {
        let scene: PipewireInputScene = PipewireInputScene::new(
            event_loop_proxy,
            map_node_to_stream,
            pw_core,
            resources,
            source_id.clone(),
        );
        Self {
            class: NodeClass::PipewireInput,
            family: NodeFamily::Source,
            id: source_id.clone(),
            name: source_id.clone(),
            scene,
            values: PipewireInputValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a PwInputNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_inner: &mut Ui,
    ) {
    // Header:
        self.name = draw_node_header(
            self.class.clone(),
            self.id.clone(),
            self.name.clone(),
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Options:
        draw_node_options(
            Rc::clone(&options_gui),
            ui_inner,
        );
        // Grid::new("Options").show(ui_inner, |ui_grid| {
        //     state = draw_node_state(state.clone(), ui_grid);
        // });


        ui_inner.separator();
        ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
        ui_inner
            .label(
                RichText::new(
                    "prototype stub\n!!! not complete !!!",
                )
                .color(
                    Color32::RED,
                ),
            );
        ui_inner.add_space(PANEL_NODE_SPACING_SMALL);

    // Output:
        ui_inner.separator();
        ui_inner.vertical_centered(|ui_source| {
            ui_source.label("Output ...");
            self.values.pos_out = ui_source.min_rect().max;
        });
    }

    /// The side panel of a PwInputNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_id_to_camera: Rc<RefCell<HashMap<u32, PipewireCamera>>>,
        // pw_sender: channel::Sender<PipewireMessage>,
        ui: &mut Ui,
    ) {
    // Target:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.heading(RichText::new("connect target\nusing a\ngraph manager\n(e.g. qpwgraph)").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label("Targets:");
        ui.label("( test )");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        // combobox???
        for camera in map_id_to_camera.borrow().values() {
            if ui.button(camera.description.clone()).clicked() {
                self.values.target = Some(camera.device);

                // Send a pipewire event to change the target???

            };
        }
    }
}

// Helpers:
// impl PipewireInputNode {
// }

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
