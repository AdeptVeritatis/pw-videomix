
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            bind::{
                elements::source_label::SourceFPS,
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        NODE_OUTPUT_HEIGHT,
        NODE_OUTPUT_WIDTH,
        PANEL_NODE_SPACING_SMALL,
        STACK_FPS_OFFSET,
    },
    impls::{
        midir::{
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        vulkano::scenes::render::stack_scene::{
            StackLayer,
            StackScene,
        },
    },
    nodes::{
        sources::SourceNode,
        NodeClass,
        NodeFamily,
        NodeState,
        draw_node_header,
        draw_node_options,
        draw_node_output,
        draw_node_qty,
        draw_node_state,
    },
};
use egui_winit_vulkano::{
    egui::{
        Grid,
        Pos2,
        Ui,
        Vec2,
    },
    RenderResources,
};
use std::{
    cell::RefCell,
    rc::Rc,
    sync::Arc,
    time::{
        // Duration,
        Instant,
    },
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct StackValues {
    // Size of the canvas, the stack is projected on. // ???
    pub output_size: Vec2,
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    pub pos_out: Pos2,
    // State: play, pause, stop
    pub state: NodeState,

    // Stack specific settings.
    pub fps: f32,
}

impl Default for StackValues {
    fn default() -> Self {
        Self {
            output_size: Vec2::new(NODE_OUTPUT_WIDTH, NODE_OUTPUT_HEIGHT),
            pos_out: Pos2::default(),
            state: NodeState::Play,

            fps: STACK_FPS_OFFSET,
        }
    }
}

// ----------------------------------------------------------------------------

// When multiple pictures are opened.
#[derive(Debug, Clone)]
pub struct StackNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    pub scene: StackScene,
    pub values: StackValues,
    pub updated: Instant,
}

// Constructors and draw:
impl StackNode {
    /// Create a stack node with an output image and a list of layer images.
    pub fn new(
        resources: RenderResources,
        source_id: String,
    ) -> Self {
        // Create scene with empty layers.
        let scene: StackScene = StackScene::new(
            resources,
        );
        Self {
            class: NodeClass::Stack,
            family: NodeFamily::Source,
            id: source_id.to_owned(),
            name: source_id.to_owned(),
            scene,
            values: StackValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a StackNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw (
        &mut self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_inner: &mut Ui,
    ) {
    // Header:
        self.name = draw_node_header(
            self.class.clone(),
            self.id.clone(),
            self.name.clone(),
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Options:
        ui_inner.separator();
        Grid::new("Options").show(ui_inner, |ui_grid| {
            self.values.state = draw_node_state(self.values.state.clone(), ui_grid);
        });
        draw_node_options(
            Rc::clone(&options_gui),
            ui_inner,
        );
    // Output:
        ui_inner.separator();
        ui_inner.vertical_centered(|ui_source| {
            self.values.output_size = draw_node_output(
                event_loop_proxy.clone(),
                self.family.clone(),
                self.id.clone(),
                self.values.output_size,
                ui_source,
            );
            self.values.pos_out = ui_source.min_rect().max;
        });
    }

    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
        ui.add_space(PANEL_NODE_SPACING_SMALL);
    // Options:
        ui.label("Options:");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        Grid::new("Values").show(ui, |ui_grid| {
            // only play | pause button in one button???
            // state = draw_node_state(state.clone(), ui_grid);
            // ui_grid.end_row();
    // Sort:
            ui_grid.label("sort:");
            // if ui_grid.button("ABCxyz").clicked() {
            if ui_grid.button(" path ").clicked() {
                self.scene.layers = self.sort_layers_id();
            };
            if ui_grid.button(" name ").clicked() {
                self.scene.layers = self.sort_layers_name();
            };
            ui_grid.end_row();
    // FPS:
            self.values.fps = SourceFPS::show(
                self
                    .values
                    .fps,
                map_control_to_node
                    .clone(),
                midi_node_rc
                    .clone(),
                self
                    .id
                    .clone(),
                ui_grid,
            );
            ui_grid
                .end_row();
    // Quantity:
            draw_node_qty(
                self
                    .values
                    .fps,
                self
                    .scene
                    .layers
                    .len(),
                ui_grid,
            );
        });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
    // Pictures list:
        ui.label("Pictures:");
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.vertical_centered_justified(|ui_list| {
            for
                layer
            in
                self
                    .scene
                    .layers
                    .to_owned()
            {
                ui_list.label(layer.name);
            }
        });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
    }

    pub fn control_bind(
        &self,
        bind: String,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "fps" => {
                node.values.fps = result;
                map_source_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        SourceNode::Stack(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> SourceNode {
        let mut values: StackValues = self
            .values
            .clone();

        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "fps" => {
                        values.fps = SourceFPS::change_midi(
                            values
                                .fps,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        SourceNode::Stack(
            Self {
                values,
                ..self
                    .to_owned()
            }
        )
    }

}

// Helpers:
impl StackNode {
    fn sort_layers_id(
        &self,
    ) -> Vec<StackLayer> {
        let mut layers: Vec<StackLayer> = self.scene.layers.clone();
        layers.sort_by(|a, b| a.id.cmp(&b.id));
        layers
    }

    fn sort_layers_name(
        &self,
    ) -> Vec<StackLayer> {
        let mut layers: Vec<StackLayer> = self.scene.layers.clone();
        layers.sort_by(|a, b| a.name.cmp(&b.name));
        layers
    }

    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        resources: RenderResources,
    ) -> Self {
        let scene: StackScene = match
            self
                .values
                .state
        {
            NodeState::Stop | NodeState::Pause => self.scene,
            NodeState::Play => {
                self
                    .scene
                    .render(
                        command_buffer_allocator,
                        descriptor_set_allocator,
                        self.values.fps,
                        resources,
                    )
            },
        };
        Self {
            scene,
            ..self
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
