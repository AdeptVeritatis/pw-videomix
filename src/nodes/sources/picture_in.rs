#![allow(clippy::derivable_impls)]

use crate::{
    app::events::{
        gui_events_node::{
            NodeEvent,
            NodeEventClass,
        },
        GuiEvent,
    },
    constants::{
        PANEL_NODE_SPACING_SMALL,
        STANDARD_NODE_BACKGROUND,
    },
    impls::{
        files::{
            FilePath,
            TextureMessage,
        },
        vulkano::scenes::{
            copy::copy_data_to_image_scene::PictureScene,
            render::show_texture_scene::TextureScene,
        },
    },
    nodes::{
        NodeClass,
        NodeFamily,
    },
};
use egui_winit_vulkano::{
    egui::{
        epaint::PaintCallback,
        Frame,
        Grid,
        Pos2,
        Rect,
        Response,
        Sense,
        // TextureHandle,
        Ui,
        Vec2,
    },
    CallbackFn,
    RenderResources,
};
use std::{
    // fmt,
    sync::Arc,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

// #[derive(Clone)]
// pub struct PicturePreview {
//     aspect_ratio: f32,
//     texture: TextureHandle,
// }
//
// impl fmt::Debug for PicturePreview {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(
//             f,
//             "Preview [aspect_ratio: {:?}, texture_id from TextureHandle: {:?}]",
//             self.aspect_ratio,
//             self.texture.id(),
//         )
//     }
// }

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct PictureValues {
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    pub pos_out: Pos2,
}

impl Default for PictureValues {
    fn default() -> Self {
        Self {
            pos_out: Pos2::default(),
        }
    }
}

// ----------------------------------------------------------------------------

/// For single pictures opened or dropped in the app.
#[derive(Debug, Clone)]
pub struct PictureNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    pub path: FilePath,
    // Show image as preview.
    pub preview: TextureScene,
    // Source image buffer to connect sinks to.
    pub scene: PictureScene,
    pub values: PictureValues,
}

// Constructors and draw:
impl PictureNode {
    /// Create a picture node with an output image and a preview image.
    pub fn new(
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        id: String,
        message: TextureMessage,
        resources: RenderResources,
    ) -> Self {
        // PictureScene:
        // println!("test pic in before scene");
        let scene: PictureScene = PictureScene::new(
            command_buffer_allocator,
            message
                .image_data,
            resources
                .clone(),
            message
                .size,
        );

        // Preview:
        // println!("test pic in after scene");
        let preview = TextureScene::new(resources);
        Self {
            class: NodeClass::Picture,
            family: NodeFamily::Source,
            id,
            name: message
                .file_path
                .file_name
                .clone(),
            path: message
                .file_path
                .clone(),
            preview,
            scene,
            values: PictureValues::default(),
        }
    }

    /// Draw a PictureNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        ui_inner: &mut Ui,
    ) {
    // Caption:
        ui_inner.vertical_centered(|ui_source| {
            ui_source.small(self.path.file_name.clone());
            // Update position of input connection.
            self.values.pos_out = ui_source.min_rect().max;
        });
    // Frame:
        Frame::canvas(ui_inner.style())
            .fill(STANDARD_NODE_BACKGROUND)
            .show(ui_inner, |ui_canvas| {
                let sense: Response = self
                    .clone()
                    .render(
                        descriptor_set_allocator,
                        ui_canvas,
                    );
                // Context menu for preview.
                sense.context_menu(|ui_context| {
                    if ui_context.button("Remove PictureNode").clicked() {
                        ui_context.close_menu();
                        event_loop_proxy
                            .send_event(
                                GuiEvent::ChangeNode(
                                    NodeEvent {
                                        family: self.family.clone(),
                                        id: self.id.clone(),
                                        event_class: NodeEventClass::Remove,
                                    }
                                )
                            )
                            .ok();
                    }
                });
            });
    }

    pub fn draw_panel(
        &mut self,
        ui: &mut Ui,
    ) {
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
    // Info:
        ui
            .label(
                "Info:",
            );
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        ui
            .separator();
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
        Grid::new(
            "Info",
        )
            .max_col_width(
                120.0,
            )
            .show(
                ui,
                |ui_grid| {
            // Directory:
                    // use Function!!!
                    ui_grid
                        .label(
                            "directory:",
                        );
                    ui_grid
                        .small(
                        // .label(
                            self
                                .path
                                .full
                                // .as_str()
                                .rsplit_once(
                                    self
                                        .path
                                        .file_name
                                        .as_str()
                                )
                                .unwrap()
                                .0
                        );
                    ui_grid
                        .end_row();
            // // File name:
            //         // use Function!!!
            //         ui_grid
            //             .label(
            //                 "file name:",
            //             );
            //         ui_grid
            //             .label(
            //                 self
            //                     .path
            //                     .file_name
            //                     .as_str()
            //             );
            //         ui_grid
            //             .end_row();
            // Resolution:
                    // use Function!!!
                    ui_grid
                        .label(
                            "size:",
                        );
                    ui_grid
                        .label(
                            format!(
                                "{} * {}",
                                self
                                    .scene
                                    .size
                                    .x,
                                self
                                    .scene
                                    .size
                                    .y,
                            ),
                        );
                    ui_grid
                        .end_row();
                }
            );
        // ui.add_space(PANEL_NODE_SPACING_SMALL);
        // ui.separator();
        // ui.add_space(PANEL_NODE_SPACING_SMALL);
    }
}

// Helpers:
impl PictureNode {
    fn callback(
        self,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        // image_input: Arc<ImageView>,
        scene_rect: Rect,
        preview_size: Vec2,
    ) -> PaintCallback {
        let (viewport, callback_rect): (Viewport, Rect) = {
            let preview_width: f32 = preview_size.x;
            // File name label takes more space and forces node to constantly expand.
            let preview_height: f32 = preview_size.y;
            let image_aspect_ratio: f32 = self.scene.size.x * self.scene.size.y.recip(); // ???
            let extent: [f32; 2] = match
                preview_width * preview_height.recip() // preview_aspect_ratio
                > image_aspect_ratio
            {
                true => [
                    image_aspect_ratio * preview_height,
                    preview_height,
                ],
                false => [
                    preview_width,
                    image_aspect_ratio.recip() * preview_width,
                ],
            };
            let callback_rect = Rect::from_center_size(
                scene_rect.center(),
                Vec2::new(extent[0], extent[1]),
            );
            let viewport: Viewport = Viewport {
                offset: [
                    callback_rect.min.x,
                    callback_rect.min.y,
                ],
                extent,
                depth_range: 0.0..=1.0,
            };

            (viewport, callback_rect)
        };

        // Paint callback:
        PaintCallback {
            rect: callback_rect, //??????????????????????????????????
            // rect: ui_preview.available_rect_before_wrap(),
            callback: Arc::new(CallbackFn::new(move |_info, callback_context| {
                // let mut window_scene = window_scene.lock();
                self
                    .preview
                    .clone()
                    .render(
                        callback_context,
                        descriptor_set_allocator
                            .clone(),
                        self.scene.image_storage.clone(),
                        // image_storage.clone(),
                        viewport.clone(),
                    );
            })),
        }
    }

    fn render(
        self,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        ui_canvas: &mut Ui,
    ) -> Response {
        // Calculate size and create rect.
        let preview_size: Vec2 = Vec2::new(
            ui_canvas.available_width(),
            ui_canvas.available_height(),
        );
        // Needs a sense or preview will be non-interactive.
        let (scene_rect, sense) = ui_canvas.allocate_exact_size(
            preview_size,
            Sense::click(),
        );

        let paint_callback: PaintCallback = self
            .clone()
            .callback(
                descriptor_set_allocator,
                // self.scene.image_storage,
                scene_rect,
                preview_size,
            );
        ui_canvas.painter().add(paint_callback);
        sense
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// pub fn _get_image_size(rect: Rect, texture: TextureHandle) -> [f32; 2] {
//     match rect.aspect_ratio() > texture.aspect_ratio() {
//         // height bound:
//         true => [texture.aspect_ratio() * rect.max.y, rect.max.y],
//         // width bound:
//         false => [rect.max.x, texture.aspect_ratio().recip() * rect.max.x],
//     }
// }
