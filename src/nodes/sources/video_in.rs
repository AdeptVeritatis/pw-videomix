#![allow(clippy::derivable_impls)]

use crate::{
    constants::{
        PANEL_NODE_SPACING_SMALL,
        PANEL_NODE_SPACING_TITLE,
    },
    impls::{
        files::FilePath,
        gstreamer::pipeline_video_in::VideoPipeline,
        vulkano::scenes::copy::video_scene::VideoScene,
    },
    nodes::{
        NodeClass,
        NodeFamily,
    },
};
use egui_winit_vulkano::{
    egui::{
        Color32,
        Pos2,
        RichText,
        Ui,
        Vec2,
    },
    RenderResources,
};
use gstreamer::ClockTime;
use std::{
    fmt,
    sync::Arc,
    time::{
        Duration,
        Instant,
    },
};
use vulkano::command_buffer::allocator::StandardCommandBufferAllocator;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub enum VideoState {
    Pause,
    Play,
    Stop,
}

impl fmt::Display for VideoState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Pause => write!(f, "paused"),
            Self::Play => write!(f, "playing"),
            Self::Stop => write!(f, "stopped"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct VideoValues {
    pub state: VideoState,
    // Position of the output source connector from the node.
    // Gets updated during drawing.
    pub pos_out: Pos2,
}

impl Default for VideoValues {
    fn default() -> Self {
        Self {
            state: VideoState::Play,
            pos_out: Pos2::default(),
        }
    }
}

// ----------------------------------------------------------------------------

/// For videos opened or dropped in the app.
#[derive(Debug, Clone)]
pub struct VideoNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    pub path: FilePath,
    pub pipeline: VideoPipeline,
    // Show image as preview.
    // pub preview: TextureScene,
    // Source image buffer to connect sinks to.
    pub scene: Option<VideoScene>,
    pub values: VideoValues,

    pub created: Instant,
    pub updated: Instant,
    pub duration_update: Duration,
    pub offset_node: Duration,
}

// Constructors and draw:
impl VideoNode {
    /// Create a video node with an output image.
    pub fn new(
        id: String,
        file_path: FilePath,
    ) -> Self {
        let created: Instant = Instant::now();
        let updated: Instant = created.clone();
        // GStreamer pipeline:
        let pipeline: VideoPipeline = VideoPipeline::new(
            file_path.clone(),
        );
        Self {
            class: NodeClass::Video,
            family: NodeFamily::Source,
            id,
            name: file_path.file_name.clone(),
            path: file_path.clone(),
            pipeline,
            // preview,
            scene: None,
            values: VideoValues::default(),

            created,
            updated,
            duration_update: Duration::new(0, 0),
            offset_node: Duration::new(0, 0),
        }
    }

    /// Draw a VideoNode on a Ui.
    /// What you see as a node on the main panel.
    pub fn draw(
        &mut self,
        // event_loop_proxy: EventLoopProxy<GuiEvent>,
        ui_inner: &mut Ui,
    ) {
    // Caption:
        ui_inner.add_space(PANEL_NODE_SPACING_TITLE);
        ui_inner.vertical_centered(|ui_source| {
            ui_source.small(self.path.file_name.clone());
            // Update position of input connection.
            self.values.pos_out = ui_source.min_rect().max;
        });
        ui_inner.add_space(PANEL_NODE_SPACING_TITLE);
    }

    /// The side panel of a PwInputNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        ui: &mut Ui,
    ) {
    // Hint:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.heading(
            RichText::new("! experimental!")
                .color(Color32::RED)
        );
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(
            format!(
                "state: {}",
                self
                    .values
                    .state,
            )
        );
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(
            format!(
                "duration update: {}",
                self
                    .duration_update
                    .as_micros(),
            )
        );
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(
            format!(
                "offset node: {}",
                self
                    .offset_node
                    .as_micros(),
            )
        );
        ui.add_space(PANEL_NODE_SPACING_SMALL);
    }
}

// Helpers:
impl VideoNode {
    pub fn update(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        resources: RenderResources,
        updated_app: Instant,
    ) -> Self {
        let updated: Instant = Instant::now();
        // let last_update: Instant = self.updated;
        let offset_node: Duration = updated.duration_since(updated_app);

        let pipeline: VideoPipeline = self.pipeline.to_owned();
        let appsink = pipeline.appsink.clone(); // clone??? not in a gst-closure, so no ref-cycle???
        let mut scene: Option<VideoScene> = self.scene.clone();
        // let values: VideoValues = self.values.clone();

        match
            scene
                .clone()
        {
    // Pull preroll:
            None => match
                appsink
                    .try_pull_preroll(ClockTime::ZERO)
            {
                None => (),
                Some(sample) => {
    // Create scene and image:
                    let caps_ref = sample
                        .caps()
                        .unwrap();
                    let (height, width): (i32, i32) = match
                        caps_ref
                            .structure(0)
                    {
                        None => (480, 480), // ?????????????????????????
                        Some(structure) => (
                            structure
                                .get::<i32>("height")
                                .unwrap(),
                            structure
                                .get::<i32>("width")
                                .unwrap(),
                        ),
                    };
                    scene = Some(
                        VideoScene::new(
                            resources.clone(),
                            Vec2::new(width as f32, height as f32),
                        )
                    );
                },
            },
            Some(video_scene) => {
    // Pull sample:
                match
                    appsink
                        // .try_pull_sample(ClockTime::ZERO)
                        .pull_sample()
                {
                    Err(_error) => {
                        // println!("ii is eos: {}", pipeline.appsink.is_eos());
                        // println!("!! error: {_error}");
    // Restart video:
                        pipeline
                            .restart();
                    },
                    Ok(sample) => {
    // Decode frame:
                        scene = Some(
                            video_scene
                                .refresh(
                                    command_buffer_allocator,
                                    sample
                                        .buffer()
                                        .unwrap()
                                        .map_readable()
                                        .unwrap()
                                        .as_slice()
                                        .to_owned(),
                                    resources.clone(),
                                )
                        );
                    },
                };
            },
        };
        let duration_update: Duration = Instant::now().duration_since(updated);
        Self {
            pipeline,
            scene,
            // values,
            updated,
            duration_update,
            offset_node,
            ..self.to_owned()
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
