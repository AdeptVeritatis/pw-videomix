
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SourceToNode,
        },
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
        gui::{
            bind::{
                elements::sink_button::SinkRecordVideo,
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        ENCODER_FILE_STRING,
        ENCODER_FPS,
        PANEL_NODE_SPACING_SMALL,
        // PANEL_NODE_SPACING_TITLE,
        STANDARD_NODE_SIZE,
    },
    impls::{
        gstreamer::{
            pipeline_video_out::EncoderPipeline,
            profiles::EncoderProfile,
        },
        midir::{
            // MidiClass,
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        vulkano::scenes::copy::copy_connection_scene::ConnectionScene,
    },
    nodes::{
        connections::InputConnection,
        NodeClass,
        NodeFamily,
        draw_node_connect_single,
        draw_node_header,
        draw_node_options,
    },
};
use egui_winit_vulkano::{
    egui::{
        Color32,
        ComboBox,
        Context,
        DragValue,
        Grid,
        Response,
        RichText,
        Sense,
        // TextBuffer,
        TextEdit,
        Ui,
        Vec2,
        Window,
    },
    RenderResources,
};
use gstreamer::{
    prelude::ElementExt,
    ClockTime,
    State,
};
use rfd::FileDialog;
use std::{
    cell::RefCell,
    fmt,
    ops::RangeInclusive,
    path::{
        // Path,
        PathBuf,
    },
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    buffer::Subbuffer,
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        CopyImageToBufferInfo,
        PrimaryCommandBufferAbstract,
    },
    image::Image,
    sync::GpuFuture,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
pub enum EncoderContainer {
    Mkv,
    WebMvp8,
    WebMvp9
}

impl fmt::Display for EncoderContainer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Mkv => write!(f, "mkv"),
            Self::WebMvp8 => write!(f, "webm (vp8)"),
            Self::WebMvp9 => write!(f, "webm (vp9)"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
pub enum EncoderMode {
    // Stops after a certain amount of time.
    Duration,
    // Stops after a certain amount of frames.
    Frames,
    // Unlimited mode, but scales changing size of input images.
    // Scaling,
    // No automatic stop. (Will start new video files everytime the size changes. not implemented yet)
    Unlimited,
}

impl fmt::Display for EncoderMode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Duration => write!(f, "Duration"),
            Self::Frames => write!(f, "Frames"),
            // Self::Scaling => write!(f, "Scaling"),
            Self::Unlimited => write!(f, "unlimited"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct EncoderValues {
    pub container_format: EncoderContainer,
    pub directory: PathBuf,
    pub file_counter: usize,
    pub file_decimals: usize,
    pub file_string: String,
    pub frame: u32,
    pub max_frames: u32,
    pub max_time: u32,
    pub mode: EncoderMode,
    pub source: Option<InputConnection>,
    // May be selectable later for different codecs, fps and more.
    pub video_profile: EncoderProfile,
}

impl Default for EncoderValues {
    fn default() -> Self {
        Self {
            container_format: EncoderContainer::Mkv,
            directory: PathBuf::default(),
            file_counter: 0,
            file_decimals: 3,
            file_string: String::from(ENCODER_FILE_STRING),
            frame: 0,
            max_frames: 0,
            max_time: 0,
            mode: EncoderMode::Unlimited,
            source: None,
            video_profile: EncoderProfile::default(),
        }
    }
}

// ----------------------------------------------------------------------------

/// View the content of a node in the main panel.
#[derive(Debug, Clone)]
pub struct EncoderNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    pub pipeline: Option<EncoderPipeline>,
    pub values: EncoderValues,
    pub updated: Instant,
}

// Constructor and draw:
impl EncoderNode {
    pub fn new(
        id: String,
    ) -> Self {
        let name: String = id.clone();
        Self {
            class: NodeClass::Encoder,
            family: NodeFamily::Sink,
            id,
            name,
            pipeline: None,
            values: EncoderValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw an EncoderNode on a Ui.
    /// What you see as a node in the main panel.
    pub fn draw(
        &mut self,
        context: Context,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        // midi_node_rc: Rc<RefCell<MidiNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
    ) {
        match
            Window::new(self.name.clone())
                .default_size(Vec2::new(STANDARD_NODE_SIZE, STANDARD_NODE_SIZE))
                .min_width(100.0) // so source_id does not wrap!!!!!!!!!!!!
                .resizable(true)
                .title_bar(false)
                .show(&context, |ui_encoder| {
                    ui_encoder.vertical_centered(|ui_inner| {
        // Header:
                        self.name = draw_node_header(
                            self.class.clone(),
                            self.id.clone(),
                            self.name.clone(),
                            Rc::clone(&options_gui),
                            ui_inner,
                        );
        // Options:
                        draw_node_options(
                            Rc::clone(&options_gui),
                            ui_inner,
                        );
        // Record/stop button:
                        // ui_inner.separator();
                        // ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                        //
                        // let response = ui_inner
                        //     .add(
                        //         Button::new(
                        //             match
                        //                 values
                        //                     .pipeline
                        //             {
                        //                 None => {},
                        //                 Some(_) => {
                        //                     format!("Record [  ] ( {} )", counter)
                        //                 },
                        //             }
                        //             )
                        //             .sense(Sense::click())
                        //     );
                        // if response.clicked() {
                        //     self
                        //         .clone()
                        //         .snapshot(
                        //             counter,
                        //             resources,
                        //         );
                        //     counter += 1;
                        // }
                        // ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
        // Input:
                        // Show the connect widget.
                        ui_inner
                            .separator();
                        self.values.source = draw_node_connect_single(
                            Rc::clone(&map_filter_to_node),
                            Rc::clone(&map_source_to_node),
                            self.class.clone(),
                            self.family.clone(),
                            self.id.clone(),
                            self.name.clone(),
                            self.values.source.clone(),
                            ui_area,
                            ui_inner,
                        );
                    });
                })
        {
            None => (),
            Some(inner_response) => {
                if
                    inner_response
                        .response
                        .interact(
                            Sense::click(),
                        )
                        .clicked()
                {
                    event_loop_proxy
                        .send_event(
                            GuiEvent::ChangeNode(
                                NodeEvent {
                                    family: self.family.clone(),
                                    id: self.id.clone(),
                                    event_class: NodeEventClass::Select,
                                }
                            )
                        )
                        .ok();
                }
                inner_response
                    .response
                    .context_menu(|ui_context| {
                        if
                            ui_context
                                .button("Remove EncoderNode")
                                .clicked()
                        {
                            ui_context
                                .close_menu();
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: self.family.clone(),
                                            id: self.id.clone(),
                                            event_class: NodeEventClass::Remove,
                                        }
                                    )
                                )
                                .ok();
                        }
                    });
            },
        };
    }

    /// The side panel of a EncoderNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        resources: RenderResources,
        ui: &mut Ui,
    ) {
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("! experimental !").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("expect dropped frames\nand timing errors").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("built for 60 fps").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
    // Options:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.vertical_centered(|ui_header| {
            ui_header.label("Options:")
        });

        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);

        Grid::new("Encoder")
            .max_col_width(200.0)
            .show(ui, |ui_grid| {
                match
                    self
                        .pipeline
                        .to_owned()
                {
            // No pipeline created, select values:
                    None => {
                        ui_grid.label("folder:");
                        match
                            self
                                .values
                                .directory
                                .file_name()
                        {
                            None => ui_grid.label(RichText::new("! no folder selected !").color(Color32::RED)),
                            Some(os_str) => match
                                os_str.to_str()
                            {
                                None => ui_grid.label(RichText::new("! path with unreadable characters selected !").color(Color32::RED)), // ???
                                Some(final_component) => ui_grid.label(final_component),
                            },
                        };
                        ui_grid.end_row();
                        ui_grid.label("");
                        if ui_grid.button(" select ").clicked() {
                            match FileDialog::new()
                                .set_title("Select Folder - pw-videomix")
                                .pick_folder()
                            {
                                None => (),
                                Some(folder) => {
                                    println!("-- folder selected: {:?}", folder);
                                    self.values.directory = folder;
                                },
                            };
                        }
                        // ui_grid.small(directory.display().to_string());
                        ui_grid.end_row();
                        ui_grid.label("file name:");
                        ui_grid
                            .label(
                                match
                                    self
                                        .values
                                        .file_decimals
                                {
                                    0 => format!(
                                        "{}.{}",
                                        self
                                            .values
                                            .file_string,
                                        self
                                            .values
                                            .video_profile.extension,
                                    ),
                                    // use function!!!
                                    _ => format!(
                                        "{}{:0decimals$}.{}",
                                        self
                                            .values
                                            .file_string,
                                        self
                                            .values
                                            .file_counter,
                                        self
                                            .values
                                            .video_profile.extension,
                                        decimals = self
                                            .values
                                            .file_decimals,
                                    ),
                                }
                            );

                        ui_grid.end_row();
                        ui_grid.end_row();

                        ui_grid.label("file prefix:");
                        ui_grid
                            .add(
                                TextEdit::singleline(
                                    &mut self
                                        .values
                                        .file_string
                                )
                                .hint_text("File name")
                                .desired_width(120.0),
                            );
                        ui_grid.end_row();
                        ui_grid.label("file number:");
                        ui_grid.add(
                            DragValue::new(
                                &mut self
                                    .values
                                    .file_counter
                            )
                                .fixed_decimals(0)
                                .range(
                                    RangeInclusive::new(
                                        0,
                                        match
                                            self
                                                .values
                                                .file_decimals
                                        {
                                            1 => 9,
                                            2 => 99,
                                            3 => 999,
                                            4 => 9999,
                                            5 => 99999,
                                            6 => 999999,
                                            _ => 0,
                                        },
                                    )
                                )
                                .speed(1),
                        );
                        ui_grid.end_row();
                        ui_grid
                            .label("file decimals:");
                        ui_grid.add(
                            DragValue::new(
                                &mut self
                                    .values
                                    .file_decimals,
                            )
                                .fixed_decimals(0)
                                .range(RangeInclusive::new(0, 6))
                                .speed(0.1),
                        );
                        // ui_grid.end_row();
                    },
            // Pipeline already created, only show values:
                    Some(_encoder_pipeline) => {
                        ui_grid.label("folder:");
                        ui_grid.label(
                            self
                                .values
                                .directory
                                .file_name()
                                .unwrap()
                                .to_str()
                                .unwrap()
                            // directory.display().to_string()
                        );
                        ui_grid.end_row();
                        ui_grid.end_row();
                        ui_grid.label("file name:");
                        ui_grid.label(
                                match
                                    self
                                        .values
                                        .file_decimals
                                {
                                    0 => format!(
                                        "{}.{}",
                                        self
                                            .values
                                            .file_string,
                                        self
                                            .values
                                            .container_format,
                                    ),
                                    // use function!!!
                                    _ => format!(
                                        "{}{:0decimals$}.{}",
                                        self
                                            .values
                                            .file_string,
                                        self
                                            .values
                                            .file_counter,
                                        self
                                            .values
                                            .video_profile
                                            .extension,
                                        decimals = self
                                            .values
                                            .file_decimals,
                                    ),
                                }
                            );

                        ui_grid.end_row();
                        ui_grid.end_row();

                        ui_grid.label("file prefix:");
                        ui_grid.label(self.values.file_string.clone());
                        ui_grid.end_row();
                        ui_grid.label("file number:");
                        ui_grid.label(self.values.file_counter.to_string());
                        ui_grid.end_row();
                        ui_grid.label("file decimals:");
                        ui_grid.label(self.values.file_decimals.to_string());
                        // ui_grid.end_row();
                    },
                };
            });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        match
            self
                .pipeline
                .to_owned()
        {
            None => {
                ComboBox::from_id_salt(format!("ComboBox {} profile", self.id))
                    .selected_text(format!("profile: {}", self.values.container_format))
                    .wrap()
                    // .width(120.0) // ???
                    .show_ui(ui, |ui_combo| {
                        for
                            container
                        in
                            [
                                EncoderContainer::Mkv,
                                EncoderContainer::WebMvp8,
                                EncoderContainer::WebMvp9,
                            ]
                        {
                            ui_combo.selectable_value(
                                &mut self
                                    .values
                                    .container_format,
                                container.clone(),
                                container.to_string(),
                            );
                        };
                        self.values.video_profile = EncoderProfile::select(self.values.container_format.to_owned());
                    });
            },
            Some(_) => {
                ui.vertical(|ui_label| {
                    ui_label.label(format!("profile: {}", self.values.container_format));
                });
            },
        };

        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);

    // Frames:
        match
            self
                .pipeline
                .to_owned()
        {
            None => {
                ComboBox::from_id_salt(format!("ComboBox {} mode", self.id))
                    .selected_text(format!("mode: {}", self.values.mode))
                    .wrap()
                    // .width(120.0) // ???
                    .show_ui(ui, |ui_combo| {
                        for
                            encoder_mode
                        in
                            [
                                EncoderMode::Duration,
                                EncoderMode::Frames,
                                // EncoderMode::Scaling,
                                EncoderMode::Unlimited,
                            ]
                        {
                            ui_combo.selectable_value(
                                &mut self
                                    .values
                                    .mode,
                                encoder_mode
                                    .clone(),
                                encoder_mode
                                    .to_string(),
                            );
                        };
                    });
                ui.add_space(PANEL_NODE_SPACING_SMALL);
                Grid::new("Frames-stopped")
                    .max_col_width(200.0)
                    .show(ui, |ui_grid| {
                        ui_grid.label("frames:");
                        ui_grid.add(
                            DragValue::new(&mut self
                                .values
                                .max_frames
                            )
                                .fixed_decimals(0)
                                .range(RangeInclusive::new(0, 432000)) // 2h@60fps
                                .speed(0.5),
                        );
                        ui_grid.end_row();
                        ui_grid.label("duration:");
                        ui_grid.add(
                            DragValue::new(&mut self
                                .values
                                .max_time
                            )
                                .fixed_decimals(0)
                                .range(RangeInclusive::new(0, 7200)) // 2h in sec
                                .speed(0.5)
                                .suffix("s"),
                        );
                        ui_grid.end_row();
                        ui_grid.label("");
                        ui_grid.label(
                            format!("@{ENCODER_FPS} fps")
                        );
                        // ui_grid.end_row();
                    });
            },
            Some(encoder_pipeline) => {
                ui.vertical(|ui_label| {
                    ui_label.label(format!("mode: {}", self.values.mode));
                });
                ui.add_space(PANEL_NODE_SPACING_SMALL);
                Grid::new("Frames-recording")
                    .max_col_width(200.0)
                    .show(ui, |ui_grid| {
                        ui_grid.label("frames:");
                        ui_grid.label(
                            format!(
                                "{} | {}",
                                self.values.max_frames,
                                encoder_pipeline.frame,
                            )
                        );
                        ui_grid.end_row();
                        ui_grid.label("duration:");
                        let duration: ClockTime = encoder_pipeline.frame * encoder_pipeline.pts_diff;
                        ui_grid.label(format!(
                            "{} | {:02}:{:02}:{:02}.{:03}",
                            self.values.max_time,
                            duration.hours(),
                            duration.minutes(),
                            duration.seconds(),
                            duration.mseconds() % 1000,
                        ));
                        ui_grid.end_row();
                        ui_grid.label("");
                        ui_grid.label(
                            format!(
                                "@{} fps",
                                encoder_pipeline
                                    .video_info
                                    .fps(),
                            )
                        );
                        // ui_grid.end_row();
                    });
            },
        };
        ui.add_space(PANEL_NODE_SPACING_SMALL);

    // Record and stop button:
        let label: String = match
            self
                .pipeline
                .to_owned()
        {
            None => "Record Video [ 🎥 ⏺️ ]", // "Record Video [ 🎥 📹 📼 ⏺️ ]"
            Some(_encoder_pipeline) => "Stop Recording [ ⏹️ ]",
                // match encoder_pipeline.pipeline
                //     .state(ClockTime::ZERO) // why zero and not none???
                //     .1
                // { State::Playing => "Pause Recording [ ⏸️ ]",
                //     _ => "Record Video [ 🎥 ⏺️ ]",}
        }
            .to_string();

        let response: Response = SinkRecordVideo::show(
            label,
            map_control_to_node
                .clone(),
            midi_node_rc
                .clone(),
            self
                .id
                .clone(),
            ui,
        );

        if
            response
                .clicked()
        {
            (
                self.pipeline,
                self.values,
            ) = self
                .record_button_pressed(
                    self
                        .pipeline
                        .to_owned(),
                    resources,
                    self
                        .values
                        .to_owned(),
                );
        }
        ui.add_space(PANEL_NODE_SPACING_SMALL);
    }

    pub fn control_bind(
        &mut self,
        bind: String,
        resources: RenderResources,
    ) {
        match
            bind
                .as_str()
        {
            "record_video" => {
                // ???????????????????????????????????????????????????????????????????????

                (
                    self.pipeline,
                    self.values,
                ) = self
                    .record_button_pressed(
                        self
                            .pipeline
                            .to_owned(),
                        resources,
                        self
                            .values
                            .to_owned(),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &mut self,
        midi_bind: &MidiBind,
        _midi_event: MidiMessage,
        // resources: RenderResources,
    ) {
        let _controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "record_video" => {
                        // (
                        //     self.pipeline,
                        //     self.values,
                        // ) = self
                        //     .record_button_pressed(
                        //         self
                        //             .pipeline
                        //             .to_owned(),
                        //         resources,
                        //         self
                        //             .values
                        //             .to_owned(),
                        //     );
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        // SinkNode::Encoder(
        //     self
        //         .to_owned()
        // )
    }

}

// Helpers:
impl EncoderNode {
    pub fn update(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        resources: RenderResources,
    ) -> Self {
        match
            self
                .values
                .source
                .clone()
        {
            // Encoder has no connection.
            None => self.to_owned(),
            // Encoder has a connection.
            Some(mut connection) => {
                // Update ConnectionScene.
                let scene: ConnectionScene = connection.scene
                    .clone()
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone(),
                    );

                let mut pipeline: Option<EncoderPipeline> = self.pipeline.to_owned();
                let mut values: EncoderValues = self.values.clone();
                let mut file_counter: usize = values.file_counter;
                let frame: u32 = values.frame;
                let max_frames: u32 = values.max_frames;
                let max_time: u32 = values.max_time;

        // Encode frame:
                match
                    connection
                        .clone()
                        .scene
                        .image_sink
                {
                    None => println!("!! error: could not get image from connection"),
                    Some(image_view) => {
                        match
                            pipeline
                                .clone()
                        {
                            None => (),
                            Some(mut encoder_pipeline) => {
                                // timing based misses a lot of frames???
                                // if
                                //     now
                                //     >=
                                //     updated + pts_diff.into()
                                // {}

                                let image: Arc<Image> = image_view
                                    .image()
                                    .to_owned();
                                let vulkano_buffer: Subbuffer<[u8]> = encoder_pipeline
                                    .vulkano_buffer
                                    .clone();
                                let mut command_buffer_builder =
                                    AutoCommandBufferBuilder::primary(
                                        command_buffer_allocator,
                                        // resources.command_buffer_allocator,
                                        resources.queue.queue_family_index(),
                                        CommandBufferUsage::OneTimeSubmit,
                                    )
                                    .unwrap();
                                command_buffer_builder
                                    .copy_image_to_buffer(
                                        CopyImageToBufferInfo::image_buffer(
                                            image,
                                            vulkano_buffer.clone(),
                                        )
                                    )
                                    .unwrap();
                                let command_buffer = command_buffer_builder
                                    .build()
                                    .unwrap();
                                let finished = command_buffer.execute(resources.queue.clone()).unwrap();
                                finished
                                    .then_signal_fence_and_flush()
                                    .unwrap()
                                    .wait(None)
                                    .unwrap();

                                encoder_pipeline = encoder_pipeline
                                    .update(
                                        vulkano_buffer,
                                    );

                                // let bus = encoder_pipeline
                                //     .pipeline
                                //     .bus()
                                //     .unwrap();
                                // for msg in bus.iter() {
                                //     match msg.view() {
                                //         MessageView::Eos(_) => (),
                                //         MessageView::Error(_) => (),
                                //         MessageView::StateChanged(_state) => {
                                //             // println!("ii {:?}: {:?} -> {:?}", _state.src().unwrap(), _state.old(), _state.current());
                                //             // println!("ii {:?} -> {:?}", _state.old(), _state.current());
                                //         },
                                //         MessageView::Buffering(_buffering) => println!("{_buffering:?}"),
                                //         _ => (),
                                //     }
                                // }
                                // let state = encoder_pipeline.pipeline.state(ClockTime::NONE);
                                // println!("{:?} - {:?}", state.1, state.2);
                                pipeline = match
                                    values
                                        .mode
                                {
                                    EncoderMode::Duration => {
                                        match
                                            encoder_pipeline
                                                .frame
                                        {
                                            frame if
                                                frame * encoder_pipeline.pts_diff
                                                >=
                                                ClockTime::from_seconds(max_time as u64)
                                            => {
                                                println!("[] stopped recording");
                                                encoder_pipeline
                                                    .pipeline
                                                    .set_state(State::Null)
                                                    .unwrap();
                                                file_counter += 1;
                                                None
                                            },
                                            _ => Some(encoder_pipeline),
                                        }
                                    },
                                    EncoderMode::Frames => {
                                        match
                                            encoder_pipeline
                                                .frame
                                        {
                                            frame if
                                                frame
                                                >= max_frames as u64
                                            => {
                                                println!("[] stopped recording");
                                                encoder_pipeline
                                                    .pipeline
                                                    .set_state(State::Null)
                                                    .unwrap();
                                                file_counter += 1;
                                                None
                                            },
                                            _ => Some(encoder_pipeline),
                                        }
                                    },
                                    // EncoderMode::Scaling => Some(encoder_pipeline),
                                    EncoderMode::Unlimited => Some(encoder_pipeline),
                                };
                            },
                        };
                    },
                };
            // Store scene.
                connection = InputConnection {
                    scene,
                    ..connection
                };
            // Store values.
                values = EncoderValues {
                    file_counter,
                    frame,
                    source: Some(connection),
                    ..values
                };
                Self {
                    pipeline,
                    values,
                    updated: Instant::now(),
                    ..self.to_owned()
                }
            },
        }
    }

}


impl EncoderNode {
    fn record_button_pressed(
        &self,
        mut pipeline: Option<EncoderPipeline>,
        resources: RenderResources,
        mut values: EncoderValues,
    ) -> (
        Option<EncoderPipeline>,
        EncoderValues,
    ) {
        match
            pipeline
        {
// Record button:
            None => {
                match
                    values
                        .source
                        .clone()
                {
                    None => {
                        println!("!! encoder node not connected");
                    },
                    Some(connection) => {
                        match
                            values
                                .directory
                                .is_dir()
                                // .file_name()
                        {
                            false => {
                                println!(
                                    "!! no folder selected {}",
                                    values
                                        .directory
                                        .display(),
                                )
                            },
                            true => {
                                // use function!!!
                                let file_name: String = format!(
                                    "{}{:0decimals$}.{}",
                                    values
                                        .file_string,
                                    values
                                        .file_counter,
                                    values
                                        .video_profile.extension,
                                    decimals = values
                                        .file_decimals,
                                );
                                let file_path: PathBuf = values
                                    .directory
                                    .as_path()
                                    .join(
                                        PathBuf::from(file_name.clone())
                                    );
                                match
                                    file_path
                                        .exists()
                                {
                                    true => {
                                        println!("!! video file already exists: {file_name}");
                                        values.file_counter += 1;
                                    },
                                    false => {
                                        match
                                            file_path
                                                .into_os_string()
                                                .to_str()
                                        {
                                            None => {
                                                println!("!! could not read file path");
                                            },
                                            Some(output_file) => {
                                                println!("() recording to file: {output_file}");
                                                let encoder_pipeline = EncoderPipeline::new(
                                                        connection.clone(),
                                                        String::from(output_file),
                                                        resources.clone(),
                                                        self.values.clone(),
                                                    );
                                                encoder_pipeline
                                                    .pipeline
                                                    .set_state(State::Playing)
                                                    .unwrap();
                                                pipeline = Some(encoder_pipeline);
                                            },
                                        };
                                    },
                                };
                            },
                        };
                    },
                };
            },
// Stop button:
            Some(encoder_pipeline) => {
                println!("[] stopped recording");
                encoder_pipeline
                    .appsrc
                    .end_of_stream()
                    .unwrap();
                encoder_pipeline
                    .pipeline
                    .set_state(State::Null)
                    .unwrap();
                pipeline = None;
                values.file_counter += 1;

                // match
                //     encoder_pipeline
                //         .pipeline
                //         .state(ClockTime::NONE) // None or Zero???
                //         .1
                // {
                //     State::VoidPending => (),
                //     State::Null => (),
                //     State::Ready => (),
                //     State::Paused => {
                //         encoder_pipeline
                //             .pipeline
                //             .set_state(State::Playing)
                //             .unwrap();
                //     },
                //     State::Playing => {
                //         encoder_pipeline
                //             .pipeline
                //             .set_state(State::Paused)
                //             .unwrap();
                //     },
                // };
                // pipeline = Some(encoder_pipeline);
            },
        };

        (
            pipeline,
            values,
        )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------


