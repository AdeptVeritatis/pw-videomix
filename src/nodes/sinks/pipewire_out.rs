
use crate::{
    app::{
        // context_menu::{
        //     // label_with_midi,
        //     midi_context_menu,
        // },
        data::app_nodes::{
            FilterToNode,
            SourceToNode,
        },
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
        gui::options::GuiOptions,
    },
    constants::{
        // ENCODER_FPS,
        PANEL_NODE_SPACING_SMALL,
        // PANEL_NODE_SPACING_TITLE,
        STANDARD_NODE_SIZE,
    },
    impls::{
        midir::{
            // MidiBind,
            // MidiClass,
            MidiNode,
        },
        pipewire::PipewireStream,
        vulkano::scenes::copy::{
            copy_connection_scene::ConnectionScene,
            pw_output_scene::PipewireOutputScene,
        },
    },
    nodes::{
        connections::InputConnection,
        NodeClass,
        NodeFamily,
        draw_node_connect_single,
        draw_node_header,
        draw_node_options,
    },
};
use egui_winit_vulkano::{
    egui::{
        // Button,
        Color32,
        // ComboBox,
        Context,
        // DragValue,
        // Grid,
        RichText,
        Sense,
        // TextBuffer,
        // TextEdit,
        Ui,
        Vec2,
        Window,
    },
    RenderResources,
};
use pipewire::core::Core;
// use rfd::FileDialog;
use std::{
    cell::RefCell,
    collections::HashMap,
    // fmt,
    // ops::RangeInclusive,
    // path::{
    //     // Path,
    //     PathBuf,
    // },
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    buffer::{
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
    //     AutoCommandBufferBuilder,
    //     CommandBufferUsage,
    //     CopyImageToBufferInfo,
    //     PrimaryCommandBufferAbstract,
    },
    // image::Image,
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    // sync::GpuFuture,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct PwOutputValues {
    // pub frame: u32,
    pub source: Option<InputConnection>,
}

impl Default for PwOutputValues {
    fn default() -> Self {
        Self {
            source: None,
        }
    }
}

// ----------------------------------------------------------------------------

/// View the content of a node in the main panel.
#[derive(Debug, Clone)]
pub struct PwOutputNode { // PipewireOutputNode
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    // pub pipeline: Option<PwOutputPipeline>,
    pub scene: PipewireOutputScene,
    pub values: PwOutputValues,
    pub updated: Instant,
}

// Constructor and draw:
impl PwOutputNode {
    pub fn new(
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        pw_core: Core,
        resources: RenderResources,
        sink_id: String,
    ) -> Self {
        let scene: PipewireOutputScene = PipewireOutputScene::new(
            event_loop_proxy,
            map_node_to_stream,
            pw_core,
            resources,
            sink_id.clone(),
        );
        Self {
            class: NodeClass::PipewireOutput,
            family: NodeFamily::Sink,
            id: sink_id.clone(),
            name: sink_id,
            // pipeline: None,
            scene,
            values: PwOutputValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw an PwOutputGstNode on a Ui.
    /// What you see as a node in the main panel.
    pub fn draw(
        &mut self,
        context: Context,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        // midi_node_rc: Rc<RefCell<MidiNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
    ) {
        match
            Window::new(self.name.clone())
                .default_size(Vec2::new(STANDARD_NODE_SIZE, STANDARD_NODE_SIZE))
                .min_width(100.0) // so source_id does not wrap!!!!!!!!!!!!
                .resizable(true)
                .title_bar(false)
                .show(&context, |ui_encoder| {
                    ui_encoder.vertical_centered(|ui_inner| {
        // Header:
                        self.name = draw_node_header(
                            self.class.clone(),
                            self.id.clone(),
                            self.name.clone(),
                            Rc::clone(&options_gui),
                            ui_inner,
                        );
        // Options:
                        draw_node_options(
                            Rc::clone(&options_gui),
                            ui_inner,
                        );
        // Record/stop button:
                        ui_inner.separator();
                        ui_inner.add_space(PANEL_NODE_SPACING_SMALL);

                        ui_inner
                            .label(
                                RichText::new(
                                    "prototype stub\n!!! not complete !!!",
                                )
                                .color(
                                    Color32::RED,
                                ),
                            );
                        // let response = ui_inner
                        //     .add(
                        //         Button::new(
                        //             match
                        //                 values
                        //                     .pipeline
                        //             {
                        //                 None => {},
                        //                 Some(_) => {
                        //                     format!("Record [  ] ( {} )", counter)
                        //                 },
                        //             }
                        //             )
                        //             .sense(Sense::click())
                        //     );
                        // if response.clicked() {
                        //     self
                        //         .clone()
                        //         .snapshot(
                        //             counter,
                        //             resources,
                        //         );
                        //     counter += 1;
                        // }
                        ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
        // Input:
                        // Show the connect widget.
                        ui_inner
                            .separator();
                        self.values.source = draw_node_connect_single(
                            Rc::clone(&map_filter_to_node),
                            Rc::clone(&map_source_to_node),
                            self.class.clone(),
                            self.family.clone(),
                            self.id.clone(),
                            self.name.clone(),
                            self.values.source.clone(),
                            ui_area,
                            ui_inner,
                        );
                    });
                })
        {
            None => (),
            Some(inner_response) => {
                if
                    inner_response
                        .response
                        .interact(
                            Sense::click(),
                        )
                        .clicked()
                {
                    event_loop_proxy
                        .send_event(
                            GuiEvent::ChangeNode(
                                NodeEvent {
                                    family: self.family.clone(),
                                    id: self.id.clone(),
                                    event_class: NodeEventClass::Select,
                                }
                            )
                        )
                        .ok();
                }
                inner_response
                    .response
                    .context_menu(|ui_context| {
                        if
                            ui_context
                                .button("Remove PipeWire Output")
                                .clicked()
                        {
                            ui_context
                                .close_menu();
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: self.family.clone(),
                                            id: self.id.clone(),
                                            event_class: NodeEventClass::Remove,
                                        }
                                    )
                                )
                                .ok();
                        }
                    });
            },
        };
    }

    /// The side panel of a EncoderNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &self,
        _midi_node_rc: Rc<RefCell<MidiNode>>,
        _resources: RenderResources,
        ui: &mut Ui,
    ) -> Self {
        // let pipeline: Option<PwOutputPipeline> = self.pipeline.to_owned();
        // let mut values: PwOutputGstValues = self.values.clone();
        // let frame: u32 = values.frame;
        // let source: Option<InputConnection> = values.source.clone();

        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("! experimental !").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("expect dropped frames\nand timing errors").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.label(RichText::new("built for 60 fps").color(Color32::RED));
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
    // Options:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.vertical_centered(|ui_header| {
            ui_header.label("Options:")
        });

        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);

        // Grid::new("PwOutput")
        //     .max_col_width(200.0)
        //     .show(ui, |ui_grid| {
        //         match
        //             pipeline
        //                 .clone()
        //         {
        //     // No pipeline created, select values:
        //             None => {
        //                 ui_grid.label("...");
        //                 ui_grid.end_row();
        //             },
        //     // Pipeline already created, only show values:
        //             Some(_pw_output_gst_pipeline) => {
        //                 ui_grid.label("...");
        //                 ui_grid.end_row();
        //             },
        //         };
        //     });

        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);

    // Store changed values:
        // values = PwOutputGstValues {
        //     ..values
        // };
        Self {
            // pipeline,
            // values,
            ..self.to_owned()
        }
    }

}

// Helpers:
impl PwOutputNode {
    pub fn update(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        resources: RenderResources,
    ) -> Self {
        match
            self
                .values
                .source
                .clone()
        {
            // PwOutput has no connection.
            None => self.to_owned(),
            // PwOutput has a connection.
            Some(mut connection) => {
                // Update ConnectionScene.
                let scene: ConnectionScene = connection.scene
                    .clone()
                    .update(
                        command_buffer_allocator,
                        resources
                            .clone()
                    );

                // let mut pw_output_gst_pipeline: PwOutputGstPipeline = match
                //     self
                //         .pipeline
                //         .clone()
                // {
                //     None => {
                //         let pipeline = PwOutputGstPipeline::new(
                //             connection.clone(),
                //             resources.clone(),
                //         );
                //         pipeline
                //             .pipeline
                //             .set_state(State::Playing)
                //             .unwrap();
                //         pipeline
                //     },
                //     Some(pipeline) => pipeline,
                // };
                let mut values: PwOutputValues = self.values.clone();

        // Encode frame:
                match
                    connection
                        .clone()
                        .scene
                        .image_sink
                {
                    None => println!("!! error: could not get image from connection"),
                    Some(image_view) => {
                        // timing based misses a lot of frames???
                        // if
                        //     now
                        //     >=
                        //     updated + pts_diff.into()
                        // {}

                        // Update scene:
                        // let mut scene = self.scene.clone();
                        let extent = image_view.image().extent();
                        let size = extent[0] * extent[1] * 4;
                        // let size = 8192; // ?????????????????????????????????????????????????????????

                        let vulkano_buffer: Subbuffer<[u8]> = vulkano::buffer::Buffer::from_iter(
                            resources.memory_allocator.clone(),
                            BufferCreateInfo {
                                usage: BufferUsage::TRANSFER_DST,
                                ..Default::default()
                            },
                            AllocationCreateInfo {
                                memory_type_filter: MemoryTypeFilter::PREFER_HOST
                                    | MemoryTypeFilter::HOST_RANDOM_ACCESS,
                                ..Default::default()
                            },
                            (0..size).map(|_| 42u8), // 0u8
                        )
                        .unwrap();

                        // let image: Arc<Image> = image_view
                        //     .image()
                        //     .to_owned();
                        // let vulkano_buffer: Subbuffer<[u8]> = pw_output_pipeline
                        //     .vulkano_buffer
                        //     .clone();
                        // let mut command_buffer_builder =
                        //     AutoCommandBufferBuilder::primary(
                        //         resources.command_buffer_allocator,
                        //         resources.queue.queue_family_index(),
                        //         CommandBufferUsage::OneTimeSubmit,
                        //     )
                        //     .unwrap();
                        // command_buffer_builder
                        //     .copy_image_to_buffer(
                        //         CopyImageToBufferInfo::image_buffer(
                        //             image,
                        //             vulkano_buffer.clone(),
                        //         )
                        //     )
                        //     .unwrap();
                        // let command_buffer = command_buffer_builder
                        //     .build()
                        //     .unwrap();
                        // let finished = command_buffer.execute(resources.queue.clone()).unwrap();
                        // finished
                        //     .then_signal_fence_and_flush()
                        //     .unwrap()
                        //     .wait(None)
                        //     .unwrap();
                        //
                        // pw_output_pipeline = pw_output_pipeline
                        //     .update(
                        //         vulkano_buffer,
                        //     );

                        // Update stream:
                        match
                            map_node_to_stream
                                .borrow()
                                .get(&self.id)
                        {
                            None => (),
                            Some(PipewireStream::InputStream(_)) => (),
                            Some(PipewireStream::OutputStream(stream)) => {
                                stream
                                    // .to_owned()
                                    .update(
                                        vulkano_buffer.read().unwrap().as_ref(),
                                        // Vec::new(), // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    );
                            },
                        };
                    },
                };
            // Store scene.
                connection = InputConnection {
                    scene,
                    ..connection
                };
            // Store values.
                values = PwOutputValues {
                    source: Some(connection),
                    ..values
                };
                Self {
                    // pipeline: Some(pw_output_gst_pipeline),
                    values,
                    updated: Instant::now(),
                    ..self.to_owned()
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
