
use crate::{
    app::{
        data::app_nodes::{
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
        gui::options::GuiOptions,
    },
    constants::{
        WINDOW_NODE_SIZE,
        WINDOW_SINK_HEIGHT,
        WINDOW_SINK_WIDTH,
    },
    impls::vulkano::scenes::render::show_texture_scene::TextureScene,
    nodes::{
        connections::InputConnection,
        sinks::SinkNode,
        NodeClass,
        NodeFamily,
        NodeState,
        draw_node_connect_single,
        draw_node_header,
        draw_node_state,
        draw_node_window_fullscreen,
        draw_node_window_size,
    },
};
use egui_winit_vulkano::{
    egui::{
        epaint::PaintCallback,
        CentralPanel,
        Context,
        Grid,
        Rect,
        Sense,
        Ui,
        Vec2,
        Visuals,
        Window,
    },
    CallbackFn,
    Gui,
    RenderResources,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    image::view::ImageView,
    pipeline::graphics::viewport::Viewport,
    VulkanError,
};
use vulkano_util::renderer::VulkanoWindowRenderer;
use winit::{
    event_loop::EventLoopProxy,
    window::WindowId,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct WindowValues {
    pub fullscreen: bool,
    pub size: Vec2,
    pub source: Option<InputConnection>,
    pub state: NodeState,
}

impl Default for WindowValues {
    fn default() -> Self {
        Self {
            fullscreen: false,
            size: Vec2::new(WINDOW_SINK_WIDTH, WINDOW_SINK_HEIGHT),
            source: None,
            state: NodeState::Stop,
        }
    }
}

// ----------------------------------------------------------------------------

// For every sink window.
#[derive(Debug, Clone)]
pub struct WindowNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    pub scene: TextureScene,
    pub values: WindowValues,
    pub updated: Instant,
    pub window_id: WindowId,
}

// Constructors and draw:
impl WindowNode {
    pub fn new(
        id: String,
        name: String,
        resources: RenderResources,
        window_id: WindowId,
    ) -> Self {
        let scene: TextureScene = TextureScene::new(resources);
        Self {
            class: NodeClass::Window,
            family: NodeFamily::Sink,
            id,
            name,
            scene,
            values: WindowValues::default(),
            updated: Instant::now(),
            window_id,
        }
    }

    /// What is shown as node in the main panel.
    pub fn draw(
        &mut self,
        context: Context,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        ui_area: &mut Ui,
    ) {
        // Draw the window node on the main panel:
        match
            Window::new(
                self
                    .name
                    .clone(),
            )
                .default_size(
                    WINDOW_NODE_SIZE,
                )
                .resizable(
                    true,
                )
                .title_bar(
                    false,
                )
                .show(&context, |ui_window| {
                    ui_window.vertical_centered(|ui_inner| {
    // Header:
                        self.name = draw_node_header(
                            self.class.clone(),
                            self.id.clone(),
                            self.name.clone(),
                            Rc::clone(&options_gui),
                            ui_inner,
                        );
    // Options:
                        ui_inner.separator();
                        Grid::new("Header").show(ui_inner, |ui_grid| {
                            self.values.state = draw_node_state(self.values.state.clone(), ui_grid);
                            // Display name of image in current frame
                            // Display frame count???
                            // Change size:
                            ui_grid.end_row();
                            self.values.size = draw_node_window_size(self.values.size, ui_grid);
                            // Set fullscreen:
                            ui_grid.end_row();
                            self.values.fullscreen = draw_node_window_fullscreen(self.values.fullscreen, ui_grid);
                        });
    // Input:
                        // Show the connect widget.
                        // Get new connection or use old.
                        ui_inner.separator();
                        self.values.source = draw_node_connect_single(
                                Rc::clone(&map_filter_to_node),
                                Rc::clone(&map_source_to_node),
                                self.class.clone(),
                                self.family.clone(),
                                self.id.clone(),
                                self.name.clone(),
                                self.values.source.clone(),
                                ui_area,
                                ui_inner,
                            );
    // Frame:
                        // Show name of the shown frame. Like single pictures of a stack and frame numbers.???
                        // ui_inner.separator();
                        // ui_inner.vertical_centered(|ui_label| {
                        //     ui_label.label("Frame:");
                        // });
                        // ui_inner.label(values.frame_cur.clone());
                    });
                })
        {
    // Remove WindowNode:
            None => (),
            Some(inner_response) => {
                inner_response
                    .response
                    .context_menu(|ui_context| {
                        if ui_context.button("Remove WindowNode").clicked() {
                            ui_context.close_menu();
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: self.family.clone(),
                                            id: self.id.clone(),
                                            event_class: NodeEventClass::Remove,
                                        }
                                    )
                                )
                                .ok();
                        }
                    });
            },
        };
    }
}

// Helpers:
impl WindowNode {
    // Same as monitor.rs callback() except offset!!!
    fn callback(
        self,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_input: Arc<ImageView>,
        scene_rect: Rect,
        window_size: Vec2,
    ) -> PaintCallback {
        let window_width: f32 = window_size.x;
        let window_height: f32 = window_size.y;
        let image_extent: [u32; 3] = image_input.image().extent();
        let image_aspect_ratio: f32 = image_extent[0] as f32 / image_extent[1] as f32;
        let extent: [f32; 2] = match
            window_width * window_height.recip() // window_aspect_ratio
            > image_aspect_ratio
        {
            // Height bound.
            true => [
                image_aspect_ratio * window_height,
                window_height,
            ],
            // Width bound.
            false => [
                window_width,
                image_aspect_ratio.recip() * window_width,
            ],
        };
        let callback_rect = Rect::from_center_size(
            scene_rect.center(),
            Vec2::new(extent[0], extent[1]),
        );
        let viewport: Viewport = Viewport {
            offset: [
                callback_rect.min.x,
                callback_rect.min.y - 8.0,
            ],
            extent,
            depth_range: 0.0..=1.0,
        };

        // Paint callback:
        PaintCallback {
            rect: callback_rect, //??????????????????????????????????
            // rect: window_rect, //??????????????????????????????????
            callback: Arc::new(CallbackFn::new(move |_info, callback_context| {
                // let mut window_scene = window_scene.lock();
                self
                    .scene
                    .render(
                        callback_context,
                        descriptor_set_allocator
                            .clone(),
                        image_input.clone(),
                        viewport.clone(),
                    );
            })),
        }
    }

    /// What is shown as content in the window sink.
    fn render(
        self,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        context: Context,
    ) {
        context.set_visuals(Visuals::dark());
        CentralPanel::default()
            .show(&context, |ui| {
                ui.centered_and_justified(|ui_inner| {
                    match
                        self
                            .values
                            .source
                            .clone()
                    {
                        None => {
                            ui_inner.heading("no connection");
                        },
                        Some(connection) => {
                            match
                                self
                                    .values
                                    .state
                            {
                                NodeState::Stop => {
                                    ui_inner.heading("stopped");
                                },
                                NodeState::Pause | NodeState::Play => {
                                    match
                                        connection
                                            .scene
                                            .image_sink
                                    {
                                        None => (),
                                        Some(image_input) => {
                                            // Calculate size and create rect.
                                            let window_size: Vec2 = self.values.size;
                                            let (scene_rect, _) = ui_inner.allocate_exact_size(
                                                window_size,
                                                Sense::click(),
                                            );
                                            // Display frame buffer content.
                                            let callback: PaintCallback = self
                                                .clone()
                                                .callback(
                                                    descriptor_set_allocator,
                                                    image_input,
                                                    scene_rect,
                                                    window_size,
                                                );
                                            ui_inner.painter().add(callback);
                                        },
                                    };
                                },
                            };
                        },
                    };
                });
            });
    }

    /// Update target image.
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
        window_id: WindowId,
    ) -> Self {
        match
            self
                .values
                .source
        {
            // Window has no connection.
            None => self,
            // Window has a connection.
            Some(mut connection) => {
                // Update target image.
                // Only if update in connection is set.
                // Is set true on creation.
                match
                    self
                        .values
                        .state
                {
                    NodeState::Stop => (),
                    NodeState::Pause => (),
                    NodeState::Play => {
                        // Copy source buffer to frame buffer.
                        // if connection.update_wanted {
                        //     // connection.update();
                        // }
                        connection = connection
                            .update_window(
                                command_buffer_allocator,
                                map_window_to_gui
                                    .clone(),
                                window_id,
                            );
                    },
                };
                let values = WindowValues {
                    source: Some(connection),
                    ..self.values
                };
                Self {
                    values,
                    updated: Instant::now(),
                    ..self
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn render_sink_windows(
    descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
    map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
    renderer: &mut VulkanoWindowRenderer,
    window_id: WindowId,
) {
// ) -> VulkanoWindowRenderer {
    // quick fix???
    if
        !map_window_to_gui
            .borrow()
            .contains_key(
                &window_id
            )
    {
        println!("!! error: gui for window {:?} not found", window_id);
        // return renderer
        return
    };

    // Get gui handler:
    let gui_sink: Option<Gui> = match
        map_window_to_gui
            .borrow_mut()
            .remove(&window_id)
    {
        None => {
            println!("gui for window {:?} not found", window_id);
            None
        },
        Some(mut gui_sink) => {
            match
                map_window_to_sink
                    .borrow()
                    .get(&window_id)
            {
                None => println!("WindowId not found"),
                Some(sink_id) => {
                    match
                        map_sink_to_node
                            .borrow()
                            .map
                            .get(sink_id)
                    {
                        None => println!("SinkNode not found"),
                        Some(sink_node) => {
                            match
                                sink_node
                            {
                                SinkNode::Window(window_node) => {

                                    // Create ui:
                                    gui_sink.immediate_ui(|gui_immediate| {
                                        // let image_target = renderer.get_additional_image_view(0);
                                        window_node
                                            .clone()
                                            .render(
                                                descriptor_set_allocator,
                                                gui_immediate
                                                    .context(),
                                                // image_target,
                                            );
                                    });
                                    // Render gui.
                                    match
                                        renderer
                                            .acquire(
                                                None,
                                                |_| {},
                                            )
                                    {
                                        Ok(before_future) => {
                                            // Acquire swapchain future
                                            let after_future = gui_sink.draw_on_image(
                                                before_future,
                                                // after_update,
                                                renderer.swapchain_image_view(),
                                            );
                                            // Present swapchain:
                                            renderer.present(after_future, true);
                                        },
                                        Err(VulkanError::OutOfDate) => {
                                            renderer.resize();
                                            println!("!! error: {:?}\n!! recreate surface of resized window", VulkanError::OutOfDate);
                                            // still draw to swapchain afterwards???
                                        },
                                        Err(error) => {
                                            panic!("{:?}", error);
                                        },
                                    };
                                },
                                _ => println!("WindowNode not found"),
                            }
                        },
                    }
                },
            };
            Some(gui_sink)
        },
    };

    // Put gui back into map, as it was removed before:
    match
        gui_sink
    {
        None => (),
        Some(gui) => {
            map_window_to_gui
                .borrow_mut()
                .insert(
                    window_id,
                    gui,
                );
        },
    };

    // renderer
}

// ----------------------------------------------------------------------------
//  Funtions - utils:
// ----------------------------------------------------------------------------

// pub fn get_window_context(
//     map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
//     window_id: WindowId,
// ) -> Context {
//     let gui: Gui = match map_window_to_gui.borrow_mut().remove(&window_id) {
//         Some(gui) => gui,
//         None => panic!("gui for window not found"),
//     };
//     let context: Context = gui.context();
//     // Put gui back into map:
//     map_window_to_gui
//         .borrow_mut()
//         .insert(window_id.to_owned(), gui);
//     context
// }

// ----------------------------------------------------------------------------

// pub fn get_window_id(
//     map_window_to_node: Rc<RefCell<HashMap<WindowId, WindowNode>>>,
//     window_name: String,
//     window_main: WindowId,
// ) -> WindowId {
//     let mut window_id: WindowId = window_main;
//     for window_key in map_window_to_node.borrow().keys() {
//         if get_window_name(window_key.to_owned()) == window_name {
//             window_id = window_key.to_owned();
//             break;
//         }
//     }
//     window_id
// }

// ----------------------------------------------------------------------------

pub fn get_window_name(
    window_id: WindowId,
) -> String {
    let name_string: String = format!("{window_id:?}");
    let name_prefix: Vec<&str> = name_string.split("WindowId(").collect();
    let name_suffix: Vec<&str> = name_prefix[1].split_terminator(")").collect();
    format!("Window {}", name_suffix[0])
}
