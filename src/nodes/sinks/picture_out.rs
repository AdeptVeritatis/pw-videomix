
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
        gui::{
            bind::{
                elements::sink_label::{
                    SinkCounter,
                    SinkSequenceDuration,
                    SinkSequenceNum,
                    SinkSequenceSelect,
                },
                midi_bind::MidiBind,
            },
            options::GuiOptions,
        },
    },
    constants::{
        PANEL_NODE_SPACING_SMALL,
        SNAPSHOT_FILE_STRING,
        SNAPSHOT_COUNTER_OFFSET,
        SNAPSHOT_FRAME_OFFSET,
        SNAPSHOT_QTY_OFFSET,
        SNAPSHOT_TIMESPAN_OFFSET,
        STANDARD_NODE_SIZE,
    },
    impls::{
        midir::{
            MidiControllerClass,
            MidiMessage,
            MidiNode,
        },
        vulkano::scenes::copy::copy_connection_scene::ConnectionScene,
    },
    nodes::{
        connections::InputConnection,
        sinks::SinkNode,
        NodeClass,
        NodeFamily,
        draw_node_connect_single,
        draw_node_header,
        draw_node_options,
    },
};
use egui_winit_vulkano::{
    egui::{
        Button,
        Color32,
        ComboBox,
        Context,
        Grid,
        RichText,
        Sense,
        Ui,
        Vec2,
    },
    RenderResources,
};
use rfd::FileDialog;
use std::{
    cell::RefCell,
    fmt,
    fs::File,
    io::BufWriter,
    path::{
        Path,
        PathBuf,
    },
    rc::Rc,
    sync::Arc,
    time::{
        Duration,
        Instant,
    },
};
use vulkano::{
    buffer::{
        Buffer,
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        CopyImageToBufferInfo,
        PrimaryCommandBufferAbstract,
    },
    image::Image,
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    sync::GpuFuture,
};

use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
pub enum SequenceClass {
    SelectFrames,
    TimeSpan,
}

impl fmt::Display for SequenceClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::SelectFrames => write!(f, "Select Frames"),
            Self::TimeSpan => write!(f, "Time Span"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct SnapshotValues {
    pub counter: u32,
    pub directory: PathBuf,
    pub file_string: String,

    pub resize_do: bool,
    pub resize_size: Vec2,

    pub source: Option<InputConnection>,

    // Sequences:
    // Class:
    pub sequence_class: SequenceClass,
    // Number of frames to save.
    pub sequence_num: u32,
    // Runner of number of frames to save.
    // Is set on activation with snapshots button.
    pub sequence_run: u32,

    // Class - select frames:
    // Numer of actual frame.
    // Only count frames, if sequence_run > 0.
    pub sequence_frame: u32,
    // Number of the last selected frame.
    pub sequence_last: u32,
    // Number of frames until next snapshot in frames u32.
    pub sequence_select: u32,

    // Class - time span:
    // Time span between two snapshots in milliseconds u64.
    pub sequence_duration: u32,
    // Timestamp for SequenceClass::TimeSpan.
    pub sequence_updated: Instant,
}

impl Default for SnapshotValues {
    fn default() -> Self {
        Self {
            counter: SNAPSHOT_COUNTER_OFFSET,
            directory: PathBuf::default(),
            file_string: String::from(SNAPSHOT_FILE_STRING),

            resize_do: false,
            resize_size: Vec2::default(),

            sequence_class: SequenceClass::TimeSpan,
            sequence_num: SNAPSHOT_QTY_OFFSET,
            sequence_run: 0,

            sequence_frame: 1,
            sequence_last: 0,
            sequence_select: SNAPSHOT_FRAME_OFFSET,

            sequence_duration: SNAPSHOT_TIMESPAN_OFFSET,
            sequence_updated: Instant::now(),

            source: None,
        }
    }
}

// ----------------------------------------------------------------------------

/// View the content of a node in the main panel.
#[derive(Debug, Clone)]
pub struct SnapshotNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    pub values: SnapshotValues,
    pub updated: Instant,
}

// Constructors and draw:
impl SnapshotNode {
    pub fn new(
        id: String,
    ) -> Self {
        Self {
            class: NodeClass::Snapshot,
            family: NodeFamily::Sink,
            id: id.clone(),
            name: id.clone(),
            values: SnapshotValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a SnapshotNode on a Ui.
    /// What you see as a node in the main panel.
    pub fn draw(
        &mut self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        context: Context,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        // midi_node_rc: Rc<RefCell<MidiNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        resources: RenderResources,
        ui_area: &mut Ui,
    ) {
        match
            egui_winit_vulkano::egui::Window::new(self.name.clone())
                .default_size(Vec2::new(STANDARD_NODE_SIZE, STANDARD_NODE_SIZE))
                .min_width(100.0) // so source_id does not wrap!!!!!!!!!!!!
                .resizable(true)
                .title_bar(false)
                .show(&context, |ui_snapshot| {
                    ui_snapshot.vertical_centered(|ui_inner| {
        // Header:
                        self.name = draw_node_header(
                            self.class.clone(),
                            self.id.clone(),
                            self.name.clone(),
                            Rc::clone(&options_gui),
                            ui_inner,
                        );
        // Options:
                        draw_node_options(
                            Rc::clone(&options_gui),
                            ui_inner,
                        );
                        // Grid::new("Options").show(ui_inner, |ui_grid| {
                        //     state = draw_node_state(state.clone(), ui_grid);
                        // });
        // Snapshot button for single, quick snapshots:
                        ui_inner.separator();
                        ui_inner.add_space(PANEL_NODE_SPACING_SMALL);

                        let response = ui_inner
                            .add(
                                Button::new(format!("Snapshot [ 📷 ] ( {} )", self.values.counter))
                                    .sense(Sense::click())
                            );
                        // needs render resources, not practical !!!
                        // better other solution for single snapshots like an event based system. ???
                        // response
                        //     .clone()
                        //     .context_menu(|ui_context| {
                        //         midi_context_menu(
                        //             String::from("snapshot"),
                        //             MidiBind {
                        //                 family: NodeFamily::Sink,
                        //                 id: self.id.clone(),
                        //                 key: String::from("snapshot_single"),
                        //                 location: vec!(String::from("values")),
                        //                 ..Default::default()
                        //             },
                        //             MidiClass::KeyNoteOn,
                        //             Rc::clone(&midi_node_rc),
                        //             ui_context,
                        //         )
                        //
                        //     });
                        if response.clicked() {
                            self
                                .clone()
                                .snapshot(
                                    command_buffer_allocator,
                                    self.values.counter,
                                    resources,
                                );
                            self.values.counter += 1;
                        }
                        // Print directory.
                        // Or warning, if no directory is selected.
                        ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                        match
                            self
                                .values
                                .directory
                                .file_name()
                        {
                            None => ui_inner.label(RichText::new("! no folder selected !").color(Color32::RED)),
                            Some(os_str) => match
                                os_str.to_str()
                            {
                                None => ui_inner.label(RichText::new("! path with unreadable characters selected !").color(Color32::RED)), // ???
                                Some(final_component) => ui_inner.small(final_component),
                            },
                        };
                        ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
        // Input:
                        // Show the connect widget.
                        ui_inner.separator();
                        self.values.source = draw_node_connect_single(
                            Rc::clone(&map_filter_to_node),
                            Rc::clone(&map_source_to_node),
                            self.class.clone(),
                            self.family.clone(),
                            self.id.clone(),
                            self.name.clone(),
                            self.values.source.clone(),
                            ui_area,
                            ui_inner,
                        );
                    });
                })
        {
            None => (),
            Some(inner_response) => {
                if
                    inner_response
                        .response
                        .interact(
                            Sense::click(),
                        )
                        .clicked()
                {
                    event_loop_proxy
                        .send_event(
                            GuiEvent::ChangeNode(
                                NodeEvent {
                                    family: self.family.clone(),
                                    id: self.id.clone(),
                                    event_class: NodeEventClass::Select,
                                }
                            )
                        )
                        .ok();
                }
                // use function with format!("Remove {:?}")???
                inner_response
                    .response
                    .context_menu(|ui_context| {
                        if
                            ui_context
                                .button("Remove SnapshotNode")
                                .clicked()
                        {
                            ui_context
                                .close_menu();
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: self.family.clone(),
                                            id: self.id.clone(),
                                            event_class: NodeEventClass::Remove,
                                        }
                                    )
                                )
                                .ok();
                        }
                });
            }
        };
    }

    /// The side panel of a SnapshotNode.
    /// For more options to save space on main panel.
    pub fn draw_panel(
        &mut self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
    // Options:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.vertical_centered(|ui_header| {
            ui_header.label("Options:")
        });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        Grid::new("Snapshot")
            .max_col_width(200.0)
            .show(ui, |ui_grid| {
                ui_grid.label("folder:");
                if ui_grid.button(" select ").clicked() {
                    match FileDialog::new()
                        .set_title("Select Folder - pw-videomix")
                        .pick_folder()
                    {
                        None => (),
                        Some(folder) => {
                            println!("{:?}", folder);
                            self.values.directory = folder;
                        },
                    };
                }
                ui_grid.end_row();
                ui_grid.label("");

                match
                    self
                        .values
                        .directory
                        .file_name()
                {
                    None => ui_grid.label(RichText::new("! no folder selected !").color(Color32::RED)),
                    Some(os_str) => match
                        os_str.to_str()
                    {
                        None => ui_grid.label(RichText::new("! path with unreadable characters selected !").color(Color32::RED)), // ???
                        Some(final_component) => ui_grid.label(final_component),
                    },
                };

                // ui_grid.end_row();
                // ui_grid.label("");
                // ui_grid.small(
                //     directory
                //         .display()
                //         .to_string()
                // );
                ui_grid.end_row();
                ui_grid.label("filename:");
                ui_grid.label(format!(
                    "{}{{:04}}.png",
                    self.values.file_string,
                ));
                ui_grid
                    .end_row();
                self.values.counter = SinkCounter::show(
                    self
                        .values
                        .counter,
                    map_control_to_node
                        .clone(),
                    midi_node_rc
                        .clone(),
                    self
                        .id
                        .clone(),
                    ui_grid,
                );
                // ui_grid.end_row();
            });

    // Snapshot series:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);

    // Class selector:
        ui.vertical_centered(|ui_header| {
            ui_header.label("Sequences:")
        });
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ui.separator();
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        ComboBox::from_id_salt(format!("ComboBox {} snapshots", self.id))
            .selected_text(
                self
                    .values
                    .sequence_class
                    .to_string()
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    for
                        class
                    in
                        [
                            SequenceClass::SelectFrames,
                            SequenceClass::TimeSpan,
                        ]
                    {
                        ui_combo
                            .selectable_value(
                                &mut self
                                    .values
                                    .sequence_class,
                                class
                                    .clone(),
                                class
                                    .to_string(),
                            );
                    }
                }
            );
    // Number:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        Grid::new("Snapshotseries")
            .max_col_width(200.0)
            .show(ui, |ui_grid| {
                self.values.sequence_num = SinkSequenceNum::show(
                    self
                        .values
                        .sequence_num,
                    map_control_to_node
                        .clone(),
                    midi_node_rc
                        .clone(),
                    self
                        .id
                        .clone(),
                    ui_grid,
                );
                ui_grid
                    .end_row();
                match
                    self
                        .values
                        .sequence_class
                {
    // Class - select frames:
                    SequenceClass::SelectFrames => {
                        self.values.sequence_select = SinkSequenceSelect::show(
                            self
                                .values
                                .sequence_select,
                            map_control_to_node
                                .clone(),
                            midi_node_rc
                                .clone(),
                            self
                                .id
                                .clone(),
                            ui_grid,
                        );
                        // ui_grid.end_row();
                    },
    // Class - time span:
                    SequenceClass::TimeSpan => {
                        self.values.sequence_duration = SinkSequenceDuration::show(
                            self
                                .values
                                .sequence_duration,
                            map_control_to_node
                                .clone(),
                            midi_node_rc
                                .clone(),
                            self
                                .id
                                .clone(),
                            ui_grid,
                        );
                        // ui_grid.end_row();
                    },
                };

            });
    // Snapshot button:
        ui.add_space(PANEL_NODE_SPACING_SMALL);
        // if ui.button("Snapshot Series [ 📷 ]").clicked() {
        //     sequence_run = sequence_num;
        // }
        let response = ui
            .add(
                Button::new("Snapshot Series [ 📷 ]")
                    .sense(Sense::click())
            );
        // // context menu for midi bindings !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // // not a LabelBind but a ButtonBind!!!
        // response
        //     .clone()
        //     .context_menu(|ui_context| {
        //         midi_context_menu(
        //             String::from("snapshot series"),
        //             MidiBind {
        //                 family: NodeFamily::Sink,
        //                 id: self.id.clone(),
        //                 key: String::from("snapshot_series"),
        //                 location: vec!(String::from("values")),
        //                 ..Default::default()
        //             },
        //             MidiClass::KeyNoteOn,
        //             Rc::clone(&midi_node_rc),
        //             ui_context,
        //         )
        //
        //     });
        if
            response
                .clicked()
        {
            self.values.sequence_run = self
                .values
                .sequence_num;
        }
        ui
            .add_space(
                PANEL_NODE_SPACING_SMALL,
            );
    }

    pub fn control_bind(
        &self,
        bind: String,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        node_id: String,
        result: f32,
    ) {
        let mut node = self
            .to_owned();
        match
            bind
                .as_str()
        {
            "counter" => {
                node.values.counter = result as u32;
                map_sink_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        SinkNode::Snapshot(
                            node,
                        ),
                    );
            },
            "sequence_duration" => {
                node.values.sequence_duration = result as u32;
                map_sink_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        SinkNode::Snapshot(
                            node,
                        ),
                    );
            },
            "sequence_num" => {
                node.values.sequence_num = result as u32;
                map_sink_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        SinkNode::Snapshot(
                            node,
                        ),
                    );
            },
            "sequence_select" => {
                node.values.sequence_select = result as u32;
                map_sink_to_node
                    .borrow_mut()
                    .map
                    .insert(
                        node_id,
                        SinkNode::Snapshot(
                            node,
                        ),
                    );
            },
            bind => println!("!! error: unknown binding {bind}"),
        };
    }

    pub fn received_midi(
        &self,
        midi_bind: &MidiBind,
        midi_event: MidiMessage,
        speed: f32,
    ) -> SinkNode {
        let mut values: SnapshotValues = self
            .values
            .clone();

        let controller_class: MidiControllerClass = midi_bind
            .controller_class
            .clone();
        let location: Vec<String> = midi_bind
            .bind
            .location
            .clone();
        match
            location[0]
                .as_str()
        {
            "values" => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "counter" => {
                        values.counter = SinkCounter::change_midi(
                            values
                                .counter,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "sequence_duration" => {
                        values.sequence_duration = SinkSequenceDuration::change_midi(
                            values
                                .sequence_duration,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "sequence_num" => {
                        values.sequence_num = SinkSequenceNum::change_midi(
                            values
                                .sequence_num,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "sequence_select" => {
                        values.sequence_select = SinkSequenceSelect::change_midi(
                            values
                                .sequence_select,
                            controller_class,
                            midi_event,
                            speed,
                        );
                    },
                    "snapshot_series" => {
                        values.sequence_run = values
                            .sequence_num;
                    },
                    "snapshot_single" => {
                        // self
                        //     .clone()
                        //     .snapshot(
                        //         counter,
                        //         resources,
                        //     );
                        // counter += 1; // ???
                    },
                    _ => (),
                };
            },
            _ => (),
        };
        SinkNode::Snapshot(
            Self {
                values,
                ..self
                    .to_owned()
            }
        )
    }

}

// Helpers:
impl SnapshotNode {
    fn snapshot(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        counter: u32,
        resources: RenderResources,
    ) {
        match
            self
                .values
                .source
        {
            None => println!("!! snapshot node not connected"),
            Some(connection) => {
                match
                    connection
                        .scene
                        .image_sink
                {
                    None => println!("!! error: could not get image from connection"),
                    Some(image_view) => {
                        let image: Arc<Image> = image_view.image().to_owned();
                        let size_x = image.extent()[0];
                        let size_y = image.extent()[1];

                        let buffer: Subbuffer<[u8]> = Buffer::from_iter(
                            resources.memory_allocator,
                            BufferCreateInfo {
                                usage: BufferUsage::TRANSFER_DST,
                                ..Default::default()
                            },
                            AllocationCreateInfo {
                                memory_type_filter: MemoryTypeFilter::PREFER_HOST
                                    | MemoryTypeFilter::HOST_RANDOM_ACCESS,
                                ..Default::default()
                            },
                            (0..size_x * size_y * 4).map(|_| 0u8),
                        )
                        .unwrap();

                        let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
                            command_buffer_allocator,
                            // resources.command_buffer_allocator,
                            resources.queue.queue_family_index(),
                            CommandBufferUsage::OneTimeSubmit,
                        )
                        .unwrap();
                        command_buffer_builder
                            .copy_image_to_buffer(
                                CopyImageToBufferInfo::image_buffer(
                                    image,
                                    buffer.clone(),
                                )
                            )
                            .unwrap();

                        let command_buffer = command_buffer_builder
                            .build()
                            .unwrap();

                        let finished = command_buffer.execute(resources.queue).unwrap();
                        finished
                            .then_signal_fence_and_flush()
                            .unwrap()
                            .wait(None)
                            .unwrap();

                        // Create PNG:
                        let path_buf = self.values.directory.clone();
                        match
                            path_buf
                                .is_dir()
                        {
                            false => {
                                println!("!! not a directory: {:?}", path_buf);
                            },
                            true => {
                                // Set path:
                                let file_string: String = format!(
                                    "{}{:04}.png",
                                    self.values.file_string,
                                    counter.clone(),
                                );

                                let buffer_content = buffer
                                    .read()
                                    .unwrap();
                                let path = self
                                    .values
                                    .directory
                                    .as_path()
                                    .join(
                                        Path::new(
                                            file_string
                                                .as_str()
                                        )
                                    );
                                // Create file:
                                match
                                    path
                                        .try_exists()
                                {
                                    Err(error) => println!("!! error: {error}"),
                                    Ok(true) => println!("!! picture not written: file already exists"),
                                    Ok(false) => {
                                        let file = File::create(&path).unwrap();
                                        let buf_writer = &mut BufWriter::new(file);
                                        let mut encoder = png::Encoder::new(
                                            buf_writer,
                                            size_x,
                                            size_y,
                                        );
                                        // Encode and write data:
                                        encoder
                                            .set_color(png::ColorType::Rgba);
                                        encoder
                                            .set_depth(png::BitDepth::Eight);
                                        let mut writer = encoder
                                            .write_header()
                                            .unwrap();
                                        writer
                                            .write_image_data(&buffer_content)
                                            .unwrap();

                                        match
                                            path
                                                .canonicalize()
                                        {
                                            Err(error) => println!("!! error: {error}"),
                                            Ok(path) => println!("ii 📷 saved to {}", path.display()),
                                        }
                                    },
                                };
                            },
                        };
                    },
                };
            },
        };
    }

    pub fn update(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        resources: RenderResources,
    ) -> Self {
        match
            self
                .values
                .source
                .clone()
        {
            // Snapshot has no connection.
            None => self.to_owned(),
            // Snapshot has a connection.
            Some(mut connection) => {
                // Update ConnectionScene.
                let scene: ConnectionScene = connection.scene
                    .clone()
                    .update(
                        command_buffer_allocator
                            .clone(),
                        resources
                            .clone(),
                    );

                let mut values: SnapshotValues = self.values.clone();
                let mut counter: u32 = values.counter;
                let mut sequence_frame: u32 = values.sequence_frame;
                let mut sequence_last: u32 = values.sequence_last;
                let mut sequence_run: u32 = values.sequence_run;
                let mut sequence_updated: Instant = values.sequence_updated;
        // Do snapshot series:
                if sequence_run > 0 {
                    // println!("ii snapshot series: {sequence_run}");
                    match
                        values
                            .sequence_class
                    {
                        SequenceClass::SelectFrames => {
                            if sequence_frame >=  sequence_last + values.sequence_select {
                                self
                                    .clone()
                                    .snapshot(
                                        command_buffer_allocator,
                                        counter,
                                        resources.clone(),
                                    );
                                counter += 1;
                                sequence_run -= 1;
                                sequence_last = sequence_frame;
                            }
                        },
                        SequenceClass::TimeSpan => {
                            if Instant::now() >= sequence_updated + Duration::from_millis(values.sequence_duration as u64) {
                                self
                                    .clone()
                                    .snapshot(
                                        command_buffer_allocator,
                                        counter,
                                        resources.clone(),
                                    );
                                counter += 1;
                                sequence_run -= 1;
                                sequence_updated = Instant::now();
                            }
                        },
                    };
                    sequence_frame += 1;
                }

            // Store scene.
                connection = InputConnection {
                    scene,
                    ..connection
                };
            // Store values.
                values = SnapshotValues {
                    counter,
                    sequence_frame,
                    sequence_last,
                    sequence_run,
                    sequence_updated,
                    source: Some(connection),
                    ..values
                };
                Self {
                    values,
                    updated: Instant::now(),
                    ..self.to_owned()
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
