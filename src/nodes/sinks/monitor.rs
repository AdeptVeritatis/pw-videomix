#![allow(clippy::derivable_impls)]

use crate::{
    app::{
        data::app_nodes::{
            FilterToNode,
            SourceToNode,
        },
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
    },
    constants::{
        MONITOR_SINK_HEIGHT,
        MONITOR_SINK_WIDTH,
        STANDARD_NODE_BACKGROUND,
    },
    impls::vulkano::scenes::{
        copy::copy_connection_scene::ConnectionScene,
        render::show_texture_scene::TextureScene,
    },
    nodes::{
        connections::InputConnection,
        NodeClass,
        NodeFamily,
        draw_node_connect_single,
    },
};
use egui_winit_vulkano::{
    egui::{
        epaint::{
            // text::LayoutJob,
            CircleShape,
            // Fonts,
            PaintCallback,
            TextShape,
        },
        Color32,
        Context,
        // FontDefinitions,
        // FontFamily,
        // FontId,
        Frame,
        // Galley,
        // Mesh,
        Pos2,
        Rect,
        Response,
        Sense,
        Shape,
        Stroke,
        Ui,
        Vec2,
        Window,
    },
    CallbackFn,
    RenderResources,
};
use std::{
    cell::RefCell,
    rc::Rc,
    sync::Arc,
    time::Instant,
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    image::view::ImageView,
    pipeline::graphics::viewport::Viewport,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct MonitorValues {
    pub source: Option<InputConnection>,
}

impl Default for MonitorValues {
    fn default() -> Self {
        Self {
            source: None,
        }
    }
}

// ----------------------------------------------------------------------------

/// View the content of a node in the main panel.
#[derive(Debug, Clone)]
pub struct MonitorNode {
    pub class: NodeClass,
    pub family: NodeFamily,
    pub id: String,
    pub name: String,
    pub scene: TextureScene,
    pub shape: Shape, // greeter shape
    pub values: MonitorValues,
    pub updated: Instant,
}

// Constructor and draw:
impl MonitorNode {
    pub fn new(
        id: String,
        resources: RenderResources,
    ) -> Self {
        // greeter shape for testing???
        // let fonts: Fonts = Fonts::new(
        //     1.0,
        //     8 * 1024,
        //     FontDefinitions::default(),
        // );
        // // let font_family: FontFamily = FontFamily::Proportional;
        // let font_family: FontFamily = FontFamily::Monospace;
        // let font_id: FontId = FontId::new(12.0, font_family);
        // // let font_id: FontId = FontId::default();
        // let layout_job: LayoutJob = LayoutJob::simple(
        //     id.clone(),
        //     font_id,
        //     Color32::WHITE,
        //     512.0,
        // );
        // let galley = Fonts::layout_job(&fonts, layout_job);
        // let text: TextShape = TextShape::new(
        //     // Pos2::new(MONITOR_SINK_WIDTH / 2.0, MONITOR_SINK_HEIGHT / 2.0),
        //     Pos2::ZERO,
        //     galley,
        // );
        // let shape: Shape = Shape::Text(text);

        // Test circle to show, dimensions, rect and shapes are working correct.
        // Touches the sides and crosses the horizontal borders without clipping through.
        let circle_width: f32 = 16.0;
        let circle_shape: CircleShape = CircleShape::stroke(
            Pos2::new(MONITOR_SINK_WIDTH / 2.0, MONITOR_SINK_HEIGHT / 2.0), // center, but "wrong" context
            ( MONITOR_SINK_WIDTH - circle_width ) / 2.0, //16.0, // radius
            Stroke::new(
                circle_width,
                Color32::DARK_GREEN, //DARK_RED,
            ),
        );
        let shape: Shape = Shape::Circle(circle_shape);

        // let shape: Shape = Shape::Noop;

        let name: String = id.clone();
        let scene = TextureScene::new(resources);

        Self {
            class: NodeClass::Monitor,
            family: NodeFamily::Sink,
            id,
            name,
            scene,
            shape,
            values: MonitorValues::default(),
            updated: Instant::now(),
        }
    }

    /// Draw a MonitorNode on a Ui.
    /// What you see as a node in the main panel.
    pub fn draw(
        &mut self,
        context: Context,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        ui_area: &mut Ui,
    ) {
        let pixel_diff: f32 = 21.0; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        match
            Window::new(self.name.clone())
                .default_size(Vec2::new(MONITOR_SINK_WIDTH, MONITOR_SINK_HEIGHT + pixel_diff))
                .min_width(100.0) // so source_id does not wrap!!!!!!!!!!!!
                .resizable(true)
                .title_bar(false)
                .show(&context, |ui_monitor| {
                    ui_monitor.vertical_centered(|ui_inner| {
        // Frame:
                        Frame::canvas(ui_inner.style())
                            .fill(STANDARD_NODE_BACKGROUND)
                            .show(ui_inner, |ui_canvas| {
                                let sense: Response = self
                                    .clone()
                                    .render(
                                        descriptor_set_allocator,
                                        pixel_diff,
                                        ui_canvas,
                                    );
                                // Context menu for frame.
                                sense.context_menu(|ui_context| {
                                    if ui_context.button("Remove MonitorNode").clicked() {
                                        ui_context.close_menu();
                                        event_loop_proxy
                                            .send_event(
                                                GuiEvent::ChangeNode(
                                                    NodeEvent {
                                                        family: self.family.clone(),
                                                        id: self.id.clone(),
                                                        event_class: NodeEventClass::Remove,
                                                    }
                                                )
                                            )
                                            .ok();
                                    }
                                });
                            });
        // Input:
                        // Show the connect widget.
                        // Get new connection or use old.
                        self.values.source = draw_node_connect_single(
                            Rc::clone(&map_filter_to_node),
                            Rc::clone(&map_source_to_node),
                            self.class.clone(),
                            self.family.clone(),
                            self.id.clone(),
                            self.name.clone(),
                            self.values.source.clone(),
                            ui_area,
                            ui_inner,
                        );
                    });
                })
        {
            None => (),
            Some(inner_response) => {
                inner_response
                    .response
                    .context_menu(|ui_context| {
                        if ui_context.button("Remove MonitorNode").clicked() {
                            ui_context.close_menu();
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            family: self.family.clone(),
                                            id: self.id.clone(),
                                            event_class: NodeEventClass::Remove,
                                        }
                                    )
                                )
                                .ok();
                        }
                    });
            },
        };
    }
}

// Helpers:
impl MonitorNode {
    // Same as window.rs callback() except offset.
    fn callback(
        self,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_input: Arc<ImageView>,
        scene_rect: Rect,
        monitor_size: Vec2,
    ) -> PaintCallback {

        let (viewport, callback_rect): (Viewport, Rect) = {
            let monitor_width: f32 = monitor_size.x;
            // File name label takes more space and forces node to constantly expand.
            let monitor_height: f32 = monitor_size.y;
            let image_extent: [u32; 3] = image_input.image().extent();
            let image_aspect_ratio: f32 = image_extent[0] as f32 / image_extent[1] as f32;
            let extent: [f32; 2] = match
                monitor_width * monitor_height.recip() // monitor_aspect_ratio
                > image_aspect_ratio
            {
                true => [
                    image_aspect_ratio * monitor_height,
                    monitor_height,
                ],
                false => [
                    monitor_width,
                    image_aspect_ratio.recip() * monitor_width,
                ],
            };
            let callback_rect = Rect::from_center_size(
                scene_rect.center(),
                Vec2::new(extent[0], extent[1]),
            );
            let viewport: Viewport = Viewport {
                offset: [
                    callback_rect.min.x,
                    callback_rect.min.y,
                ],
                extent,
                depth_range: 0.0..=1.0,
            };

            (viewport, callback_rect)
        };

        // Paint callback:
        PaintCallback {
            rect: callback_rect, //??????????????????????????????????
            // rect: ui_preview.available_rect_before_wrap(),
            callback: Arc::new(CallbackFn::new(move |_info, callback_context| {
                self
                    .scene
                    .render(
                        callback_context,
                        descriptor_set_allocator
                            .clone(),
                        image_input
                            .clone(),
                        viewport
                            .clone(),
                    );
            })),
        }
    }

    /// What is shown as content in the monitor node.
    fn render(
        self,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        pixel_diff: f32,
        ui_canvas: &mut Ui,
    ) -> Response {
        // Calculate size and create rect.
        let monitor_size: Vec2 = Vec2::new(
            ui_canvas.available_width(),
            ui_canvas.available_height() - pixel_diff,
        );
        let (scene_rect, sense) = ui_canvas.allocate_exact_size(
            monitor_size,
            Sense::click(),
        );
        // let (_sense, painter) = ui_canvas.allocate_painter(size, Sense::click());
        // let rect = painter.clip_rect();

        // Use greeter shape from node or paint callback from connection.
        let shape: Shape = match
            self
                .values
                .source
                .clone()
        {
            None => self.shape.clone(),
            Some(connection) => match
                connection
                    .scene
                    .image_sink
            {
                None => self.shape.clone(),
                Some(image_input) => {
                    let callback: PaintCallback = self
                        .clone()
                        .callback(
                            descriptor_set_allocator,
                            image_input,
                            scene_rect,
                            monitor_size,
                        );
                    Shape::Callback(callback)
                },
            },
        };

        // Add shape to painter.
        match
            shape
        {
            // Shape::Noop => {
            //     ui_monitor.label("unconnected").with_new_rect(scene_rect);
            // },
            Shape::Text(text) => {
                ui_canvas
                    .painter()
                    .add(
                        TextShape::new(
                            scene_rect
                                .center(),
                            text
                                .galley,
                            Color32::ORANGE,
                        ),
                    );
            },
            Shape::Circle(circle) => {
                ui_canvas
                    .painter()
                    .with_clip_rect(scene_rect)
                    .add(
                        CircleShape::stroke(
                            scene_rect.center(),
                            circle.radius,
                            circle.stroke,
                        )
                    );
            },
            Shape::Mesh(mesh) => {
                ui_canvas
                    .painter()
                    .add(mesh);
            },
            Shape::Callback(callback) => {
                ui_canvas
                    .painter()
                    .add(callback);
            },
            _ => (),
        };
        sense
    }

    /// Update target image.
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        resources: RenderResources,
    ) -> Self {
        match
            self
                .values
                .source
        {
            // Monitor has no connection.
            None => self,
            // Monitor has a connection.
            Some(mut connection) => {
                // Update ConnectionScene.
                // println!("test monitor before scene");
                let scene: ConnectionScene = connection
                    .scene
                    .clone()
                    .update(
                        command_buffer_allocator,
                        resources
                            .clone(),
                    );
                // println!("test monitor after scene");
                connection = InputConnection {
                    scene,
                    ..connection
                };
                let values = MonitorValues {
                    source: Some(connection),
                    // ..self.values
                };
                Self {
                    values,
                    updated: Instant::now(),
                    ..self
                }
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
