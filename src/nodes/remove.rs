
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        gui::options::GuiOptions,
    },
    nodes::{
        sinks::SinkNode,
        sources::SourceNode,
        NodeFamily,
    },
};
#[cfg(unix)]
use crate::impls::pipewire::PipewireStream;
use egui_winit_vulkano::Gui;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};
use winit::window::WindowId;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn remove_node(
    map_control_to_node: Rc<RefCell<ControlToNode>>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    #[cfg(unix)]
    map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
    node_family: NodeFamily,
    node_id: String,
    options_gui: Rc<RefCell<GuiOptions>>,
) {
    // Close node panel or empty Filter::None will be created.
    // Move upwards, if more node families use the node panel. ???done now
    let selected_node: Option<String> = options_gui.borrow().selected_node.clone();
    match
        selected_node
    {
        None => (),
        Some(selected_node) if selected_node == node_id => {
            options_gui
                .borrow_mut()
                .selected_node
                .take();
        },
        Some(_) => (),
    };

    match
        node_family
    {
        NodeFamily::None => (),
        NodeFamily::Control => {
            // Remove ControlNode:
            match
                map_control_to_node
                    .borrow_mut()
                    .map
                    .remove(&node_id)
            {
                None => println!("!! ControlNode not found"),
                Some(control_node) => {
                    println!("<- remove ControlNode \"{node_id}\" ( {control_node} )");
                    // match
                    //     control_node
                    // {
                    //     ControlNode::Number(_) => {
                    //     },
                    // };
                },
            };
        },
        NodeFamily::Filter => {
            // Remove FilterNode:
            match
                map_filter_to_node
                    .borrow_mut()
                    .map
                    .remove(&node_id)
            {
                None => println!("!! FilterNode not found"),
                Some(filter_node) => {
                    println!("<- remove FilterNode \"{node_id}\" ( {filter_node} )");
                    // match
                    //     filter_node
                    // {
                    //     FilterNode::None => (),
                    //     FilterNode::Fader(_) => {
                    //     },
                    //     FilterNode::Mandala(_) => {
                    //     },
                    //     FilterNode::Mixer(_) => {
                    //     },
                },
            };
        },
        NodeFamily::Sink => {
            // Remove SinkNode:
            match
                map_sink_to_node
                    .borrow_mut()
                    .map
                    .remove(&node_id)
            {
                None => println!("!! SinkNode not found: {node_id}"),
                Some(sink_node) => {
                    println!("<- remove SinkNode \"{node_id}\" ( {sink_node} )");
                    match
                        sink_node
                    {
                        SinkNode::None => (),
                        #[cfg(feature = "gstreamer")]
                        SinkNode::Encoder(_) => (),
                        SinkNode::Monitor(_) => (),
                        #[cfg(unix)]
                        SinkNode::PipewireOutput(_) => (),
                        // #[cfg(feature = "gstreamer")]
                        // SinkNode::PwOutputGst(_) => (),
                        SinkNode::Snapshot(_) => (),
                        SinkNode::Window(window_node) => {
                            // Remove gui:
                            map_window_to_gui.borrow_mut().remove(&window_node.window_id);
                        },
                    };
                },
            };
        },
        NodeFamily::Source => {
            // Remove SourceNode:
            match
                map_source_to_node
                    .borrow_mut()
                    .map
                    .remove(&node_id)
            {
                None => println!("!! SourceNode not found"),
                Some(source_node) => {
                    println!("<- remove SourceNode \"{node_id}\" ( {source_node} )");
                    match
                        source_node
                    {
                        SourceNode::None => (),
                        SourceNode::Picture(_) => (),
                        // #[cfg(feature = "gstreamer")]
                        // SourceNode::PwInputGst(_) => (),
                        SourceNode::Stack(_) => (),
                        #[cfg(feature = "gstreamer")]
                        SourceNode::Video(_) => (),
                        #[cfg(unix)]
                        SourceNode::PipewireInput(pw_input_node) => {
                            match
                                map_node_to_stream
                                    .borrow_mut()
                                    .remove(&pw_input_node.id)
                            {
                                None => println!("!! stream from \"{}\" already removed", pw_input_node.id),
                                Some(PipewireStream::InputStream(input_stream)) => match
                                    input_stream
                                        .stream
                                        .disconnect()
                                {
                                    Ok(_) => println!("<- stream from \"{}\" disconnected", pw_input_node.id),
                                    Err(error) => println!("\n!! error: {error}"),
                                },
                                Some(PipewireStream::OutputStream(output_stream)) => match
                                    output_stream
                                        .stream
                                        .disconnect()
                                {
                                    Ok(_) => println!("<- stream from \"{}\" disconnected", pw_input_node.id),
                                    Err(error) => println!("\n!! error: {error}"),
                                },
                            };
                        },
                    };
                },
            };
        },
    };
}
