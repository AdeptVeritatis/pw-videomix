pub mod control;
pub mod connections;
pub mod filter;
pub mod remove;
pub mod sinks;
pub mod sources;

use crate::{
    app::{
        data::app_nodes::{
            FilterToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::options::{
            ChangeNodeName,
            GuiOptions,
        },
        print::PrintClass,
    },
    constants::{
        NODE_FPS_DECIMALS,
        NODE_FPS_RANGE,
        NODE_FPS_SPEED,
        NODE_LAYER_CENTER_OFFSET,
        NODE_LAYER_MIX_OFFSET,
        NODE_LAYER_SCALE_OFFSET,
        PANEL_NODE_SPACING_SMALL,
        WINDOW_SINK_SIZE_MAX,
    },
    nodes::{
        connections::{
            InputConnection,
            InputConnectionConstructor,
        },
        filter::FilterNode,
        sources::SourceNode,
    },
};
use egui_winit_vulkano::egui::{
    ComboBox,
    DragValue,
    Grid,
    Pos2,
    TextEdit,
    Ui,
    Vec2,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    // cmp::max,
    collections::BTreeMap,
    fmt,
    ops::RangeInclusive,
    rc::Rc,
    time::Instant,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

/// Available classes of nodes.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum NodeClass {
    ColorMixer,
    ColorRotator,
    #[cfg(feature = "gstreamer")]
    Encoder,
    Fader,
    FunctionGenerator,
    Kaleidoscope,
    Mandala,
    Mixer,
    Monitor,
    Number,
    Picture,
    #[cfg(unix)]
    PipewireInput,
    #[cfg(unix)]
    PipewireOutput,
    #[cfg(feature = "gstreamer")]
    PwInputGst,
    #[cfg(feature = "gstreamer")]
    PwOutputGst,
    Snapshot,
    Stack,
    Trigger,
    Trigonometry,
    #[cfg(feature = "gstreamer")]
    Video,
    Window,
    None,
}

impl fmt::Display for NodeClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match
            *self
        {
            Self::None => write!(f, "none"),
            Self::ColorMixer => write!(f, "ColorMixer"),
            Self::ColorRotator => write!(f, "ColorRotator"),
            #[cfg(feature = "gstreamer")]
            Self::Encoder => write!(f, "Encoder"),
            Self::Fader => write!(f, "Fader"),
            Self::FunctionGenerator => write!(f, "Function Generator"),
            Self::Kaleidoscope => write!(f, "Kaleidoscope"),
            Self::Mandala => write!(f, "Mandala"),
            Self::Mixer => write!(f, "Mixer"),
            Self::Monitor => write!(f, "Monitor"),
            Self::Number => write!(f, "Number"),
            Self::Picture => write!(f, "Picture"),
            #[cfg(unix)]
            Self::PipewireInput => write!(f, "PipeWire Input"),
            #[cfg(unix)]
            Self::PipewireOutput => write!(f, "PipeWire Output"),
            #[cfg(feature = "gstreamer")]
            Self::PwInputGst => write!(f, "PipeWire Input (gst)"),
            #[cfg(feature = "gstreamer")]
            Self::PwOutputGst => write!(f, "PipeWire Output (gst)"),
            Self::Snapshot => write!(f, "Snapshot"),
            Self::Stack => write!(f, "Stack"),
            Self::Trigger => write!(f, "Trigger"),
            Self::Trigonometry => write!(f, "Trigonometry"),
            #[cfg(feature = "gstreamer")]
            Self::Video => write!(f, "Video"),
            Self::Window => write!(f, "Window"),
        }
    }
}

// ----------------------------------------------------------------------------

/// Available families of nodes.
#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum NodeFamily {
    Control,
    Source,
    Filter,
    Sink,
    None,
}

impl fmt::Display for NodeFamily {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::None => write!(f, "none"),
            Self::Control => write!(f, "Control"),
            Self::Filter => write!(f, "Filter"),
            Self::Sink => write!(f, "Sink"),
            Self::Source => write!(f, "Source"),
        }
    }
}

// ----------------------------------------------------------------------------

// better name: NodeSource???
#[derive(Debug, Clone)]
pub struct NodeLayer {
    pub center: Pos2,
    pub connection: InputConnection,
    pub mix_factor: f32,
    pub scale_factor: f32,
}

impl NodeLayer {
    fn new(
        connection: InputConnection,
    ) -> Self {
        Self {
            center: Pos2::new(NODE_LAYER_CENTER_OFFSET, NODE_LAYER_CENTER_OFFSET),
            connection,
            mix_factor: NODE_LAYER_MIX_OFFSET,
            scale_factor: NODE_LAYER_SCALE_OFFSET,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum NodeState {
    Play,
    Pause,
    Stop,
    // Undefined,
}

impl fmt::Display for NodeState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Pause => write!(f, "Pause"), // "Pause", "⏸️"
            Self::Play => write!(f, "Play"), // "Play", "▶️"
            Self::Stop => write!(f, "Stop"), // "Stop", "⏹"
            // NodeState::Undefined => write!(f, "Undefined"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct NodeValues {
    pub source_class: NodeClass,
    pub source_family: NodeFamily,
    pub source_id: String,
    pub source_name: String,
}

impl
    Default
for
    NodeValues
{
    fn
        default
    () ->
        Self
    {
        Self {
            source_class: NodeClass::None,
            source_family: NodeFamily::None,
            source_id: String::new(),
            source_name: String::new(),
        }
    }
}

// ----------------------------------------------------------------------------
// Functions - node:
// ----------------------------------------------------------------------------

pub fn draw_node_connect_single(
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    node_class: NodeClass,
    node_family: NodeFamily,
    node_id: String,
    node_name: String,
    mut source: Option<InputConnection>,
    ui_area: &mut Ui,
    ui_node: &mut Ui,
) -> Option<InputConnection> {
    // Values of selected node from combobox.
    let mut node_values: NodeValues = NodeValues {
        source_class: NodeClass::None,
        source_family: NodeFamily::None,
        source_id: String::new(),
        source_name: String::new(),
    };
    let mut sink_pos: Pos2 = Pos2::default();

    ui_node.vertical_centered(|ui_inner| {
        // Header:
        if node_class != NodeClass::Monitor {
            ui_inner.label("Input:");
        }

        // Combobox:
        let source_name: String = match
            source
                .clone()
        {
            None => String::from("select"),
            Some(connection) => connection.source_name, // source_name???
        };
        ui_inner.vertical_centered(|ui_combo| {
            node_values = draw_node_combobox(
                Rc::clone(&map_filter_to_node),
                Rc::clone(&map_source_to_node),
                node_id.clone(),
                node_values.clone(),
                source_name,
                ui_combo,
            );
            // Set sink_pos from connection.
            // After ui elements to get correct size.
            sink_pos = ui_combo.min_rect().min;
        });
    });
    // Create new connection:
    source = match
        node_values
            .source_name
            .is_empty()
    {
        // Use old connection.
        true => source,
        // Newly connected.
        false => Some(
            InputConnection::new(
                InputConnectionConstructor {
                    node_class,
                    node_family,
                    node_id,
                    node_name,
                    node_values,
                },
                map_filter_to_node
                    .clone(),
                map_source_to_node
                    .clone(),
            )
        ),
    };
    // Draw connection:
    // Or throw connection away, when source is not available.
    match
        source
    {
        None => None,
        Some(mut connection) => {
            match
                // Update position of output connection.
                connection.get_source_update(
                    Rc::clone(&map_filter_to_node),
                    Rc::clone(&map_source_to_node),
                )
            {
                (None, _) => None,
                (Some(source_pos), source_extent) => {
                    connection = InputConnection {
                        sink_pos,
                        source_pos: Some(source_pos),
                        source_extent,
                        ..connection
                    };
                    connection
                        // .clone()
                        .draw(ui_area);
                    Some(connection)
                },
            }
        },
    }
}

// ----------------------------------------------------------------------------

pub fn draw_node_connect_multi(
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    node_class: NodeClass,
    node_family: NodeFamily,
    node_id: String,
    node_name: String,
    mut sources: BTreeMap<String, NodeLayer>,
    // mut sources: HashMap<String, NodeLayer>,
    ui_area: &mut Ui,
    ui_node: &mut Ui,
) -> BTreeMap<String, NodeLayer> {
    // Values of selected node from combobox.
    let mut node_values: NodeValues = NodeValues {
        source_class: NodeClass::None,
        source_family: NodeFamily::None,
        source_id: String::new(),
        source_name: String::new(),
    };

    ui_node.vertical_centered(|ui_inner| {
        // Header:
        ui_inner.label("Input:");
        // Combobox:
        node_values = draw_node_combobox(
            Rc::clone(&map_filter_to_node),
            Rc::clone(&map_source_to_node),
            node_id.clone(),
            node_values.clone(),
            String::from("add"),
            ui_inner,
        );
        // Create new connection:
        sources = match
            node_values
                .source_name
                .clone()
                .is_empty()
        {
            // // Use old connections.
            true => sources.clone(),
            // Newly connected.
            false => {
                let connection: InputConnection = InputConnection::new(
                    InputConnectionConstructor {
                        node_class:
                            node_class
                                .clone(),
                        node_family:
                            node_family
                                .clone(),
                        node_id:
                            node_id
                                .clone(),
                        node_name,
                        node_values:
                            node_values
                                .clone(),
                    },
                    map_filter_to_node
                        .clone(),
                    map_source_to_node
                        .clone(),
                );

                let layer_id: String = format!(
                    "{} - {:?}",
                    connection.source_id,
                    // Needs unique name to connect same input multiple times to one node.
                    Instant::now(),
                );
                // println!("ii {}", layer_id);
                let node_layer: NodeLayer = NodeLayer::new(
                    connection,
                );
                sources
                    .insert(
                        layer_id,
                        node_layer,
                    );
                sources.clone()
            },
        };
        // Sources:
        sources = sources
            .clone()
            .into_iter()
            .map(|(layer_id, mut node_layer)| {
                // Draw source input widget:
                // And update sink_pos.

                // Only filter can have multiple input connections.
                // Just look in filter map for now.
                // May need a sink_family and sink_class.
                // Or a whole node object Node<Filter<MandalaNode>> for all families.
                node_layer = match
                    map_filter_to_node
                        .borrow()
                        .map
                        .get(
                            &node_layer
                                .connection
                                .sink_id
                         )
                {
                    None => node_layer,
                    Some(FilterNode::None) => node_layer,
                // Filter with single inputs:
                    Some(FilterNode::ColorMixer(_)) => node_layer,
                    Some(FilterNode::ColorRotator(_)) => node_layer,
                    Some(FilterNode::Kaleidoscope(_)) => node_layer,
                    Some(FilterNode::Mandala(_)) => node_layer,
                // Filter with multiple inputs:
                    Some(FilterNode::Fader(_)) => {
                        draw_node_sources_layer(
                            event_loop_proxy.clone(),
                            layer_id.clone(),
                            node_layer.clone(),
                            ui_inner,
                        )
                    },
                    Some(FilterNode::Mixer(_)) => {
                        draw_node_sources_layer(
                            event_loop_proxy.clone(),
                            layer_id.clone(),
                            node_layer.clone(),
                            ui_inner,
                        )
                    },
                };

                // Draw connection:
                // Or throw connection away, when source is not available.
                // Fix throwing away!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                match
                    // Update position of output connection.
                    node_layer
                        .connection
                        .clone()
                        .get_source_update(
                            Rc::clone(&map_filter_to_node),
                            Rc::clone(&map_source_to_node),
                        )
                {
                    (None, _) => (),
                    (Some(source_pos), source_extent) => {
                        let connection: InputConnection = InputConnection {
                            source_pos: Some(source_pos),
                            source_extent,
                            ..node_layer.connection.clone()
                        };
                        connection
                            .clone()
                            .draw(ui_area);
                        node_layer = NodeLayer {
                            connection,
                            ..node_layer.clone()
                        };
                    },
                };
                (layer_id.to_owned(), node_layer)
            })
            .collect();
    });
    sources
}

// ----------------------------------------------------------------------------

pub fn draw_node_header(
    node_class: NodeClass,
    node_id: String,
    mut node_name: String,
    options_gui: Rc<RefCell<GuiOptions>>,
    ui: &mut Ui,
) -> String {
    let options: GuiOptions = options_gui
        .borrow()
        .to_owned();
    match
        options
            .change_name
    {
        None => {
            draw_node_header_name(
                node_class,
                options
                    .node_header_full,
                node_id,
                node_name
                    .clone(),
                options_gui,
                ui,
            );
        },
        Some(mut change_node_name) => {
            match
                change_node_name
                    .node_id
                ==
                node_id
            {
                false => {
                    draw_node_header_name(
                        node_class,
                        options
                            .node_header_full,
                        node_id,
                        node_name
                            .clone(),
                        options_gui,
                        ui,
                    );
                },
                true => {
                    // Grid::new("change name")
                    //     .show(
                    //         ui,
                    //         |ui_name| {
                    ui
                        .horizontal(
                            |ui_name| {
                                ui_name
                                    .add(
                                        TextEdit::singleline(
                                            &mut change_node_name
                                                .node_name,
                                        )
                                            .hint_text(
                                                "Node name",
                                            )
                                            .desired_width(
                                                116.0,
                                            ),
                                    );

                                options_gui
                                    .borrow_mut()
                                    .change_name
                                = match
                                    ui_name
                                        .button(
                                            "✔"
                                        )
                                        .clicked()
                                {
                                    false => Some(change_node_name),
                                    true => {
                                        node_name = change_node_name
                                            .node_name;
                                        None
                                    },
                                };
                                if
                                    ui_name
                                        .button(
                                            "❌"
                                        )
                                        .clicked()
                                {
                                    options_gui
                                        .borrow_mut()
                                        .change_name
                                    = None;
                                };
                            },
                        );
                },
            };
        },
    };

    node_name
}

pub fn draw_node_header_name(
    node_class: NodeClass,
    node_header_full: bool,
    node_id: String,
    node_name: String,
    options_gui: Rc<RefCell<GuiOptions>>,
    ui: &mut Ui,
) {
    let name: String = match
        node_name
            .is_empty()
    {
        true => String::from("<empty>"),
        false => node_name
            .clone(),
    };
    match
        node_header_full
    {
        true => {
            ui.add_space(PANEL_NODE_SPACING_SMALL);
            ui
                .label(
                    name,
                )
                .context_menu(
                    |ui_context| {
                        if
                            ui_context
                                .button(
                                    "Change name",
                                )
                                .clicked()
                        {
                            options_gui
                                .borrow_mut()
                                .change_name
                            = Some(
                                ChangeNodeName{
                                    node_id,
                                    node_name: node_name
                                        .clone(),
                                },
                            );
                        };
                    },
                );
            ui.add_space(PANEL_NODE_SPACING_SMALL);
            ui.label(format!("{}", node_class));
            ui.add_space(PANEL_NODE_SPACING_SMALL);
        },
        false => {
            ui
                .label(
                    format!(
                        "{} - {}",
                        name,
                        node_class,
                    )
                )
                .context_menu(
                    |ui_context| {
                        if
                            ui_context
                                .button(
                                    "Change name",
                                )
                                .clicked()
                        {
                            options_gui
                                .borrow_mut()
                                .change_name
                            = Some(
                                ChangeNodeName{
                                    node_id,
                                    node_name: node_name
                                        .clone(),
                                },
                            );
                        };
                    },
                );
        },
    };
}

// ----------------------------------------------------------------------------

pub fn draw_node_options(
    options_gui: Rc<RefCell<GuiOptions>>,
    ui: &mut Ui,
) {
    if options_gui.borrow().panel_node_hint {
        ui.separator();
        ui.label("click node\nfor more options");
    }
}

// ----------------------------------------------------------------------------

pub fn draw_node_output(
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    node_family: NodeFamily,
    node_id: String,
    mut output_size: Vec2,
    ui: &mut Ui,
) -> Vec2 {
    let compare_size: Vec2 = output_size;
    ui.vertical_centered(|ui_label| {
        ui_label.label("Output:");
    });
    Grid::new("Output").show(ui, |ui_grid| {
        ui_grid.label("width:");
        ui_grid.add(
            DragValue::new(&mut output_size.x)
                .speed(0.1)
                .fixed_decimals(0)
                .range(RangeInclusive::new(1, WINDOW_SINK_SIZE_MAX)),
        );
        ui_grid.label("height:");
        ui_grid.add(
            DragValue::new(&mut output_size.y)
                .speed(0.1)
                .fixed_decimals(0)
                .range(RangeInclusive::new(1, WINDOW_SINK_SIZE_MAX)),
        );
    });
    if output_size != compare_size {
        event_loop_proxy
            .send_event(
                GuiEvent::RecreateImage(
                    // Only one interaction possible at a time???
                    (
                        node_family,
                        node_id.clone(),
                        output_size,
                    )
                )
            )
            .ok();
    }
    output_size
}

// ----------------------------------------------------------------------------
// Functions - values:
// ----------------------------------------------------------------------------

pub fn draw_node_fps(
    mut fps: f32,
    ui: &mut Ui,
) -> f32 {
    // ui.label("fps:");
    ui.add(
        DragValue::new(&mut fps)
            .fixed_decimals(NODE_FPS_DECIMALS)
            .range(NODE_FPS_RANGE)
            .speed(NODE_FPS_SPEED),
    );
    ui.label(format!("{} ms", (fps.recip() * 10000.0).round() / 10.0,));
    fps
}

// ----------------------------------------------------------------------------

pub fn draw_node_qty(
    fps: f32,
    quantity: usize,
    ui: &mut Ui,
) {
    ui.label("qty:");
    ui.label(quantity.to_string());
    ui.label(
        match
            quantity as f32 * (fps.recip() * 10000.0).round() / 10.0
        {
            total if total > 1000.0 => format!("\u{2211} {} s", (total / 100.0).round() / 10.0),
            total => format!("\u{2211} {} ms", total),
        },
    );
}

// ----------------------------------------------------------------------------

fn draw_node_state(
    mut state: NodeState,
    ui: &mut Ui,
) -> NodeState {
    ui.label("state:");
    match state {
        NodeState::Play => {
            ui.label("playing");
            ui.vertical_centered_justified(|ui_pause| {
                if ui_pause.button(NodeState::Pause.to_string()).clicked() {
                    state = NodeState::Pause;
                }
            });
            ui.vertical_centered_justified(|ui_stop| {
                if ui_stop.button(NodeState::Stop.to_string()).clicked() {
                    state = NodeState::Stop;
                }
            });
        },
        NodeState::Pause => {
            ui.vertical_centered_justified(|ui_play| {
                if ui_play.button(NodeState::Play.to_string()).clicked() {
                    state = NodeState::Play;
                }
            });
            ui.label("paused");
            ui.vertical_centered_justified(|ui_stop| {
                if ui_stop.button(NodeState::Stop.to_string()).clicked() {
                    state = NodeState::Stop;
                }
            });
        },
        NodeState::Stop => {
            ui.vertical_centered_justified(|ui_play| {
                if ui_play.button(NodeState::Play.to_string()).clicked() {
                    state = NodeState::Play;
                }
            });
            ui.vertical_centered_justified(|ui_pause| {
                if ui_pause.button(NodeState::Pause.to_string()).clicked() {
                    state = NodeState::Pause;
                }
            });
            ui.label("stopped");
        },
        // NodeState::Undefined => (),
    }
    // ui.label("");
    // ui.label(match state {
    //     NodeState::Pause => "paused",
    //     NodeState::Play => "playing",
    //     NodeState::Stop => "stopped",
    //     // NodeState::Undefined => "undefined",
    // });
    state
}

// ----------------------------------------------------------------------------
// Functions - combobox:
// ----------------------------------------------------------------------------

// Check for source group simplifications!!!
fn draw_node_combobox(
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    node_id: String,
    mut node_values: NodeValues,
    source_name: String,
    ui: &mut Ui,
) -> NodeValues {
    ComboBox::from_id_salt(format!("ComboBox {node_id} source"))
        .selected_text(source_name)
        .wrap()
        .show_ui(ui, |ui_combo| {
            // ui_combo.style_mut().wrap = Some(false);
            ui_combo.set_min_width(60.0);
            // Show filter:
            // borrow once and move around the borrow??? but it is borrowed then!
            node_values = match
                map_filter_to_node
                    .borrow()
                    .map
                    .len()
            {
                0 => node_values.clone(),
                1 => match
                    map_filter_to_node
                        .borrow()
                        .map
                        .keys()
                        .last()
                {
                    Some(filter_id) => {
                        if &node_id != filter_id {
                            draw_combo_header(
                                String::from("Filter:"),
                                ui_combo,
                            );
                        }
                        // List all filter:
                        draw_combo_list_filter(
                            Rc::clone(&map_filter_to_node),
                            node_id.clone(),
                            &mut node_values,
                            ui_combo,
                        )
                    }
                    None => {
                        PrintClass::ShouldNotHappen.print();
                        // println!("this shouldn't happen");
                        node_values.clone()
                    }
                },
                _ => {
                    draw_combo_header(
                        String::from("Filter:"),
                        ui_combo,
                    );
                    // List all filter:
                    draw_combo_list_filter(
                        Rc::clone(&map_filter_to_node),
                        node_id.clone(),
                        &mut node_values,
                        ui_combo,
                    )
                }
            };
            // Show sources:
            node_values = match
                map_source_to_node
                    .borrow()
                    .map
                    .len()
            {
                0 => node_values.clone(),
                1 => {
                    draw_combo_header(
                        String::from("Source:"),
                        ui_combo,
                    );
                    // List all sources:
                    draw_combo_list_sources(
                        Rc::clone(&map_source_to_node),
                        &mut node_values,
                        ui_combo,
                    )
                },
                _ => {
                    draw_combo_header(
                        String::from("Sources:"),
                        ui_combo,
                    );
                    // List all sources:
                    draw_combo_list_sources(
                        Rc::clone(&map_source_to_node),
                        &mut node_values,
                        ui_combo,
                    )
                },
            };
        });
    node_values
}

// ----------------------------------------------------------------------------

fn draw_combo_header(
    header: String,
    ui: &mut Ui,
) {
    ui.separator();
    // ui_combo.style_mut().visuals.extreme_bg_color = Color32::RED;
    // ui_combo.label(RichText::new("--- Filter: ---").background_color(Color32::LIGHT_GRAY));
    ui.vertical_centered(|ui_label| {
        ui_label.label(header);
    });
    ui.separator();
}

// ----------------------------------------------------------------------------

fn draw_combo_list_filter(
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    name: String,
    node_values: &mut NodeValues,
    ui: &mut Ui,
) -> NodeValues {
    for (filter_id, filter_node) in map_filter_to_node.borrow().map.iter() {
        // Not showing itself is only important for filters.
        // Pictures and stacks don't have a combobox.
        // Windows are not in the list to connect to.
        if &name != filter_id {
            let (source_class, source_name): (NodeClass, String) = match
                filter_node
            {
                FilterNode::None => (NodeClass::None, String::new()),
                FilterNode::ColorMixer(node) => (NodeClass::ColorMixer, node.name.clone()),
                FilterNode::ColorRotator(node) => (NodeClass::ColorRotator, node.name.clone()),
                FilterNode::Fader(node) => (NodeClass::Fader, node.name.clone()),
                FilterNode::Kaleidoscope(node) => (NodeClass::Kaleidoscope, node.name.clone()),
                FilterNode::Mandala(node) => (NodeClass::Mandala, node.name.clone()),
                FilterNode::Mixer(node) => (NodeClass::Mixer, node.name.clone()),
            };
            ui.selectable_value(
                node_values,
                NodeValues {
                    source_class,
                    source_family: NodeFamily::Filter,
                    source_id: filter_id.to_owned(),
                    source_name: source_name
                        .clone(),
                },
                // format!("{filter_id} ( {filter_node} )"),
                format!("{source_name} ( {filter_node} )"),
            );
        }
    }
    // node_values or simply filter_node???
    node_values.to_owned()
}

// ----------------------------------------------------------------------------

fn draw_combo_list_sources(
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    node_values: &mut NodeValues,
    ui: &mut Ui,
) -> NodeValues {
    for (source_id, source_node) in map_source_to_node.borrow().map.iter() {
        let (source_class, source_name): (NodeClass, String) = match
            source_node
        {
            SourceNode::None => (NodeClass::None, String::new()),
            SourceNode::Picture(picture_node) => (NodeClass::Picture, picture_node.path.file_name.clone()),
            #[cfg(unix)]
            SourceNode::PipewireInput(pw_input_node) => (NodeClass::PipewireInput, pw_input_node.name.clone()),
            // #[cfg(feature = "gstreamer")]
            // SourceNode::PwInputGst(pw_input_gst_node) => (NodeClass::PwInputGst, pw_input_gst_node.name.clone()),
            SourceNode::Stack(stack_node) => (NodeClass::Stack, stack_node.name.clone()),
            #[cfg(feature = "gstreamer")]
            SourceNode::Video(video_node) => (NodeClass::Video, video_node.name.clone()),
        };
        ui.selectable_value(
            node_values,
            NodeValues {
                source_class,
                source_family: NodeFamily::Source,
                source_id: source_id.to_owned(),
                source_name: source_name.clone(),
            },
            // format!("{source_id} ( {source_name} )"),
            source_name.clone(),
        );
    }
    node_values.to_owned()
}

// ----------------------------------------------------------------------------
// Functions - layer:
// ----------------------------------------------------------------------------

fn draw_node_sources_layer(
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    layer_id: String,
    node_layer: NodeLayer,
    ui: &mut Ui,
) -> NodeLayer {
    let mut connection: InputConnection = node_layer.connection.clone();
    let mut sink_pos: Pos2 = connection.sink_pos;

    ui.separator();
    Grid::new(connection.source_id.clone())
        .show(ui, |ui_layer| {
            if ui_layer.small_button("remove").clicked() {
                event_loop_proxy
                    .send_event(
                        GuiEvent::LayerRemove((
                            connection.sink_id.clone(), // filter_id
                            layer_id.clone(), // layer_id
                        )),
                    )
                    .ok();
            }
            ui_layer.label(connection.source_name.clone());
            // ui_layer.end_row();
            sink_pos = ui_layer.min_rect().min;
        });

    connection = InputConnection {
        sink_pos,
        ..connection.clone()
    };
    NodeLayer {
        connection,
        ..node_layer
    }
}

// ----------------------------------------------------------------------------
// Functions - window:
// ----------------------------------------------------------------------------

fn draw_node_window_fullscreen(
    mut fullscreen: bool,
    ui: &mut Ui,
) -> bool {
    ui.label("fullscreen:");
    ui.checkbox(&mut fullscreen, "");
    ui.label("toggle:");
    ui.label("' F '");
    fullscreen
}

// ----------------------------------------------------------------------------

fn draw_node_window_size(
    mut size: Vec2,
    ui: &mut Ui,
) -> Vec2 {
    // ui.label("size:");
    ui.label("width:");
    ui.add(
        DragValue::new(&mut size.x)
            .speed(0.1)
            .fixed_decimals(0)
            .range(RangeInclusive::new(1, WINDOW_SINK_SIZE_MAX)),
    );
    ui.label("height:");
    ui.add(
        DragValue::new(&mut size.y)
            .speed(0.1)
            .fixed_decimals(0)
            .range(RangeInclusive::new(1, WINDOW_SINK_SIZE_MAX)),
    );
    size
}
