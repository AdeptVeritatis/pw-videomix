
use crate::{
    impls::serde::AssetLoader,
    nodes::{
        connections::{
            InputConnection,
            InputConnectionConstructor,
        },
        NodeClass,
        NodeFamily,
    },
};
use serde::{
    de::{
        // self,
        Deserializer,
        DeserializeSeed,
        MapAccess,
        Visitor,
    },
    Deserialize,
};
use std::fmt;

// ----------------------------------------------------------------------------
// InputConnection:
// ----------------------------------------------------------------------------

pub struct
    InputConnectionDeserializer<'a, L> {
        pub asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    InputConnectionDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        Option<InputConnection>;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            // Scene,
            // UpdateWanted,
            SourceClass,
            SourceFamily,
            SourceId,
            SourceName,
            // SourceExtent,
            // SourcePos,
            SinkClass,
            SinkFamily,
            SinkId,
            SinkName,
            // SinkExtent,
            // SinkPos,
        }

        struct
            InputConnectionVisitor<'a, L>
        {
            asset_loader:
                &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            InputConnectionVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                Option<InputConnection>;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct InputConnection",
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    constructor
                =
                    InputConnectionConstructor::default();

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return
                        Err(error),
                    Ok(None) => false,
                    Ok(Some(Field::SinkClass)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(class) => {
                            constructor
                                .node_class
                            =
                                class;

                            true
                        },
                    },
                    Ok(Some(Field::SinkFamily)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(family) => {
                            constructor
                                .node_family
                            =
                                family;

                            true
                        },
                    },
                    Ok(Some(Field::SinkId)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(id) => {
                            constructor
                                .node_id
                            = id;

                            true
                        },
                    },
                    Ok(Some(Field::SinkName)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(name) => {
                            constructor
                                .node_name
                            = name;

                            true
                        },
                    },
                    Ok(Some(Field::SourceClass)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(class) => {
                            constructor
                                .node_values
                                .source_class
                            =
                                class;

                            true
                        },
                    },
                    Ok(Some(Field::SourceFamily)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(family) => {
                            constructor
                                .node_values
                                .source_family
                            =
                                family;

                            true
                        },
                    },
                    Ok(Some(Field::SourceId)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(id) => {
                            constructor
                                .node_values
                                .source_id
                            = id;

                            true
                        },
                    },
                    Ok(Some(Field::SourceName)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(name) => {
                            constructor
                                .node_values
                                .source_name
                            = name;

                            true
                        },
                    },
                } {};

                match
                    constructor
                        .node_class
                    ==
                    NodeClass::None
                ||
                    constructor
                        .node_family
                    ==
                    NodeFamily::None
                ||
                    constructor
                        .node_values
                        .source_class
                    ==
                    NodeClass::None
                ||
                    constructor
                        .node_values
                        .source_family
                    ==
                    NodeFamily::None
                ||
                    constructor
                        .node_id
                        .is_empty()
                |
                    constructor
                        .node_name
                        .is_empty()
                |
                    constructor
                        .node_values
                        .source_id
                        .is_empty()
                |
                    constructor
                        .node_values
                        .source_name
                        .is_empty()
                {
                    true => (),
                    false => {
                        self
                            .asset_loader
                            .store_input_connection_constructor(
                                constructor,
                            );
                    },
                };

                Ok(
                    None,
                )
            }
        }

        const
            SOURCE_CLASS
        :
            &str
        =
            "source_class";

        const
            SOURCE_FAMILY
        :
            &str
        =
            "source_family";

        const
            SOURCE_ID
        :
            &str
        =
            "source_id";

        const
            SOURCE_NAME
        :
            &str
        =
            "source_name";

        const
            SINK_CLASS
        :
            &str
        =
            "sink_class";

        const
            SINK_FAMILY
        :
            &str
        =
            "sink_family";

        const
            SINK_ID
        :
            &str
        =
            "sink_id";

        const
            SINK_NAME
        :
            &str
        =
            "sink_name";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                SINK_CLASS,
                SINK_FAMILY,
                SINK_ID,
                SINK_NAME,
                SOURCE_CLASS,
                SOURCE_FAMILY,
                SOURCE_ID,
                SOURCE_NAME,
            ];

        deserializer
            .deserialize_struct(
                "Option<InputConnection>",
                FIELDS,
                InputConnectionVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
