pub mod serde_color_mixer;
pub mod serde_color_rotator;
pub mod serde_fader;
pub mod serde_kaleidoscope;
pub mod serde_mandala;
pub mod serde_mixer;

use crate::{
    app::data::app_nodes::FilterToNode,
    impls::serde::{
        serde_filter::{
            serde_color_mixer::ColorMixerNodeDeserializer,
            serde_color_rotator::ColorRotatorNodeDeserializer,
            serde_fader::FaderNodeDeserializer,
            serde_kaleidoscope::KaleidoscopeNodeDeserializer,
            serde_mandala::MandalaNodeDeserializer,
            serde_mixer::MixerNodeDeserializer,
        },
        AssetLoader,
    },
    nodes::filter::FilterNode,
};
use serde::{
    de::{
        self,
        Deserializer,
        DeserializeSeed,
        EnumAccess,
        MapAccess,
        VariantAccess,
        Visitor,
    },
    Deserialize,
};
use std::{
    collections::HashMap,
    fmt,
};

// ----------------------------------------------------------------------------
// FilterToNode:
// ----------------------------------------------------------------------------

pub struct
    FilterToNodeDeserializer<'a, L> {
        pub asset_loader: &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    FilterToNodeDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        FilterToNode;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>
    {
        struct
            FilterToNodeVisitor<'a, L>
        {
            asset_loader: &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            FilterToNodeVisitor<'_, L>
        where
            L:
                AssetLoader
        {
            type
                Value
            =
                FilterToNode;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct FilterToNode",
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                match
                    map
                        .next_key::<&str>()
                {
                    Err(error) => {
                        Err(error)
                    },
                    Ok(None) =>  {
                        Err(de::Error::missing_field(MAP))
                    },
                    Ok(Some(key)) if
                        key
                        ==
                        MAP
                    => {
                        match
                            map
                                .next_value_seed(
                                    FilterMapDeserializer {
                                        asset_loader:
                                            self
                                                .asset_loader,
                                    },
                                )
                        {
                            Err(error) => Err(error),
                            Ok(filter_map) => Ok(
                                FilterToNode {
                                    map:
                                        filter_map,
                                },
                            ),
                        }
                    },
                    Ok(Some(key)) => {
                        Err(de::Error::unknown_field(key, &[]))
                    },
                }
            }
        }

        const
            MAP
        :
            &str
        =
            "map";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                MAP,
            ];

        deserializer
            .deserialize_struct(
                "FilterToNode",
                FIELDS,
                FilterToNodeVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// HashMap<String, FilterNode>:
// ----------------------------------------------------------------------------

pub struct
    FilterMapDeserializer<'a, L> {
        asset_loader: &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    FilterMapDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        HashMap<String, FilterNode>;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>
    {
        struct
            FilterIdDeserializer {}

        impl<'de>
            DeserializeSeed<'de>
        for
            FilterIdDeserializer
        {
            type
                Value
            =
                String;

            fn
                deserialize<D>
            (
                self,
                deserializer:
                    D,
            ) ->
                Result<
                    Self::Value,
                    D::Error,
                >
            where
                D:
                    Deserializer<'de>
            {
                String::deserialize(
                    deserializer
                )
            }
        }

        struct
            FilterMapVisitor<'a, L>
        {
            asset_loader: &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            FilterMapVisitor<'_, L>
        where
            L:
                AssetLoader
        {
            type
                Value
            =
                HashMap<String, FilterNode>;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "a map of FilterNodes"
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result <
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    hash_map
                :
                    HashMap<
                        String,
                        FilterNode,
                    >
                =
                    HashMap::new();

                while
                    match
                        map
                            .next_entry_seed(
                                FilterIdDeserializer {},
                                FilterNodeDeserializer {
                                    asset_loader:
                                        self
                                            .asset_loader,
                                },
                            )
                    {
                        Err(_error) => false, // ???
                        Ok(None) => false,
                        Ok(Some((key, value))) => {
                            hash_map
                                .insert(
                                    key,
                                    value,
                                );
                            true
                        },
                    }
                {}

                Ok(hash_map)
            }
        }

        deserializer
            .deserialize_map(
                FilterMapVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// FilterNode:
// ----------------------------------------------------------------------------

pub struct
    FilterNodeDeserializer<'a, L> {
        asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    FilterNodeDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        FilterNode;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        struct
            FieldVisitor<'a, L>
        {
            asset_loader: &'a mut L,
        }

        impl<'de, L: AssetLoader>
            Visitor<'de>
        for
            FieldVisitor<'_, L>
        {
            type
                Value
            =
                FilterNode;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "FilterNode",
                    )
            }

            fn
                visit_enum<A>
            (
                self,
                data:
                    A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A: EnumAccess<'de>,
            {
                #[derive(Deserialize)]
                // #[serde(field_identifier, rename_all = "PascalCase")]
                #[serde(field_identifier)]
                enum
                    FieldVariant
                {
                    ColorMixer,
                    ColorRotator,
                    Fader,
                    Kaleidoscope,
                    Mandala,
                    Mixer,
                }

                match
                    data
                        .variant()
                {
                    Err(error) => Err(error),
                    Ok((variant_name, variant_data)) => {
                        match
                            variant_name
                        {
                            FieldVariant::ColorMixer => match
                                variant_data
                                    .newtype_variant_seed(
                                            ColorMixerNodeDeserializer {
                                                asset_loader:
                                                    self
                                                        .asset_loader,
                                            }
                                    )
                            {
                                Err(error) => Err(error),
                                Ok(node) => Ok(
                                    FilterNode::ColorMixer(
                                        node,
                                    )
                                ),
                            },
                            FieldVariant::ColorRotator => match
                                variant_data
                                    .newtype_variant_seed(
                                            ColorRotatorNodeDeserializer {
                                                asset_loader:
                                                    self
                                                        .asset_loader,
                                            }
                                    )
                            {
                                Err(error) => Err(error),
                                Ok(node) => Ok(
                                    FilterNode::ColorRotator(
                                        node,
                                    )
                                ),

                            },
                            FieldVariant::Fader => match
                                variant_data
                                    .newtype_variant_seed(
                                            FaderNodeDeserializer {
                                                asset_loader:
                                                    self
                                                        .asset_loader,
                                            }
                                    )
                            {
                                Err(error) => Err(error),
                                Ok(node) => Ok(
                                    FilterNode::Fader(
                                        node,
                                    )
                                ),

                            },
                            FieldVariant::Kaleidoscope => match
                                variant_data
                                    .newtype_variant_seed(
                                            KaleidoscopeNodeDeserializer {
                                                asset_loader:
                                                    self
                                                        .asset_loader,
                                            }
                                    )
                            {
                                Err(error) => Err(error),
                                Ok(node) => Ok(
                                    FilterNode::Kaleidoscope(
                                        node,
                                    )
                                ),

                            },
                            FieldVariant::Mandala => match
                                variant_data
                                    .newtype_variant_seed(
                                            MandalaNodeDeserializer {
                                                asset_loader:
                                                    self
                                                        .asset_loader,
                                            }
                                    )
                            {
                                Err(error) => Err(error),
                                Ok(node) => Ok(
                                    FilterNode::Mandala(
                                        node,
                                    )
                                ),

                            },
                            FieldVariant::Mixer => match
                                variant_data
                                    .newtype_variant_seed(
                                            MixerNodeDeserializer {
                                                asset_loader:
                                                    self
                                                        .asset_loader,
                                            }
                                    )
                            {
                                Err(error) => Err(error),
                                Ok(node) => Ok(
                                    FilterNode::Mixer(
                                        node,
                                    )
                                ),

                            },
                        }
                    },
                }
            }
        }

        const
            COLOR_MIXER
        :
            &str
        =
            "ColorMixer";

        const
            COLOR_ROTATOR
        :
            &str
        =
            "ColorRotator";

        const
            FADER
        :
            &str
        =
            "Fader";

        const
            KALEIDOSCOPE
        :
            &str
        =
            "Kaleidoscope";

        const
            MANDALA
        :
            &str
        =
            "Mandala";

        const
            MIXER
        :
            &str
        =
            "Mixer";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                COLOR_MIXER,
                COLOR_ROTATOR,
                FADER,
                KALEIDOSCOPE,
                MANDALA,
                MIXER,
            ];

        deserializer
            .deserialize_enum(
                "FilterNode",
                FIELDS,
                FieldVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
