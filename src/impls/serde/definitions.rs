
use egui_winit_vulkano::egui::{
    Pos2,
    Vec2,
};
use serde::{
    de::{
        Deserializer,
        DeserializeSeed,
        MapAccess,
        Visitor,
    },
    Deserialize,
    Serialize,
};
use std::fmt;

#[derive(Serialize, Deserialize)]
#[serde(remote = "Pos2")]
pub struct Pos2Def {
    pub x: f32,
    pub y: f32,
}

impl
    Default
for
    Pos2Def
{
    fn
        default()
    ->
        Self
    {
        Self {
            x: 0.0,
            y: 0.0,
        }
    }
}

pub struct
    Pos2DefDeserializer {}

impl<'de>
    DeserializeSeed<'de>
for
    Pos2DefDeserializer
{
    type
        Value
    =
        Pos2Def;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            X,
            Y,
        }

        struct
            ValuesVisitor {}

        impl<'de>
            Visitor<'de>
        for
            ValuesVisitor
        {
            type
                Value
            =
                Pos2Def;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct Pos2Def",
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    position
                =
                    Pos2Def::default();

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return Err(error),
                    Ok(None) => {
                        false
                    },
                    Ok(Some(Field::X)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(pos_x) => {
                            position
                                .x
                            =
                                pos_x;

                            true
                        },
                    },
                    Ok(Some(Field::Y)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(pos_y) => {
                            position
                                .y
                            =
                                pos_y;

                            true
                        },
                    },
                } {};

                Ok(
                    position,
                )
            }
        }

        const
            X
        :
            &str
        =
            "x";

        const
            Y
        :
            &str
        =
            "y";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                X,
                Y,
            ];

        deserializer
            .deserialize_struct(
                "Pos2Def",
                FIELDS,
                ValuesVisitor {},
            )
    }
}

// ----------------------------------------------------------------------------
// Vec2Def:
// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize)]
#[serde(remote = "Vec2")]
pub struct Vec2Def {
    pub x: f32,
    pub y: f32,
}

impl
    Default
for
    Vec2Def
{
    fn
        default()
    ->
        Self
    {
        Self {
            x: 0.0,
            y: 0.0,
        }
    }
}

pub struct
    Vec2DefDeserializer {}

impl<'de>
    DeserializeSeed<'de>
for
    Vec2DefDeserializer
{
    type
        Value
    =
        Vec2Def;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            X,
            Y,
        }

        struct
            ValuesVisitor {}

        impl<'de>
            Visitor<'de>
        for
            ValuesVisitor
        {
            type
                Value
            =
                Vec2Def;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct Vec2Def",
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    position
                =
                    Vec2Def::default();

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return Err(error),
                    Ok(None) => {
                        false
                    },
                    Ok(Some(Field::X)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(pos_x) => {
                            position
                                .x
                            =
                                pos_x;

                            true
                        },
                    },
                    Ok(Some(Field::Y)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(pos_y) => {
                            position
                                .y
                            =
                                pos_y;

                            true
                        },
                    },
                } {};

                Ok(
                    position,
                )
            }
        }

        const
            X
        :
            &str
        =
            "x";

        const
            Y
        :
            &str
        =
            "y";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                X,
                Y,
            ];

        deserializer
            .deserialize_struct(
                "Vec2Def",
                FIELDS,
                ValuesVisitor {},
            )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
