
use crate::{
    impls::serde::{
        serde_connections::InputConnectionDeserializer,
        AssetLoader,
    },
    nodes::{
        filter::color_rotator::{
            ColorRotatorNode,
            ColorRotatorValues,
        },
        NodeClass,
        NodeFamily,
    },
};
use serde::{
    de::{
        self,
        Deserializer,
        DeserializeSeed,
        MapAccess,
        Visitor,
    },
    Deserialize,
};
use std::{
    fmt,
    time::Instant,
};

// ----------------------------------------------------------------------------
// ColorRotatorNode:
// ----------------------------------------------------------------------------

pub struct
    ColorRotatorNodeDeserializer<'a, L> {
        pub asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    ColorRotatorNodeDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        ColorRotatorNode;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            Class,
            Family,
            Id,
            Name,
            // Scene,
            Values,
        }

        struct
            ColorRotatorVisitor<'a, L>
        {
            asset_loader:
                &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            ColorRotatorVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                ColorRotatorNode;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct ColorRotatorNode"
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    node
                =
                    ColorRotatorNode {
                        class:
                            NodeClass::ColorRotator,
                        family:
                            NodeFamily::Filter,
                        id: String::new(),
                        name: String::new(),
                        scene:
                            self
                                .asset_loader
                                .new_color_mixer_scene(),
                        values:
                            ColorRotatorValues::default(),
                        updated:
                            Instant::now(),
                    };

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return
                        Err(error),
                    Ok(None) => {
                        false

                        // match
                        //     id
                        //         .is_empty()
                        // |
                        //     name
                        //         .is_empty()
                        // |
                        //     values
                        //         .is_none()
                        // {
                        //     true => {
                        //         return Err(
                        //             de::Error::invalid_length(
                        //                 0,
                        //                 &"no keys left",
                        //             ),
                        //         );
                        //     },
                        //     false => false,
                        // },
                    },
                    Ok(Some(Field::Class)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(class) => match
                            class
                        {
                            NodeClass::ColorRotator => true,
                            _ => return
                                Err(
                                    de::Error::custom(
                                        "{class} not NodeClass::ColorRotator",
                                    ),
                                ),
                        },
                    },
                    Ok(Some(Field::Family)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(family) => match
                            family
                        {
                            NodeFamily::Filter => true,
                            _ => return Err(
                                de::Error::custom(
                                    "{family} not NodeFamily::Filter",
                                ),
                            ),
                        },
                    },
                    Ok(Some(Field::Id)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(string) => {
                            node
                                .id
                            =
                                string;

                            true
                        },
                    },
                    Ok(Some(Field::Name)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(string) => {
                            node
                                .name
                            =
                                string;

                            true
                        },
                    },
                    Ok(Some(Field::Values)) => match
                        map
                            .next_value_seed(
                                ColorRotatorValuesDeserializer {
                                    asset_loader:
                                        self
                                            .asset_loader,
                                },
                            )
                    {
                        Err(error) => return
                            Err(error),
                        Ok(values) => {
                            node
                                .values
                            =
                                values;

                            true
                        },
                    },
                } {};

                Ok(
                    node,
                )
            }
        }

        const
            CLASS
        :
            &str
        =
            "class";

        const
            FAMILY
        :
            &str
        =
            "family";

        const
            ID
        :
            &str
        =
            "id";

        const
            NAME
        :
            &str
        =
            "name";

        const
            VALUES
        :
            &str
        =
            "values";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                CLASS,
                FAMILY,
                ID,
                NAME,
                VALUES,
            ];

        deserializer
            .deserialize_struct(
                "ColorRotatorNode",
                FIELDS,
                ColorRotatorVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// ColorRotatorValues:
// ----------------------------------------------------------------------------

pub struct
    ColorRotatorValuesDeserializer<'a, L> {
        pub asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    ColorRotatorValuesDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        ColorRotatorValues;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            Angle,
            BoundHigh,
            BoundLow,
            GainB,
            GainG,
            GainR,
            RotSpeed,
            // OutputSize,
            // PosOut,
            Source,
        }

        struct
            ValuesVisitor<'a, L>
        {
            asset_loader:
                &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            ValuesVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                ColorRotatorValues;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct ColorRotatorValues",
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    values
                =
                    ColorRotatorValues::default();

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return Err(error),
                    Ok(None) => {
                        false
                    },
                    Ok(Some(Field::Angle)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(angle) => {
                            values
                                .angle
                            =
                                angle;

                            true
                        },
                    },
                    Ok(Some(Field::BoundHigh)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(bound_high) => {
                            values
                                .bound_high
                            =
                                bound_high;

                            true
                        },
                    },
                    Ok(Some(Field::BoundLow)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(bound_low) => {
                            values
                                .bound_low
                            =
                                bound_low;

                            true
                        },
                    },
                    Ok(Some(Field::GainB)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(gain_b) => {
                            values
                                .gain_b
                            =
                                gain_b;

                            true
                        },
                    },
                    Ok(Some(Field::GainG)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(gain_g) => {
                            values
                                .gain_g
                            =
                                gain_g;

                            true
                        },
                    },
                    Ok(Some(Field::GainR)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(gain_r) => {
                            values
                                .gain_r
                            =
                                gain_r;

                            true
                        },
                    },
                    Ok(Some(Field::RotSpeed)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(rot_speed) => {
                            values
                                .rot_speed
                            =
                                rot_speed;

                            true
                        },
                    },
                    // Ok(Some(Field::OutputSize)) => match
                    //     map
                    //         .next_value()
                    // {
                    //     Err(error) => return Err(
                    //         error,
                    //     ),
                    //     Ok(output_size) => {
                    //         // let vec_2: Vec2 = output_size;
                    //         let vec_def: Vec2Def = output_size;
                    //         // println!(
                    //         //     "!! output_size: {output_size}",
                    //         // );
                    //         values
                    //             .output_size
                    //         =
                    //             Vec2::new(
                    //                 NODE_OUTPUT_WIDTH,
                    //                 NODE_OUTPUT_HEIGHT,
                    //                 // vec_def
                    //                 //     .x,
                    //                 // vec_def
                    //                 //     .y,
                    //             );
                    //         true
                    //     },
                    // },
                    // Ok(Some(Field::PosOut)) => match
                    //     map
                    //         .next_value()
                    // {
                    //     Err(error) => return Err(
                    //         error,
                    //     ),
                    //     Ok(_) => {
                    //         values
                    //             .pos_out
                    //         =
                    //             Pos2::default();
                    //         true
                    //     },
                    // },
                    Ok(Some(Field::Source)) => {
                        match
                            map
                                .next_value_seed(
                                    InputConnectionDeserializer {
                                        asset_loader:
                                            self.
                                                asset_loader,
                                    }
                                )
                        {
                            Err(_error) => {
                                // println!("!! error: {error}"); // "!! error: invalid type: null, expected struct InputConnection at line..."
                                true
                                // return Err(error),
                            },
                            Ok(source) => {
                                values
                                    .source
                                =
                                    source;

                                true
                            },
                        }
                    },
                } {};

                Ok(
                    values,
                )
            }
        }

        const
            ANGLE
        :
            &str
        =
            "angle";

        const
            BOUND_HIGH
        :
            &str
        =
            "bound_high";

        const
            BOUND_LOW
        :
            &str
        =
            "bound_low";

        const
            GAIN_R
        :
            &str
        =
            "gain_r";

        const
            GAIN_G
        :
            &str
        =
            "gain_g";

        const
            GAIN_B
        :
            &str
        =
            "gain_b";

        const
            ROT_SPEED
        :
            &str
        =
            "rot_speed";

        // const
        //     OUTPUT_SIZE
        // :
        //     &str
        // =
        //     "output_size";

        // const
        //     POS_OUT
        // :
        //     &str
        // =
        //     "pos_out";

        const
            SOURCE
        :
            &str
        =
            "source";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                ANGLE,
                BOUND_HIGH,
                BOUND_LOW,
                GAIN_B,
                GAIN_G,
                GAIN_R,
                ROT_SPEED,
                // OUTPUT_SIZE,
                // POS_OUT,
                SOURCE,
            ];

        deserializer
            .deserialize_struct(
                "ColorRotatorValues",
                FIELDS,
                ValuesVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
