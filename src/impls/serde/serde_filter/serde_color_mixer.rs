
use crate::{
    impls::serde::{
        serde_connections::InputConnectionDeserializer,
        // definitions::{
        //     Pos2Def,
        //     Vec2Def,
        // },
        AssetLoader,
    },
    nodes::{
        filter::color_mixer::{
            ColorMixerNode,
            ColorMixerValues,
        },
        NodeClass,
        NodeFamily,
    },
};
// use egui_winit_vulkano::egui::{
//     Pos2,
//     Vec2,
// };
use serde::{
    de::{
        self,
        Deserializer,
        DeserializeSeed,
        MapAccess,
        Visitor,
    },
    Deserialize,
};
use std::{
    fmt,
    time::Instant,
};

// ----------------------------------------------------------------------------
// ColorMixerNode:
// ----------------------------------------------------------------------------

pub struct
    ColorMixerNodeDeserializer<'a, L> {
        pub asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    ColorMixerNodeDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        ColorMixerNode;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            Class,
            Family,
            Id,
            Name,
            // Scene,
            Values,
        }

        struct
            ColorMixerVisitor<'a, L>
        {
            asset_loader:
                &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            ColorMixerVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                ColorMixerNode;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct ColorMixerNode",
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    node
                =
                    ColorMixerNode {
                        class:
                            NodeClass::ColorMixer,
                        family:
                            NodeFamily::Filter,
                        id: String::new(),
                        name: String::new(),
                        scene:
                            self
                                .asset_loader
                                .new_color_mixer_scene(),
                        values:
                            ColorMixerValues::default(),
                        updated:
                            Instant::now(),
                    };

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return
                        Err(error),
                    Ok(None) => {
                        false

                        // match
                        //     id
                        //         .is_empty()
                        // |
                        //     name
                        //         .is_empty()
                        // |
                        //     values
                        //         .is_none()
                        // {
                        //     true => {
                        //         return Err(
                        //             de::Error::invalid_length(
                        //                 0,
                        //                 &"no keys left",
                        //             ),
                        //         );
                        //     },
                        //     false => false,
                        // },
                    },
                    Ok(Some(Field::Class)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(class) => match
                            class
                        {
                            NodeClass::ColorMixer => true,
                            _ => return
                                Err(
                                    de::Error::custom(
                                        "{class} not NodeClass::ColorMixer",
                                    ),
                                ),
                        },
                    },
                    Ok(Some(Field::Family)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(family) => match
                            family
                        {
                            NodeFamily::Filter => true,
                            _ => return
                                Err(
                                    de::Error::custom(
                                        "{family} not NodeFamily::Filter",
                                    ),
                                ),
                        },
                    },
                    Ok(Some(Field::Id)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(string) => {
                            node
                                .id
                            =
                                string;

                            true
                        },
                    },
                    Ok(Some(Field::Name)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(string) => {
                            node
                                .name
                            =
                                string;

                            true
                        },
                    },
                    Ok(Some(Field::Values)) => match
                        map
                            .next_value_seed(
                                ColorMixerValuesDeserializer {
                                    asset_loader:
                                        self
                                            .asset_loader,
                                },
                            )
                    {
                        Err(error) => return
                            Err(error),
                        Ok(values) => {
                            node
                                .values
                            =
                                values;

                            true
                        },
                    },
                } {};

                Ok(
                    node,
                )
            }
        }

        const
            CLASS
        :
            &str
        =
            "class";

        const
            FAMILY
        :
            &str
        =
            "family";

        const
            ID
        :
            &str
        =
            "id";

        const
            NAME
        :
            &str
        =
            "name";

        const
            VALUES
        :
            &str
        =
            "values";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                CLASS,
                FAMILY,
                ID,
                NAME,
                VALUES,
            ];

        deserializer
            .deserialize_struct(
                "ColorMixerNode",
                FIELDS,
                ColorMixerVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// ColorMixerValues:
// ----------------------------------------------------------------------------

pub struct
    ColorMixerValuesDeserializer<'a, L> {
        pub asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    ColorMixerValuesDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        ColorMixerValues;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            MixerMatrix,
            // OutputSize,
            // PosOut,
            Source,
        }

        struct
            ValuesVisitor<'a, L>
        {
            asset_loader:
                &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            ValuesVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                ColorMixerValues;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct ColorMixerValues",
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    values
                =
                    ColorMixerValues::default();

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return Err(error),
                    Ok(None) => {
                        false
                    },
                    Ok(Some(Field::MixerMatrix)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(mixer_matrix) => {
                            values
                                .mixer_matrix
                            =
                                mixer_matrix;

                            true
                        },
                    },
                    // Ok(Some(Field::OutputSize)) => match
                    //     map
                    //         .next_value()
                    // {
                    //     Err(error) => return Err(
                    //         error,
                    //     ),
                    //     Ok(output_size) => {
                    //         // let vec_2: Vec2 = output_size;
                    //         let vec_def: Vec2Def = output_size;
                    //         // println!(
                    //         //     "!! output_size: {output_size}",
                    //         // );
                    //         values
                    //             .output_size
                    //         =
                    //             Vec2::new(
                    //                 NODE_OUTPUT_WIDTH,
                    //                 NODE_OUTPUT_HEIGHT,
                    //                 // vec_def
                    //                 //     .x,
                    //                 // vec_def
                    //                 //     .y,
                    //             );
                    //         true
                    //     },
                    // },
                    // Ok(Some(Field::PosOut)) => match
                    //     map
                    //         .next_value()
                    // {
                    //     Err(error) => return Err(
                    //         error,
                    //     ),
                    //     Ok(_) => {
                    //         values
                    //             .pos_out
                    //         =
                    //             Pos2::default();
                    //         true
                    //     },
                    // },
                    Ok(Some(Field::Source)) => {
                        match
                            map
                                .next_value_seed(
                                    InputConnectionDeserializer {
                                        asset_loader:
                                            self.
                                                asset_loader,
                                    }
                                )
                        {
                            Err(_error) => {
                                // println!("!! error: {error}"); // "!! error: invalid type: null, expected struct InputConnection at line..."
                                true
                                // return Err(error),
                            },
                            Ok(source) => {
                                values
                                    .source
                                =
                                    source;

                                true
                            },
                        }
                    },
                } {};

                Ok(
                    values,
                )
            }
        }

        const
            MIXER_MATRIX
        :
            &str
        =
            "mixer_matrix";

        // const
        //     OUTPUT_SIZE
        // :
        //     &str
        // =
        //     "output_size";

        // const
        //     POS_OUT
        // :
        //     &str
        // =
        //     "pos_out";

        const
            SOURCE
        :
            &str
        =
            "source";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                MIXER_MATRIX,
                // OUTPUT_SIZE,
                // POS_OUT,
                SOURCE,
            ];

        deserializer
            .deserialize_struct(
                "ColorMixerValues",
                FIELDS,
                ValuesVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
