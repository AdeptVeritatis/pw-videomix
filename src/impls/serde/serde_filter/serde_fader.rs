
use crate::{
    impls::serde::AssetLoader,
    nodes::{
        filter::fader::{
            FaderNode,
            FaderValues,
        },
        NodeClass,
        NodeFamily,
    },
};
use serde::{
    de::{
        self,
        Deserializer,
        DeserializeSeed,
        MapAccess,
        Visitor,
    },
    Deserialize,
};
use std::{
    fmt,
    time::Instant,
};

// ----------------------------------------------------------------------------
// FaderNode:
// ----------------------------------------------------------------------------

pub struct
    FaderNodeDeserializer<'a, L> {
        pub asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    FaderNodeDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        FaderNode;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            Class,
            Family,
            Id,
            Name,
            // Scene,
            Values,
        }

        struct
            FaderVisitor<'a, L>
        {
            asset_loader:
                &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            FaderVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                FaderNode;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct FaderNode"
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    id
                :
                    String
                =
                    String::new();

                let mut
                    name
                :
                    String
                =
                    String::new();

                let mut
                    values
                :
                    Option<FaderValues>
                =
                    None;

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return Err(error),
                    Ok(None) => match
                        id
                            .is_empty()
                    |
                        name
                            .is_empty()
                    |
                        values
                            .is_none()
                    {
                        true => {
                            return Err(
                                de::Error::invalid_length(
                                    0,
                                    &"no keys left",
                                ),
                            );
                        },
                        false => false,
                    },
                    Ok(Some(Field::Class)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(class) => match
                            class
                        {
                            NodeClass::Fader => true,
                            _ => return
                                Err(
                                    de::Error::custom(
                                        "{class} not NodeClass::Fader",
                                    ),
                                ),
                        },
                    },
                    Ok(Some(Field::Family)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(family) => match
                            family
                        {
                            NodeFamily::Filter => true,
                            _ => return Err(
                                de::Error::custom(
                                    "{family} not NodeFamily::Filter",
                                ),
                            ),
                        },
                    },
                    Ok(Some(Field::Id)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(string) => {
                            id = string;
                            true
                        },
                    },
                    Ok(Some(Field::Name)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(string) => {
                            name = string;
                            true
                        },
                    },
                    Ok(Some(Field::Values)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(new_values) => {
                            values = Some(
                                new_values,
                            );
                            true
                        },
                    },
                } {};

                Ok(
                    FaderNode {
                        class:
                            NodeClass::Fader,
                        family:
                            NodeFamily::Filter,
                        id,
                        name,
                        scene:
                            self
                                .asset_loader
                                .new_fader_scene(),
                        values:
                            match
                                values
                            {
                                None => return Err(
                                    de::Error::missing_field(
                                        VALUES,
                                    )
                                ),
                                Some(values) => values,
                            },
                        updated:
                            Instant::now(),
                    },
                )
            }
        }

        const
            CLASS
        :
            &str
        =
            "class";

        const
            FAMILY
        :
            &str
        =
            "family";

        const
            ID
        :
            &str
        =
            "id";

        const
            NAME
        :
            &str
        =
            "name";

        const
            VALUES
        :
            &str
        =
            "values";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                CLASS,
                FAMILY,
                ID,
                NAME,
                VALUES,
            ];

        deserializer
            .deserialize_struct(
                "FaderNode",
                FIELDS,
                FaderVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
