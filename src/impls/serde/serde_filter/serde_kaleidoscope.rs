
use crate::{
    impls::serde::{
        definitions::{
            Pos2DefDeserializer,
            // Vec2DefDeserializer,
        },
        serde_connections::InputConnectionDeserializer,
        AssetLoader,
    },
    nodes::{
        filter::kaleidoscope::{
            KaleidoscopeNode,
            KaleidoscopeValues,
        },
        NodeClass,
        NodeFamily,
    },
};
use egui_winit_vulkano::egui::{
    Pos2,
    // Vec2,
};
use serde::{
    de::{
        self,
        Deserializer,
        DeserializeSeed,
        MapAccess,
        Visitor,
    },
    Deserialize,
};
use std::{
    fmt,
    time::Instant,
};

// ----------------------------------------------------------------------------
// KaleidoscopeNode:
// ----------------------------------------------------------------------------

pub struct
    KaleidoscopeNodeDeserializer<'a, L> {
        pub asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    KaleidoscopeNodeDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        KaleidoscopeNode;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            Class,
            Family,
            Id,
            Name,
            // Scene,
            Values,
        }

        struct
            KaleidoscopeVisitor<'a, L>
        {
            asset_loader:
                &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            KaleidoscopeVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                KaleidoscopeNode;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct KaleidoscopeNode"
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    node
                =
                    KaleidoscopeNode {
                        class:
                            NodeClass::Kaleidoscope,
                        family:
                            NodeFamily::Filter,
                        id: String::new(),
                        name: String::new(),
                        scene:
                            self
                                .asset_loader
                                .new_kaleidoscope_scene(),
                        values:
                            KaleidoscopeValues::default(),
                        updated:
                            Instant::now(),
                    };

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return
                        Err(error),
                    Ok(None) => {
                        false

                        // match
                        //     id
                        //         .is_empty()
                        // |
                        //     name
                        //         .is_empty()
                        // |
                        //     values
                        //         .is_none()
                        // {
                        //     true => {
                        //         return Err(
                        //             de::Error::invalid_length(
                        //                 0,
                        //                 &"no keys left",
                        //             ),
                        //         );
                        //     },
                        //     false => false,
                        // },
                    },
                    Ok(Some(Field::Class)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(class) => match
                            class
                        {
                            NodeClass::Kaleidoscope => true,
                            _ => return
                                Err(
                                    de::Error::custom(
                                        "{class} not NodeClass::Kaleidoscope",
                                    ),
                                ),
                        },
                    },
                    Ok(Some(Field::Family)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(family) => match
                            family
                        {
                            NodeFamily::Filter => true,
                            _ => return Err(
                                de::Error::custom(
                                    "{family} not NodeFamily::Filter",
                                ),
                            ),
                        },
                    },
                    Ok(Some(Field::Id)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(string) => {
                            node
                                .id
                            =
                                string;

                            true
                        },
                    },
                    Ok(Some(Field::Name)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(string) => {
                            node
                                .name
                            =
                                string;

                            true
                        },
                    },
                    Ok(Some(Field::Values)) => match
                        map
                            .next_value_seed(
                                KaleidoscopeValuesDeserializer {
                                    asset_loader:
                                        self
                                            .asset_loader,
                                },
                            )
                    {
                        Err(error) => return
                            Err(error),
                        Ok(values) => {
                            node
                                .values
                            =
                                values;

                            true
                        },
                    },
                } {};

                Ok(
                    node,
                )
            }
        }

        const
            CLASS
        :
            &str
        =
            "class";

        const
            FAMILY
        :
            &str
        =
            "family";

        const
            ID
        :
            &str
        =
            "id";

        const
            NAME
        :
            &str
        =
            "name";

        const
            VALUES
        :
            &str
        =
            "values";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                CLASS,
                FAMILY,
                ID,
                NAME,
                VALUES,
            ];

        deserializer
            .deserialize_struct(
                "KaleidoscopeNode",
                FIELDS,
                KaleidoscopeVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// KaleidoscopeValues:
// ----------------------------------------------------------------------------

pub struct
    KaleidoscopeValuesDeserializer<'a, L> {
        pub asset_loader:
            &'a mut L,
    }

impl<'de, L>
    DeserializeSeed<'de>
for
    KaleidoscopeValuesDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        KaleidoscopeValues;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            State,
            MeshPosition,
            MeshRotate,
            MeshRotation,
            MeshRotationSpeed,
            MeshSize,
            TexAngle,
            TexAxisA,
            TexAxisB,
            TexLattice,
            TexPosition,
            TexRotate,
            TexRotation,
            TexRotationPreset,
            TexRotationSpeed,
            TexSide,
            TexSymmetry,
            // OutputSize,
            // PosOut,
            Source,
        }

        struct
            ValuesVisitor<'a, L>
        {
            asset_loader:
                &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            ValuesVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                KaleidoscopeValues;

            fn
                expecting
            (
                &self,
                formatter:
                    &mut fmt::Formatter,
            ) ->
                fmt::Result
            {
                formatter
                    .write_str(
                        "struct KaleidoscopeValues",
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    values
                =
                    KaleidoscopeValues::default();

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return Err(error),
                    Ok(None) => {
                        false
                    },
                    Ok(Some(Field::State)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(state) => {
                            values
                                .state
                            =
                                state;

                            true
                        },
                    },
                    Ok(Some(Field::MeshPosition)) => match
                        map
                            .next_value_seed(
                                Pos2DefDeserializer {}
                            )
                    {
                        Err(error) => return
                            Err(error),
                        Ok(mesh_position) => {
                            values
                                .mesh_position
                            =
                                Pos2 {
                                    x:
                                        mesh_position
                                            .x,
                                    y:
                                        mesh_position
                                            .y,
                                };

                            true
                        },
                    },
                    Ok(Some(Field::MeshRotate)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(mesh_rotate) => {
                            values
                                .mesh_rotate
                            =
                                mesh_rotate;

                            true
                        },
                    },
                    Ok(Some(Field::MeshRotation)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(mesh_rotation) => {
                            values
                                .mesh_rotation
                            =
                                mesh_rotation;

                            true
                        },
                    },
                    Ok(Some(Field::MeshRotationSpeed)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(mesh_rotation_speed) => {
                            values
                                .mesh_rotation_speed
                            =
                                mesh_rotation_speed;

                            true
                        },
                    },
                    Ok(Some(Field::MeshSize)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(mesh_size) => {
                            values
                                .mesh_size
                            =
                                mesh_size;

                            true
                        },
                    },
                    Ok(Some(Field::TexAngle)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_angle) => {
                            values
                                .tex_angle
                            =
                                tex_angle;

                            true
                        },
                    },
                    Ok(Some(Field::TexAxisA)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_axis_a) => {
                            values
                                .tex_axis_a
                            =
                                tex_axis_a;

                            true
                        },
                    },
                    Ok(Some(Field::TexAxisB)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_axis_b) => {
                            values
                                .tex_axis_b
                            =
                                tex_axis_b;

                            true
                        },
                    },
                    Ok(Some(Field::TexLattice)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_lattice) => {
                            values
                                .tex_lattice
                            =
                                tex_lattice;

                            true
                        },
                    },
                    Ok(Some(Field::TexPosition)) => match
                        map
                            .next_value_seed(
                                Pos2DefDeserializer {}
                            )
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_position) => {
                            values
                                .tex_position
                            =
                                Pos2 {
                                    x:
                                        tex_position
                                            .x,
                                    y:
                                        tex_position
                                            .y,
                                };

                            true
                        },
                    },
                    Ok(Some(Field::TexRotate)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_rotate) => {
                            values
                                .tex_rotate
                            =
                                tex_rotate;

                            true
                        },
                    },
                    Ok(Some(Field::TexRotation)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_rotation) => {
                            values
                                .tex_rotation
                            =
                                tex_rotation;

                            true
                        },
                    },
                    Ok(Some(Field::TexRotationPreset)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_rotation_preset) => {
                            values
                                .tex_rotation_preset
                            =
                                tex_rotation_preset;

                            true
                        },
                    },
                    Ok(Some(Field::TexRotationSpeed)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_rotation_speed) => {
                            values
                                .tex_rotation_speed
                            =
                                tex_rotation_speed;

                            true
                        },
                    },
                    Ok(Some(Field::TexSide)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_side) => {
                            values
                                .tex_side
                            =
                                tex_side;

                            true
                        },
                    },
                    Ok(Some(Field::TexSymmetry)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return
                            Err(error),
                        Ok(tex_symmetry) => {
                            values
                                .tex_symmetry
                            =
                                tex_symmetry;

                            true
                        },
                    },
                    // Ok(Some(Field::OutputSize)) => match
                    //     map
                    //         .next_value()
                    // {
                    //     Err(error) => return Err(
                    //         error,
                    //     ),
                    //     Ok(output_size) => {
                    //         // let vec_2: Vec2 = output_size;
                    //         let vec_def: Vec2Def = output_size;
                    //         // println!(
                    //         //     "!! output_size: {output_size}",
                    //         // );
                    //         values
                    //             .output_size
                    //         =
                    //             Vec2::new(
                    //                 NODE_OUTPUT_WIDTH,
                    //                 NODE_OUTPUT_HEIGHT,
                    //                 // vec_def
                    //                 //     .x,
                    //                 // vec_def
                    //                 //     .y,
                    //             );
                    //         true
                    //     },
                    // },
                    // Ok(Some(Field::PosOut)) => match
                    //     map
                    //         .next_value()
                    // {
                    //     Err(error) => return Err(
                    //         error,
                    //     ),
                    //     Ok(_) => {
                    //         values
                    //             .pos_out
                    //         =
                    //             Pos2::default();
                    //         true
                    //     },
                    // },
                    Ok(Some(Field::Source)) => {
                        match
                            map
                                .next_value_seed(
                                    InputConnectionDeserializer {
                                        asset_loader:
                                            self.
                                                asset_loader,
                                    }
                                )
                        {
                            Err(_error) => {
                                // println!("!! error: {error}"); // "!! error: invalid type: null, expected struct InputConnection at line..."
                                true
                                // return Err(error),
                            },
                            Ok(source) => {
                                values
                                    .source
                                =
                                    source;

                                true
                            },
                        }
                    },
                } {};

                Ok(
                    values,
                )
            }
        }

        const
            STATE
        :
            &str
        =
            "state";

        const
            MESH_POSITION
        :
            &str
        =
            "mesh_position";

        const
            MESH_ROTATE
        :
            &str
        =
            "mesh_rotate";

        const
            MESH_ROTATION
        :
            &str
        =
            "mesh_rotation";

        const
            MESH_ROTATION_SPEED
        :
            &str
        =
            "mesh_rotation_speed";

        const
            MESH_SIZE
        :
            &str
        =
            "mesh_size";

        const
            TEX_ANGLE
        :
            &str
        =
            "tex_angle";

        const
            TEX_AXIS_A
        :
            &str
        =
            "tex_axis_a";

        const
            TEX_AXIS_B
        :
            &str
        =
            "tex_axis_b";

        const
            TEX_LATTICE
        :
            &str
        =
            "tex_lattice";

        const
            TEX_POSITION
        :
            &str
        =
            "tex_position";

        const
            TEX_ROTATE
        :
            &str
        =
            "tex_rotate";

        const
            TEX_ROTATION
        :
            &str
        =
            "tex_rotation";

        const
            TEX_ROTATION_PRESET
        :
            &str
        =
            "tex_rotation_preset";

        const
            TEX_ROTATION_SPEED
        :
            &str
        =
            "mesh_rotation_speed";

        const
            TEX_SIDE
        :
            &str
        =
            "tex_side";

        const
            TEX_SYMMETRY
        :
            &str
        =
            "tex_symmetry";

        // const
        //     OUTPUT_SIZE
        // :
        //     &str
        // =
        //     "output_size";

        // const
        //     POS_OUT
        // :
        //     &str
        // =
        //     "pos_out";

        const
            SOURCE
        :
            &str
        =
            "source";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                STATE,
                MESH_POSITION,
                MESH_ROTATE,
                MESH_ROTATION,
                MESH_ROTATION_SPEED,
                MESH_SIZE,
                TEX_ANGLE,
                TEX_AXIS_A,
                TEX_AXIS_B,
                TEX_LATTICE,
                TEX_POSITION,
                TEX_ROTATE,
                TEX_ROTATION,
                TEX_ROTATION_PRESET,
                TEX_ROTATION_SPEED,
                TEX_SIDE,
                TEX_SYMMETRY,
                // OUTPUT_SIZE,
                // POS_OUT,
                SOURCE,
            ];

        deserializer
            .deserialize_struct(
                "KaleidoscopeValues",
                FIELDS,
                ValuesVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
