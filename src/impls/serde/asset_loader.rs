
use crate::{
    impls::vulkano::scenes::render::{
        color_mixer_scene::ColorMixerScene,
        fader_scene::FaderScene,
        kaleidoscope_scene::KaleidoscopeScene,
        mandala_scene::MandalaScene,
        mixer_scene::MixerScene,
    },
    nodes::connections::InputConnectionConstructor,
};
use egui_winit_vulkano::RenderResources;
use std::{
    cell::RefCell,
    // path::PathBuf,
    rc::Rc,
};

// ----------------------------------------------------------------------------

pub trait
    AssetLoader
{
    fn
        new_color_mixer_scene
    (
        &mut self,
    ) ->
        ColorMixerScene;

    fn
        new_fader_scene
    (
        &mut self,
    ) ->
        FaderScene;

    fn
        new_kaleidoscope_scene
    (
        &mut self,
    ) ->
        KaleidoscopeScene;

    fn
        new_mandala_scene
    (
        &mut self,
    ) ->
        MandalaScene;

    fn
        new_mixer_scene
    (
        &mut self,
    ) ->
        MixerScene;

    fn
        store_input_connection_constructor
    (
        &mut self,
        constructor: InputConnectionConstructor,
    );

}

// ----------------------------------------------------------------------------

pub struct
    AssetLoaderImpl<'a>
{
    pub input_connections: Rc<RefCell<Vec<InputConnectionConstructor>>>,
    pub render_resources: RenderResources<'a>,
}

impl<'a>
    AssetLoader
for
    AssetLoaderImpl<'a>
{
    fn
        new_color_mixer_scene
    (
        &mut self,
    ) ->
        ColorMixerScene
    {
        ColorMixerScene::new(
            self
                .render_resources
                .clone()
        )
    }

    fn
        new_fader_scene
    (
        &mut self,
    ) ->
        FaderScene
    {
        FaderScene::new(
            self
                .render_resources
                .clone()
        )
    }

    fn
        new_kaleidoscope_scene
    (
        &mut self,
    ) ->
        KaleidoscopeScene
    {
        KaleidoscopeScene::new(
            self
                .render_resources
                .clone()
        )
    }

    fn
        new_mandala_scene
    (
        &mut self,
    ) ->
        MandalaScene
    {
        MandalaScene::new(
            self
                .render_resources
                .clone()
        )
    }

    fn
        new_mixer_scene
    (
        &mut self,
    ) ->
        MixerScene
    {
        MixerScene::new(
            self
                .render_resources
                .clone()
        )
    }

    fn
        store_input_connection_constructor
    (
        &mut self,
        constructor: InputConnectionConstructor,
    ) {
        self
            .input_connections
            .borrow_mut()
            .push(
                constructor,
            );
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
