pub mod asset_loader;
pub mod definitions;
pub mod serde_connections;
// pub mod serde_control;
pub mod serde_filter;
// pub mod serde_sink;
// pub mod serde_source;

use crate::{
    app::data::{
        app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        StorageContainer,
    },
    impls::serde::{
        asset_loader::AssetLoader,
        serde_filter::FilterToNodeDeserializer,
    },
};
use serde::{
    de::{
        self,
        Deserializer,
        DeserializeSeed,
        MapAccess,
        Visitor,
    },
    Deserialize,
};
use std::fmt;

// ----------------------------------------------------------------------------

pub struct
    StorageDeserializer<'a, L>
{
    pub asset_loader:
        &'a mut L,
}

impl<'de, L>
    DeserializeSeed<'de>
for
    StorageDeserializer<'_, L>
where
    L:
        AssetLoader,
{
    type
        Value
    =
        StorageContainer;

    fn
        deserialize<D>
    (
        self,
        deserializer:
            D,
    ) ->
        Result<
            Self::Value,
            D::Error,
        >
    where
        D:
            Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum
            Field
        {
            MapControlToNode,
            MapFilterToNode,
            MapSinkToNode,
            MapSourceToNode,
        }

        // impl<'de>
        //     Deserialize<'de>
        // for
        //     Field
        // {
        //     fn
        //         deserialize<D>
        //     (
        //         deserializer:
        //             D,
        //     ) ->
        //         Result<
        //             Field,
        //             D::Error,
        //         >
        //     where
        //         D:
        //             Deserializer<'de>,
        //     {
        //         struct
        //             FieldVisitor;
        //
        //         impl<'de>
        //             Visitor<'de>
        //         for
        //             FieldVisitor
        //         {
        //             type
        //                 Value
        //             =
        //                 Field;
        //
        //             fn
        //                 expecting
        //             (
        //                 &self,
        //                 formatter:
        //                     &mut fmt::Formatter,
        //             ) ->
        //                 fmt::Result
        //             {
        //                 formatter
        //                     .write_str(
        //                         "container maps",
        //                     )
        //             }
        //
        //             fn
        //                 visit_str<E>
        //             (
        //                 self,
        //                 value:
        //                     &str,
        //             ) ->
        //                 Result<
        //                     Field,
        //                     E,
        //                 >
        //             where
        //                 E:
        //                     de::Error,
        //             {
        //                 match
        //                     value
        //                 {
        //                     MAP_CONTROL_TO_NODE => Ok(Field::MapControlToNode),
        //                     MAP_FILTER_TO_NODE => Ok(Field::MapFilterToNode),
        //                     MAP_SINK_TO_NODE => Ok(Field::MapSinkToNode),
        //                     MAP_SOURCE_TO_NODE => Ok(Field::MapSourceToNode),
        //                     _ => Err(
        //                         de::Error::unknown_field(
        //                             value,
        //                             FIELDS,
        //                         ),
        //                     ),
        //                 }
        //             }
        //         }
        //
        //         deserializer
        //             .deserialize_identifier(
        //                 FieldVisitor,
        //             )
        //     }
        // }

        struct
            StorageVisitor<'a, L>
        {
            asset_loader: &'a mut L,
        }

        impl<'de, L>
            Visitor<'de>
        for
            StorageVisitor<'_, L>
        where
            L:
                AssetLoader,
        {
            type
                Value
            =
                StorageContainer;

            fn
                expecting
            (
                &self,
                formatter: &mut fmt::Formatter,
            ) -> fmt::Result {
                formatter
                    .write_str(
                        "struct StorageContainer"
                    )
            }

            fn
                visit_map<A>
            (
                self,
                mut map: A,
            ) ->
                Result<
                    Self::Value,
                    A::Error,
                >
            where
                A:
                    MapAccess<'de>,
            {
                let mut
                    map_control_to_node
                :
                    Option<ControlToNode>
                =
                    None;
                let mut
                    map_filter_to_node
                :
                    Option<FilterToNode>
                =
                    None;
                let mut
                    map_sink_to_node
                :
                    Option<SinkToNode>
                =
                    None;
                let mut
                    map_source_to_node
                :
                    Option<SourceToNode>
                =
                    None;

                while
                    match
                        map
                            .next_key()
                {
                    Err(error) => return Err(error),
                    Ok(None) => match
                        map_control_to_node
                            .is_none()
                    |
                        map_filter_to_node
                            .is_none()
                    |
                        map_sink_to_node
                            .is_none()
                    |
                        map_source_to_node
                            .is_none()
                    {
                        true => {
                            // dbg!(map_control_to_node);
                            // dbg!(map_filter_to_node);
                            // dbg!(map_sink_to_node);
                            // dbg!(map_source_to_node);
                            return Err(
                                de::Error::invalid_length(
                                    0,
                                    &"no keys left",
                                ),
                            );
                        },
                        false => false,
                    },
                    Ok(Some(Field::MapControlToNode)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(value) => match
                            map_control_to_node
                        {
                            Some(_) => return Err(
                                de::Error::duplicate_field(
                                    MAP_CONTROL_TO_NODE,
                                ),
                            ),
                            None => {
                                map_control_to_node = Some(value);
                                true
                            },
                        },
                    },
                    Ok(Some(Field::MapFilterToNode)) => match
                        map
                            .next_value_seed(
                                FilterToNodeDeserializer {
                                    asset_loader:
                                        self
                                            .asset_loader,
                                }
                            )
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(value) => match
                            map_filter_to_node
                        {
                            Some(_) => return Err(
                                de::Error::duplicate_field(
                                    MAP_FILTER_TO_NODE,
                                ),
                            ),
                            None => {
                                map_filter_to_node = Some(value);
                                true
                            },
                        },
                    },
                    Ok(Some(Field::MapSinkToNode)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(value) => match
                            map_sink_to_node
                        {
                            Some(_) => return Err(
                                de::Error::duplicate_field(
                                    MAP_SINK_TO_NODE,
                                ),
                            ),
                            None => {
                                map_sink_to_node = Some(value);
                                true
                            },
                        },
                    },
                    Ok(Some(Field::MapSourceToNode)) => match
                        map
                            .next_value()
                    {
                        Err(error) => return Err(
                            error,
                        ),
                        Ok(value) => match
                            map_source_to_node
                        {
                            Some(_) => return Err(
                                de::Error::duplicate_field(
                                    MAP_SOURCE_TO_NODE,
                                ),
                            ),
                            None => {
                                map_source_to_node = Some(value);
                                true
                            },
                        },
                    },
                } {};

                Ok(
                    StorageContainer {
                        map_control_to_node:
                            match
                                map_control_to_node
                            {
                                None => return
                                    Err(
                                        de::Error::missing_field(
                                            MAP_CONTROL_TO_NODE,
                                        ),
                                    ),
                                Some(map) => map,
                            },
                        map_filter_to_node:
                            match
                                map_filter_to_node
                            {
                                None => return
                                    Err(
                                        de::Error::missing_field(
                                            MAP_FILTER_TO_NODE,
                                        ),
                                    ),
                                Some(map) => map,
                            },
                        map_sink_to_node:
                            match
                                map_sink_to_node
                            {
                                None => return
                                    Err(
                                        de::Error::missing_field(
                                            MAP_SINK_TO_NODE,
                                        ),
                                    ),
                                Some(map) => map,
                            },
                        map_source_to_node:
                            match
                                map_source_to_node
                            {
                                None => return
                                    Err(
                                        de::Error::missing_field(
                                            MAP_SOURCE_TO_NODE,
                                        )
                                    ),
                                Some(map) => map,
                            },
                    },
                )
            }
        }

        const
            MAP_CONTROL_TO_NODE
        :
            &str
        =
            "map_control_to_node";

        const
            MAP_FILTER_TO_NODE
        :
            &str
        =
            "map_filter_to_node";

        const
            MAP_SINK_TO_NODE
        :
            &str
        =
            "map_sink_to_node";

        const
            MAP_SOURCE_TO_NODE
        :
            &str
        =
            "map_source_to_node";

        const
            FIELDS
        :
            &[&str]
        =
            &[
                MAP_CONTROL_TO_NODE,
                MAP_FILTER_TO_NODE,
                MAP_SINK_TO_NODE,
                MAP_SOURCE_TO_NODE,
            ];

        deserializer
            .deserialize_struct(
                "StorageContainer",
                FIELDS,
                StorageVisitor {
                    asset_loader:
                        self
                            .asset_loader,
                },
            )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
