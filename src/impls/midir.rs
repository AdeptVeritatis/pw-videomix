pub mod change_values;
pub mod value_classes;

use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::{
            gui_events_midi::{
                MidiEvent,
                MidiEventClass,
            },
            GuiEvent,
        },
        gui::{
            bind::{
                midi_bind::MidiBind,
                BindClass,
            },
            options::GuiOptions,
        },
    },
    constants::{
        MIDI_EVENT_CONTROLLER,
        MIDI_EVENT_KEY_NOTE_OFF,
        MIDI_EVENT_KEY_NOTE_ON,
        MIDI_EVENT_PAD_AFTERTOUCH,
        MIDI_EVENT_PAD_NOTE_OFF,
        MIDI_EVENT_PAD_NOTE_ON,
        MIDI_EVENT_PITCH_BEND,
    },
    impls::midir::change_values::{
        control_values,
        filter_values,
        sink_values,
        source_values,
    },
    nodes::NodeFamily,
};
use midir::{
    Ignore,
    MidiInput,
    MidiInputConnection,
    MidiInputPort,
    MidiOutput,
    MidiOutputConnection,
    MidiOutputPort,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    fmt,
    rc::Rc,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

// #[derive(Debug)]
pub struct MidiConnections {
    pub input: MidiInputConnection<EventLoopProxy<GuiEvent>>,
    pub output: MidiOutputConnection,
}

// ----------------------------------------------------------------------------

/// Class of MIDI event.
#[derive(Debug, Clone, PartialEq)]
pub enum MidiClass {
    None,
    Controller,
    KeyNoteOff,
    KeyNoteOn,
    PadNoteOff,
    PadNoteOn,
}

impl fmt::Display for MidiClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::None => write!(f, "none"),
            Self::Controller => write!(f, "controller"),
            Self::KeyNoteOff => write!(f, "key note off"),
            Self::KeyNoteOn => write!(f, "key note on"),
            Self::PadNoteOff => write!(f, "pad note off"),
            Self::PadNoteOn => write!(f, "pad note on"),
        }
    }
}

// ----------------------------------------------------------------------------

/// Mode of MIDI value handling.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum MidiControllerClass {
    // From 0 to 127.
    Absolute,
    // For rotary encoder:
    // Direction and speed of turning a knob.
    // (ccw/left) -3, -2, -1, offset, +1, +2, +3 (cw/right)
    // Offset 0 -> 125, 126, 127, 0, 1, 2, 3
    // Offset 16 -> 13, 14, 15, 16, 17, 18, 19
    // Offset 64 -> 61, 62, 63, 64, 65, 66, 67
    RelativeOffset0,
    RelativeOffset16,
    RelativeOffset64,
}

impl fmt::Display for MidiControllerClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Absolute => write!(f, "absolute"),
            Self::RelativeOffset0 => write!(f, "relative [0]"), // "relative [0]", "relative0"
            Self::RelativeOffset16 => write!(f, "relative [16]"), // "relative [16]", "relative16"
            Self::RelativeOffset64 => write!(f, "relative [64]"), // "relative [64]", "relative64"
        }
    }
}
impl MidiControllerClass {
    fn get_controller_speed(
        &self,
        midi_event_value: u8,
        speed_fast: bool,
    ) -> f32 {
        match
            (
                speed_fast,
                match
                    self
                {
                    Self::Absolute
                    => 0.0,
                    Self::RelativeOffset0
                    => match
                        midi_event_value
                    {
                        125 => -5.0,
                        126 => -2.0,
                        127 => -1.0,
                        0 => 0.0,
                        1 => 1.0,
                        2 => 2.0,
                        3 => 5.0,
                        _ => 0.0,
                    },
                    Self::RelativeOffset16
                    => match
                        midi_event_value
                    {
                        13 => -5.0,
                        14 => -2.0,
                        15 => -1.0,
                        16 => 0.0,
                        17 => 1.0,
                        18 => 2.0,
                        19 => 5.0,
                        _ => 0.0,
                    },
                    Self::RelativeOffset64
                    => match
                        midi_event_value
                    {
                        61 => -5.0,
                        62 => -2.0,
                        63 => -1.0,
                        64 => 0.0,
                        65 => 1.0,
                        66 => 2.0,
                        67 => 5.0,
                        _ => 0.0,
                    },
                },
             )
        {
            (true, speed) => speed * 10.0,
            (false, speed) => speed,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, Copy)]
pub enum MidiControllerSpeedClass {
    Push,
    Toggle,
}

impl fmt::Display for MidiControllerSpeedClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Push => write!(f, "push"),
            Self::Toggle => write!(f, "toggle"),
        }
    }
}

// ----------------------------------------------------------------------------

/// Message received from a MIDI device.
// Timestamp is also available but not used.
#[derive(Clone)]
pub struct MidiMessage {
    pub channel: u8,
    pub class: u8,
    pub value: u8,
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct MidiNode {
    pub bind_enable: BindClass,
    pub bind_list: HashMap<u8, MidiBind>,
    pub bind_value: Option<MidiBind>,
    pub connection: Option<String>,
    pub controller_class: MidiControllerClass,
    pub enabled: bool,
    // pub ports_in: Vec<String>,
    // pub ports_out: Vec<String>,
}

impl Default for MidiNode {
    fn default() -> Self {
        Self {
            bind_enable: BindClass::None,
            bind_list: HashMap::new(),
            bind_value: None,
            connection: None,
            controller_class: MidiControllerClass::Absolute,
            enabled: false,
            // ports_in: Vec::new(),
        }
    }
}

impl MidiNode {
    pub fn message_received(
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        midi_connection_rc: Rc<RefCell<Option<MidiConnections>>>,
        midi_event: MidiMessage,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) -> Self {
        // println!(
        //     "ii MIDI event: ch {}, class {}, value {}",
        //     midi_event.channel,
        //     midi_event.class,
        //     midi_event.value,
        // );
        let mut bind_list: HashMap<u8, MidiBind> = self.bind_list.to_owned();
        // Check midi class and bind new MIDI channel to variable.
        let class: MidiClass = match
            midi_event
                .class
        {
            MIDI_EVENT_PAD_AFTERTOUCH => MidiClass::None,
            MIDI_EVENT_PITCH_BEND => MidiClass::None,
            MIDI_EVENT_KEY_NOTE_OFF => MidiClass::KeyNoteOff,
            MIDI_EVENT_PAD_NOTE_OFF => MidiClass::PadNoteOff,
            MIDI_EVENT_CONTROLLER => {
                match
                    self
                        .bind_enable
                {
                    BindClass::Value => {
                        match
                            self
                                .bind_value
                                .to_owned()
                        {
                            None => (),
                            Some(midi_bind) => {
                                println!("ii binding MIDI channel");
                                bind_list
                                    .insert(
                                        midi_event
                                            .channel,
                                        midi_bind,
                                    );
                            },
                        };
                    },
                    _ => (),
                };
                MidiClass::Controller
            },
            MIDI_EVENT_KEY_NOTE_ON => {
                match
                    self
                        .bind_enable
                {
                    BindClass::Button => {
                        match
                            self
                                .bind_value
                                .to_owned()
                        {
                            None => (),
                            Some(midi_bind) => {
                                println!("ii binding MIDI channel");
                                bind_list
                                    .insert(
                                        midi_event
                                            .channel,
                                        midi_bind,
                                    );
                            },
                        };
                    },
                    _ => (),
                };
                MidiClass::KeyNoteOn
            },
            MIDI_EVENT_PAD_NOTE_ON => {
                match
                    self
                        .bind_enable
                {
                    BindClass::Button => {
                        match
                            self
                                .bind_value
                                .to_owned()
                        {
                            None => (),
                            Some(midi_bind) => {
                                println!("ii binding MIDI channel");
                                bind_list
                                    .insert(
                                        midi_event
                                            .channel,
                                        midi_bind,
                                    );
                            },
                        };
                    },
                    _ => (),
                };
                MidiClass::PadNoteOn
            },
            _ => MidiClass::None,
        };

        match
            bind_list
                .clone()
                .get(&midi_event.channel)
        {
            // MIDI channel not bound.
            None => (),
            // MIDI channel already bound.
            Some(midi_bind) => {
                match
                    class
                {
                    MidiClass::None => (),
                    MidiClass::KeyNoteOff => {
                        self
                            .button_released(
                                midi_bind,
                                midi_connection_rc,
                                midi_event,
                                options_gui,
                            );
                    },
                    MidiClass::PadNoteOff => {
                        self
                            .button_released(
                                midi_bind,
                                midi_connection_rc,
                                midi_event,
                                options_gui,
                            );
                    },
                    MidiClass::Controller => {
                        self
                            .change_value(
                                map_control_to_node,
                                map_filter_to_node,
                                map_sink_to_node,
                                map_source_to_node,
                                midi_bind,
                                midi_connection_rc,
                                midi_event,
                                options_gui,
                            );
                    },
                    MidiClass::KeyNoteOn => {
                        self
                            .change_value(
                                map_control_to_node,
                                map_filter_to_node,
                                map_sink_to_node,
                                map_source_to_node,
                                midi_bind,
                                midi_connection_rc,
                                midi_event,
                                options_gui,
                            );
                    },
                    MidiClass::PadNoteOn => {
                        self
                            .change_value(
                                map_control_to_node,
                                map_filter_to_node,
                                map_sink_to_node,
                                map_source_to_node,
                                midi_bind,
                                midi_connection_rc,
                                midi_event,
                                options_gui,
                            );
                    },
                }
            },
        };
        Self {
            bind_enable: BindClass::None,
            bind_list,
            bind_value: None,
            ..self.to_owned()
        }

    }

    fn change_value(
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        midi_bind: &MidiBind,
        midi_connection_rc: Rc<RefCell<Option<MidiConnections>>>,
        midi_event: MidiMessage,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) {
        // println!("ii bound MIDI channel: {:?}", midi_bind);
        // Remove old binding to variable.
        // But old MIDI channel is not known!
        // if self.bind_enable { bind_list.remove(  ); }

        let mut gui_options: GuiOptions = options_gui
            .borrow()
            .to_owned();
        // Get direction and speed.
        let speed: f32 = midi_bind
            .controller_class
            .get_controller_speed(
                midi_event
                    .value,
                gui_options
                    .speed_fast,
            );
        // Load value parameters.
        match
            midi_bind
                .bind
                .node_family
        {
            NodeFamily::None => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "fast_knob" => {
                        match
                            gui_options
                                .speed_class
                        {
                            MidiControllerSpeedClass::Push => {
                                gui_options.speed_fast = true;
                            },
                            MidiControllerSpeedClass::Toggle => {
                                gui_options.speed_fast = !gui_options
                                    .speed_fast;
                            },
                        };
                        let midi_connections: Option<MidiConnections> = match
                            midi_connection_rc
                                .replace(
                                    None,
                                )
                        {
                            None => None,
                            Some(mut connections) => match
                                connections
                                    .output
                                    .send(
                                        &[
                                            144, // MESSAGE_BUTTON,
                                            midi_event
                                                .channel,
                                            match
                                                gui_options
                                                    .speed_fast
                                            {
                                                true => 1,
                                                false => 0,
                                            },
                                            // 1, // ON
                                        ]
                                    )
                            {
                                Err(error) => {
                                    println!("\n!! Problem sending midi message:\n error: {error:?}"); // start midi selection option again?
                                    Some(connections)
                                },
                                Ok(_) => Some(connections),
                            },
                        };
                        midi_connection_rc
                            .replace(
                                midi_connections,
                            );
                        options_gui
                            .replace(
                                gui_options,
                            );
                    },
                    _ => (),
                };
            },
            NodeFamily::Control => {
                control_values(
                    map_control_to_node,
                    midi_bind,
                    midi_event,
                    speed,
                );
            }
            NodeFamily::Filter => {
                filter_values(
                    map_filter_to_node,
                    midi_bind,
                    midi_event,
                    speed,
                );
            },
            NodeFamily::Sink => {
                sink_values(
                    map_sink_to_node,
                    midi_bind,
                    midi_event,
                    speed,

                );
            },
            NodeFamily::Source => {
                source_values(
                    map_source_to_node,
                    midi_bind,
                    midi_event,
                    speed,
                );
            },
        };
    }

    fn button_released(
        &self,
        midi_bind: &MidiBind,
        midi_connection_rc: Rc<RefCell<Option<MidiConnections>>>,
        midi_event: MidiMessage,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) {
        let mut gui_options: GuiOptions = options_gui
            .borrow()
            .to_owned();
        match
            midi_bind
                .bind
                .node_family
        {
            NodeFamily::None => {
                match
                    midi_bind
                        .bind
                        .key
                        .as_str()
                {
                    "fast_knob" => match
                        gui_options
                            .speed_class
                    {
                        MidiControllerSpeedClass::Toggle => (),
                        MidiControllerSpeedClass::Push => {
                            let midi_connections: Option<MidiConnections> = match
                                midi_connection_rc
                                    .replace(
                                        None,
                                    )
                            {
                                None => None,
                                Some(mut connections) => match
                                    connections
                                        .output
                                        .send(
                                            &[
                                                144, // MESSAGE_BUTTON,
                                                midi_event
                                                    .channel,
                                                0, // OFF
                                            ]
                                        )
                                {
                                    Err(error) => {
                                        println!("\n!! Problem sending midi message:\n error: {error:?}"); // start midi selection option again?
                                        Some(connections)
                                    },
                                    Ok(_) => Some(connections),
                                },
                            };
                            midi_connection_rc
                                .replace(
                                    midi_connections,
                                );
                            gui_options.speed_fast = false;
                            options_gui
                                .replace(
                                    gui_options,
                                );
                        },
                    },
                    _ => (),
                };
            },
            _ => (),
        };
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn connect_midi(
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    port_connect: String,
) -> Option<MidiConnections> {
    println!("ii connect MIDI to {port_connect}");
    let mut midi_input = MidiInput::new("pw-videomix input").unwrap(); // !!!

    // Create midir output port:
    let midi_output = match
        MidiOutput::new(
            "pw-videomix output"
        )
    {
        Err(error) => panic!("\n!! problem creating midi output:\n   error: {error:?}"),
        Ok(port) => port,
    };
    midi_input
        .ignore(Ignore::None);
    let mut input_port: Option<MidiInputPort> = None;
    for
        port
    in
        midi_input
            .ports()
    {
        match
            midi_input
                .port_name(&port)
        {
            Err(error) => println!("!! error: {error}"),
            Ok(port_name)
                if
                    port_name
                    ==
                    port_connect
            => {
                input_port = Some(port.to_owned());
                break
            },
            Ok(_) => (),
        }
    }
    let mut output_port: Option<MidiOutputPort> = None;
    for
        port
    in
        midi_output
            .ports()
    {
        match
            midi_output
                .port_name(&port)
        {
            Err(error) => println!("!! error: {error}"),
            Ok(port_name)
                if
                    port_name
                    ==
                    port_connect
            => {
                output_port = Some(port.to_owned());
                break
            },
            Ok(_) => (),
        }
    }
    // midi_connection_rc
    //     .replace(
    match
        input_port
    {
        None => {
            println!("!! error: no MIDI connection created");
            None
        },
        Some(midi_input_port) => match
            midi_input
                .connect(
                    &midi_input_port,
                    "pw-videomix in",
                    move |_stamp, message, event_loop_proxy| {
                        // println!("{}: {:?}", stamp, message);
                        event_loop_proxy
                            .clone()
                            .send_event(
                                GuiEvent::Midi(
                                    MidiEvent {
                                        event_class: MidiEventClass::Received(
                                            MidiMessage {
                                                channel: message[1],
                                                class: message[0],
                                                value: message[2],
                                            }
                                        ),
                                    }
                                ),
                            )
                            .ok();
                    },
                    event_loop_proxy.clone(),
                )
        {
            Err(error) => {
                println!("!! error: {error}");
                None
            },
            Ok(input) => match
                output_port
            {
                None => {
                    println!("!! error: no MIDI connection created");
                    None
                },
                Some(midi_output_port) => match
                    midi_output
                        .connect(
                            &midi_output_port,
                            "pw-videomix out",
                        )
                {
                    Err(error) => {
                        println!("!! error: {error}");
                        None
                    },
                    Ok(output) => Some(
                        MidiConnections {
                            input,
                            output,
                        }
                    ),
                },
            },
        },
    }
}
