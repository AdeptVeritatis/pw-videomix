
// ----------------------------------------------------------------------------
// Textures - image_view_sampler:
// ----------------------------------------------------------------------------

pub mod vs_texture {
    vulkano_shaders::shader! {
        ty: "vertex",
        src:
            "
                #version 450
                layout(location = 0) in vec2 position;
                layout(location = 1) in vec2 tex_coords;

                layout(location = 0) out vec2 f_tex_coords;

                void main() {
                    gl_Position = vec4(position, 0.0, 1.0);
                    f_tex_coords = tex_coords;
                }
            "
    }
}

// ----------------------------------------------------------------------------

pub mod fs_texture {
    vulkano_shaders::shader! {
        ty: "fragment",
        src:
            "
                #version 450
                layout(location = 0) in vec2 v_tex_coords;

                layout(location = 0) out vec4 f_color;

                layout(set = 0, binding = 0) uniform sampler2D tex;

                void main() {
                    f_color = texture(tex, v_tex_coords);
                }
            ",
    }
}

// ----------------------------------------------------------------------------
// Color mixer:
// ----------------------------------------------------------------------------

pub mod fs_color_mixer {
    vulkano_shaders::shader! {
        ty: "fragment",
        src:
            "
                #version 450
                layout(location = 0) in vec2 v_tex_coords;

                layout(location = 0) out vec4 f_color;

                layout(push_constant) uniform PushConstants {
                    float mix_rr;
                    float mix_rg;
                    float mix_rb;
                    float mix_gr;
                    float mix_gg;
                    float mix_gb;
                    float mix_br;
                    float mix_bg;
                    float mix_bb;
                } pc;

                layout(set = 0, binding = 0) uniform sampler2D tex;

                void main() {
                    f_color = texture(tex, v_tex_coords);
                    vec4 i_color = f_color;
                    f_color.r = (i_color.r * pc.mix_rr + i_color.g * pc.mix_rg + i_color.b * pc.mix_rb);
                    f_color.g = (i_color.r * pc.mix_gr + i_color.g * pc.mix_gg + i_color.b * pc.mix_gb);
                    f_color.b = (i_color.r * pc.mix_br + i_color.g * pc.mix_bg + i_color.b * pc.mix_bb);
                }
            ",
    }
}

// ----------------------------------------------------------------------------
// Fader:
// ----------------------------------------------------------------------------

pub mod fs_fader {
    vulkano_shaders::shader! {
        ty: "fragment",
        // vulkan_version: "1.2",
        // spirv_version: "1.5",
        src:
            "
                #version 450

                #extension GL_EXT_nonuniform_qualifier : enable

                layout(location = 0) in vec2 v_tex_coords;
                layout(location = 1) in flat uint tex_i;

                layout(location = 0) out vec4 f_color;

                layout(set = 0, binding = 0) uniform sampler s;
                layout(set = 0, binding = 1) uniform texture2D tex[];

                layout(push_constant) uniform PushConstants {
                    vec2 mix;
                } pc_mix;

                void main() {
                    f_color = texture(nonuniformEXT(sampler2D(tex[tex_i], s)), v_tex_coords);
                    f_color.rgb = f_color.rgb * pc_mix.mix[tex_i];
                }
            ",
    }
}

// ----------------------------------------------------------------------------
// Mixer:
// ----------------------------------------------------------------------------

pub mod vs_mixer {
    vulkano_shaders::shader! {
        ty: "vertex",
        src:
            "
                #version 450
                layout(location = 0) in vec2 position;
                layout(location = 1) in vec2 tex_coords;
                layout(location = 2) in uint tex_i;

                layout(location = 0) out vec2 f_tex_coords;
                layout(location = 1) out flat uint f_tex_i;

                void main() {
                    gl_Position = vec4(position, 0.0, 1.0);
                    f_tex_i = tex_i;
                    f_tex_coords = tex_coords;
                }
            ",
    }
}

// ----------------------------------------------------------------------------

pub mod fs_mixer {
    vulkano_shaders::shader! {
        ty: "fragment",
        // vulkan_version: "1.2",
        // spirv_version: "1.5",
        src:
            "
                #version 450

                #extension GL_EXT_nonuniform_qualifier : enable

                layout(location = 0) in vec2 v_tex_coords;
                layout(location = 1) in flat uint tex_i;

                layout(location = 0) out vec4 f_color;

                layout(set = 0, binding = 0) uniform sampler s;
                layout(set = 0, binding = 1) buffer Mixer {
                    float mix[];
                } buffer_mix;
                layout(set = 0, binding = 2) uniform texture2D tex[];

                void main() {
                    f_color = texture(nonuniformEXT(sampler2D(tex[tex_i], s)), v_tex_coords);
                    f_color.rgb = f_color.rgb * buffer_mix.mix[tex_i];
                }
            ",
    }
}
