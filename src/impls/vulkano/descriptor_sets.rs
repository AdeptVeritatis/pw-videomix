
use egui_winit_vulkano::RenderResources;
use std::{
    fmt,
    sync::Arc,
};
use vulkano::{
    buffer::Subbuffer,
    descriptor_set::{
        allocator::StandardDescriptorSetAllocator,
        layout::DescriptorSetLayout,
        // PersistentDescriptorSet,
        WriteDescriptorSet,
    },
    image::{
        sampler::{
            Filter,
            Sampler,
            SamplerAddressMode,
            SamplerCreateInfo,
            SamplerMipmapMode,
        },
        view::ImageView,
    },
    pipeline::layout::PipelineLayout,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub enum DescriptorSetClass {
    ImageViewArray,
    ImageViewSampler,
}

impl fmt::Display for DescriptorSetClass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match
            *self
        {
            DescriptorSetClass::ImageViewArray => write!(f, "ImageViewArray"),
            DescriptorSetClass::ImageViewSampler => write!(f, "ImageViewSampler"),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct  DescriptorSet {
    pub class: DescriptorSetClass,
    pub descriptor_set: Arc<vulkano::descriptor_set::DescriptorSet>,
}

impl DescriptorSet {
    pub fn image_view_array_buffer(
        buffer_values: Subbuffer<[f32]>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_inputs: Vec<Arc<ImageView>>,
        layout: Arc<PipelineLayout>,
        resources: RenderResources,
    ) -> Self {
        let descriptor_set_layout: &Arc<DescriptorSetLayout> = layout
            .set_layouts()
            .first()
            // .get(0)
            .unwrap();
        let sampler: Arc<Sampler> = Sampler::new(
            resources.queue.device().clone(),
            SamplerCreateInfo {
                mag_filter: Filter::Linear,
                min_filter: Filter::Linear,
                address_mode: [SamplerAddressMode::Repeat; 3],
                mipmap_mode: SamplerMipmapMode::Linear,
                ..Default::default()
            },
        )
        .unwrap();
        let descriptor_set = vulkano::descriptor_set::DescriptorSet::new_variable(
            descriptor_set_allocator,
            // resources.descriptor_set_allocator,
            descriptor_set_layout.clone(),
            image_inputs.len() as u32,
            [
                WriteDescriptorSet::sampler(0, sampler),
                WriteDescriptorSet::buffer(1, buffer_values.clone()),
                WriteDescriptorSet::image_view_array(2, 0, image_inputs), // keep this the last binding!
            ],
            [],
        )
        .unwrap();
        Self {
            class: DescriptorSetClass::ImageViewArray,
            descriptor_set,
        }
    }

    pub fn image_view_array_push(
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_next: Arc<ImageView>,
        image_prev: Arc<ImageView>,
        layout: Arc<PipelineLayout>,
        resources: RenderResources,
    ) -> Self {
        let descriptor_set_layout: &Arc<DescriptorSetLayout> = layout
            .set_layouts()
            .first()
            // .get(0)
            .unwrap();
        let sampler: Arc<Sampler> = Sampler::new(
            resources.queue.device().clone(),
            SamplerCreateInfo {
                mag_filter: Filter::Linear,
                min_filter: Filter::Linear,
                address_mode: [SamplerAddressMode::Repeat; 3],
                mipmap_mode: SamplerMipmapMode::Linear,
                ..Default::default()
            },
        )
        .unwrap();
        let descriptor_set = vulkano::descriptor_set::DescriptorSet::new_variable(
            descriptor_set_allocator,
            // resources.descriptor_set_allocator,
            descriptor_set_layout.clone(),
            2,
            [
                WriteDescriptorSet::sampler(0, sampler),
                WriteDescriptorSet::image_view_array(1, 0, [image_next, image_prev]),
            ],
            [],
        )
        .unwrap();
        Self {
            class: DescriptorSetClass::ImageViewArray,
            descriptor_set,
        }
    }

    pub fn image_view_sampler(
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_input: Arc<ImageView>,
        layout: Arc<PipelineLayout>,
        resources: RenderResources,
    ) -> Self {
        // let descriptor_set_layout: &Arc<DescriptorSetLayout> = self
        //     .pipeline
        //     .layout()
        //     .set_layouts()
        //     .get(0)
        //     .unwrap();
        let descriptor_set_layout: &Arc<DescriptorSetLayout> = layout
            .set_layouts()
            .first()
            // .get(0)
            .unwrap();
        let sampler: Arc<Sampler> = Sampler::new(
            resources.queue.device().clone(),
            SamplerCreateInfo {
                mag_filter: Filter::Linear,
                min_filter: Filter::Linear,
                address_mode: [SamplerAddressMode::Repeat; 3],
                mipmap_mode: SamplerMipmapMode::Linear,
                ..Default::default()
            },
        )
        .unwrap();

        let descriptor_set = vulkano::descriptor_set::DescriptorSet::new(
            descriptor_set_allocator,
            // resources.descriptor_set_allocator,
            descriptor_set_layout.clone(),
            [
                WriteDescriptorSet::image_view_sampler(0, image_input, sampler),
                // WriteDescriptorSet::sampler(0, sampler),
                // WriteDescriptorSet::image_view(1, image_input),
            ],
            [],
        )
        .unwrap();
        Self {
            class: DescriptorSetClass::ImageViewSampler,
            descriptor_set
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
