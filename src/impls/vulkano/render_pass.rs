
use crate::constants::IMAGE_FORMAT;
use std::sync::Arc;
use vulkano::{
    device::Device,
    render_pass::RenderPass,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn create_single_renderpass(
    device: Arc<Device>,
) -> Arc<RenderPass> {
    vulkano::single_pass_renderpass!(
        device.clone(),
        attachments: {
            color: {
                format: IMAGE_FORMAT,
                samples: 1,
                load_op: Clear,
                store_op: Store,
            },
        },
        pass: {
            color: [color],
            depth_stencil: {},
        },
    )
    .unwrap()
}
