
use vulkano::{
    buffer::BufferContents,
    pipeline::graphics::vertex_input::Vertex,
};

// ----------------------------------------------------------------------------

#[repr(C)]
#[derive(Debug, Clone, BufferContents, Vertex)]
pub struct LayerVertex {
    #[format(R32G32_SFLOAT)]
    pub position: [f32; 2],
    #[format(R32G32_SFLOAT)]
    pub tex_coords: [f32; 2],
    #[format(R32_UINT)]
    pub tex_i: u32,
}

// ----------------------------------------------------------------------------

#[repr(C)]
#[derive(Debug, Clone, BufferContents, Vertex)]
pub struct TextureVertex {
    #[format(R32G32_SFLOAT)]
    pub position: [f32; 2],
    #[format(R32G32_SFLOAT)]
    pub tex_coords: [f32; 2],
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
