pub mod copy_connection_scene;
pub mod copy_data_to_image_scene;
#[cfg(unix)]
pub mod pw_input_scene;
#[cfg(unix)]
pub mod pw_output_scene;
#[cfg(feature = "gstreamer")]
pub mod video_scene;

// All pipelines copying data.

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
