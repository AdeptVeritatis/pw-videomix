// All pipelines using a render pass.

pub mod color_mixer_scene;
pub mod fader_scene;
pub mod kaleidoscope_scene;
pub mod mandala_scene;
pub mod mixer_scene;
// Secondary render pass for render callbacks.
pub mod show_texture_scene;
// Both, a copy and a render pass scene.
pub mod stack_scene;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
