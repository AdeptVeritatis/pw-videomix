
use crate::{
    constants::MIXER_MAX_INPUTS,
    impls::vulkano::{
        descriptor_sets::DescriptorSet,
        render_pass::create_single_renderpass,
        shader::{
            fs_mixer,
            vs_mixer,
        },
        vertices::LayerVertex,
        create_image_storage,
    },
    nodes::filter::FilterMesh,
};
use egui_winit_vulkano::RenderResources;
use std::{
    fmt,
    sync::Arc,
};
use vulkano::{
    buffer::{
        Buffer,
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        RenderPassBeginInfo,
    },
    descriptor_set::{
        allocator::StandardDescriptorSetAllocator,
        layout::DescriptorBindingFlags,
        // PersistentDescriptorSet,
    },
    device::Device,
    image::{
        view::ImageView,
        Image,
    },
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    pipeline::{
        graphics::{
            color_blend::{
                AttachmentBlend,
                BlendFactor,
                BlendOp,
                ColorBlendState,
                ColorBlendAttachmentState,
                ColorComponents,
            },
            input_assembly::{
                InputAssemblyState,
                PrimitiveTopology,
            },
            multisample::MultisampleState,
            rasterization::RasterizationState,
            vertex_input::{
                Vertex,
                VertexDefinition,
            },
            viewport::ViewportState,
            GraphicsPipeline,
            GraphicsPipelineCreateInfo,
        },
        layout::{
            PipelineLayout,
            PipelineDescriptorSetLayoutCreateInfo,
        },
        DynamicState,
        PipelineBindPoint,
        PipelineShaderStageCreateInfo,
    },
    render_pass::{
        Framebuffer,
        FramebufferCreateInfo,
        RenderPass,
        Subpass,
    },
    sync::GpuFuture,
};

// ----------------------------------------------------------------------------

// See vulkano / examples / runtime-array.

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct MixerScene {
    // VertexBuffer, IndexBuffer & Pipeline should be in it.
    // Layout is already part of pipeline but is inline private. Don't know, how to use it.
    // RenderPass is already in resources.
    // DescriptorSets, Sampler, SwapchainImage or Textures are not part of it.

    // Buffer to transfer the mixer values into GPU for each layer.
    pub buffer_mix: Subbuffer<[f32]>,

    // Image to store the mixed result.
    pub image_storage: Arc<ImageView>,

    // Gets index and vertex buffer from outside. Later, constants will be pushed into the buffer here.
    pub layout: Arc<PipelineLayout>,
    pub pipeline: Arc<GraphicsPipeline>,
    pub render_pass: Arc<RenderPass>,
}

impl fmt::Debug for MixerScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("MixerScene")
            .field("image_storage", &self.image_storage.image().extent())
            .finish()
    }
}

impl MixerScene {
    pub fn new(
        resources: RenderResources,
    ) -> Self {
        let device: Arc<Device> = resources.queue.device().to_owned();
    // Render pass:
        let render_pass: Arc<RenderPass> = create_single_renderpass(
            device.clone(),
        );
        let subpass: Subpass = Subpass::from(
            render_pass.clone(),
            0,
        )
        .unwrap();
    // Image storage:
        let image: Arc<Image> = create_image_storage(
            resources.clone(),
        );
    // Pipeline:
        let (pipeline, layout): (Arc<GraphicsPipeline>, Arc<PipelineLayout>) = {
            // Shader modules:
            let vs = vs_mixer::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let fs = fs_mixer::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let vertex_input_state = LayerVertex::per_vertex()
                .definition(&vs)
                .unwrap();
            // Layout:
            let stages = [
                PipelineShaderStageCreateInfo::new(vs),
                PipelineShaderStageCreateInfo::new(fs),
            ];
            let layout = {
                let mut layout_create_info =
                    PipelineDescriptorSetLayoutCreateInfo::from_stages(&stages);

                // Adjust the info for set 0, binding 2 to make it variable with more descriptors.
                let binding = layout_create_info.set_layouts[0]
                    .bindings
                    .get_mut(&2)
                    .unwrap();
                binding.binding_flags |= DescriptorBindingFlags::VARIABLE_DESCRIPTOR_COUNT;
                binding.descriptor_count = MIXER_MAX_INPUTS;

                PipelineLayout::new(
                    device.clone(),
                    layout_create_info
                        .into_pipeline_layout_create_info(device.clone())
                        .unwrap(),
                )
                .unwrap()
            };
            let pipeline = GraphicsPipeline::new(
                device.clone(),
                None,
                GraphicsPipelineCreateInfo {
                    stages: stages.into_iter().collect(),
                    vertex_input_state: Some(vertex_input_state),
                    input_assembly_state: Some(InputAssemblyState {
                        topology: PrimitiveTopology::TriangleList,
                        ..Default::default()
                    }),
                    viewport_state: Some(ViewportState::default()),
                    rasterization_state: Some(RasterizationState::default()),
                    multisample_state: Some(MultisampleState::default()),
                    color_blend_state: Some(ColorBlendState::with_attachment_states(
                        subpass.num_color_attachments(),
                        ColorBlendAttachmentState {
                            // blend: None,
                            blend: Some(AttachmentBlend{
                                // color_blend_op: BlendOp::Add,
                                // src_color_blend_factor: BlendFactor::SrcAlpha,
                                color_blend_op: BlendOp::Max,
                                src_color_blend_factor: BlendFactor::One,
                                dst_color_blend_factor: BlendFactor::One,
                                alpha_blend_op: BlendOp::Max,
                                src_alpha_blend_factor: BlendFactor::One,
                                dst_alpha_blend_factor: BlendFactor::One,
                            }),
                            color_write_mask: ColorComponents::all(),
                            color_write_enable: true,
                        },
                    )),
                    dynamic_state: [DynamicState::Viewport].into_iter().collect(),
                    subpass: Some(subpass.clone().into()),
                    ..GraphicsPipelineCreateInfo::layout(layout.clone())
                },
            )
            .unwrap();
            ( pipeline, layout)
        };
    // Buffer mix values:
        // let buffer_mix: Subbuffer<f32> = Buffer::new_sized(, , ).unwrap();
        let buffer_mix: Subbuffer<[f32]> = Buffer::from_iter(
            resources.memory_allocator,
            BufferCreateInfo {
                usage: BufferUsage::STORAGE_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            [1.0],
        )
        .unwrap();
    // MixerScene:
        Self {
            buffer_mix,
            image_storage: ImageView::new_default(image).unwrap(),
            layout,
            pipeline,
            render_pass,
        }
    }

    /// Draw the result of the "primary" pipeline into a framebuffer.
    pub fn render(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_inputs: Vec<Arc<ImageView>>,
        image_output: Arc<ImageView>,
        mixer_mesh: FilterMesh,
        resources: RenderResources,
    ) {
    // Framebuffer:
        let framebuffer = Framebuffer::new(
            self.render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![image_output.clone()],
                ..Default::default()
            },
        )
        .unwrap();
    // Vertex buffer:
        let vertex_buffer: Subbuffer<[LayerVertex]> = Buffer::from_iter(
            resources.clone().memory_allocator,
            BufferCreateInfo {
                usage: BufferUsage::VERTEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            mixer_mesh.vertices,
        )
        .unwrap();
    // Index buffer:
        let index_buffer: Subbuffer<[u32]> = Buffer::from_iter(
            resources.memory_allocator.clone(),
            BufferCreateInfo {
                usage: BufferUsage::INDEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            mixer_mesh.indices,
        )
        .unwrap();
    // Descriptor set:
        let desc_set = DescriptorSet::image_view_array_buffer(
            self.buffer_mix.clone(),
            descriptor_set_allocator
                .clone(),
            image_inputs,
            self.layout.clone(),
            resources.clone(),
        )
        .descriptor_set;
        // let desc_set = self.create_descriptor_set(
        //     image_inputs,
        //     resources.clone(),
        // );
        // Add the scene's rendering commands to the command buffer
        let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
            command_buffer_allocator,
            // resources.command_buffer_allocator,
            resources.queue.queue_family_index(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        unsafe {
            command_buffer_builder
                .begin_render_pass(
                    RenderPassBeginInfo {
                        clear_values: vec![Some([0.0; 4].into())],
                        // clear_values: vec![None],
                        ..RenderPassBeginInfo::framebuffer(framebuffer)
                    },
                    Default::default(),
                )
                .unwrap()
                .set_viewport(
                    0,
                    [mixer_mesh.viewport]
                    .into_iter()
                    .collect(),
                )
                .unwrap()
                .bind_pipeline_graphics(self.pipeline.clone())
                .unwrap()
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.layout.clone(),
                    0,
                    desc_set,
                )
                .unwrap()
                .bind_vertex_buffers(0, vertex_buffer)
                .unwrap()
                .bind_index_buffer(index_buffer.clone())
                .unwrap()
                .draw_indexed(index_buffer.len() as u32, 1, 0, 0, 0)
        }
            .unwrap()
            .end_render_pass(Default::default())
            .unwrap();

        let command_buffer = command_buffer_builder.build().unwrap();
        let future = vulkano::sync::now(resources.queue.device().clone())
            .then_execute(resources.queue, command_buffer)
            .unwrap()
            .then_signal_fence_and_flush()
            .unwrap();
        future.wait(None).unwrap();
        // println!("mandala scene rendered");
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
