#![allow(clippy::identity_op)]

use crate::{
    // constants::MIXER_MAX_INPUTS,
    impls::vulkano::{
        descriptor_sets::DescriptorSet,
        render_pass::create_single_renderpass,
        shader::{
            fs_fader::{
                self,
                PushConstants,
            },
            vs_mixer,
            fs_texture,
            vs_texture,
        },
        vertices::{
            LayerVertex,
            TextureVertex,
        },
        create_image_storage,
    },
    nodes::filter::FilterMesh,
};
use egui_winit_vulkano::RenderResources;
use std::{
    fmt,
    sync::Arc,
};
use vulkano::{
    buffer::{
        Buffer,
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        RenderPassBeginInfo,
    },
    descriptor_set::{
        allocator::StandardDescriptorSetAllocator,
        layout::DescriptorBindingFlags,
    },
    device::Device,
    image::{
        view::ImageView,
        Image,
    },
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    pipeline::{
        graphics::{
            color_blend::{
                AttachmentBlend,
                BlendFactor,
                BlendOp,
                ColorBlendState,
                ColorBlendAttachmentState,
                ColorComponents,
            },
            input_assembly::{
                InputAssemblyState,
                PrimitiveTopology,
            },
            multisample::MultisampleState,
            rasterization::RasterizationState,
            vertex_input::{
                Vertex,
                VertexDefinition,
            },
            viewport::{
                Viewport,
                ViewportState,
            },
            GraphicsPipeline,
            GraphicsPipelineCreateInfo,
        },
        layout::{
            PipelineLayout,
            PipelineDescriptorSetLayoutCreateInfo,
        },
        DynamicState,
        PipelineBindPoint,
        PipelineShaderStageCreateInfo,
    },
    render_pass::{
        Framebuffer,
        FramebufferCreateInfo,
        RenderPass,
        Subpass,
    },
    sync::GpuFuture,
};

// ----------------------------------------------------------------------------

// See vulkano / examples / runtime-array.

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct FaderScene {
    // VertexBuffer, IndexBuffer & Pipeline should be in it.
    // Layout is already part of pipeline but is inline private. Don't know, how to use it.
    // RenderPass is already in resources.
    // DescriptorSets, Sampler, SwapchainImage or Textures are not part of it.

    // Image to store the result.
    pub image_storage: Arc<ImageView>,

    // Gets index and vertex buffer from outside. Later, constants will be pushed into the buffer here.
    pub layout_hold: Arc<PipelineLayout>,
    pub layout_mix: Arc<PipelineLayout>,
    pub pipeline_hold: Arc<GraphicsPipeline>,
    pub pipeline_mix: Arc<GraphicsPipeline>,
    pub render_pass: Arc<RenderPass>,
}

impl fmt::Debug for FaderScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("FaderScene")
            .field("image_storage", &self.image_storage.image().extent())
            .finish()
    }
}

impl FaderScene {
    pub fn new(
        resources: RenderResources,
    ) -> Self {
        let device: Arc<Device> = resources.queue.device().to_owned();
    // Render pass:
        let render_pass: Arc<RenderPass> = create_single_renderpass(
            device.clone(),
        );
        let subpass: Subpass = Subpass::from(
            render_pass.clone(),
            0,
        )
        .unwrap();
    // Image storage:
        let image: Arc<Image> = create_image_storage(
            resources.clone(),
        );
    // Pipeline - hold:
        let (pipeline_hold, layout_hold): (Arc<GraphicsPipeline>, Arc<PipelineLayout>) = {
            // Shader modules:
            let vs = vs_texture::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let fs = fs_texture::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let vertex_input_state = TextureVertex::per_vertex()
                .definition(&vs)
                .unwrap();
            // Layout:
            let stages = [
                PipelineShaderStageCreateInfo::new(vs),
                PipelineShaderStageCreateInfo::new(fs),
            ];
            let layout = PipelineLayout::new(
                device.clone(),
                PipelineDescriptorSetLayoutCreateInfo::from_stages(&stages)
                    .into_pipeline_layout_create_info(device.clone())
                    .unwrap(),
            )
            .unwrap();
            let pipeline = GraphicsPipeline::new(
                device.clone(),
                None,
                GraphicsPipelineCreateInfo {
                    stages: stages.into_iter().collect(),
                    vertex_input_state: Some(vertex_input_state),
                    input_assembly_state: Some(InputAssemblyState {
                        topology: PrimitiveTopology::TriangleStrip,
                        ..Default::default()
                    }),
                    viewport_state: Some(ViewportState::default()),
                    rasterization_state: Some(RasterizationState::default()),
                    multisample_state: Some(MultisampleState::default()),
                    color_blend_state: Some(ColorBlendState::with_attachment_states(
                        subpass.num_color_attachments(),
                        ColorBlendAttachmentState::default(),
                    )),
                    dynamic_state: [DynamicState::Viewport].into_iter().collect(),
                    subpass: Some(subpass.clone().into()),
                    ..GraphicsPipelineCreateInfo::layout(layout.clone())
                },
            )
            .unwrap();
            ( pipeline, layout)
        };
    // Pipeline - mix:
        let (pipeline_mix, layout_mix): (Arc<GraphicsPipeline>, Arc<PipelineLayout>) = {
            // Shader modules:
            let vs = vs_mixer::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let fs = fs_fader::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let vertex_input_state = LayerVertex::per_vertex()
                .definition(&vs)
                .unwrap();
            // Layout:
            let stages = [
                PipelineShaderStageCreateInfo::new(vs),
                PipelineShaderStageCreateInfo::new(fs),
            ];
            let layout = {
                let mut layout_create_info =
                    PipelineDescriptorSetLayoutCreateInfo::from_stages(&stages);

                // Adjust the info for set 0, binding 2 to make it variable with more descriptors.
                let binding = layout_create_info.set_layouts[0]
                    .bindings
                    .get_mut(&1)
                    .unwrap();
                binding.binding_flags |= DescriptorBindingFlags::VARIABLE_DESCRIPTOR_COUNT;
                binding.descriptor_count = 2;

                PipelineLayout::new(
                    device.clone(),
                    layout_create_info
                        .into_pipeline_layout_create_info(device.clone())
                        .unwrap(),
                )
                .unwrap()
            };
            let pipeline = GraphicsPipeline::new(
                device.clone(),
                None,
                GraphicsPipelineCreateInfo {
                    stages: stages.into_iter().collect(),
                    vertex_input_state: Some(vertex_input_state),
                    input_assembly_state: Some(InputAssemblyState {
                        topology: PrimitiveTopology::TriangleList,
                        ..Default::default()
                    }),
                    viewport_state: Some(ViewportState::default()),
                    rasterization_state: Some(RasterizationState::default()),
                    multisample_state: Some(MultisampleState::default()),
                    color_blend_state: Some(ColorBlendState::with_attachment_states(
                        subpass.num_color_attachments(),
                        ColorBlendAttachmentState {
                            // blend: None,
                            blend: Some(AttachmentBlend{
                                // color_blend_op: BlendOp::Add,
                                // src_color_blend_factor: BlendFactor::SrcAlpha,
                                color_blend_op: BlendOp::Max,
                                src_color_blend_factor: BlendFactor::One,
                                dst_color_blend_factor: BlendFactor::One,
                                alpha_blend_op: BlendOp::Max,
                                src_alpha_blend_factor: BlendFactor::One,
                                dst_alpha_blend_factor: BlendFactor::One,
                            }),
                            color_write_mask: ColorComponents::all(),
                            color_write_enable: true,
                        },
                    )),
                    dynamic_state: [DynamicState::Viewport].into_iter().collect(),
                    subpass: Some(subpass.clone().into()),
                    ..GraphicsPipelineCreateInfo::layout(layout.clone())
                },
            )
            .unwrap();
            ( pipeline, layout)
        };
    // FaderScene:
        Self {
            image_storage: ImageView::new_default(image).unwrap(),
            layout_hold,
            layout_mix,
            pipeline_hold,
            pipeline_mix,
            render_pass,
        }
    }

    fn fader_mesh(
        self,
        extent_next: [u32; 3],
        extent_prev: [u32; 3],
        viewport: Viewport,
    ) -> FilterMesh {
    // ) -> Vec<LayerVertex> {
        let mut indices: Vec<u32> = Vec::new();
        let mut vertices: Vec<LayerVertex> = Vec::new();

        let framebuffer_extent: [u32; 3] = self.image_storage.image().extent();
        let framebuffer_aspect_ratio: f32 = framebuffer_extent[0] as f32 / framebuffer_extent[1] as f32;
        for (tex_i, layer_extent) in [extent_next, extent_prev].iter().enumerate() {
            let layer_aspect_ratio: f32 = layer_extent[0] as f32 / layer_extent[1] as f32;
            match
                framebuffer_aspect_ratio
                > layer_aspect_ratio
            {
                // "relative portrait"
                true => {
                    // 0 - TL
                    vertices
                        .push(
                            LayerVertex {
                                position: [-1.0 * layer_aspect_ratio * framebuffer_aspect_ratio.recip(), -1.0],
                                tex_coords: [0.0, 0.0],
                                tex_i: tex_i as u32,
                            }
                         );
                    // 1 - TR
                    vertices
                        .push(
                            LayerVertex {
                                position: [layer_aspect_ratio * framebuffer_aspect_ratio.recip(), -1.0],
                                tex_coords: [1.0, 0.0],
                                tex_i: tex_i as u32,
                            }
                         );
                    // 2 - BR
                    vertices
                        .push(
                            LayerVertex {
                                position: [layer_aspect_ratio * framebuffer_aspect_ratio.recip(), 1.0],
                                tex_coords: [1.0, 1.0],
                                tex_i: tex_i as u32,
                            }
                         );
                    // 3 - BL
                    vertices
                        .push(
                            LayerVertex {
                                position: [-1.0 * layer_aspect_ratio * framebuffer_aspect_ratio.recip(), 1.0],
                                tex_coords: [0.0, 1.0],
                                tex_i: tex_i as u32,
                            }
                         );
                },
                // "relative landscape"
                false => {
                    // 0 - TL
                    vertices
                        .push(
                            LayerVertex {
                                position: [-1.0, - 1.0 * framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                                tex_coords: [0.0, 0.0],
                                tex_i: tex_i as u32,
                            }
                         );
                    // 1 - TR
                    vertices
                        .push(
                            LayerVertex {
                                position: [1.0, - 1.0 * framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                                tex_coords: [1.0, 0.0],
                                tex_i: tex_i as u32,
                            }
                         );
                    // 2 - BR
                    vertices
                        .push(
                            LayerVertex {
                                position: [1.0, framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                                tex_coords: [1.0, 1.0],
                                tex_i: tex_i as u32,
                            }
                         );
                    // 3 - BL
                    vertices
                        .push(
                            LayerVertex {
                                position: [-1.0, framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                                tex_coords: [0.0, 1.0],
                                tex_i: tex_i as u32,
                            }
                         );
                },
            };
            // Next 6 indices for next 4 vertices.
            indices.push(tex_i as u32 * 4 + 0);
            indices.push(tex_i as u32 * 4 + 1);
            indices.push(tex_i as u32 * 4 + 2);
            indices.push(tex_i as u32 * 4 + 2);
            indices.push(tex_i as u32 * 4 + 3);
            indices.push(tex_i as u32 * 4 + 0);
        }
        // let extent: [f32; 2] = [self.values.output_size.x, self.values.output_size.y];
        // let viewport: Viewport = Viewport {
        //     offset: [
        //         0.0,
        //         0.0,
        //     ],
        //     extent,
        //     depth_range: 0.0..=1.0,
        // };
        FilterMesh {
            indices,
            vertices,
            viewport,
        }
        // vertices
    }

}

// Render scene:
impl FaderScene {
// Render - hold:
    /// Draw the result of the "primary" pipeline into a framebuffer.
    pub fn render_hold(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_input: Arc<ImageView>,
        resources: RenderResources,
    ) {
    // Framebuffer:
        let framebuffer = Framebuffer::new(
            self.render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![self.image_storage.clone()],
                ..Default::default()
            },
        )
        .unwrap();

    // Vertex buffer:
        let framebuffer_extent: [u32; 3] = self.image_storage.image().extent();
        let framebuffer_aspect_ratio: f32 = framebuffer_extent[0] as f32 / framebuffer_extent[1] as f32;
        let layer_extent: [u32; 3] = image_input.image().extent();
        let layer_aspect_ratio: f32 = layer_extent[0] as f32 / layer_extent[1] as f32;
        let vertices = match
            framebuffer_aspect_ratio
            > layer_aspect_ratio
        {
            // "relative portrait"
            true => [
                // 0 - TL
                TextureVertex {
                    position: [-1.0 * layer_aspect_ratio * framebuffer_aspect_ratio.recip(), -1.0],
                    tex_coords: [0.0, 0.0],
                },
                // 1 - TR
                TextureVertex {
                    position: [layer_aspect_ratio * framebuffer_aspect_ratio.recip(), -1.0],
                    tex_coords: [1.0, 0.0],
                },
                // 2 - BL
                TextureVertex {
                    position: [-1.0 * layer_aspect_ratio * framebuffer_aspect_ratio.recip(), 1.0],
                    tex_coords: [0.0, 1.0],
                },
                // 3 - BR
                TextureVertex {
                    position: [layer_aspect_ratio * framebuffer_aspect_ratio.recip(), 1.0],
                    tex_coords: [1.0, 1.0],
                },
            ],
            // "relative landscape"
            false => [
                // 0 - TL
                TextureVertex {
                    position: [-1.0, - 1.0 * framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                    tex_coords: [0.0, 0.0],
                },
                // 1 - TR
                TextureVertex {
                    position: [1.0, - 1.0 * framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                    tex_coords: [1.0, 0.0],
                },
                // 2 - BL
                TextureVertex {
                    position: [-1.0, framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                    tex_coords: [0.0, 1.0],
                },
                // 3 - BR
                TextureVertex {
                    position: [1.0, framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                    tex_coords: [1.0, 1.0],
                },
            ],
        };
        let vertex_buffer: Subbuffer<[TextureVertex]> = Buffer::from_iter(
            resources.clone().memory_allocator,
            BufferCreateInfo {
                usage: BufferUsage::VERTEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            vertices,
        )
        .unwrap();
    // Viewport:
        // create self.stack_mesh() in stack_node!!!!!
        let extent: [f32; 2] = [
            framebuffer_extent[0] as f32,
            framebuffer_extent[1] as f32,
        ];
        let viewport: Viewport = Viewport {
            offset: [
                0.0,
                0.0,
            ],
            extent,
            depth_range: 0.0..=1.0,
        };
    // Descriptor set:
        let desc_set = DescriptorSet::image_view_sampler(
            descriptor_set_allocator,
            image_input.clone(),
            self.layout_hold.clone(),
            resources.clone(),
        )
        .descriptor_set;
    // Command buffer:
        let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
            command_buffer_allocator,
            // resources.command_buffer_allocator.clone(),
            resources.queue.queue_family_index(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();

        unsafe {
            command_buffer_builder
                .begin_render_pass(
                    RenderPassBeginInfo {
                        clear_values: vec![Some([0.0; 4].into())],
                        // clear_values: vec![None],
                        ..RenderPassBeginInfo::framebuffer(framebuffer)
                    },
                    Default::default(),
                )
                .unwrap()
                .set_viewport(
                    0,
                    [viewport]
                    .into_iter()
                    .collect(),
                )
                .unwrap()
                .bind_pipeline_graphics(self.pipeline_hold.clone())
                .unwrap()
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.layout_hold.clone(),
                    0,
                    desc_set,
                )
                .unwrap()
                .bind_vertex_buffers(0, vertex_buffer.clone())
                .unwrap()
                .draw(vertex_buffer.len() as u32, 1, 0, 0)
        }
            .unwrap()
            .end_render_pass(Default::default())
            .unwrap();

        // dbg!(image_storage.image().extent());
        let command_buffer = command_buffer_builder
            .build()
            .unwrap();
        // let _future = command_buffer
        //     .execute(resources.queue)
        //     .unwrap()
        //     .then_signal_fence_and_flush()
        //     .unwrap();
        let future = vulkano::sync::now(resources.queue.device().clone())
            .then_execute(resources.queue, command_buffer)
            .unwrap()
            .then_signal_fence_and_flush()
            .unwrap();
        future.wait(None).unwrap();

        // println!("mandala scene rendered");
    }
// Render - mix:
    /// Draw the result of the "primary" pipeline into a framebuffer.
    pub fn render_mix(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_next: Arc<ImageView>,
        image_prev: Arc<ImageView>,
        mix: f32,
        resources: RenderResources,
        viewport: Viewport,
    ) {
    // FilterMesh:
        let fader_mesh: FilterMesh = self
        // let fader_mesh: Vec<LayerVertex> = self
            .clone()
            .fader_mesh(
                image_next.image().extent(),
                image_prev.image().extent(),
                viewport,
            );
    // PushConstants:
        let push_constants: PushConstants = PushConstants {
            mix: [mix, (1.0 - mix)],
        };
    // Framebuffer:
        let framebuffer = Framebuffer::new(
            self.render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![self.image_storage.clone()],
                ..Default::default()
            },
        )
        .unwrap();
    // Vertex buffer:
        let vertex_buffer: Subbuffer<[LayerVertex]> = Buffer::from_iter(
            resources.clone().memory_allocator,
            BufferCreateInfo {
                usage: BufferUsage::VERTEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            fader_mesh.vertices,
            // fader_mesh,
        )
        .unwrap();
    // Index buffer:
        let index_buffer: Subbuffer<[u32]> = Buffer::from_iter(
            resources.memory_allocator.clone(),
            BufferCreateInfo {
                usage: BufferUsage::INDEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            fader_mesh.indices,
        )
        .unwrap();
    // Descriptor set:
        let desc_set = DescriptorSet::image_view_array_push(
            descriptor_set_allocator,
            image_next,
            image_prev,
            self.layout_mix.clone(),
            resources.clone(),
        )
        .descriptor_set;
        // Add the scene's rendering commands to the command buffer
        let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
            command_buffer_allocator,
            // resources.command_buffer_allocator,
            resources.queue.queue_family_index(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        unsafe {
            command_buffer_builder
                .begin_render_pass(
                    RenderPassBeginInfo {
                        clear_values: vec![Some([0.0; 4].into())],
                        // clear_values: vec![None],
                        ..RenderPassBeginInfo::framebuffer(framebuffer)
                    },
                    Default::default(),
                )
                .unwrap()
                .set_viewport(
                    0,
                    [fader_mesh.viewport]
                    // [viewport]
                    .into_iter()
                    .collect(),
                )
                .unwrap()
                .bind_pipeline_graphics(self.pipeline_mix.clone())
                .unwrap()
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.layout_mix.clone(),
                    0,
                    desc_set,
                )
                .unwrap()
                .bind_vertex_buffers(0, vertex_buffer)
                .unwrap()
                .bind_index_buffer(index_buffer.clone())
                .unwrap()
                .push_constants(self.layout_mix.clone(), 0, push_constants)
                .unwrap()
                .draw_indexed(index_buffer.len() as u32, 1, 0, 0, 0)
        }
            .unwrap()
            .end_render_pass(Default::default())
            .unwrap();

        let command_buffer = command_buffer_builder.build().unwrap();
        let future = vulkano::sync::now(resources.queue.device().clone())
            .then_execute(resources.queue, command_buffer)
            .unwrap()
            .then_signal_fence_and_flush()
            .unwrap();
        future.wait(None).unwrap();
        // println!("fader scene rendered");
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
