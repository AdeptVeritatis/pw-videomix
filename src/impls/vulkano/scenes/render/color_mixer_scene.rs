
use crate::impls::vulkano::{
    descriptor_sets::DescriptorSet,
    render_pass::create_single_renderpass,
    shader::{
        fs_color_mixer::{
            self,
            PushConstants,
        },
        vs_texture,
    },
    vertices::TextureVertex,
    create_image_storage,
};
use egui_winit_vulkano::RenderResources;
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    fmt,
    sync::Arc,
};
use vulkano::{
    buffer::{
        Buffer,
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        RenderPassBeginInfo,
    },
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    device::Device,
    image::{
        view::ImageView,
        Image,
    },
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    pipeline::{
        graphics::{
            color_blend::{
                ColorBlendAttachmentState,
                ColorBlendState,
            },
            input_assembly::{
                InputAssemblyState,
                PrimitiveTopology,
            },
            multisample::MultisampleState,
            rasterization::RasterizationState,
            vertex_input::{
                Vertex,
                VertexDefinition,
            },
            viewport::{
                Viewport,
                ViewportState,
            },
            GraphicsPipeline,
            GraphicsPipelineCreateInfo,
        },
        layout::{
            PipelineLayout,
            PipelineDescriptorSetLayoutCreateInfo,
        },
        DynamicState,
        PipelineBindPoint,
        PipelineShaderStageCreateInfo,
    },
    render_pass::{
        Framebuffer,
        FramebufferCreateInfo,
        RenderPass,
        Subpass,
    },
    sync::{
        self,
        GpuFuture,
    },
};

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ColorMixerMatrix {
    pub mix_rr: f32,
    pub mix_rg: f32,
    pub mix_rb: f32,

    pub mix_gr: f32,
    pub mix_gg: f32,
    pub mix_gb: f32,

    pub mix_br: f32,
    pub mix_bg: f32,
    pub mix_bb: f32,
}

impl Default for ColorMixerMatrix {
    fn default() -> Self {
        Self {
            mix_rr: 1.0,
            mix_rg: 0.0,
            mix_rb: 0.0,

            mix_gr: 0.0,
            mix_gg: 1.0,
            mix_gb: 0.0,

            mix_br: 0.0,
            mix_bg: 0.0,
            mix_bb: 1.0,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct ColorMixerScene {
    // VertexBuffer, ... & Pipeline should be in it.
    // Layout is already part of pipeline but is inline private. Don't know, how to use it.
    // New RenderPass.
    // DescriptorSets, Sampler, SwapchainImage or Textures are not part of it.

    pub image_storage: Arc<ImageView>,

    pub layout: Arc<PipelineLayout>,
    pub pipeline: Arc<GraphicsPipeline>,
    pub render_pass: Arc<RenderPass>,
    pub vertex_buffer: Subbuffer<[TextureVertex]>,
}

impl fmt::Debug for ColorMixerScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("ColorMixerScene")
            .field("image_storage", &self.image_storage.image().extent())
            .finish()
    }
}

impl ColorMixerScene {
    pub fn new(
        resources: RenderResources,
    ) -> Self {
        let device: Arc<Device> = resources.queue.device().to_owned();
    // Render pass:
        let render_pass: Arc<RenderPass> = create_single_renderpass(
            device.clone(),
        );
        let subpass: Subpass = Subpass::from(
            render_pass.clone(),
            0,
        )
        .unwrap();
    // Image storage:
        let image: Arc<Image> = create_image_storage(
            resources.clone(),
        );
    // Pipeline:
        let (pipeline, layout): (Arc<GraphicsPipeline>, Arc<PipelineLayout>) = {
            // Shader modules:
            let vs = vs_texture::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let fs = fs_color_mixer::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let vertex_input_state = TextureVertex::per_vertex()
                .definition(&vs)
                .unwrap();
            // Layout:
            let stages = [
                PipelineShaderStageCreateInfo::new(vs),
                PipelineShaderStageCreateInfo::new(fs),
            ];
            let layout = PipelineLayout::new(
                device.clone(),
                PipelineDescriptorSetLayoutCreateInfo::from_stages(&stages)
                    .into_pipeline_layout_create_info(device.clone())
                    .unwrap(),
            )
            .unwrap();
            (GraphicsPipeline::new(
                device.clone(),
                None,
                GraphicsPipelineCreateInfo {
                    stages: stages.into_iter().collect(),
                    vertex_input_state: Some(vertex_input_state),
                    input_assembly_state: Some(
                        InputAssemblyState {
                            topology: PrimitiveTopology::TriangleStrip,
                            ..Default::default()
                        }
                    ),
                    viewport_state: Some(ViewportState::default()),
                    rasterization_state: Some(RasterizationState::default()),
                    multisample_state: Some(MultisampleState::default()),
                    color_blend_state: Some(ColorBlendState::with_attachment_states(
                        subpass.num_color_attachments(),
                        ColorBlendAttachmentState::default(),
                    )),
                    dynamic_state: [DynamicState::Viewport].into_iter().collect(),
                    subpass: Some(subpass.clone().into()),
                    ..GraphicsPipelineCreateInfo::layout(layout.clone())
                },
            )
            .unwrap(), layout)
        };
    // Vertex buffer:
        let vertices = [
            // 0 - TL
            TextureVertex {
                position: [-1.0, -1.0],
                tex_coords: [0.0, 0.0],
            },
            // 1 - TR
            TextureVertex {
                position: [1.0, -1.0],
                tex_coords: [1.0, 0.0],
            },
            // 2 - BL
            TextureVertex {
                position: [-1.0, 1.0],
                tex_coords: [0.0, 1.0],
            },
            // 3 - BR
            TextureVertex {
                position: [1.0, 1.0],
                tex_coords: [1.0, 1.0],
            },
        ];
        let vertex_buffer = Buffer::from_iter(
            resources.memory_allocator,
            BufferCreateInfo {
                usage: BufferUsage::VERTEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            vertices,
        )
        .unwrap();
    // ColorMixerScene:
        Self {
            image_storage: ImageView::new_default(image).unwrap(),
            layout,
            pipeline,
            render_pass,
            vertex_buffer,
        }
    }

    /// Draw the result of the "primary" pipeline into a framebuffer.
    pub fn render(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_input: Arc<ImageView>,
        image_output: Arc<ImageView>,
        mixer_matrix: ColorMixerMatrix,
        resources: RenderResources,
        viewport: Viewport,
    ) {
    // Push constants:
        let push_constants: PushConstants = PushConstants {
            mix_rr: mixer_matrix.mix_rr,
            mix_rg: mixer_matrix.mix_rg,
            mix_rb: mixer_matrix.mix_rb,
            mix_gr: mixer_matrix.mix_gr,
            mix_gg: mixer_matrix.mix_gg,
            mix_gb: mixer_matrix.mix_gb,
            mix_br: mixer_matrix.mix_br,
            mix_bg: mixer_matrix.mix_bg,
            mix_bb: mixer_matrix.mix_bb,
        };

    // Framebuffer:
        let framebuffer = Framebuffer::new(
            self.render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![image_output.clone()],
                ..Default::default()
            },
        )
        .unwrap();
    // Descriptor set:
        let desc_set = DescriptorSet::image_view_sampler(
            descriptor_set_allocator,
            image_input,
            self.layout.clone(),
            resources.clone(),
        )
        .descriptor_set;
        // Add the scene's rendering commands to the command buffer
        let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
            command_buffer_allocator,
            // resources.command_buffer_allocator,
            resources.queue.queue_family_index(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        unsafe {
            command_buffer_builder
                .begin_render_pass(
                    RenderPassBeginInfo {
                        clear_values: vec![Some([0.0; 4].into())],
                        ..RenderPassBeginInfo::framebuffer(framebuffer)
                    },
                    Default::default(),
                )
                .unwrap()
                .set_viewport(
                    0,
                    [viewport]
                    .into_iter()
                    .collect(),
                )
                .unwrap()
                .bind_pipeline_graphics(self.pipeline.clone())
                .unwrap()
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.layout.clone(),
                    0,
                    desc_set,
                )
                .unwrap()
                .bind_vertex_buffers(0, self.vertex_buffer.clone())
                .unwrap()
                .push_constants(self.layout.clone(), 0, push_constants)
                .unwrap()
                .draw(self.vertex_buffer.len() as u32, 1, 0, 0)
        }
            .unwrap()
            .end_render_pass(Default::default())
            .unwrap();

        let command_buffer = command_buffer_builder.build().unwrap();
        let future = sync::now(resources.queue.device().clone())
            .then_execute(resources.queue, command_buffer)
            .unwrap()
            .then_signal_fence_and_flush()
            .unwrap();
        future.wait(None).unwrap();
        // println!("mandala scene rendered");
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
