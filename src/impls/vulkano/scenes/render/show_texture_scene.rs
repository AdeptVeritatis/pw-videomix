
use crate::impls::vulkano::{
    descriptor_sets::DescriptorSet,
    shader::{
        fs_texture,
        vs_texture,
    },
    vertices::TextureVertex,
};
use egui_winit_vulkano::{
    CallbackContext,
    RenderResources,
};
use std::{
    fmt,
    sync::Arc,
};
use vulkano::{
    buffer::{
        Buffer,
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    image::view::ImageView,
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    pipeline::{
        graphics::{
            color_blend::{
                ColorBlendState,
                ColorBlendAttachmentState,
            },
            input_assembly::InputAssemblyState,
            multisample::MultisampleState,
            rasterization::RasterizationState,
            vertex_input::{
                Vertex,
                VertexDefinition,
            },
            viewport::{
                Viewport,
                ViewportState,
            },
            GraphicsPipeline,
            GraphicsPipelineCreateInfo,
        },
        layout::{
            PipelineLayout,
            PipelineDescriptorSetLayoutCreateInfo,
        },
        DynamicState,
        PipelineBindPoint,
        PipelineShaderStageCreateInfo,
    },
};

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct TextureScene {
    // VertexBuffer, IndexBuffer & Pipeline should be in it.
    // Layout is already part of pipeline but is inline private. Don't know, how to use it.
    // RenderPass is already in resources.
    // DescriptorSets, Sampler, SwapchainImage or Textures are not part of it.

    index_buffer: Subbuffer<[u32]>,
    layout: Arc<PipelineLayout>,
    pipeline: Arc<GraphicsPipeline>,
    vertex_buffer: Subbuffer<[TextureVertex]>,
}

impl fmt::Debug for TextureScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("TextureScene")
    }
}

impl TextureScene {
    pub fn new(
        resources: RenderResources,
    ) -> Self {
    // Pipeline:
        let (pipeline, layout): (Arc<GraphicsPipeline>, Arc<PipelineLayout>) = {
            let device = resources.queue.device();
            // Shader modules:
            let vs = vs_texture::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let fs = fs_texture::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let vertex_input_state = TextureVertex::per_vertex()
                .definition(&vs)
                .unwrap();
            // Layout:
            let stages = [
                PipelineShaderStageCreateInfo::new(vs),
                PipelineShaderStageCreateInfo::new(fs),
            ];
            let layout = PipelineLayout::new(
                device.clone(),
                PipelineDescriptorSetLayoutCreateInfo::from_stages(&stages)
                    .into_pipeline_layout_create_info(device.clone())
                    .unwrap(),
            )
            .unwrap();
            (GraphicsPipeline::new(
                device.clone(),
                None,
                GraphicsPipelineCreateInfo {
                    stages: stages.into_iter().collect(),
                    vertex_input_state: Some(vertex_input_state),
                    input_assembly_state: Some(InputAssemblyState::default()),
                    viewport_state: Some(ViewportState::default()),
                    rasterization_state: Some(RasterizationState::default()),
                    multisample_state: Some(MultisampleState::default()),
                    color_blend_state: Some(ColorBlendState::with_attachment_states(
                        resources.subpass.num_color_attachments(),
                        ColorBlendAttachmentState::default(),
                    )),
                    dynamic_state: [DynamicState::Viewport].into_iter().collect(),
                    subpass: Some(resources.subpass.clone().into()),
                    ..GraphicsPipelineCreateInfo::layout(layout.clone())
                },
            )
            .unwrap(), layout)
        };
    // Vertex buffer:
        let vertices = [
            // 0 - TL
            TextureVertex {
                position: [-1.0, -1.0],
                tex_coords: [0.0, 0.0],
            },
            // 1 - TR
            TextureVertex {
                position: [1.0, -1.0],
                tex_coords: [1.0, 0.0],
            },
            // 2 - BL
            TextureVertex {
                position: [-1.0, 1.0],
                tex_coords: [0.0, 1.0],
            },
            // 3 - BR
            TextureVertex {
                position: [1.0, 1.0],
                tex_coords: [1.0, 1.0],
            },
        ];
        let vertex_buffer = Buffer::from_iter(
            resources.clone().memory_allocator,
            BufferCreateInfo {
                usage: BufferUsage::VERTEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            vertices,
        )
        .unwrap();
    // Index buffer:
        let indices = [0, 2, 3, 0, 3, 1];
        let index_buffer = Buffer::from_iter(
            resources.memory_allocator,
            BufferCreateInfo {
                usage: BufferUsage::INDEX_BUFFER,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            indices,
        )
        .unwrap();
    // TextureScene:
        Self {
            index_buffer,
            layout,
            pipeline,
            vertex_buffer,
        }
    }

    /// Draw the result of the secondary pipeline in a render callback.
    pub fn render(
        &self,
        context: &mut CallbackContext<'_>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        image_input: Arc<ImageView>,
        viewport: Viewport,
    ) {
    // Descriptor set:
        let desc_set = DescriptorSet::image_view_sampler(
            descriptor_set_allocator,
            image_input,
            self.layout.clone(),
            context.resources.clone(),
        )
        .descriptor_set;
        // Add the scene's rendering commands to the command buffer

        unsafe {
            context
                .builder
                //.begin_render_pass -> no, because secondary render pass???
                .set_viewport(
                    0,
                    [viewport]
                    .into_iter()
                    .collect(),
                )
                .unwrap()
                .bind_pipeline_graphics(self.pipeline.clone())
                .unwrap()
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.layout.clone(),
                    0,
                    desc_set,
                )
                .unwrap()
                .bind_vertex_buffers(0, self.vertex_buffer.clone())
                .unwrap()
                .bind_index_buffer(self.index_buffer.clone())
                .unwrap()
                // .draw(self.vertex_buffer.len() as u32, 1, 0, 0)
                .draw_indexed(self.index_buffer.len() as u32, 1, 0, 0, 0)
        }
            .unwrap();
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
