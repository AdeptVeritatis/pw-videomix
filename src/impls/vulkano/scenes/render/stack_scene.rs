
use crate::{
    constants::IMAGE_FORMAT,
    impls::{
        files::TextureMessage,
        vulkano::{
            descriptor_sets::DescriptorSet,
            render_pass::create_single_renderpass,
            shader::{
                fs_texture,
                vs_texture,
            },
            vertices::TextureVertex,
            create_image_storage,
        },
    },
};
use egui_winit_vulkano::{
    egui::Vec2,
    RenderResources,
};
use std::{
    fmt,
    sync::Arc,
    time::{
        Duration,
        Instant,
    },
};
use vulkano::{
    buffer::{
        Buffer,
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        CopyBufferToImageInfo,
        PrimaryCommandBufferAbstract,
        RenderPassBeginInfo
    },
    descriptor_set::allocator::StandardDescriptorSetAllocator,
    device::Device,
    image::{
        view::ImageView,
        Image,
        ImageCreateInfo,
        ImageType,
        ImageUsage,
    },
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    pipeline::{
        graphics::{
            color_blend::{
                ColorBlendState,
                ColorBlendAttachmentState,
            },
            input_assembly::{
                InputAssemblyState,
                PrimitiveTopology,
            },
            multisample::MultisampleState,
            rasterization::RasterizationState,
            vertex_input::{
                Vertex,
                VertexDefinition,
            },
            viewport::{
                Viewport,
                ViewportState,
            },
            GraphicsPipeline,
            GraphicsPipelineCreateInfo,
        },
        layout::{
            PipelineLayout,
            PipelineDescriptorSetLayoutCreateInfo,
        },
        DynamicState,
        PipelineBindPoint,
        PipelineShaderStageCreateInfo,
    },
    render_pass::{
        Framebuffer,
        FramebufferCreateInfo,
        RenderPass,
        Subpass,
    },
    sync::GpuFuture,
};

// ----------------------------------------------------------------------------

// rename to StackLayerScene and move to src/scenes/copy/???
// same as or similar to PictureScene???
#[derive(Clone)]
pub struct StackLayer {
    pub id: String,
    pub image_layer: Arc<ImageView>,
    pub name: String,
    pub size: Vec2,
}

impl fmt::Debug for StackLayer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("StackLayer")
            .field("id", &self.id)
            .field("image_layer", &self.image_layer.image().extent())
            .field("name", &self.name)
            .field("size", &self.size)
            .finish()
    }
}

impl StackLayer {
    pub fn new(
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        resources: RenderResources,
        texture_message: TextureMessage,
    ) -> Self {
        let picture_buffer: Subbuffer<[u8]> = Buffer::from_iter(
            resources.clone().memory_allocator,
            BufferCreateInfo {
                usage: BufferUsage::TRANSFER_SRC,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                    | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            texture_message.image_data,
        )
        .unwrap();

        let extent: [u32; 3] = [
            texture_message.size.x as u32,
            texture_message.size.y as u32,
            1,
        ];
        let image = Image::new(
            resources.memory_allocator,
            ImageCreateInfo {
                image_type: ImageType::Dim2d,
                format: IMAGE_FORMAT,
                extent,
                usage: ImageUsage::TRANSFER_DST | ImageUsage::TRANSFER_SRC | ImageUsage::SAMPLED,
                ..Default::default()
            },
            AllocationCreateInfo::default(),
        )
        .unwrap();

        let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
            command_buffer_allocator,
            // resources.command_buffer_allocator,
            resources.queue.queue_family_index(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        command_buffer_builder
            // Clear the image buffer.
            // .clear_color_image(ClearColorImageInfo::image(image.clone()))
            // .unwrap()
            .copy_buffer_to_image(CopyBufferToImageInfo::buffer_image(
                picture_buffer,
                image.clone(),
            ))
            .unwrap();

        let command_buffer = command_buffer_builder
            .build()
            .unwrap();
        let finished = command_buffer.execute(resources.queue).unwrap();
        let _future = finished.then_signal_fence_and_flush().unwrap();

        Self {
            id: texture_message.file_path.full,
            image_layer: ImageView::new_default(image).unwrap(),
            name: texture_message.file_path.file_name,
            size: texture_message.size,
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct StackScene {
    // Selects the actual frame.
    pub counter: usize,
    // List of layers with images and sizes.
    pub layers: Vec<StackLayer>,
    // Size of actual image, independent of output size.
    // pub size: Vec2,
    // Time of last frame change.
    pub updated: Instant,

    // Image to copy the actual picture to.
    // Connections get data from here.
    pub image_storage: Arc<ImageView>,
    pub layout: Arc<PipelineLayout>,
    pub pipeline: Arc<GraphicsPipeline>,
    pub render_pass: Arc<RenderPass>,
}

impl fmt::Debug for StackScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("StackScene")
            .field("counter", &self.counter)
            .field("layers", &self.layers.len())
            .field("updated", &self.updated)
            .field("image_storage", &self.image_storage.image().extent())
            .finish()
    }
}

impl StackScene {
    pub fn new(
        resources: RenderResources,
    ) -> Self {
        let device: Arc<Device> = resources.queue.device().to_owned();
    // Render pass:
        let render_pass: Arc<RenderPass> = create_single_renderpass(
            device.clone(),
        );
        let subpass: Subpass = Subpass::from(
            render_pass.clone(),
            0,
        )
        .unwrap();
    // Image storage:
        let image: Arc<Image> = create_image_storage(
            resources.clone(),
        );
    // Pipeline:
        let (pipeline, layout): (Arc<GraphicsPipeline>, Arc<PipelineLayout>) = {
            // Shader modules:
            let vs = vs_texture::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let fs = fs_texture::load(device.clone())
                .unwrap()
                .entry_point("main")
                .unwrap();
            let vertex_input_state = TextureVertex::per_vertex()
                .definition(&vs)
                .unwrap();
            // Layout:
            let stages = [
                PipelineShaderStageCreateInfo::new(vs),
                PipelineShaderStageCreateInfo::new(fs),
            ];
            let layout = PipelineLayout::new(
                device.clone(),
                PipelineDescriptorSetLayoutCreateInfo::from_stages(&stages)
                    .into_pipeline_layout_create_info(device.clone())
                    .unwrap(),
            )
            .unwrap();
            let pipeline = GraphicsPipeline::new(
                device.clone(),
                None,
                GraphicsPipelineCreateInfo {
                    stages: stages.into_iter().collect(),
                    vertex_input_state: Some(vertex_input_state),
                    input_assembly_state: Some(InputAssemblyState {
                        topology: PrimitiveTopology::TriangleStrip,
                        ..Default::default()
                    }),
                    viewport_state: Some(ViewportState::default()),
                    rasterization_state: Some(RasterizationState::default()),
                    multisample_state: Some(MultisampleState::default()),
                    color_blend_state: Some(ColorBlendState::with_attachment_states(
                        subpass.num_color_attachments(),
                        ColorBlendAttachmentState::default(),
                    )),
                    dynamic_state: [DynamicState::Viewport].into_iter().collect(),
                    subpass: Some(subpass.clone().into()),
                    ..GraphicsPipelineCreateInfo::layout(layout.clone())
                },
            )
            .unwrap();
            ( pipeline, layout)
        };
    // StackScene:
        Self {
            counter: 0,
            layers: Vec::new(),
            updated: Instant::now(),

            image_storage: ImageView::new_default(image).unwrap(),
            layout,
            pipeline,
            render_pass,
        }
    }

    /// Draw the result in the framebuffer.
    pub fn render(
        mut self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
        fps: f32,
        resources: RenderResources,
    ) -> Self {
        let now: Instant = Instant::now();
        // whole setup needs too long!!!
        // looses around 0.01s per frame???
        if now >= self.updated + Duration::from_secs_f32(fps.recip()) {
            // Copy layer image to image storage.
            match
                self.layers.get(self.counter)
            {
                None => (),
                Some(stack_layer) => {
                    // Like connection scene!!!???
                    // stack_layer.id
                    // stack_layer.name
                    // println!(
                    //     "{:?} {:?}",
                    //     stack_layer.image_layer.image().extent(),
                    //     self.image_storage.image().extent(),
                    // );
                // Storage image:
                    // let image_storage: Arc<Image> = self
                    //     .image_storage
                    //     .image()
                    //     .to_owned();
                // Framebuffer:
                    let framebuffer = Framebuffer::new(
                        self.render_pass.clone(),
                        FramebufferCreateInfo {
                            attachments: vec![self.image_storage.clone()],
                            ..Default::default()
                        },
                    )
                    .unwrap();

                // Vertex buffer:
                    let framebuffer_extent: [u32; 3] = self.image_storage.image().extent();
                    let framebuffer_aspect_ratio: f32 = framebuffer_extent[0] as f32 / framebuffer_extent[1] as f32;
                    let layer_aspect_ratio: f32 = stack_layer.size.x / stack_layer.size.y;
                    let vertices = match
                        framebuffer_aspect_ratio
                        > layer_aspect_ratio
                    {
                        // "relative portrait"
                        true => [
                            // 0 - TL
                            TextureVertex {
                                position: [-1.0 * layer_aspect_ratio * framebuffer_aspect_ratio.recip(), -1.0],
                                tex_coords: [0.0, 0.0],
                            },
                            // 1 - TR
                            TextureVertex {
                                position: [layer_aspect_ratio * framebuffer_aspect_ratio.recip(), -1.0],
                                tex_coords: [1.0, 0.0],
                            },
                            // 2 - BL
                            TextureVertex {
                                position: [-1.0 * layer_aspect_ratio * framebuffer_aspect_ratio.recip(), 1.0],
                                tex_coords: [0.0, 1.0],
                            },
                            // 3 - BR
                            TextureVertex {
                                position: [layer_aspect_ratio * framebuffer_aspect_ratio.recip(), 1.0],
                                tex_coords: [1.0, 1.0],
                            },
                        ],
                        // "relative landscape"
                        false => [
                            // 0 - TL
                            TextureVertex {
                                position: [-1.0, - 1.0 * framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                                tex_coords: [0.0, 0.0],
                            },
                            // 1 - TR
                            TextureVertex {
                                position: [1.0, - 1.0 * framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                                tex_coords: [1.0, 0.0],
                            },
                            // 2 - BL
                            TextureVertex {
                                position: [-1.0, framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                                tex_coords: [0.0, 1.0],
                            },
                            // 3 - BR
                            TextureVertex {
                                position: [1.0, framebuffer_aspect_ratio * layer_aspect_ratio.recip()],
                                tex_coords: [1.0, 1.0],
                            },
                        ],
                    };
                    let vertex_buffer: Subbuffer<[TextureVertex]> = Buffer::from_iter(
                        resources.clone().memory_allocator,
                        BufferCreateInfo {
                            usage: BufferUsage::VERTEX_BUFFER,
                            ..Default::default()
                        },
                        AllocationCreateInfo {
                            memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                                | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                            ..Default::default()
                        },
                        vertices,
                    )
                    .unwrap();
                // Viewport:
                    // create self.stack_mesh() in stack_node!!!!!
                    let extent: [f32; 2] = [
                        framebuffer_extent[0] as f32,
                        framebuffer_extent[1] as f32,
                    ];
                    let viewport: Viewport = Viewport {
                        offset: [
                            0.0,
                            0.0,
                        ],
                        extent,
                        depth_range: 0.0..=1.0,
                    };
                // Descriptor set:
                    let desc_set = DescriptorSet::image_view_sampler(
                        descriptor_set_allocator,
                        stack_layer.image_layer.clone(),
                        self.layout.clone(),
                        resources.clone(),
                    )
                    .descriptor_set;
                // Command buffer:
                    let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
                        command_buffer_allocator,
                        // resources.command_buffer_allocator,
                        resources.queue.queue_family_index(),
                        CommandBufferUsage::OneTimeSubmit,
                    )
                    .unwrap();

                    unsafe {
                        command_buffer_builder
                            .begin_render_pass(
                                RenderPassBeginInfo {
                                    clear_values: vec![Some([0.0; 4].into())],
                                    // clear_values: vec![None],
                                    ..RenderPassBeginInfo::framebuffer(framebuffer)
                                },
                                Default::default(),
                            )
                            .unwrap()
                            .set_viewport(
                                0,
                                [viewport]
                                .into_iter()
                                .collect(),
                            )
                            .unwrap()
                            .bind_pipeline_graphics(self.pipeline.clone())
                            .unwrap()
                            .bind_descriptor_sets(
                                PipelineBindPoint::Graphics,
                                self.layout.clone(),
                                0,
                                desc_set,
                            )
                            .unwrap()
                            .bind_vertex_buffers(0, vertex_buffer.clone())
                            .unwrap()
                            .draw(vertex_buffer.len() as u32, 1, 0, 0)
                    }
                        .unwrap()
                        .end_render_pass(Default::default())
                        .unwrap();

                    // dbg!(image_storage.image().extent());
                    let command_buffer = command_buffer_builder
                        .build()
                        .unwrap();
                    // let _future = command_buffer
                    //     .execute(resources.queue)
                    //     .unwrap()
                    //     .then_signal_fence_and_flush()
                    //     .unwrap();
                    let future = vulkano::sync::now(resources.queue.device().clone())
                        .then_execute(resources.queue, command_buffer)
                        .unwrap()
                        .then_signal_fence_and_flush()
                        .unwrap();
                    future.wait(None).unwrap();

                    // stack_layer.image_layer


                    // stack_layer.size;

                }
            };
            // Update frame values.
            self.updated = now;
            if !self.layers.is_empty() {
                self.counter += 1;
                self.counter %= self.layers.len();
            }
        }
        self
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
