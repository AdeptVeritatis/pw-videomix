
use crate::constants::IMAGE_FORMAT;
use egui_winit_vulkano::{
    egui::Vec2,
    RenderResources,
};
use std::{
    fmt,
    sync::Arc,
};
use vulkano::{
    buffer::{
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        CopyBufferToImageInfo,
        PrimaryCommandBufferAbstract,
    },
    image::{
        view::ImageView,
        Image,
        ImageCreateInfo,
        ImageType,
        ImageUsage,
    },
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    sync::GpuFuture,
};

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct VideoScene {
    pub image_storage: Arc<ImageView>,
    pub size: Vec2,
}

impl fmt::Debug for VideoScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("VideoScene")
            .field("image_storage", &self.image_storage.image().extent())
            .field("size", &self.size)
            .finish()
    }
}

impl VideoScene {
    pub fn new(
        resources: RenderResources,
        size: Vec2,
    ) -> Self {
        let extent: [u32; 3] = [size.x as u32, size.y as u32, 1];
        let image = Image::new(
            resources.memory_allocator,
            ImageCreateInfo {
                image_type: ImageType::Dim2d,
                format: IMAGE_FORMAT,
                extent,
                usage: ImageUsage::TRANSFER_DST | ImageUsage::TRANSFER_SRC | ImageUsage::SAMPLED,
                ..Default::default()
            },
            AllocationCreateInfo::default(),
        )
        .unwrap();

        Self {
            image_storage: ImageView::new_default(image).unwrap(),
            size,
        }
    }

    pub fn refresh(
        &self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        image_data: Vec<u8>,
        resources: RenderResources,
    ) -> Self {
        let image: Arc<Image> = self
            .image_storage
            .image()
            .to_owned();

        let vulkano_buffer: Subbuffer<[u8]> = vulkano::buffer::Buffer::from_iter(
            resources.memory_allocator.clone(),
            BufferCreateInfo {
                usage: BufferUsage::TRANSFER_DST | BufferUsage::TRANSFER_SRC,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter: MemoryTypeFilter::PREFER_HOST
                    | MemoryTypeFilter::HOST_RANDOM_ACCESS,
                ..Default::default()
            },
            image_data,
        )
        .unwrap();
        let mut command_buffer_builder =
            AutoCommandBufferBuilder::primary(
                command_buffer_allocator,
                // resources.command_buffer_allocator,
                resources.queue.queue_family_index(),
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap();
        command_buffer_builder
            .copy_buffer_to_image(
                CopyBufferToImageInfo::buffer_image(
                    vulkano_buffer,
                    image.clone(),
                )
            )
            .unwrap();
        let command_buffer = command_buffer_builder
            .build()
            .unwrap();
        let finished = command_buffer.execute(resources.queue.clone()).unwrap();
        finished
            .then_signal_fence_and_flush()
            .unwrap()
            .wait(None)
            .unwrap();
        Self {
            image_storage: ImageView::new_default(image).unwrap(),
            ..self.to_owned()
        }
    }

}

