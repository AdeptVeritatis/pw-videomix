
use crate::constants::IMAGE_FORMAT;
use egui_winit_vulkano::RenderResources;
use std::{
    fmt,
    sync::Arc,
};
use vulkano::{
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        CopyImageInfo,
        PrimaryCommandBufferAbstract,
    },
    image::{
        view::ImageView,
        Image,
        ImageCreateInfo,
        ImageType,
        ImageUsage,
    },
    memory::allocator::AllocationCreateInfo,
    sync::GpuFuture,
};

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct ConnectionScene {
    // Copy image to.
    pub image_sink: Option<Arc<ImageView>>,
    // Copy image from.
    pub image_source: Arc<ImageView>,
}

impl fmt::Debug for ConnectionScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("ConnectionScene")
            .field("image_sink", &self.image_sink.is_some())
            .field("image_source", &self.image_source.image().extent())
            .finish()
    }
}

impl ConnectionScene {
    pub fn update(
        self,
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        resources: RenderResources,
    ) -> Self {
        // dbg!(self.image_source.image().extent());
        // dbg!(self.image_source.image().memory_requirements());
        // dbg!(image_source.image().format());
        // dbg!(image_source.image().format_features());
        // dbg!(image_source.image().usage());

        let image_sink: Arc<ImageView> = match
            self
                .clone()
                .image_sink
        {
            Some(image_sink) => {
                image_sink.to_owned()
            },
            None => {
                let extent = self.image_source.image().extent();

                let image: Arc<Image> = Image::new(
                    resources.memory_allocator,
                    ImageCreateInfo {
                        image_type: ImageType::Dim2d,
                        format: IMAGE_FORMAT,
                        extent,
                        usage: ImageUsage::TRANSFER_DST | ImageUsage::TRANSFER_SRC | ImageUsage::SAMPLED,
                        ..Default::default()
                    },
                    AllocationCreateInfo::default(),
                )
                .unwrap();
                ImageView::new_default(image).unwrap()
            },
        };
        let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
            command_buffer_allocator,
            // resources.command_buffer_allocator,
            resources.queue.queue_family_index(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        command_buffer_builder
            .copy_image(
                CopyImageInfo::images(
                    self.image_source
                        .image()
                        .to_owned(),
                    image_sink
                        .image()
                        .to_owned(),
                )
            )
            .unwrap();
        // dbg!(image_sink.image().extent());
        let command_buffer = command_buffer_builder
            .build()
            .unwrap();
        let finished = command_buffer.execute(resources.queue).unwrap();
        let _future = finished.then_signal_fence_and_flush().unwrap();
        // println!("test copy connection before return");

        Self {
            image_sink: Some(image_sink),
            ..self
        }
    }
}
