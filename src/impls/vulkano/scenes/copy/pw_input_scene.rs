
use crate::{
    app::events::GuiEvent,
    constants::{
        IMAGE_FORMAT,
        PIPEWIRE_INPUT_SIZE_HEIGHT,
        PIPEWIRE_INPUT_SIZE_WIDTH,
    },
    impls::pipewire::{
        streams_in::{
            PipewireInputStream,
            PipewireInputUserData,
        },
        PipewireStream,
    },
};
use egui_winit_vulkano::{
    egui::Vec2,
    RenderResources,
};
use pipewire::core::Core;
use std::{
    cell::RefCell,
    collections::HashMap,
    fmt,
    rc::Rc,
    sync::Arc,
};
use vulkano::{
    image::{
        view::ImageView,
        Image,
        ImageCreateInfo,
        ImageType,
        ImageUsage,
    },
    memory::allocator::AllocationCreateInfo,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct PipewireInputScene {
    pub image_storage: Arc<ImageView>,
    pub size: Vec2,
}

impl fmt::Debug for PipewireInputScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("PipewireInputScene")
            .field("image_storage", &self.image_storage.image().extent())
            .field("size", &self.size)
            .finish()
    }
}

impl PipewireInputScene {
    pub fn new(
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        pw_core: Core,
        resources: RenderResources,
        source_id: String, // source/sink???
    ) -> Self {
        let size: Vec2 = Vec2::new(
            PIPEWIRE_INPUT_SIZE_WIDTH,
            PIPEWIRE_INPUT_SIZE_HEIGHT,
        );
        let extent: [u32; 3] = [
            size.x as u32,
            size.y as u32,
            1,
        ];
        let image: Arc<Image> = Image::new(
            resources.memory_allocator.clone(),
            ImageCreateInfo {
                image_type: ImageType::Dim2d,
                format: IMAGE_FORMAT,
                extent,
                usage: ImageUsage::TRANSFER_DST | ImageUsage::TRANSFER_SRC | ImageUsage::SAMPLED,
                ..Default::default()
            },
            AllocationCreateInfo::default(),
        )
        .unwrap();
        // Prepare user data for pipewire stream.
        let user_data: PipewireInputUserData = PipewireInputUserData {
            event_loop_proxy,
            frame_num: 0,
            memory_allocator: resources.memory_allocator.clone(),
            source_id: source_id.clone(),
        };
        // Create pipewire Input stream.
        // Stream creates a buffer for every frame.
        // Event receives this buffer and copies buffer data to image_storage of scene.
        let pipewire_stream: PipewireStream = PipewireStream::InputStream(
            PipewireInputStream::new(
                pw_core,
                user_data.clone(),
            )
        );
        // Store stream.
        map_node_to_stream
            .borrow_mut()
            .insert(
                source_id,
                pipewire_stream,
            );
        // Store scene.
        Self {
            image_storage: ImageView::new_default(image).unwrap(),
            size,
        }
    }
}
