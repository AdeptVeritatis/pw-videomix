
use crate::constants::IMAGE_FORMAT;
use egui_winit_vulkano::{
    egui::Vec2,
    RenderResources,
};
use std::{
    fmt,
    sync::Arc,
};
use vulkano::{
    buffer::{
        Buffer,
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        CopyBufferToImageInfo,
        PrimaryCommandBufferAbstract,
    },
    image::{
        view::ImageView,
        Image,
        ImageCreateInfo,
        ImageType,
        ImageUsage,
    },
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
    sync::GpuFuture,
};

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct PictureScene {
    pub image_storage: Arc<ImageView>,
    pub size: Vec2,
}

impl fmt::Debug for PictureScene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("PictureScene")
            .field("image_storage", &self.image_storage.image().extent())
            .field("size", &self.size)
            .finish()
    }
}

impl PictureScene {
    pub fn new(
        command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
        image_data: Vec<u8>,
        resources: RenderResources,
        size: Vec2,
    ) -> Self {
        // on cleanup, check stack_scene.rs too!!!
        let picture_buffer: Subbuffer<[u8]> = Buffer::from_iter(
            resources
                .memory_allocator
                .clone(),
            BufferCreateInfo {
                usage: BufferUsage::TRANSFER_SRC,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter:
                    MemoryTypeFilter::PREFER_DEVICE
                    |
                    MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            image_data,
        )
        .unwrap();

        let extent: [u32; 3] = [
            size.x as u32,
            size.y as u32,
            1,
        ];
        let image = Image::new(
            resources
                .memory_allocator,
            ImageCreateInfo {
                image_type: ImageType::Dim2d,
                format: IMAGE_FORMAT,
                extent,
                usage:
                    ImageUsage::TRANSFER_DST
                    |
                    ImageUsage::TRANSFER_SRC
                    |
                    ImageUsage::SAMPLED,
                ..Default::default()
            },
            AllocationCreateInfo::default(),
        )
        .unwrap();

        let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
            command_buffer_allocator,
            // resources.command_buffer_allocator,
            resources
                .queue
                .queue_family_index(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        command_buffer_builder
            // Clear the image buffer.
            // .clear_color_image(ClearColorImageInfo::image(image.clone()))
            // .unwrap()
            .copy_buffer_to_image(
                CopyBufferToImageInfo::buffer_image(
                    picture_buffer,
                    image
                        .clone(),
                )
            )
            .unwrap();

        let command_buffer = command_buffer_builder
            .build()
            .unwrap();
        let finished = command_buffer
            .execute(
                resources
                    .queue
            )
            .unwrap();
        let _future = finished
            .then_signal_fence_and_flush()
            .unwrap();

        // println!("test copy data before ImageView");
        let image_storage: Arc<ImageView> = match
            ImageView::new_default(
                image
            )
        {
            Err(error) => panic!("!! error: {error}"),
            Ok(image_view) => image_view,
        };

        // println!("test copy data after ImageView, before return");
        Self {
            image_storage,
            // image_storage: ImageView::new_default(image).unwrap(),
            size,
        }
    }
}
