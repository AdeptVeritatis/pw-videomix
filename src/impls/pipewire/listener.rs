
use crate::{
    // gui::events::GuiEvent,
    impls::pipewire::PipewireCamera,
};
use pipewire::{
    // prelude::ReadableDict,
    registry::{
        self,
        Registry,
    },
    types::ObjectType,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};
// use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------
//  Functions:
// ----------------------------------------------------------------------------

pub fn listener_add(
    // event_loop_proxy: EventLoopProxy<GuiEvent>,
    registry: Rc<Registry>,
    map_id_to_camera: Rc<RefCell<HashMap<u32, PipewireCamera>>>,
) -> registry::Listener {
    let registry_clone = registry.clone();
    registry
        .add_listener_local()
        // Called when a global object is added:
        .global(move |global| {
            match global.props {
                None => (),
                Some(ref props) => {
                    match
                        global
                            .type_
                     {
                        ObjectType::Node => {
                            match
                                props
                                    .get("media.class")
                            {
                                None => (),
                                Some("Video/Source") => {
                                    match
                                        props
                                            .get("media.role")
                                    {
                                        None => (),
                                        Some("Camera") => {
                                            let node_proxy: pipewire::node::Node = registry_clone
                                                .bind(global)
                                                .expect("Failed to bind node proxy");
                                            let node_listener = node_proxy
                                                .add_listener_local()
                                                // .param(move |param1, param2, param3, param4, param5| {
                                                .info(move |info| {
                                                    println!(
                                                        // "ii node params: {:?} | {:?} | {:?} | {:?} | {:?}",
                                                        // param1,
                                                        // param2,
                                                        // param3,
                                                        // param4,
                                                        // param5.unwrap().type_(),
                                                        "ii node info: {:#?}",
                                                        info,
                                                        // info.props(),
                                                        // info.state(),
                                                    );
                                                })
                                                .register();
                                            let node: u32 = global.id;
                                            let description: String = props
                                                .get("node.description")
                                                .unwrap()
                                                .to_string();
                                            let device: u32 = props
                                                .get("device.id")
                                                .unwrap()
                                                .parse::<u32>()
                                                .unwrap();
                                            let camera: PipewireCamera = PipewireCamera {
                                                description,
                                                device,
                                                name: props.get("node.name").unwrap().to_string(),
                                                nick: props.get("node.nick").unwrap().to_string(),
                                                node,
                                                node_listener,
                                                serial: props.get("object.serial").unwrap().parse::<u32>().unwrap(),
                                            };
                                            // println!("-> new camera source: {camera:#?}");
                                            map_id_to_camera
                                                .clone()
                                                .borrow_mut()
                                                .insert(
                                                    node,
                                                    camera,
                                                );

                                            // Get size and connection status from pw_node listener.
                                            // Update node scene. ???

                                        },
                                        Some(_) => (),
                                    };
                                },
                                Some(_) => (),
                            };
                        },
                        _ => (),
                    };
                },
            };
        })
        .register()
}

// ----------------------------------------------------------------------------

pub fn listener_remove(
    // event_loop_proxy: EventLoopProxy<GuiEvent>,
    registry: Rc<Registry>,
    map_id_to_camera: Rc<RefCell<HashMap<u32, PipewireCamera>>>,
) -> registry::Listener {
    registry
        .add_listener_local()
        // Called when a global object is removed:
        .global_remove(move |id| {
            // Id is same as props.get("device.id") from listener_add.
            match
                map_id_to_camera
                    .borrow_mut()
                    .remove(&id)
             {
                None => (),
                Some(camera) => {
                    println!("<- camera removed: \"{}\" id({id})", camera.description);

                    // Update node scene???

                },
            };
        })
        .register()
}
