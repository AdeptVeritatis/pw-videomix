
use crate::app::events::GuiEvent;
use image::EncodableLayout;
use pipewire::{
    core::Core,
    keys::{
        MEDIA_CATEGORY,
        MEDIA_CLASS,
        MEDIA_ROLE,
        // MEDIA_SOFTWARE,
        MEDIA_TYPE,
        // NODE_DESCRIPTION,
        // PORT_ALIAS,
    },
    properties::properties,
    spa::{
        param::{
            format::{
                FormatProperties,
                MediaSubtype,
                MediaType,
            },
            video::VideoFormat,
            ParamType,
        },
        pod::{
            serialize::PodSerializer,
            Pod,
            Value,
            object,
            property,
        },
        utils::{
            Direction,
            Fraction,
            Rectangle,
            SpaTypes,
        },
    },
    stream::{
        Stream,
        StreamFlags,
        StreamListener,
        StreamState,
    },
};
use std::{
    // fmt,
    sync::Arc,
};
use vulkano::{
    // buffer::{
    //     Buffer,
    //     BufferCreateInfo,
    //     BufferUsage,
    //     Subbuffer,
    // },
    memory::allocator::{
        // AllocationCreateInfo,
        MemoryAllocator,
        // MemoryTypeFilter,
    },
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct PipewireOutputUserData {
    pub event_loop_proxy: EventLoopProxy<GuiEvent>,
    pub frame_num: u32,
    pub memory_allocator: Arc<dyn MemoryAllocator>,
    // sink_id!!!
    pub source_id: String, // source as in SourceNode::PipewireInput(_)
}

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct PipewireOutputStream {
    pub stream: Stream,
    pub stream_listener: StreamListener<PipewireOutputUserData>,
    // pub target: Option<u32>,
}

// impl fmt::Debug for PipewireOutputStream {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(
//             f,
//             "stream: {:?}",
//             self.stream.name(),
//         )
//     }
// }

impl PipewireOutputStream {
    pub fn new(
        core: Core,
        user_data: PipewireOutputUserData,
    ) -> Self {
    // Create stream:
        println!("-> create stream");
        let stream: Stream = match
            Stream::new(
                &core,
                user_data.source_id.as_str(),
                properties! {
                    *MEDIA_TYPE => "Video",
                    *MEDIA_CATEGORY => "Playback", // "Capture",
                    *MEDIA_ROLE => "Movie", // "Screen" // "Camera"
                    *MEDIA_CLASS => "Video/Source", // "Video/Sink" // ???
                    // *MEDIA_SOFTWARE => "pw-videomix",
                    // *NODE_DESCRIPTION => user_data.source_id.as_str(),
                    // *PORT_ALIAS => "input",
                },
            )
        {
            Err(error) => panic!("!! {error}"),
            Ok(stream) => stream,
        };

        let stream_listener = match
            stream
                .add_local_listener_with_user_data(user_data)
                // .param_changed(
                //     |_stream_ref, _, _user_data, _spa_pod| {}
                // )
                .state_changed(
                    |_, _, old, new| {
                        println!("<> pw output: {old:?} -> {new:?}");
                        match
                            new
                        {
                            StreamState::Error(error) => println!("!! error: {error}"),
                            StreamState::Unconnected => (),
                            StreamState::Connecting => (),
                            StreamState::Paused => (),
                            StreamState::Streaming => {
                                // get size of stream???
                            },
                        };
                    }
                )
                // .process()
                // .drained()
                .register()
        {
            Err(error) => panic!("!! {error}"),
            Ok(listener) => {
                // println!("-> stream object created");
                listener
            },
        };
    // Params:
        let obj = object!(
            SpaTypes::ObjectParamFormat,
            ParamType::EnumFormat,
            property!(
                FormatProperties::MediaType,
                Id,
                MediaType::Video
            ),
            property!(
                FormatProperties::MediaSubtype,
                Id,
                MediaSubtype::Raw
            ),
            property!(
                FormatProperties::VideoFormat,
                Choice,
                Enum,
                Id,
                VideoFormat::RGB,
                VideoFormat::RGB,
                VideoFormat::RGBA,
                VideoFormat::RGBx,
                VideoFormat::BGRx,
                VideoFormat::YUY2,
                VideoFormat::I420,
            ),
            property!(
                FormatProperties::VideoSize,
                Choice,
                Range,
                Rectangle,
                Rectangle {
                    width: 480, // ???
                    height: 480, // ???
                },
                Rectangle {
                    width: 1,
                    height: 1,
                },
                Rectangle {
                    width: 4096,
                    height: 4096,
                }
            ),
            property!(
                FormatProperties::VideoFramerate,
                Choice,
                Range,
                Fraction,
                Fraction {
                    num: 25,
                    denom: 1,
                },
                Fraction {
                    num: 0,
                    denom: 1,
                },
                Fraction {
                    num: 1000,
                    denom: 1,
                }
            ),
        );
        let values: Vec<u8> =
            PodSerializer::serialize(
                std::io::Cursor::new(
                    Vec::new()
                ),
                &Value::Object(
                    obj
                ),
            )
            .unwrap()
            .0
            .into_inner();
        let mut params = [
            Pod::from_bytes(
                &values
            )
            .unwrap()
        ];
    // Connect:
        match
            stream
                .connect(
                    Direction::Output,
                    None, //opt.target,
                    StreamFlags::MAP_BUFFERS,
                    // StreamFlags::AUTOCONNECT | StreamFlags::MAP_BUFFERS,
                    // StreamFlags::INACTIVE | StreamFlags::MAP_BUFFERS,
                    // StreamFlags::INACTIVE,
                    // &mut [],
                    &mut params,
                )
        {
            Err(error) => panic!("{error}"),
            Ok(_) => {
                // println!("-> pw stream connected");
            },
        };
        // Store data:
        Self {
            stream,
            stream_listener,
            // target: None,
        }
    }

}

impl PipewireOutputStream {
    pub fn update(
        &self,
        // data: Vec<u8>,
        data: &[u8],
    ) {
        match
            self
                .stream
                .dequeue_buffer()
        {
            None => (),
            Some(mut buffer) => {
                println!(
                    "ii bytes: {}, complete: {:?}",
                    // "ii frame: {}, bytes: {}, complete: {:?}",
                    // "ii frame: {}, datas: {}, chunk size: {}, bytes: {}\n   complete: {:?}",
                    // "ii frame: {}, datas: {}, chunk size: {}, buf len: {}, mem size: {}\n   complete: {:?}",
                    // user_data.frame_num,
                    // buffer
                    //     .datas_mut()
                    //     .len(),
                    // buffer
                    //     .datas_mut()[0]
                    //     .chunk()
                    //     .size(),
                    buffer
                        .datas_mut()[0]
                        .data()
                        .unwrap()
                        .as_bytes()
                        .len(),
                    // std::mem::size_of::<u8>(),
                    buffer
                        .datas_mut(),
                );
                buffer
                    .datas_mut()[0]
                    .data()
                    .unwrap()
                    .clone_from_slice(
                        data,
                        // data.as_slice(),
                    );

                // let chunk = data.chunk_mut();
                // *chunk.offset_mut() = 0;
                // *chunk.stride_mut() = stride as _;
                // *chunk.size_mut() = (stride * n_frames) as _;
            },
        }
    }
}


// ----------------------------------------------------------------------------
//  Functions:
// ----------------------------------------------------------------------------
