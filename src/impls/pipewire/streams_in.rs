
use crate::app::events::GuiEvent;
use image::EncodableLayout;
use pipewire::{
    core::Core,
    keys::{
        MEDIA_CATEGORY,
        MEDIA_CLASS,
        MEDIA_ROLE,
        // MEDIA_SOFTWARE,
        MEDIA_TYPE,
        // NODE_DESCRIPTION,
        // PORT_ALIAS,
    },
    properties::properties,
    spa::{
        param::{
            format::{
                FormatProperties,
                MediaSubtype,
                MediaType,
            },
            video::VideoFormat,
            ParamType,
        },
        pod::{
            serialize::PodSerializer,
            Pod,
            Value,
            object,
            property,
        },
        utils::{
            Direction,
            Fraction,
            Rectangle,
            SpaTypes,
        },
    },
    stream::{
        Stream,
        StreamFlags,
        StreamListener,
        StreamState,
    },
};
use std::{
    // fmt,
    sync::Arc,
};
use vulkano::{
    // buffer::{
    //     Buffer,
    //     BufferCreateInfo,
    //     BufferUsage,
    //     Subbuffer,
    // },
    memory::allocator::{
        // AllocationCreateInfo,
        MemoryAllocator,
        // MemoryTypeFilter,
    },
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct PipewireInputUserData {
    pub event_loop_proxy: EventLoopProxy<GuiEvent>,
    pub frame_num: u32,
    pub memory_allocator: Arc<dyn MemoryAllocator>,
    pub source_id: String, // source as in SourceNode::PipewireInput(_)
}

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct PipewireInputStream {
    pub stream: Stream,
    pub stream_listener: StreamListener<PipewireInputUserData>,
    // pub target: Option<u32>,
}

// impl fmt::Debug for PipewireInputStream {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(
//             f,
//             "stream: {:?}",
//             self.stream.name(),
//         )
//     }
// }

impl PipewireInputStream {
    pub fn new(
        core: Core,
        user_data: PipewireInputUserData,
    ) -> Self {
    // Create stream:
        println!("-> create stream");
        let stream: Stream = match
            Stream::new(
                &core,
                user_data.source_id.as_str(),
                properties! {
                    *MEDIA_TYPE => "Video",
                    *MEDIA_CATEGORY => "Capture", // "Playback", // "Capture",
                    *MEDIA_ROLE => "Screen", // "Screen" // "Camera"
                    *MEDIA_CLASS => "Video/Sink", // "Video/Source"
                    // *MEDIA_SOFTWARE => "pw-videomix",
                    // *NODE_DESCRIPTION => user_data.source_id.as_str(),
                    // *PORT_ALIAS => "input",
                },
            )
        {
            Err(error) => panic!("!! {error}"),
            Ok(stream) => stream,
        };
        let stream_listener = match
            stream
                .add_local_listener_with_user_data(user_data)
                .param_changed(
                    |_stream_ref, _, _user_data, _spa_pod| {}
                )
                .state_changed(
                    |_, _, old, new| {
                        println!("<> pw input: {old:?} -> {new:?}");
                        match
                            new
                        {
                            StreamState::Error(error) => println!("!! error: {error}"),
                            StreamState::Unconnected => (),
                            StreamState::Connecting => (),
                            StreamState::Paused => (),
                            StreamState::Streaming => {
                                // get size of stream???
                            },
                        };
                    }
                )
                .process(
                    |stream_ref, user_data| match
                        stream_ref
                            .dequeue_buffer()
                    {
                        None => println!("-- no buffer received"),
                        Some(mut buffer) => {
                            // println!("!! new frame {frame_count}");
                            user_data.frame_num += 1;
                            println!(
                                "ii frame: {}, bytes: {}, complete: {:?}",
                                // "ii frame: {}, datas: {}, chunk size: {}, bytes: {}\n   complete: {:?}",
                                // "ii frame: {}, datas: {}, chunk size: {}, buf len: {}, mem size: {}\n   complete: {:?}",
                                user_data
                                    .frame_num,
                                // buffer
                                //     .datas_mut()
                                //     .len(),
                                // buffer
                                //     .datas_mut()[0]
                                //     .chunk()
                                //     .size(),
                                buffer
                                    .datas_mut()[0]
                                    .data()
                                    .unwrap()
                                    .as_bytes()
                                    .len(),
                                // std::mem::size_of::<u8>(),
                                buffer
                                    .datas_mut(),
                            );

                            // println!("ii chunk: {:?}", buffer.datas_mut()[0].chunk());
                            // println!("ii buffer received: {:?}", buffer.datas_mut().first_mut().unwrap());

                            // DataType::MemFd???
                            // get chunks with size, offset, stride???
                            // copy data into buffer or directly into image???

                            // Create buffer:
                            // use a buffer pool???
                            // let picture_buffer: Subbuffer<[u8]> = Buffer::from_iter(
                            //     user_data.memory_allocator.clone(),
                            //     BufferCreateInfo {
                            //         usage: BufferUsage::TRANSFER_SRC,
                            //         ..Default::default()
                            //     },
                            //     AllocationCreateInfo {
                            //         memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                            //             | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                            //         ..Default::default()
                            //     },
                            //     buffer.datas_mut()[0].data().unwrap().as_bytes().to_owned(), // not actual data!!!
                            // )
                            // .unwrap();
                            // Send event.
                            // user_data
                            //     .event_loop_proxy
                            //     .send_event(
                            //         GuiEvent::PipewireFrame(
                            //             (
                            //                 picture_buffer,
                            //                 user_data.source_id.clone(),
                            //              )
                            //         )
                            //     )
                            //     .ok();
                        },
                    }
                )
                .drained(
                    |_, _| {
                        println!("\nii stream drained");
                    }
                )
                .register()
        {
            Err(error) => panic!("!! {error}"),
            Ok(listener) => {
                // println!("-> stream object created");
                listener
            },
        };
    // Params:
        let obj = object!(
            SpaTypes::ObjectParamFormat,
            ParamType::EnumFormat,
            property!(
                FormatProperties::MediaType,
                Id,
                MediaType::Video
            ),
            property!(
                FormatProperties::MediaSubtype,
                Id,
                MediaSubtype::Raw
            ),
            property!(
                FormatProperties::VideoFormat,
                Choice,
                Enum,
                Id,
                VideoFormat::RGB,
                VideoFormat::RGB,
                VideoFormat::RGBA,
                VideoFormat::RGBx,
                VideoFormat::BGRx,
                VideoFormat::YUY2,
                VideoFormat::I420,
            ),
            property!(
                FormatProperties::VideoSize,
                Choice,
                Range,
                Rectangle,
                Rectangle {
                    width: 320,
                    height: 240,
                },
                Rectangle {
                    width: 1,
                    height: 1,
                },
                Rectangle {
                    width: 4096,
                    height: 4096,
                }
            ),
            property!(
                FormatProperties::VideoFramerate,
                Choice,
                Range,
                Fraction,
                Fraction {
                    num: 25,
                    denom: 1,
                },
                Fraction {
                    num: 0,
                    denom: 1,
                },
                Fraction {
                    num: 1000,
                    denom: 1,
                }
            ),
        );
        let values: Vec<u8> = PodSerializer::serialize(
            std::io::Cursor::new(Vec::new()),
            &Value::Object(obj),
        )
        .unwrap()
        .0
        .into_inner();
        let mut params = [Pod::from_bytes(&values).unwrap()];
    // Connect:
        match
            stream
                .connect(
                    Direction::Input,
                    None, //opt.target,
                    StreamFlags::MAP_BUFFERS,
                    // StreamFlags::AUTOCONNECT | StreamFlags::MAP_BUFFERS,
                    // StreamFlags::INACTIVE | StreamFlags::MAP_BUFFERS,
                    // StreamFlags::INACTIVE,
                    // &mut [],
                    &mut params,
                )
        {
            Err(error) => println!("!! error: {error}"),
            Ok(_) => {
                // println!("-> pw stream connected");
            },
        };
        // Store data:
        Self {
            stream,
            stream_listener,
            // target: None,
        }
    }

}

// ----------------------------------------------------------------------------
//  Functions:
// ----------------------------------------------------------------------------
