
use crate::{
    app::{
        data::app_nodes::SourceToNode,
        print::LOAD_IMAGE,
    },
    impls::vulkano::scenes::render::stack_scene::{
        StackLayer,
        StackScene,
    },
    nodes::sources::{
        picture_stack_in::StackNode,
        // video::VideoNode,
        SourceNode,
    },
};
use egui_winit_vulkano::{
    egui::Vec2,
    RenderResources,
};
use image::{
    DynamicImage,
    ImageReader,
};
use rfd::FileDialog;
use std::{
    cell::RefCell,
    path::PathBuf,
    rc::Rc,
    sync::{
        mpsc,
        Arc,
    },
    thread,
};
use vulkano::command_buffer::allocator::StandardCommandBufferAllocator;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilePath {
    pub extension: String,
    pub file_name: String,
    pub full: String,
}

// ----------------------------------------------------------------------------

#[derive(Clone, PartialEq)]
enum ObjectClass {
    Dir,
    File,
    Picture,
    Symlink,
    Unknown,
    Video,
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, Copy)]
pub enum FileBatch {
    Empty,
    Single,
    Stack,
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct TextureMessage {
    pub file_batch: FileBatch,
    pub file_path: FilePath,
    pub image_data: Vec<u8>,
    pub size: Vec2,
    pub source_id: String,
}

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct TextureChannel {
    pub receiver: mpsc::Receiver<TextureMessage>,
    pub sender: mpsc::Sender<TextureMessage>,
}

impl Default for TextureChannel {
    fn default() -> Self {
        let (sender, receiver): (
            mpsc::Sender<TextureMessage>,
            mpsc::Receiver<TextureMessage>,
        ) = mpsc::channel();
        Self {
            receiver,
            sender,
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// Button "Files / Stacks" pressed:
pub fn open_file_dialog(
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    resources: RenderResources,
    sender_texture: mpsc::Sender<TextureMessage>,
) {

    match
        match
            cfg!(feature = "gstreamer")
        {
            true => {
                FileDialog::new()
                    .set_title("Open Files - pw-videomix")
                    .add_filter(
                        "all (pictures, video)",
                        &["jpeg", "JPEG", "jpg", "JPG", "png", "PNG", "webp", "webm", "mkv", "MKV", "mp4", "MP4", "gif", "GIF"],
                    )
                    .add_filter(
                        "pictures (jpg, png, webp)",
                        &["jpeg", "JPEG", "jpg", "JPG", "png", "PNG", "webp"],
                    )
                    .add_filter(
                        "videos (webm, mkv, mp4, gif)",
                        &["webm", "mkv", "MKV", "mp4", "MP4", "gif", "GIF"],
                    )
                    // .add_filter("none", &[])
                    .pick_files()
            },
            false => {
                FileDialog::new()
                    .set_title("Open Files - pw-videomix")
                    .add_filter(
                        "pictures (jpg, png, webp)",
                        &["jpeg", "JPEG", "jpg", "JPG", "png", "PNG", "webp"],
                    )
                    // .add_filter("none", &[])
                    .pick_files()
            },
        }
    {
        None => (),
        Some(paths) => {
            file_source_added(
                map_source_to_node,
                paths,
                resources,
                sender_texture,
            );
        },
    };
}

// ----------------------------------------------------------------------------

// File was added by pressing button or dropping on panel.
pub fn file_source_added(
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    paths: Vec<PathBuf>,
    resources: RenderResources,
    sender_texture: mpsc::Sender<TextureMessage>,
) {
    // Files have been added:
    match
        paths
            .len()
    {
        0 => (),
        1 => {
            handle_objects(
                FileBatch::Single,
                #[cfg(feature = "gstreamer")]
                map_source_to_node,
                paths,
                sender_texture,
                String::from("single file"),
            );
        },
        _ => {
            // Create a StackNode:
            let source_id: String = SourceNode::stack(
                Rc::clone(&map_source_to_node),
                resources,
            );
            handle_objects(
                FileBatch::Stack,
                #[cfg(feature = "gstreamer")]
                map_source_to_node,
                paths,
                sender_texture,
                source_id,
            );
        },
    };
}

// ----------------------------------------------------------------------------

// Check single files from the whole batch.
fn handle_objects(
    file_batch: FileBatch,
    #[cfg(feature = "gstreamer")]
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    paths: Vec<PathBuf>,
    sender_picture: mpsc::Sender<TextureMessage>,
    source_id: String,
) {
    // Handle single objects from list:
    for path_buf in paths {
        let extension: String = match
            path_buf
                .extension()
        {
            None => String::from("no extension"),
            Some(os_str) => match
                os_str
                    .to_str()
            {
                None => String::from("not valid"),
                Some(extension) => extension.to_string(),
            },
        };
        let file_path: FilePath = {
            FilePath {
                extension: extension.to_owned(),
                file_name: path_buf.file_name().unwrap().to_str().unwrap().to_string(),
                full: path_buf.as_path().to_str().unwrap().to_string(),
            }
        };
        match
            check_object_class(
                extension,
                path_buf,
            )
        {
            ObjectClass::Picture => {
                handle_image_data(
                    file_batch,
                    file_path,
                    sender_picture.clone(),
                    source_id.clone(),
                );
            },
            ObjectClass::Video => {
                #[cfg(feature = "gstreamer")]
                SourceNode::video(
                    file_path,
                    Rc::clone(&map_source_to_node),
                );
            },
            _ => (),
        }
    }
}

// ----------------------------------------------------------------------------

// Handles single pictures:
// If picture is from a stack, stack was already created.
// Create preview textures even for stacks???
fn handle_image_data(
    file_batch: FileBatch,
    file_path: FilePath,
    sender_texture: mpsc::Sender<TextureMessage>,
    source_id: String,
) {
    thread::spawn(move || {
        // println!("thread spawned");
        // Processes can be very slow!
        // Create DynamicImage:
        let image_dynamic: DynamicImage = ImageReader::open(&file_path.full)
            .unwrap()
            .decode()
            .unwrap();
        let size: Vec2 = Vec2::new(image_dynamic.width() as f32, image_dynamic.height() as f32);
        let image_data: Vec<u8> = image_dynamic.to_rgba8().to_vec();
        println!("-> {LOAD_IMAGE} {}", file_path.file_name);

        let _result = sender_texture.send(
            TextureMessage{
                file_batch,
                file_path,
                image_data,
                size,
                source_id,
            }
        );
    });
}

// ----------------------------------------------------------------------------

pub fn texture_received(
    command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    resources: RenderResources,
    texture_message: TextureMessage,
) {
    match
        texture_message
            .file_batch
    {
        FileBatch::Empty => (),
        FileBatch::Single => {
            SourceNode::picture(
                command_buffer_allocator,
                map_source_to_node,
                texture_message,
                resources,
            );
        },
        FileBatch::Stack => {
            // SourceNode::Stack already created.
            // add texture to sorted list!!!
            let source_id: String = texture_message.source_id.clone();
            let source_node: SourceNode = match
                map_source_to_node
                    .borrow()
                    .map
                    .get(&source_id)
            {
                None => panic!("!! error: source node not found"), // create a new one???
                Some(source_node) => match
                    source_node
                        .to_owned()
                {
                    SourceNode::Stack(mut stack_node) => {
                        let mut scene: StackScene = stack_node.scene;
                        let stack_layer: StackLayer = StackLayer::new(
                            command_buffer_allocator,
                            resources,
                            texture_message,
                        );
                        scene
                            .layers
                            .push(stack_layer);
                        stack_node = StackNode {
                            scene,
                            ..stack_node
                        };
                        SourceNode::Stack(stack_node.to_owned())
                    },
                    source_node => source_node.to_owned(),
                },
            };
            map_source_to_node
                .borrow_mut()
                .map
                .insert(
                    source_id,
                    source_node,
                );
        },
    };
}

// ----------------------------------------------------------------------------
// Functions - object class:
// ----------------------------------------------------------------------------

fn check_object_class(
    extension: String,
    path_buf: PathBuf,
) -> ObjectClass {
    match
        check_is_file(
            path_buf.clone()
        )
    {
        ObjectClass::File => check_file_extension(extension),
        _ => match
            check_is_dir(
                path_buf.clone()
            )
        {
            ObjectClass::Dir => ObjectClass::Dir,
            _ => match
                check_is_symlink(
                    path_buf
                )
            {
                ObjectClass::Symlink => ObjectClass::Symlink,
                _ => ObjectClass::Unknown,
            }
        }
    }
}

// ----------------------------------------------------------------------------

fn check_is_file(
    path_buf: PathBuf,
) -> ObjectClass {
    match
        path_buf
            .is_file()
    {
        true => ObjectClass::File,
        false => ObjectClass::Unknown,
    }
}

// ----------------------------------------------------------------------------

fn check_file_extension(
    extension: String,
) -> ObjectClass {
    match
        extension
            .as_str()
    {
        // If picure:
        "jpeg" | "JPEG" | "jpg" | "JPG" | "png" | "PNG" | "webp" => ObjectClass::Picture,
        // If video:
        "webm" | "mkv" | "MKV" | "mp4" | "MP4" | "gif" | "GIF" => ObjectClass::Video,
        "no extension" => {
            println!("file with no extension");
            ObjectClass::Unknown
        },
        "not valid" => {
            println!("file with invalid extension");
            ObjectClass::Unknown
        },
        _ => {
            println!("file with unknown extension");
            ObjectClass::File
        },
    }
}

// ----------------------------------------------------------------------------

fn check_is_dir(
    path_buf: PathBuf,
) -> ObjectClass {
    match
        path_buf
            .is_dir()
    {
        true => {
            println!(
                "!! ignore directory:\n   {}",
                path_buf.as_path().to_str().unwrap(),
            );
            ObjectClass::Dir
        }
        false => {
            check_is_symlink(path_buf);
            ObjectClass::Unknown
        }
    }
}

// ----------------------------------------------------------------------------

fn check_is_symlink(
    path_buf: PathBuf,
) -> ObjectClass {
    match
        path_buf
            .is_symlink()
    {
        true => {
            println!(
                "!! ignore symlink:\n   {}",
                path_buf.as_path().to_str().unwrap(),
            );
            ObjectClass::Symlink
        }
        false => {
            println!(
                "!! ignore <unknown>:\n   {}",
                path_buf.as_path().to_str().unwrap(),
            );
            ObjectClass::Unknown
        }
    }
}
