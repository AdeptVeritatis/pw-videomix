pub mod listener;
pub mod streams_in;
pub mod streams_out;

use crate::{
    // gui::events::GuiEvent,
    impls::pipewire::{
        listener::{
            listener_add,
            listener_remove,
        },
        streams_in::PipewireInputStream,
        streams_out::PipewireOutputStream,
    },
    // nodes::sources::SourceNode,
};
use pipewire::{
    context::Context,
    core::{
        Core,
        PW_ID_CORE,
    },
    main_loop::MainLoop,
    node::NodeListener,
    registry::{
        Listener,
        Registry,
    },
    spa::utils::result::AsyncSeq,
};
use std::{
    cell::{
        Cell,
        RefCell,
    },
    collections::HashMap,
    rc::Rc,
};
// use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct PipewireCamera {
    pub description: String,
    pub device: u32,
    pub name: String,
    pub nick: String,
    pub node: u32,
    pub node_listener: NodeListener,
    pub serial: u32,
}

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub enum PipewireStream {
    InputStream(PipewireInputStream),
    OutputStream(PipewireOutputStream),
}

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct PipewireLoop {
    pub context: Context,
    pub core: Core,
    pub listener_add: Listener,
    pub listener_remove: Listener,
    pub mainloop: MainLoop,
    pub registry: Rc<Registry>,
}

impl PipewireLoop {
    pub fn new(
        // event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_id_to_camera: Rc<RefCell<HashMap<u32, PipewireCamera>>>,
        // map_source_to_node: Rc<RefCell<HashMap<String, SourceNode>>>,
    ) -> Self {
        println!(".. starting PipeWire");
        // Create mainloop:
        let mainloop: MainLoop = match
            MainLoop::new(
                None,
            )
        {
            Err(error) => panic!(
                "\n!! Problem creating pipewire mainloop:\n error: {:?}",
                error,
            ),
            Ok(mainloop) => mainloop,
        };
        let context: Context = match
            Context::new(&mainloop)
        {
            Err(error) => panic!(
                "\n!! Problem creating pipewire context:\n error: {:?}",
                error,
            ),
            Ok(context) => context,
        };
        let core: Core =
            match
                context.connect(None)
            {
                Err(error) => panic!(
                    "\n!! Problem connecting to pipewire core:\n error: {:?}",
                    error,
                ),
                Ok(core) => core,
            };

        let registry: Rc<Registry> = match
            core.get_registry()
        {
            Err(error) => panic!("\n!! Problem getting registry:\n error: {error:?}"),
            Ok(registry) => Rc::new(registry),
        };

        // Listen to pipewire nodes being created or destroyed.
        // Store cameras and send them to main thread.
        let listener_add: Listener = listener_add(
            // event_loop_proxy.clone(),
            Rc::clone(&registry),
            Rc::clone(&map_id_to_camera),
        );

        let listener_remove: Listener = listener_remove(
            // event_loop_proxy.clone(),
            Rc::clone(&registry),
            Rc::clone(&map_id_to_camera),
        );
        Self {
            context,
            core,
            listener_add,
            listener_remove,
            mainloop,
            registry,
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn do_roundtrip(
    core: &Core,
    mainloop: MainLoop,
) {
    let done: Rc<Cell<bool>> = Rc::new(Cell::new(false));
    let done_clone: Rc<Cell<bool>> = Rc::clone(&done);
    let mainloop_clone: MainLoop = mainloop.clone();

    let pending: AsyncSeq = match core.sync(0) {
        Ok(pending) => pending,
        Err(error) => panic!("\n!! Problem syncing pipewire core:\n error: {:?}", error,),
    };

    let _listener_core: pipewire::core::Listener = core
        .add_listener_local()
        .done(move |id, seq| {
            if id == PW_ID_CORE && seq == pending {
                done_clone.set(true);
                mainloop_clone.quit();
            }
        })
        .register();

    while !done.get() {
        //         println!("mainloop started");
        mainloop.run();
    }
    //     println!("mainloop done");
}
