pub mod descriptor_sets;
pub mod render_pass;
pub mod scenes;
pub mod shader;
pub mod vertices;

use crate::constants::{
    IMAGE_FORMAT,
    STANDARD_NODE_OUTPUT_HEIGHT,
    STANDARD_NODE_OUTPUT_WIDTH,
};
use egui_winit_vulkano::RenderResources;
use std::sync::Arc;
use vulkano::{
    image::{
        Image,
        ImageCreateInfo,
        ImageType,
        ImageUsage,
    },
    memory::allocator::AllocationCreateInfo,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn create_image_storage(
    resources: RenderResources,
) -> Arc<Image> {
    let extent: [u32; 3] = [
        STANDARD_NODE_OUTPUT_WIDTH as u32,
        STANDARD_NODE_OUTPUT_HEIGHT as u32,
        1,
    ];
    Image::new(
        resources.memory_allocator.clone(),
        ImageCreateInfo {
            image_type: ImageType::Dim2d,
            format: IMAGE_FORMAT,
            extent,
            usage: ImageUsage::TRANSFER_DST | ImageUsage::TRANSFER_SRC | ImageUsage::SAMPLED | ImageUsage::COLOR_ATTACHMENT,
            ..Default::default()
        },
        AllocationCreateInfo::default(),
    )
    .unwrap()
}
