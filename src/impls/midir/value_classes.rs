
use crate::impls::midir::{
    // MidiClass,
    MidiControllerClass,
    MidiMessage,
};
use std::ops::RangeInclusive;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// pub fn bool_switch(
//     class: MidiClass,
//     // midi_event: MidiMessage,
//     // preset: bool,
//     value: bool,
// ) -> bool {
//     match
//         class
//     {
//         MidiClass::KeyNoteOn |
//         MidiClass::PadNoteOn
//         => !value,
//         _ => value,
//     }
// }

// ----------------------------------------------------------------------------

pub fn float_range_offset_range(
    controller_class: MidiControllerClass,
    midi_event: MidiMessage,
    offset: f32,
    speed: f32,
    value: f32,
    value_range: RangeInclusive<f32>,
    value_speed: f32,
) -> f32 {
    let range_start: f32 = value_range.start().to_owned();
    let range_end: f32 = value_range.end().to_owned();
    match
        controller_class
    {
        // f32_from0.01_fillrange_default1.0#64_fillrange_to10.0
        // f32_from-1.0_fillrange_default0.0#64_fillrange_to1.0
        MidiControllerClass::Absolute
        => {
            match
                midi_event
                    .value
            {
                value if value < 64 => range_start + value as f32 / 64.0 * (offset - range_start),
                value if value > 64 => offset + (value - 64) as f32 / 63.0 * (range_end - offset),
                _ => offset,
            }
                .clamp(
                    range_start,
                    range_end,
                )
        },
        MidiControllerClass::RelativeOffset0
        |
        MidiControllerClass::RelativeOffset16
        |
        MidiControllerClass::RelativeOffset64
        => {
            (value + speed * value_speed)
                .clamp(
                    range_start,
                    range_end,
                )
        },
    }
}

// ----------------------------------------------------------------------------

pub fn float_speed_0_speed(
    controller_class: MidiControllerClass,
    midi_event: MidiMessage,
    speed: f32,
    value: f32,
    value_range: RangeInclusive<f32>,
    value_speed: f32,
) -> f32 {
    let range_start: f32 = value_range.start().to_owned();
    let range_end: f32 = value_range.end().to_owned();
    match
        controller_class
    {
        // f32_from#0_singlespeed_default0.0#64_singlespeed_to#127
        MidiControllerClass::Absolute
        => (midi_event.value as f32 - 64.0) * value_speed
            .clamp(range_start, range_end),
        MidiControllerClass::RelativeOffset0 | MidiControllerClass::RelativeOffset16 | MidiControllerClass::RelativeOffset64
        => (value + speed * value_speed)
            .clamp(range_start, range_end),
    }
}

// ----------------------------------------------------------------------------

pub fn natural_step_multistep(
    controller_class: MidiControllerClass,
    factor: u32, // = speed???
    midi_event: MidiMessage,
    speed: f32,
    value: u32,
    value_range: RangeInclusive<u32>,
) -> u32 {
    let range_start: u32 = value_range.start().to_owned();
    let range_end: u32 = value_range.end().to_owned();
    match
        controller_class
    {
        // u32_from1_singlestep_default2_singestep_#64_multistep*10_to<3599999
        MidiControllerClass::Absolute
        => match
            midi_event
                .value
            {
                value if value < 64 => range_start + midi_event.value as u32,
                // if range_end - range_start - 64 < 0???
                // value if value >= 64 => midi_event.value as u32 * factor, // ???
                // value if value >= 64 => range_start + 64 + (midi_event.value - 64) as u32 * factor, // ???
                value if value >= 64 => (1 + (range_start + 64) / factor + (midi_event.value - 64) as u32) * factor, // ???
                _ => 0,
            }
            .clamp(range_start, range_end),
        MidiControllerClass::RelativeOffset0 | MidiControllerClass::RelativeOffset16 | MidiControllerClass::RelativeOffset64
        => ((value as f32 + speed) as u32)
            .clamp(range_start, range_end),
    }
}

// ----------------------------------------------------------------------------

pub fn natural_step_range(
    controller_class: MidiControllerClass,
    midi_event: MidiMessage,
    speed: f32,
    value: u32,
    value_range: RangeInclusive<u32>,
) -> u32 {
    let range_start: u32 = value_range.start().to_owned();
    let range_end: u32 = value_range.end().to_owned();
    match
        controller_class
    {
        // u32_from2_singlestep_default6_singlestep_#64_fillrange_to360
        MidiControllerClass::Absolute
        => match
            midi_event
                .value
            {
                value if value < 64 => range_start + midi_event.value as u32,
                // if range_end - range_start - 64 < 0???
                value if value >= 64 => range_start + 64 + (((midi_event.value - 64) as f32) * (range_end - 64 - range_start) as f32 / 63.0) as u32,
                _ => 0,
            }
            .clamp(range_start, range_end),
        MidiControllerClass::RelativeOffset0 | MidiControllerClass::RelativeOffset16 | MidiControllerClass::RelativeOffset64
        => ((value as f32 + speed) as u32)
            .clamp(range_start, range_end),
    }
}
