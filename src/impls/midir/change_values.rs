
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        gui::bind::midi_bind::MidiBind,
    },
    impls::midir::MidiMessage,
    nodes::{
        control::ControlNode,
        filter::FilterNode,
        sinks::SinkNode,
        sources::SourceNode,
    },
};
// use egui_winit_vulkano::RenderResources;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn control_values(
    map_control_to_node: Rc<RefCell<ControlToNode>>,
    midi_bind: &MidiBind,
    midi_event: MidiMessage,
    speed: f32,
) {
    let control_node: ControlNode = match
        map_control_to_node
            .borrow()
            .map
            .get(
                &midi_bind
                    .bind
                    .node_id,
            )
    {
        // unbind MIDI controller, if node is removed???
        None => {
            println!("!! ControlNode not found");
            ControlNode::None
        },
        Some(ControlNode::None) => {
            println!("!! ControlNode::None not supported");
            ControlNode::None
        },
        Some(ControlNode::FunctionGenerator(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(ControlNode::Number(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(ControlNode::Trigger(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(ControlNode::Trigonometry(_)) => {
            println!("!! ControlNode::Trigonometry no need for midi");
            // trigonometry_node
            //     .received_midi(
            //         midi_bind,
            //         midi_event,
            //         speed,
            //     )
            ControlNode::None
        },
    };
    match
        control_node
    {
        ControlNode::None => (),
        _ => {
            // Store new value.
            map_control_to_node
                .borrow_mut()
                .map
                .insert(
                    midi_bind
                        .bind
                        .node_id
                        .clone(),
                    control_node,
                );
        },
    };
}

// ----------------------------------------------------------------------------

pub fn filter_values(
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    midi_bind: &MidiBind,
    midi_event: MidiMessage,
    speed: f32,
) {
    let filter_node: FilterNode = match
        map_filter_to_node
            .borrow()
            .map
            .get(
                &midi_bind
                    .bind
                    .node_id,
            )
    {
        // unbind MIDI controller, if node is removed???
        None => {
            println!("!! FilterNode not found");
            FilterNode::None
        },
        Some(FilterNode::None) => {
            println!("!! FilterNode::None not supported");
            FilterNode::None
        },
        Some(FilterNode::ColorMixer(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(FilterNode::ColorRotator(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(FilterNode::Fader(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(FilterNode::Kaleidoscope(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(FilterNode::Mandala(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(FilterNode::Mixer(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
    };
    match
        filter_node
    {
        FilterNode::None => (),
        _ => {
            // Store new value.
            map_filter_to_node
                .borrow_mut()
                .map
                .insert(
                    midi_bind
                        .bind
                        .node_id
                        .clone(),
                    filter_node,
                );
        },
    };
}

// ----------------------------------------------------------------------------

pub fn sink_values(
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    midi_bind: &MidiBind,
    midi_event: MidiMessage,
    // resources: RenderResources,
    speed: f32,
) {
    let sink_node: SinkNode = match
        map_sink_to_node
            .borrow()
            .map
            .get(
                &midi_bind
                    .bind
                    .node_id,
            )
    {
        // unbind MIDI controller, if node is removed???
        None => {
            println!("!! SinkNode not found");
            SinkNode::None
        },
        Some(SinkNode::None) => {
            println!("!! SinkNode::None not supported");
            SinkNode::None
        },
        #[cfg(feature = "gstreamer")]
        Some(SinkNode::Encoder(node)) => {
            // println!("!! MIDI for encoders not implemented yet");
            node
                .to_owned()
                .received_midi(
                    midi_bind,
                    midi_event,
                    // resources,
                );
            SinkNode::Encoder(
                node
                    .to_owned()
            )
        },
        Some(SinkNode::Monitor(node)) => {
            println!("!! no MIDI for monitors");
            SinkNode::Monitor(node.to_owned())
        },
        #[cfg(unix)]
        Some(SinkNode::PipewireOutput(node)) => {
            println!("!! MIDI for pw_output not implemented yet");
            SinkNode::PipewireOutput(node.to_owned())
        },
        // #[cfg(feature = "gstreamer")]
        // Some(SinkNode::PwOutputGst(pw_output_gst_node)) => {
        //     println!("!! MIDI for pw_output_gst not implemented yet");
        //     SinkNode::PwOutputGst(pw_output_gst_node.to_owned())
        // },
        Some(SinkNode::Snapshot(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        Some(SinkNode::Window(node)) => {
            // May be used for the play/pause/stop button.
            println!("!! no MIDI for windows");
            SinkNode::Window(node.to_owned())
        },
    };
    match
        sink_node
    {
        SinkNode::None
        |
        SinkNode::Monitor(_)
        |
        SinkNode::Window(_)
        => (),
        _ => {
            // Store new value.
            map_sink_to_node
                .borrow_mut()
                .map
                .insert(
                    midi_bind
                        .bind
                        .node_id
                        .clone(),
                    sink_node,
                );
        },
    };
}

// ----------------------------------------------------------------------------

pub fn source_values(
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    midi_bind: &MidiBind,
    midi_event: MidiMessage,
    speed: f32,
) {
    let source_node: SourceNode = match
        map_source_to_node
            .borrow()
            .map
            .get(
                &midi_bind
                    .bind
                    .node_id,
            )
    {
        // unbind MIDI controller, if node is removed???
        None => {
            println!("!! SourceNode not found");
            SourceNode::None
        },
        Some(SourceNode::None) => {
            println!("!! SourceNode::None not supported");
            SourceNode::None
        },
        Some(SourceNode::Picture(node)) => {
            println!("!! no MIDI for pictures");
            SourceNode::Picture(node.to_owned())
        },
        #[cfg(unix)]
        Some(SourceNode::PipewireInput(node)) => {
            println!("!! MIDI not implemented for pipewire yet");
            SourceNode::PipewireInput(node.to_owned())
        },
        // #[cfg(feature = "gstreamer")]
        // Some(SourceNode::PwInputGst(pw_input_gst_node)) => {
        //     println!("!! MIDI not implemented for pw_input_gst yet");
        //     SourceNode::PwInputGst(pw_input_gst_node.to_owned())
        // },
        Some(SourceNode::Stack(node)) => {
            node
                .received_midi(
                    midi_bind,
                    midi_event,
                    speed,
                )
        },
        #[cfg(feature = "gstreamer")]
        Some(SourceNode::Video(node)) => {
            println!("!! MIDI not implemented for videos yet");
            SourceNode::Video(node.to_owned())
        },
    };
    match
        source_node
    {
        SourceNode::None
        |
        SourceNode::Picture(_)
        => (),
        _ => {
            // Store new value.
            map_source_to_node
                .borrow_mut()
                .map
                .insert(
                    midi_bind
                        .bind
                        .node_id
                        .clone(),
                    source_node,
                );
        },
    };
}
