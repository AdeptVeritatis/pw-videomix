
use crate::{
    constants::ENCODER_FPS,
    impls::gstreamer::profiles::EncoderProfile,
    nodes::{
        connections::InputConnection,
        sinks::video_out::EncoderValues,
    },
};
use egui_winit_vulkano::RenderResources;
use gstreamer::{
    prelude::{
        Cast,
        ElementExt,
        GstBinExtManual,
        ObjectExt,
    },
    Caps,
    ClockTime,
    Element,
    ElementFactory,
    Format,
    // Fraction,
    // MessageView,
    Pipeline,
    // State,
};
use gstreamer_app::AppSrc;
use gstreamer_pbutils::{
    encoding_profile::EncodingProfileBuilder,
    EncodingContainerProfile,
    EncodingVideoProfile,
};
use gstreamer_video::{
    VideoFormat,
    VideoInfo,
};
use image::EncodableLayout;
use std::time::Instant;
use vulkano::{
    buffer::{
        BufferCreateInfo,
        BufferUsage,
        Subbuffer,
    },
    memory::allocator::{
        AllocationCreateInfo,
        MemoryTypeFilter,
    },
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct EncoderPipeline {
    pub appsrc: AppSrc,
    // pub extent: Vec2,
    // pub fps: f32,
    pub frame: u64,
    pub gst_buffer: gstreamer::Buffer,
    pub pipeline: Pipeline,
    pub pts_diff: ClockTime,
    // pub size: usize,
    pub video_info: VideoInfo, // has fps, extent and size!!!
    pub vulkano_buffer: Subbuffer<[u8]>,

    pub created: Instant,
    pub updated: Instant,
}

impl EncoderPipeline {
    pub fn new(
        connection: InputConnection,
        output_file: String,
        resources: RenderResources,
        values: EncoderValues,
    ) -> Self {
        let pipeline: Pipeline = Pipeline::default();

        let fps: f32 = ENCODER_FPS; // use encoder_pipeline.fps to choose from panel???
        let pts_diff: ClockTime = ClockTime::SECOND / fps as u64; // ???
        let profile: EncoderProfile = values
            .video_profile;

        let video_info: VideoInfo =
            VideoInfo::builder(
                VideoFormat::Rgbx,
                connection.source_extent.x as u32, // crashes, when sink_extent???
                connection.source_extent.y as u32,
            )
            // .fps(Fraction::new(fps as i32, 1))
            .fps(
                fps as i32, // ???
            )
            .build()
            .expect("Failed to create video info");
        let video_profile: EncodingVideoProfile =
            EncodingVideoProfile::builder(
                // "video/x-h264" "video/x-theora" "video/x-vp8" "video/x-raw"
                &Caps::builder(
                    profile
                        .codec,
                )
                .build(),
            )
            .presence(
                0,
            )
            .build();
        let container_profile: EncodingContainerProfile =
            EncodingContainerProfile::builder(
                // "video/webm" "video/x-matroska"
                &Caps::builder(
                    profile
                        .container
                )
                .build(),
            )
            .name(
                "container",
            )
            .add_profile(
                video_profile,
            )
            .build();

        let appsrc: AppSrc = AppSrc::builder()
            .caps(
                &video_info
                    .to_caps()
                    .unwrap(),
            )
            .format(
                Format::Time,
            )
            .build();
        let queue: Element = ElementFactory::make("queue")
            .build()
            .unwrap();
        let convert: Element = ElementFactory::make("videoconvert")
            .build()
            .unwrap();
        // let scale = ElementFactory::make("videoscale")
        //     .build()
        //     .unwrap();
        let encodebin: Element = ElementFactory::make("encodebin")
            .build()
            .unwrap();
        let sink: Element = ElementFactory::make("filesink")
            .property(
                "location",
                output_file,
            )
            .build()
            .unwrap();
        // let sink = ElementFactory::make("pipewiresink").build().unwrap();
        // let sink = ElementFactory::make("autovideosink").build().unwrap();

        encodebin
            .set_property(
                "profile",
                &container_profile,
            );
        let elements: &[&Element; 5] = &[
            appsrc
                .upcast_ref(), // ???
            &queue,
            &convert,
            // &scale,
            &encodebin,
            &sink,
        ];
        pipeline
            .add_many(
                elements,
            )
            .unwrap();
        Element::link_many(elements)
            .unwrap();
        for
            element
        in
            elements
        {
            element
                .sync_state_with_parent()
                .unwrap();
        }

    // Buffers:
        let gst_buffer: gstreamer::Buffer = gstreamer::Buffer::with_size(video_info.size())
            .unwrap();

        let vulkano_buffer: Subbuffer<[u8]> = vulkano::buffer::Buffer::from_iter(
            resources
                .memory_allocator
                .clone(),
            BufferCreateInfo {
                usage: BufferUsage::TRANSFER_DST,
                ..Default::default()
            },
            AllocationCreateInfo {
                memory_type_filter:
                    MemoryTypeFilter::PREFER_HOST
                    |
                    MemoryTypeFilter::HOST_RANDOM_ACCESS,
                ..Default::default()
            },
            (
                0
                ..
                video_info
                    .size()
            )
                .map(
                    |_| 0u8,
                ),
        )
        .unwrap();

        Self {
            appsrc,
            frame: 0,
            gst_buffer,
            pipeline,
            pts_diff,
            video_info,
            vulkano_buffer,

            created: Instant::now(),
            updated: Instant::now(),
        }
    }

    pub fn update(
        &self,
        vulkano_buffer: Subbuffer<[u8]>,
    ) -> Self {
        let mut frame: u64 = self
            .frame;
        let pts_diff: ClockTime = self
            .pts_diff;
        // let mut updated: Instant = self.updated;

        let now: Instant = Instant::now();
        // let mut gst_buffer = gstreamer::Buffer::with_size(self.video_info.size())
        //     .unwrap();
        let mut gst_buffer: gstreamer::Buffer = self
            .gst_buffer
            .clone();

        let gst_buffer_mut = gst_buffer
            .make_mut();
        // match
        //     gst_buffer
        //         .get_mut()
        // {
        //     None => {
        //         println!("!! no gst_buffer_mut");
        //     },
        //     Some(gst_buffer_mut) => {
                gst_buffer_mut
                    .set_pts(
                        // ClockTime::ZERO
                        frame * pts_diff
                        // ClockTime::from_mseconds(
                        //     now
                        //         .duration_since(
                        //             self.created
                        //         )
                        //         .as_millis() as u64
                        // )
                    );
                gst_buffer_mut
                    .map_writable()
                    .unwrap()
                    .copy_from_slice(
                        vulkano_buffer
                            .read()
                            .unwrap()
                            .as_bytes()
                    );
                self
                    .appsrc
                    .push_buffer(
                        gst_buffer,
                    )
                    .unwrap();
                frame += 1;
                // updated = now;
        //     },
        // };
        Self {
            frame,
            // gst_buffer,
            updated: now,
            ..self
                .to_owned()
        }
    }

}
