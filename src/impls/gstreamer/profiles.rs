
use crate::nodes::sinks::video_out::EncoderContainer;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
pub struct EncoderProfile {
    pub codec: String,
    pub container: String,
    pub extension: String,
}

impl Default for EncoderProfile {
    fn default() -> Self {
        Self {
            codec: String::from("video/x-theora"), // consts !!!
            container: String::from("video/x-matroska"),
            extension: String::from("mkv"),
        }
    }
}

impl EncoderProfile {
    pub fn select (
        container: EncoderContainer,
    ) -> Self {
        match
            container
        {
            EncoderContainer::Mkv => Self {
                codec: String::from("video/x-theora"),
                container: String::from("video/x-matroska"),
                extension: String::from("mkv"),
            },
            EncoderContainer::WebMvp8 => Self {
                codec: String::from("video/x-vp8"),
                container: String::from("video/webm"),
                extension: String::from("webm"),
            },
            EncoderContainer::WebMvp9 => Self {
                codec: String::from("video/x-vp9"),
                container: String::from("video/webm"),
                extension: String::from("webm"),
            },
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
