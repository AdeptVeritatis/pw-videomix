
use crate::impls::files::FilePath;
use gstreamer::{
    prelude::{
        Cast,
        ElementExt,
        GstBinExtManual,
        ObjectExt,
        PadExt,
    },
    Element,
    ElementFactory,
    Pipeline,
    State,
};
use gstreamer_app::AppSink;
use gstreamer_video::{
    VideoCapsBuilder,
    VideoFormat,
};
use std::time::Instant;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct VideoPipeline {
    pub appsink: AppSink,
    pub frame: u64,
    pub pipeline: Pipeline,

    pub created: Instant,
    pub updated: Instant,
}

impl VideoPipeline {
    pub fn new(
        file_path: FilePath,
    ) -> Self {
        let pipeline = Pipeline::default();

        let src = ElementFactory::make("filesrc")
            .property("location", file_path.full)
            .build()
            .unwrap();
        let decodebin = ElementFactory::make("decodebin")
            .build()
            .unwrap();

        pipeline
            .add_many(
                [
                    &src,
                    &decodebin,
                ]
            )
            .unwrap();
        Element::link_many(
            [
                &src,
                &decodebin,
            ]
        )
        .unwrap();

        let queue = ElementFactory::make("queue")
            .build()
            .unwrap();
        let convert = ElementFactory::make("videoconvert")
            .build()
            .unwrap();
        let scale = ElementFactory::make("videoscale")
            .build()
            .unwrap();

        // let video_info =
        //     VideoInfo::builder(
        //         VideoFormat::Rgbx,
        //         size.x as u32,
        //         size.y as u32,
        //     )
        //     // .fps(Fraction::new(fps as i32, 1))
        //     .build()
        //     .expect("Failed to create video info");
        let appsink = AppSink::builder()
            // .caps(
            //     &video_info
            //         .to_caps()
            //         .unwrap(),
            // )
            .caps(
                &VideoCapsBuilder::new()
                    .format(VideoFormat::Rgbx)
                    .build()
             )
            .build();

        let elements = &[
            &queue,
            &convert,
            &scale,
            appsink
                .upcast_ref(),
        ];
        pipeline
            .add_many(elements)
            .unwrap();

        Element::link_many(elements)
            .unwrap();
        for
            element
        in
            elements
        {
            element
                .sync_state_with_parent()
                .unwrap();
        };

        let sink_pad = queue
            .static_pad("sink")
            .unwrap();
        decodebin
            .connect_pad_added(
                move |
                    _dbin,
                    src_pad,
                | {
                    match
                        src_pad
                            .link(
                                &sink_pad,
                            )
                    {
                        Err(error) => {
                            println!("!! error: {error}");
                        },
                        Ok(_success) => {},
                    }
                }
            );

    // AppSink:
        appsink
            .set_property(
                "drop",
                true,
            );
        appsink
            .set_property(
                "emit-signals",
                true,
            );

        // appsink
        //     .set_callbacks(
        //         AppSinkCallbacks::builder()
        //             .new_sample(|appsink| {
        //                 let sample = appsink
        //                     .pull_sample()
        //                     .unwrap();
        //                 let buffer = sample
        //                     .buffer()
        //                     .unwrap();
        //                 let map = buffer
        //                     .map_readable()
        //                     .unwrap();
        //                 let samples = map.as_slice();
        //                 // vulkano_buffer
        //                 //     .write()
        //                 //     .unwrap()
        //                 //     .copy_from_slice(samples);
        //                 println!("sample: {}", samples.len());
        //                 Ok(FlowSuccess::Ok)
        //             })
        //             // .new_preroll(|_appsink| {
        //             //     println!("new preroll");
        //             //     Ok(FlowSuccess::Ok)
        //             // })
        //             .build()
        //     );

        match
            pipeline
                .set_state(
                    State::Playing
                )
        {
            Err(error) => println!("!! error: {error}"),
            Ok(_) => (),
        };

        Self {
            appsink,
            frame: 0,
            pipeline,

            created: Instant::now(),
            updated: Instant::now(),
        }
    }

    pub fn restart(
        &self,
    ) {
        self
            .pipeline
            .set_state(
                State::Null
            )
            .unwrap(); // !!!
        self
            .pipeline
            .set_state(
                State::Playing
            )
            .unwrap(); // !!!
    }

}
