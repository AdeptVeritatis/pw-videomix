
use crate::impls::midir::{
    MidiConnections,
    MidiNode,
};
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct AppMidi {
    pub midi_node_rc: Rc<RefCell<MidiNode>>,
    pub midi_connection_rc: Rc<RefCell<Option<MidiConnections>>>,
}

impl Default for AppMidi {
    fn default() -> Self {
        Self {
            midi_node_rc: Rc::new(RefCell::new(MidiNode::default())),
            midi_connection_rc: Rc::new(RefCell::new(None)),
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
