
use crate::app::gui::options::GuiOptions;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct AppOptions {
    pub gui: Rc<RefCell<GuiOptions>>,
}

impl Default for AppOptions {
    fn default() -> Self {
        Self {
            gui: Rc::new(
                RefCell::new(
                    GuiOptions::default(),
                ),
            ),
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
