
use std::time::Instant;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct AppTime {
    // Time:
    // Time tracking for node performance measurements.
    pub created: Instant,
    // Time when update of the nodes started. Typically after redraw.
    pub updated: Instant,

    /// Time tracking, useful for frame independent movement.
    // Time when the main window redraw started.
    pub time: Instant,
    dt: f32,
    dt_sum: f32,
    frame_count: f32,
    pub avg_fps: f32,
}

impl Default for AppTime {
    fn default() -> Self {
        Self {
            created: Instant::now(),
            updated: Instant::now(),

            time: Instant::now(),
            dt: 0.0,
            dt_sum: 0.0,
            frame_count: 0.0,
            avg_fps: 0.0,
        }
    }
}

impl AppTime {
    pub fn update(
        &mut self,
    ) {
        // Each second, update average fps & reset frame count & dt sum.
        if
            self
                .dt_sum
            >
            1.0
        {
            self.avg_fps = self.frame_count / self.dt_sum;
            self.frame_count = 0.0;
            self.dt_sum = 0.0;
        }
        self.dt = self
            .time
            .elapsed()
            .as_secs_f32();
        self.dt_sum += self.dt;
        self.frame_count += 1.0;
        self.time = Instant::now();
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
