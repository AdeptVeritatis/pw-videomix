
use egui_winit_vulkano::Gui;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
    sync::Arc,
};
use vulkano::{
    command_buffer::allocator::{
        StandardCommandBufferAllocator,
        StandardCommandBufferAllocatorCreateInfo,
    },
    descriptor_set::allocator::{
        StandardDescriptorSetAllocator,
        StandardDescriptorSetAllocatorCreateInfo,
    },
};
use vulkano::device::DeviceFeatures;
use vulkano_util::{
    context::{
        VulkanoConfig,
        VulkanoContext,
    },
    window::VulkanoWindows,
};
use winit::window::WindowId;

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct AppVulkano {
    pub context: VulkanoContext,

    pub command_buffer_allocator: Arc<StandardCommandBufferAllocator>, // should be in render resources???
    pub descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,

    pub windows: VulkanoWindows,
    pub gui_main: Option<Gui>,
    // Store Gui of sink windows.
    pub map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
    // Store sink_id to corresponding WindowId.
    pub map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
}

impl Default for AppVulkano {
    fn default() -> Self {
        println!(".. starting Vulkan");

        let context: VulkanoContext = VulkanoContext::new(
            VulkanoConfig {
                device_features: DeviceFeatures {
                    descriptor_indexing: true,
                    shader_uniform_buffer_array_non_uniform_indexing: true,
                    runtime_descriptor_array: true,
                    descriptor_binding_variable_descriptor_count: true,
                    ..DeviceFeatures::empty()
                },
                ..Default::default()
            }
        );

        let command_buffer_allocator: Arc<StandardCommandBufferAllocator> = Arc::new(
            StandardCommandBufferAllocator::new(
                context
                    .device()
                    .clone(),
                StandardCommandBufferAllocatorCreateInfo {
                    secondary_buffer_count: 32,
                    ..Default::default()
                },
            ),
        );
        let descriptor_set_allocator: Arc<StandardDescriptorSetAllocator> = Arc::new(
            StandardDescriptorSetAllocator::new(
                context
                    .device()
                    .clone(),
                StandardDescriptorSetAllocatorCreateInfo {
                    ..Default::default()
                },
            ),
        );

        let windows: VulkanoWindows = VulkanoWindows::default();

        let map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>> = Rc::new(
            RefCell::new(
                HashMap::new(),
            ),
        );
        let map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>> = Rc::new(
            RefCell::new(
                HashMap::new(),
            ),
        );

        Self {
            context,
            command_buffer_allocator,
            descriptor_set_allocator,
            windows,
            gui_main: None,
            map_window_to_gui,
            map_window_to_sink,
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
