
use directories::ProjectDirs;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct AppDirectories {
    // Directories:
    pub dir_config: String, //Path,
    pub dir_data: String, //Path,
}

impl Default for AppDirectories {
    fn default() -> Self {
        // Get directories.
        // let (dir_config, dir_data): (Path, Path) = match
        let project_dirs: ProjectDirs = match
            ProjectDirs::from(
                "cc",
                "AdeptVeritatis",
                "pw-videomix",
            )
        {
            None => panic!("!! no project directories"),
            Some(proj_dirs) => proj_dirs,
        };
        // dbg!(dir_config.clone());
        // dbg!(dir_data.clone());
        // Check, if directories exist and create them if not.
        // or only if something is actually saved???

        Self {
            dir_config: project_dirs
                .config_dir()
                .to_str()
                .unwrap()
                .to_string(),
            dir_data: project_dirs
                .data_dir()
                .to_str()
                .unwrap()
                .to_string(),
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
