
use crate::impls::files::TextureChannel;

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct
    AppThreads
{
    pub texture_channel:
        TextureChannel,
}

impl
    Default
for
    AppThreads
{
    fn
        default()
    ->
        Self
    {
        Self {
            texture_channel:
                TextureChannel::default(),
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
