
use crate::nodes::{
    control::ControlNode,
    filter::FilterNode,
    sinks::SinkNode,
    sources::SourceNode,
};
use serde::{
    Deserialize,
    Serialize,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ControlToNode {
    pub map: HashMap<String, ControlNode>,
}

impl Default for ControlToNode {
    fn default() -> Self {
        Self {
            map: HashMap::new(),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct FilterToNode {
    #[serde(skip_deserializing, default = "HashMap::new")]
    pub map: HashMap<String, FilterNode>,
}

impl Default for FilterToNode {
    fn default() -> Self {
        Self {
            map: HashMap::new(),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SinkToNode {
    #[serde(skip, default = "HashMap::new")]
    pub map: HashMap<String, SinkNode>,
}

impl Default for SinkToNode {
    fn default() -> Self {
        Self {
            map: HashMap::new(),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SourceToNode {
    #[serde(skip, default = "HashMap::new")]
    pub map: HashMap<String, SourceNode>,
}

impl Default for SourceToNode {
    fn default() -> Self {
        Self {
            map: HashMap::new(),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct AppNodes {
// Nodes:
    pub map_control_to_node: Rc<RefCell<ControlToNode>>,
    pub map_filter_to_node: Rc<RefCell<FilterToNode>>,
    pub map_sink_to_node: Rc<RefCell<SinkToNode>>,
    pub map_source_to_node: Rc<RefCell<SourceToNode>>,
}

impl Default for AppNodes {
    fn default() -> Self {
        Self {
// Nodes:
            map_control_to_node:
                Rc::new(
                    RefCell::new(
                        ControlToNode::default(),
                    ),
                ),
            map_filter_to_node:
                Rc::new(
                    RefCell::new(
                        FilterToNode::default(),
                    ),
                ),
            map_sink_to_node:
                Rc::new(
                    RefCell::new(
                        SinkToNode::default(),
                    ),
                ),
            map_source_to_node:
                Rc::new(
                    RefCell::new(
                        SourceToNode::default(),
                    ),
                ),
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
