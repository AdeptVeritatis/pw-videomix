
#[cfg(unix)]
use crate::impls::pipewire::{
    PipewireCamera,
    PipewireLoop,
    PipewireStream,
};

use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
#[cfg(unix)]
pub struct AppPipewire {
    pub pipewire_loop: PipewireLoop,
    // Store target cameras and pw_node id.
    // Use id to filter remove events.
    pub map_id_to_camera: Rc<RefCell<HashMap<u32, PipewireCamera>>>,
    // Store pipewire input streams and the node proxy listeners.
    pub map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
}

#[cfg(unix)]
impl Default for AppPipewire {
    fn default() -> Self {
        let
            map_id_to_camera
        :
            Rc<RefCell<HashMap<u32, PipewireCamera>>>
        =
            Rc::new(
                RefCell::new(
                    HashMap::new(),
                ),
            );
        let
            map_node_to_stream
        :
            Rc<RefCell<HashMap<String, PipewireStream>>>
        =
            Rc::new(
                RefCell::new(
                    HashMap::new(),
                ),
            );

        // Create pipewire core, mainloop and listeners.
        let
            pipewire_loop
        :
            PipewireLoop
        =
            PipewireLoop::new(
                // event_loop_proxy.clone(),
                map_id_to_camera
                    .clone(),
            );

        Self {
            pipewire_loop,
            map_id_to_camera,
            map_node_to_stream,
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
