pub mod button_bind;
pub mod elements;
pub mod label_bind;
pub mod midi_bind;

use crate::{
    app::{
        data::app_nodes::ControlToNode,
        gui::bind::midi_bind::{
            MidiBind,
            midi_button,
            midi_controller,
        },
    },
    constants::PANEL_NODE_SPACING_SMALL,
    impls::midir::MidiNode,
    nodes::{
        control::{
            function_generator::FunctionGeneratorNode,
            number::NumberNode,
            trigger::TriggerNode,
            trigonometry::TrigonometryNode,
            ControlNode,
        },
        NodeFamily,
    },
};
use egui_winit_vulkano::egui::{
    Response,
    Ui,
};
use serde::{
    Serialize,
    Deserialize,
};
use std::{
    cell::RefCell,
    fmt,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum BindClass {
    None,
    Button,
    // Controller,
    Value,
}

impl fmt::Display for BindClass {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        match
            *self
        {
            Self::None => write!(f, "none"),
            Self::Button => write!(f, "Button"),
            // Self::Controller => write!(f, "Controller"),
            Self::Value => write!(f, "Value"),
        }
    }
}

// ----------------------------------------------------------------------------

/// Context menu to bind a value to a control node or a MIDI channel.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BindValues {
    // Class of the element behind the label.
    pub class: BindClass,

    // Family of the node, the label belongs to.
    pub node_family: NodeFamily,
    // ID of the node, the label belongs to.
    pub node_id: String,

    // Key to change the value.
    pub key: String,
    // Location of the value.
    pub location: Vec<String>,
}

impl BindValues {
    fn show_context_menu(
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        name: String,
        response: Response,
    ) -> Response {
        response
            .context_menu(
                |ui_context| {
                    ui_context
                        .vertical_centered(
                            |ui_centered| {
                                // ui_centered
                                //     .set_min_width(
                                //         240.0,
                                //     );
                                ui_centered
                                    .add_space(
                                        PANEL_NODE_SPACING_SMALL,
                                    );
                                // ui_centered
                                //     .label(
                                //         format!(
                                //             "Bind {}",
                                //             self
                                //                 .class,
                                //         ),
                                //     );
                                ui_centered
                                    .label(
                                        format!(
                                            "Bind {}",
                                            name,
                                        ),
                                    );
                                // ui_centered
                                //     .label(
                                //         self
                                //             .label
                                //             .clone(),
                                //     );
                                ui_centered
                                    .add_space(
                                        PANEL_NODE_SPACING_SMALL,
                                    );
                                ui_centered
                                    .separator();
                                ui_centered
                                    .add_space(
                                        PANEL_NODE_SPACING_SMALL,
                                    );
                                // Show bind options depending on availability.
                                let midi_node: MidiNode = midi_node_rc
                                    .borrow()
                                    .to_owned();
                                let hide_control = map_control_to_node
                                        .borrow()
                                        .map
                                        .is_empty();
                                match
                                    hide_control
                                {
                                    true => match
                                        midi_node
                                            .enabled
                                    {
                                        true => {
                                            self
                                                .midi(
                                                    midi_node,
                                                    midi_node_rc
                                                        .clone(),
                                                    ui_centered,
                                                );
                                        },
                                        false => {
                                            ui_centered
                                                .label(
                                                    "not available\nno control nodes\nno active MIDI",
                                                );
                                        },
                                    },
                                    false => match
                                        midi_node
                                            .enabled
                                    {
                                        true => {
                                            self
                                                .control(
                                                    map_control_to_node
                                                        .clone(),
                                                    ui_centered,
                                                );
                                            ui_centered
                                                .add_space(
                                                    PANEL_NODE_SPACING_SMALL,
                                                );
                                            ui_centered
                                                .separator();
                                            ui_centered
                                                .add_space(
                                                    PANEL_NODE_SPACING_SMALL,
                                                );
                                            self
                                                .midi(
                                                    midi_node,
                                                    midi_node_rc
                                                        .clone(),
                                                    ui_centered,
                                                );
                                        },
                                        false => {
                                            self
                                                .control(
                                                    map_control_to_node
                                                        .clone(),
                                                    ui_centered,
                                                );
                                            ui_centered
                                                .add_space(
                                                    PANEL_NODE_SPACING_SMALL,
                                                );
                                        },
                                    },
                                };
                            }
                        );
                }
            );

        response
    }
}

impl BindValues {
    fn control(
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        ui: &mut Ui,
    ) {
        let control_to_node = map_control_to_node
            .borrow()
            .to_owned();

        for
            node_id
        in
            control_to_node
                .map
                .keys()
                .clone()
        {
            if

                ui
                    .button(
                        node_id,
                    )
                    .clicked()
            {
                // dbg!(node_id.clone());
                match
                    control_to_node
                        .map
                        .get(
                            node_id,
                        )
                {
                    None => (),
                    Some(ControlNode::None) => (),
                    Some(ControlNode::FunctionGenerator(node)) => {
                        let mut node: FunctionGeneratorNode = node
                            .to_owned();

                        node
                            .connected
                            .push(
                                self
                                    .to_owned()
                            );
                        // dbg!(node.connected.clone());
                        map_control_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                node_id
                                    .to_owned(),
                                ControlNode::FunctionGenerator(
                                    node,
                                ),
                            );
                    },
                    Some(ControlNode::Number(node)) => {
                        let mut node: NumberNode = node
                            .to_owned();

                        node
                            .connected
                            .push(
                                self
                                    .to_owned()
                            );
                        // dbg!(node.connected.clone());
                        map_control_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                node_id
                                    .to_owned(),
                                ControlNode::Number(
                                    node,
                                ),
                            );
                    },
                    Some(ControlNode::Trigger(node)) => {
                        let mut node: TriggerNode = node
                            .to_owned();

                        node
                            .connected
                            .push(
                                self
                                    .to_owned()
                            );
                        // dbg!(node.connected.clone());
                        map_control_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                node_id
                                    .to_owned(),
                                ControlNode::Trigger(
                                    node,
                                ),
                            );
                    },
                    Some(ControlNode::Trigonometry(node)) => {
                        let mut node: TrigonometryNode = node
                            .to_owned();

                        node
                            .connected
                            .push(
                                self
                                    .to_owned()
                            );
                        // dbg!(node.connected.clone());
                        map_control_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                node_id
                                    .to_owned(),
                                ControlNode::Trigonometry(
                                    node,
                                ),
                            );
                    },
                };

                ui
                    .close_menu();
            };
        };

        // let mut target: Option<String> = None;
        // ComboBox::from_id_salt(
        //     format!(
        //         "ComboBox {} select input",
        //         self
        //             .label,
        //     )
        // )
        //     .selected_text(
        //         "select",
        //         // match
        //         //     values
        //         //         .input
        //         //         .input
        //         //         .clone()
        //         // {
        //         //     None => String::from("none"),
        //         //     Some(input) => input,
        //         // }
        //     )
        //     .wrap()
        //     .show_ui(
        //         ui,
        //         |ui_combo| {
        //             for
        //                 node_id
        //             in
        //                 control_nodes
        //                     .keys()
        //                     .clone()
        //             {
        //                 ui_combo
        //                     .selectable_value(
        //                         &mut target,
        //                         Some(
        //                             node_id
        //                                 .to_owned(),
        //                         ),
        //                         node_id
        //                             .to_owned(),
        //                     );
        //             };
        //
        //             match
        //                 target
        //             {
        //                 None => (),
        //                 Some(node_id) => {
        //                     dbg!(node_id.clone());
        //
        //                     match
        //                         control_nodes
        //                             .get(
        //                                 &node_id,
        //                             )
        //                     {
        //                         None => (),
        //                         Some(ControlNode::None) => (),
        //                         Some(ControlNode::Number(node)) => {
        //                             let mut node: NumberNode = node
        //                                 .to_owned();
        //
        //                             node
        //                                 .connected
        //                                 .push(
        //                                     self
        //                                         .to_owned()
        //                                 );
        //
        //                             dbg!(node.connected.clone());
        //
        //                             map_control_to_node
        //                                 .borrow_mut()
        //                                 .insert(
        //                                     node_id,
        //                                     ControlNode::Number(
        //                                         node,
        //                                     ),
        //                                 );
        //                         },
        //                     };
        //
        //                     ui_combo
        //                         .close_menu();
        //                 },
        //             };
        //         },
        //     );
    }

    fn midi(
        &self,
        midi_node: MidiNode,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
        if
            ui
                // .button("Bind MIDI Controller")
                .button(
                    format!(
                        "Bind {} to MIDI",
                        self
                            .class,
                    ),
                )
                .clicked()
        {
            midi_node_rc
                .replace(
                    MidiNode {
                        bind_enable: self
                            .class,
                        bind_value: Some(
                            MidiBind {
                                controller_class: midi_node
                                    .controller_class,
                                bind: self
                                    .to_owned(),
                            }
                        ),
                        ..midi_node
                            .clone()
                    }
                );
            ui
                .close_menu();
        }
        ui
            .add_space(PANEL_NODE_SPACING_SMALL);
        ui
            .separator();
        ui
            .add_space(PANEL_NODE_SPACING_SMALL);
        match
            self
                .class
        {
            BindClass::Button => {
                midi_button(
                    ui,
                );
            },
            BindClass::Value => {
                midi_controller(
                    midi_node,
                    midi_node_rc,
                    ui,
                );
            },
            _ => (),
        };
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
