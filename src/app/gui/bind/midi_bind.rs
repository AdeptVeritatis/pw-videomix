
use crate::{
    app::gui::bind::BindValues,
    constants::PANEL_NODE_SPACING_SMALL,
    impls::midir::{
        // MidiClass,
        MidiControllerClass,
        MidiNode,
    },
};
use egui_winit_vulkano::egui::Ui;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

/// Bind a value to a MIDI channel.
#[derive(Debug, Clone)]
pub struct MidiBind {
    pub controller_class: MidiControllerClass,
    pub bind: BindValues,
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn midi_button(
    ui: &mut Ui,
) {
    ui
        // .vertical(|ui_list| {
        //     ui_list
                .small("1. Enable MIDI\n2. Connect to a MIDI device.\n3. Click the button \"Bind MIDI Button\".\n4. Press a button on the MIDI device.");
        // });
    ui
        .add_space(PANEL_NODE_SPACING_SMALL);
}

pub fn midi_controller(
    midi_node: MidiNode,
    midi_node_rc: Rc<RefCell<MidiNode>>,
    ui: &mut Ui,
) {
    ui
        // .vertical(|ui_list| {
        //     ui_list
                .small("1. Enable MIDI\n2. Connect to a MIDI device.\n3. Select the controller mode.\n4. Click the button \"Bind Value to MIDI\".\n5. Turn a knob on the MIDI device or move a slider.");
        // });
    ui
        .add_space(PANEL_NODE_SPACING_SMALL);
    ui
        .separator();
    ui
        .add_space(PANEL_NODE_SPACING_SMALL);
    ui
        .label("Controller Mode:");
    ui
        .menu_button(
            midi_node
                .controller_class
                .to_string(),
            |ui_knob| {
                // Not a nice solution!!!
                if
                    ui_knob
                        .button(format!("{}", MidiControllerClass::Absolute))
                        .clicked()
                {
                    midi_node_rc
                        .replace(
                            MidiNode {
                                controller_class: MidiControllerClass::Absolute,
                                ..midi_node.clone()
                            }
                        );
                }
                if
                    ui_knob
                        .button(format!("{}", MidiControllerClass::RelativeOffset0))
                        .clicked()
                {
                    midi_node_rc
                        .replace(
                            MidiNode {
                                controller_class: MidiControllerClass::RelativeOffset0,
                                ..midi_node.clone()
                            }
                        );
                }
                if
                    ui_knob
                        .button(format!("{}", MidiControllerClass::RelativeOffset16))
                        .clicked()
                {
                    midi_node_rc
                        .replace(
                            MidiNode {
                                controller_class: MidiControllerClass::RelativeOffset16,
                                ..midi_node.clone()
                            }
                        );
                }
                if
                    ui_knob
                        .button(format!("{}", MidiControllerClass::RelativeOffset64))
                        .clicked()
                {
                    midi_node_rc
                        .replace(
                            MidiNode {
                                controller_class: MidiControllerClass::RelativeOffset64,
                                ..midi_node.clone()
                            }
                        );
                }
            }
        );
}
