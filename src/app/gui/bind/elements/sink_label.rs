
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        gui::bind::{
            label_bind::LabelBind,
            BindClass,
            BindValues,
        },
    },
    constants::{
        SNAPSHOT_COUNTER_RANGE,
        SNAPSHOT_DECIMALS,
        SNAPSHOT_FACTOR,
        SNAPSHOT_FRAME_RANGE,
        SNAPSHOT_QTY_RANGE,
        SNAPSHOT_TIMESPAN_OFFSET,
        SNAPSHOT_TIMESPAN_RANGE,
        SNAPSHOT_SPEED,
    },
    impls::midir::{
        value_classes::{
            float_range_offset_range,
            natural_step_multistep,
        },
        MidiControllerClass,
        MidiMessage,
        MidiNode,
    },
    nodes::NodeFamily,
};
use egui_winit_vulkano::egui::{
    DragValue,
    Ui,
};
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct SinkSequenceDuration {}

impl SinkSequenceDuration {
    pub fn show(
        mut sequence_duration: u32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> u32 {
        LabelBind{
            label: String::from("every msec:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Sink,
                node_id,
                key: String::from("sequence_duration"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut sequence_duration,
                )
                    .fixed_decimals(
                        SNAPSHOT_DECIMALS,
                    )
                    .range(
                        SNAPSHOT_TIMESPAN_RANGE,
                    ) // up to 10 min
                    .speed(
                        SNAPSHOT_SPEED,
                    ),
            );

        sequence_duration
    }

    pub fn change_midi(
        sequence_duration: u32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> u32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            SNAPSHOT_TIMESPAN_OFFSET as f32,
            speed,
            sequence_duration as f32,
            SNAPSHOT_TIMESPAN_RANGE,
            1.0,
        ) as u32
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct SinkCounter {}

impl SinkCounter {
    pub fn show(
        mut counter: u32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> u32 {
        LabelBind{
            label: String::from("counter:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Sink,
                node_id,
                key: String::from("counter"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut counter,
                )
                    .fixed_decimals(
                        SNAPSHOT_DECIMALS,
                    )
                    .range(
                        SNAPSHOT_COUNTER_RANGE,
                    )
                    .speed(
                        SNAPSHOT_SPEED,
                    ),
            );

        counter
    }

    pub fn change_midi(
        counter: u32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> u32 {
        natural_step_multistep(
            controller_class,
            SNAPSHOT_FACTOR,
            midi_event,
            speed,
            counter,
            SNAPSHOT_COUNTER_RANGE,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct SinkSequenceNum {}

impl SinkSequenceNum {
    pub fn show(
        mut sequence_num: u32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> u32 {
        LabelBind{
            label: String::from("qty:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Sink,
                node_id,
                key: String::from("sequence_num"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut sequence_num,
                )
                    .fixed_decimals(
                        SNAPSHOT_DECIMALS,
                    )
                    .range(
                        SNAPSHOT_QTY_RANGE,
                    )
                    .speed(
                        SNAPSHOT_SPEED,
                    ),
            );

        sequence_num
    }

    pub fn change_midi(
        sequence_num: u32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> u32 {
        natural_step_multistep(
            controller_class,
            SNAPSHOT_FACTOR,
            midi_event,
            speed,
            sequence_num,
            SNAPSHOT_QTY_RANGE,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct SinkSequenceSelect {}

impl SinkSequenceSelect {
    pub fn show(
        mut sequence_select: u32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> u32 {
        LabelBind{
            label: String::from("nth frame:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Sink,
                node_id,
                key: String::from("sequence_select"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut sequence_select,
                )
                    .fixed_decimals(
                        SNAPSHOT_DECIMALS,
                    )
                    .range(
                        SNAPSHOT_FRAME_RANGE,
                    ) // up to 10 min @ 60 fps
                    .speed(
                        SNAPSHOT_SPEED,
                    ),
            );

        sequence_select
    }

    pub fn change_midi(
        sequence_select: u32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> u32 {
        natural_step_multistep(
            controller_class,
            SNAPSHOT_FACTOR,
            midi_event,
            speed,
            sequence_select,
            SNAPSHOT_FRAME_RANGE,
        )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
