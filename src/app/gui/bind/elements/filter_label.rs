// for angle values like tex or mesh rotation
//  10*1 + 5*3 + 5*5 + 5*10 + 5*20 + 1*50
//* 10   + 15  + 25  + 50   + 100  + 50
//∑ 10     25    50    100    200    250
//# 10     15    20    25     30     31

use crate::{
    app::{
        data::app_nodes::ControlToNode,
        gui::bind::{
            label_bind::LabelBind,
            BindClass,
            BindValues,
        },
    },
    constants::{
        // combine most variables to FILTER_...!!!
        COLOR_MIXER_DECIMALS,
        COLOR_MIXER_OFFSET,
        COLOR_MIXER_RANGE,
        COLOR_MIXER_SPEED,
        COLOR_ROTATOR_ANGLE_DECIMALS,
        COLOR_ROTATOR_ANGLE_OFFSET,
        COLOR_ROTATOR_ANGLE_RANGE,
        COLOR_ROTATOR_ANGLE_SPEED,
        COLOR_ROTATOR_BOUND_DECIMALS,
        COLOR_ROTATOR_BOUND_HIGH_OFFSET,
        COLOR_ROTATOR_BOUND_LOW_OFFSET,
        COLOR_ROTATOR_BOUND_RANGE,
        COLOR_ROTATOR_BOUND_SPEED,
        COLOR_ROTATOR_GAIN_DECIMALS,
        COLOR_ROTATOR_GAIN_OFFSET,
        COLOR_ROTATOR_GAIN_RANGE,
        COLOR_ROTATOR_GAIN_SPEED,
        COLOR_ROTATOR_SPEED_DECIMALS,
        COLOR_ROTATOR_SPEED_RANGE,
        COLOR_ROTATOR_SPEED_SPEED,
        FADER_TIME_OFFSET,
        FADER_TIME_RANGE,
        FADER_TIME_SPEED,
        FILTER_MOVE_DECIMALS,
        FILTER_MOVE_OFFSET_MESH,
        FILTER_MOVE_RANGE_MESH,
        FILTER_MOVE_SPEED,
        KALEIDOSCOPE_ANGLE_DECIMALS,
        KALEIDOSCOPE_ANGLE_RANGE,
        KALEIDOSCOPE_ANGLE_SPEED,
        KALEIDOSCOPE_AXIS_DECIMALS,
        KALEIDOSCOPE_AXIS_RANGE,
        KALEIDOSCOPE_AXIS_SPEED,
        KALEIDOSCOPE_COLOR_INACTIVE,
        MANDALA_ANGLE_DECIMALS,
        MANDALA_ANGLE_OFFSET,
        MANDALA_ANGLE_RANGE,
        MANDALA_ANGLE_SPEED,
        MANDALA_REPETITIONS_DECIMALS,
        MANDALA_REPETITIONS_RANGE,
        MANDALA_REPETITIONS_SPEED,
        MANDALA_ROTATION_SPEED_DECIMALS,
        MANDALA_ROTATION_SPEED_RANGE,
        MANDALA_ROTATION_SPEED_SPEED,
        MANDALA_SIZE_DECIMALS,
        MANDALA_SIZE_OFFSET,
        MANDALA_SIZE_RANGE,
        MANDALA_SIZE_SPEED,
        NODE_LAYER_CENTER_DECIMALS,
        NODE_LAYER_CENTER_OFFSET,
        NODE_LAYER_CENTER_RANGE,
        NODE_LAYER_CENTER_SPEED,
        NODE_LAYER_MIX_DECIMALS,
        NODE_LAYER_MIX_RANGE,
        NODE_LAYER_MIX_SPEED,
        NODE_LAYER_SCALE_DECIMALS,
        NODE_LAYER_SCALE_OFFSET,
        NODE_LAYER_SCALE_RANGE,
        NODE_LAYER_SCALE_SPEED,
        PANEL_NODE_GRID_WIDTH_MIN,
    },
    impls::midir::{
        value_classes::{
            float_range_offset_range,
            float_speed_0_speed,
            natural_step_range,
        },
        MidiControllerClass,
        MidiMessage,
        MidiNode,
    },
    nodes::{
        filter::kaleidoscope::{
            KaleidoscopeLattice,
            KaleidoscopeSymmetry,
        },
        NodeFamily,
    },
};
use egui_winit_vulkano::egui::{
    Color32,
    DragValue,
    RichText,
    Slider,
    Ui,
};
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterAngle {}

impl FilterAngle {
    pub fn show(
        mut angle: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label:String::from("angle:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("angle"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .vertical(
                |ui_width| {
                    ui_width
                        .set_min_width(
                            PANEL_NODE_GRID_WIDTH_MIN,
                        );
                    ui_width
                        .add(
                            DragValue::new(
                                &mut angle,
                            )
                                .fixed_decimals(
                                    COLOR_ROTATOR_ANGLE_DECIMALS,
                                )
                                .range(
                                    COLOR_ROTATOR_ANGLE_RANGE,
                                )
                                .speed(
                                    COLOR_ROTATOR_ANGLE_SPEED,
                                ),
                        );
                },
             );
        ui
            .add(
                Slider::new(
                    &mut angle,
                    -1.0..=1.0,
                )
                    .show_value(
                        false,
                    ),
            );

        angle
    }

    pub fn change_midi(
        angle: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_ROTATOR_ANGLE_OFFSET,
            speed,
            angle,
            COLOR_ROTATOR_ANGLE_RANGE,
            COLOR_ROTATOR_ANGLE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterBoundHigh {}

impl FilterBoundHigh {
    pub fn show(
        mut bound_high: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("upper:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("bound_high"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut bound_high,
                )
                    .fixed_decimals(
                        COLOR_ROTATOR_BOUND_DECIMALS,
                    )
                    .range(
                        COLOR_ROTATOR_BOUND_RANGE,
                    )
                    .speed(
                        COLOR_ROTATOR_BOUND_SPEED,
                    ),
            );

        bound_high
    }

    pub fn change_midi(
        bound_high: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_ROTATOR_BOUND_HIGH_OFFSET,
            speed,
            bound_high,
            COLOR_ROTATOR_BOUND_RANGE,
            COLOR_ROTATOR_BOUND_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterBoundLow {}

impl FilterBoundLow {
    pub fn show(
        mut bound_low: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("lower:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("bound_low"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut bound_low,
                )
                    .fixed_decimals(
                        COLOR_ROTATOR_BOUND_DECIMALS,
                    )
                    .range(
                        COLOR_ROTATOR_BOUND_RANGE,
                    )
                    .speed(
                        COLOR_ROTATOR_BOUND_SPEED,
                    ),
            );

        bound_low
    }

    pub fn change_midi(
        bound_low: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_ROTATOR_BOUND_LOW_OFFSET,
            speed,
            bound_low,
            COLOR_ROTATOR_BOUND_RANGE,
            COLOR_ROTATOR_BOUND_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterCenterX {}

impl FilterCenterX {
    pub fn show(
        mut center_x: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        source_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("move h:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("center_x"),
                location: vec!(
                    String::from("values"),
                    String::from("sources"),
                    source_id,
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut center_x
                )
                    .fixed_decimals(
                        NODE_LAYER_CENTER_DECIMALS,
                    )
                    .range(
                        NODE_LAYER_CENTER_RANGE,
                    )
                    .speed(
                        NODE_LAYER_CENTER_SPEED,
                    ),
            );

        center_x
    }

    pub fn change_midi(
        center_x: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            NODE_LAYER_CENTER_OFFSET,
            speed,
            center_x,
            NODE_LAYER_CENTER_RANGE,
            NODE_LAYER_CENTER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterCenterY {}

impl FilterCenterY {
    pub fn show(
        mut center_y: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        source_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("move v:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("center_y"),
                location: vec!(
                    String::from("values"),
                    String::from("sources"),
                    source_id,
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut center_y,
                )
                    .fixed_decimals(
                        NODE_LAYER_CENTER_DECIMALS,
                    )
                    .range(
                        NODE_LAYER_CENTER_RANGE,
                    )
                    .speed(
                        NODE_LAYER_CENTER_SPEED,
                    ),
            );

        center_y
    }

    pub fn change_midi(
        center_y: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            NODE_LAYER_CENTER_OFFSET,
            speed,
            center_y,
            NODE_LAYER_CENTER_RANGE,
            NODE_LAYER_CENTER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterGainR {}

impl FilterGainR {
    pub fn show(
        mut gain_r: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("gain R:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("gain_r"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut gain_r,
                )
                    .fixed_decimals(
                        COLOR_ROTATOR_GAIN_DECIMALS,
                    )
                    .range(
                        COLOR_ROTATOR_GAIN_RANGE,
                    )
                    .speed(
                        COLOR_ROTATOR_GAIN_SPEED,
                    ),
            );

        gain_r
    }

    pub fn change_midi(
        gain_r: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_ROTATOR_GAIN_OFFSET,
            speed,
            gain_r,
            COLOR_ROTATOR_GAIN_RANGE,
            COLOR_ROTATOR_GAIN_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterGainG {}

impl FilterGainG {
    pub fn show(
        mut gain_g: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("gain G:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("gain_g"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut gain_g,
                )
                    .fixed_decimals(
                        COLOR_ROTATOR_GAIN_DECIMALS,
                    )
                    .range(
                        COLOR_ROTATOR_GAIN_RANGE,
                    )
                    .speed(
                        COLOR_ROTATOR_GAIN_SPEED,
                    ),
            );

        gain_g
    }

    pub fn change_midi(
        gain_g: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_ROTATOR_GAIN_OFFSET,
            speed,
            gain_g,
            COLOR_ROTATOR_GAIN_RANGE,
            COLOR_ROTATOR_GAIN_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterGainB {}

impl FilterGainB {
    pub fn show(
        mut gain_b: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("gain B:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("gain_b"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut gain_b,
                )
                    .fixed_decimals(
                        COLOR_ROTATOR_GAIN_DECIMALS,
                    )
                    .range(
                        COLOR_ROTATOR_GAIN_RANGE,
                    )
                    .speed(
                        COLOR_ROTATOR_GAIN_SPEED,
                    ),
            );

        gain_b
    }

    pub fn change_midi(
        gain_b: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_ROTATOR_GAIN_OFFSET,
            speed,
            gain_b,
            COLOR_ROTATOR_GAIN_RANGE,
            COLOR_ROTATOR_GAIN_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMeshPositionX {}

impl FilterMeshPositionX {
    pub fn show(
        mut mesh_position_x: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("move h:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mesh_position_x"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut mesh_position_x,
                )
                    .fixed_decimals(
                        FILTER_MOVE_DECIMALS,
                    )
                    .range(
                        FILTER_MOVE_RANGE_MESH,
                    )
                    .speed(
                        FILTER_MOVE_SPEED,
                    ),
            );

        mesh_position_x
    }

    pub fn change_midi(
        mesh_position_x: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            FILTER_MOVE_OFFSET_MESH,
            speed,
            mesh_position_x,
            FILTER_MOVE_RANGE_MESH,
            FILTER_MOVE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMeshPositionY {}

impl FilterMeshPositionY {
    pub fn show(
        mut mesh_position_y: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("move v:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mesh_position_y"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut mesh_position_y,
                )
                    .fixed_decimals(
                        FILTER_MOVE_DECIMALS,
                    )
                    .range(
                        FILTER_MOVE_RANGE_MESH,
                    )
                    .speed(
                        FILTER_MOVE_SPEED,
                    ),
            );

        mesh_position_y
    }

    pub fn change_midi(
        mesh_position_y: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            FILTER_MOVE_OFFSET_MESH,
            speed,
            mesh_position_y,
            FILTER_MOVE_RANGE_MESH,
            FILTER_MOVE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMeshRotate {}

impl FilterMeshRotate {
    pub fn show(
        mut mesh_rotate: bool,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> bool {
        LabelBind{
            label: String::from("rotate:"),
            color: None,
            bind: BindValues {
                class: BindClass::Button,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mesh_rotate"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .checkbox(
                &mut mesh_rotate,
                "",
            );

        mesh_rotate
    }

    pub fn change_midi(
        mesh_rotate: bool,
    ) -> bool {
        !mesh_rotate
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMeshRotationSpeed {}

impl FilterMeshRotationSpeed {
    pub fn show(
        mut mesh_rotation_speed: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("speed:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mesh_rotation_speed"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut mesh_rotation_speed,
                )
                    .fixed_decimals(
                        MANDALA_ROTATION_SPEED_DECIMALS,
                    )
                    .range(
                        MANDALA_ROTATION_SPEED_RANGE,
                    )
                    .speed(
                        MANDALA_ROTATION_SPEED_SPEED,
                    ),
            );

        mesh_rotation_speed
    }

    pub fn change_midi(
        mesh_rotation_speed: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_speed_0_speed(
            controller_class,
            midi_event,
            speed,
            mesh_rotation_speed,
            MANDALA_ROTATION_SPEED_RANGE,
            MANDALA_ROTATION_SPEED_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMeshSize {}

impl FilterMeshSize {
    pub fn show(
        mut mesh_size: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("size:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mesh_size"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut mesh_size,
                )
                    .fixed_decimals(
                        MANDALA_SIZE_DECIMALS,
                    )
                    .range(
                        MANDALA_SIZE_RANGE,
                    )
                    .speed(
                        MANDALA_SIZE_SPEED,
                    ),
            );

        mesh_size
    }

    pub fn change_midi(
        mesh_size: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            MANDALA_SIZE_OFFSET,
            speed,
            mesh_size,
            MANDALA_SIZE_RANGE,
            MANDALA_SIZE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixFactor {
}

impl FilterMixFactor {
    pub fn show(
        mut mix_factor: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        source_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("mix:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_factor"),
                location: vec!(
                    String::from("values"),
                    String::from("sources"),
                    source_id,
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut mix_factor,
                )
                    .fixed_decimals(
                        NODE_LAYER_MIX_DECIMALS,
                    )
                    .range(
                        NODE_LAYER_MIX_RANGE,
                    )
                    .speed(
                           NODE_LAYER_MIX_SPEED,
                    ),
            );

        mix_factor
    }

    pub fn change_midi(
        mix_factor: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            0.5, // !!!
            speed,
            mix_factor,
            NODE_LAYER_MIX_RANGE,
            NODE_LAYER_MIX_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixRR {}

impl FilterMixRR {
    pub fn show(
        mut mix_rr: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("R <- R"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_rr"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_rr
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_rr
    }

    pub fn change_midi(
        mix_rr: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_rr,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixRG {}

impl FilterMixRG {
    pub fn show(
        mut mix_rg: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("R <- G"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_rg"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_rg
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_rg
    }

    pub fn change_midi(
        mix_rg: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_rg,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixRB {}

impl FilterMixRB {
    pub fn show(
        mut mix_rb: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("R <- B"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_rb"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_rb
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_rb
    }

    pub fn change_midi(
        mix_rb: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_rb,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixGR {}

impl FilterMixGR {
    pub fn show(
        mut mix_gr: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("G <- R"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_gr"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_gr
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_gr
    }

    pub fn change_midi(
        mix_gr: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_gr,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixGG {}

impl FilterMixGG {
    pub fn show(
        mut mix_gg: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("G <- G"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_gg"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_gg
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_gg
    }

    pub fn change_midi(
        mix_gg: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_gg,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixGB {}

impl FilterMixGB {
    pub fn show(
        mut mix_gb: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("G <- B"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_gb"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_gb
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_gb
    }

    pub fn change_midi(
        mix_gb: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_gb,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixBR {}

impl FilterMixBR {
    pub fn show(
        mut mix_br: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("B <- R"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_br"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_br
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_br
    }

    pub fn change_midi(
        mix_br: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_br,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixBG {}

impl FilterMixBG {
    pub fn show(
        mut mix_bg: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("B <- G"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_bg"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_bg
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_bg
    }

    pub fn change_midi(
        mix_bg: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_bg,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterMixBB {}

impl FilterMixBB {
    pub fn show(
        mut mix_bb: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("B <- B"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("mix_bb"),
                location: vec!(
                    String::from("values"),
                    String::from("mixer_matrix"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut mix_bb
                )
                    .fixed_decimals(
                        COLOR_MIXER_DECIMALS,
                    )
                    .range(
                        COLOR_MIXER_RANGE,
                    )
                    .speed(
                        COLOR_MIXER_SPEED,
                    ),
            );

        mix_bb
    }

    pub fn change_midi(
        mix_bb: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            COLOR_MIXER_OFFSET,
            speed,
            mix_bb,
            COLOR_MIXER_RANGE,
            COLOR_MIXER_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterRepetitions {}

impl FilterRepetitions {
    pub fn show(
        mut repetitions: u32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> u32 {
        LabelBind{
            label: String::from("fold:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("repetitions"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut repetitions,
                )
                    .fixed_decimals(
                        MANDALA_REPETITIONS_DECIMALS,
                    )
                    .range(
                        MANDALA_REPETITIONS_RANGE,
                    )
                    .speed(
                        MANDALA_REPETITIONS_SPEED,
                    ),
            );

        repetitions
    }

    pub fn change_midi(
        repetitions: u32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> u32 {
        natural_step_range(
            controller_class,
            midi_event,
            speed,
            repetitions,
            MANDALA_REPETITIONS_RANGE,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterRotSpeed {}

impl FilterRotSpeed {
    pub fn show(
        mut rot_speed: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("speed:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("rot_speed"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut rot_speed,
                )
                    .fixed_decimals(
                        COLOR_ROTATOR_SPEED_DECIMALS,
                    )
                    .range(
                        COLOR_ROTATOR_SPEED_RANGE,
                    )
                    .speed(
                        COLOR_ROTATOR_SPEED_SPEED,
                    ),
            );

        rot_speed
    }

    pub fn change_midi(
        rot_speed: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_speed_0_speed(
            controller_class,
            midi_event,
            // COLOR_ROTATOR_SPEED_OFFSET,
            speed,
            rot_speed,
            COLOR_ROTATOR_SPEED_RANGE,
            COLOR_ROTATOR_SPEED_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterScaleFactor {}

impl FilterScaleFactor {
    pub fn show(
        mut scale_factor: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        source_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("size:"), // "scale" ???
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("scale_factor"),
                location: vec!(
                    String::from("values"),
                    String::from("sources"),
                    source_id,
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut scale_factor,
                )
                    .fixed_decimals(
                        NODE_LAYER_SCALE_DECIMALS,
                    )
                    .range(
                        NODE_LAYER_SCALE_RANGE,
                    )
                    .speed(
                        NODE_LAYER_SCALE_SPEED,
                    ),
            );

        scale_factor
    }

    pub fn change_midi(
        scale_factor: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            NODE_LAYER_SCALE_OFFSET,
            speed,
            scale_factor,
            NODE_LAYER_SCALE_RANGE,
            NODE_LAYER_SCALE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexAngle {}

impl FilterTexAngle {
    pub fn show(
        mut tex_angle: f32,
        tex_lattice: KaleidoscopeLattice,
        tex_symmetry: KaleidoscopeSymmetry,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        let color: Option<Color32> = match
            tex_lattice
        {
            KaleidoscopeLattice::Hexagonal => Some(
                KALEIDOSCOPE_COLOR_INACTIVE,
            ),
            KaleidoscopeLattice::Oblique => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
            KaleidoscopeLattice::Rectangular => Some(
                KALEIDOSCOPE_COLOR_INACTIVE,
            ),
            KaleidoscopeLattice::Rhombic => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                |
                KaleidoscopeSymmetry::Cm // h & v
                |
                KaleidoscopeSymmetry::Pgg
                |
                KaleidoscopeSymmetry::Cmm
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
            KaleidoscopeLattice::Square => Some(
                KALEIDOSCOPE_COLOR_INACTIVE,
            ),
        };
        LabelBind{
            label: String::from("angle:"),
            color,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_angle"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );

        // not a good solution???
        match
            color
        {
            None => {
                ui
                    .label(
                        "active",
                    );
            },
            Some(color) => {
                ui
                    .label(
                        RichText::new(
                            "inactive",
                        )
                            .color(
                                color,
                            ),
                    );
            },
        };
        ui
            .add(
                DragValue::new(
                    &mut tex_angle,
                )
                    .fixed_decimals(
                        KALEIDOSCOPE_ANGLE_DECIMALS,
                    )
                    .range(
                        KALEIDOSCOPE_ANGLE_RANGE,
                    )
                    .speed(
                        KALEIDOSCOPE_ANGLE_SPEED,
                    ),
            );

        tex_angle
    }

    pub fn change_midi(
        tex_angle: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            (KALEIDOSCOPE_ANGLE_RANGE.start() + KALEIDOSCOPE_ANGLE_RANGE.end()) / 2.0,
            speed,
            tex_angle,
            KALEIDOSCOPE_ANGLE_RANGE,
            KALEIDOSCOPE_ANGLE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexAxisA {}

impl FilterTexAxisA {
    pub fn show(
        mut tex_axis_a: f32,
        tex_lattice: KaleidoscopeLattice,
        tex_symmetry: KaleidoscopeSymmetry,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        let color: Option<Color32> = match
            tex_lattice
        {
            KaleidoscopeLattice::Square => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                |
                KaleidoscopeSymmetry::Pgg
                |
                KaleidoscopeSymmetry::Pmm
                |
                KaleidoscopeSymmetry::Cmm
                |
                KaleidoscopeSymmetry::P4
                |
                KaleidoscopeSymmetry::P4g
                |
                KaleidoscopeSymmetry::P4m
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
            KaleidoscopeLattice::Oblique => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
            KaleidoscopeLattice::Hexagonal => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                |
                KaleidoscopeSymmetry::Cmm
                |
                KaleidoscopeSymmetry::P3
                |
                KaleidoscopeSymmetry::P3m1
                |
                KaleidoscopeSymmetry::P31m
                |
                KaleidoscopeSymmetry::P6
                |
                KaleidoscopeSymmetry::P6m
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
            KaleidoscopeLattice::Rectangular => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                |
                KaleidoscopeSymmetry::Pg // h & v
                // | KaleidoscopeSymmetry::Pgm
                |
                KaleidoscopeSymmetry::Pmg
                |
                KaleidoscopeSymmetry::Pm // h & v
                |
                KaleidoscopeSymmetry::Pmm
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
            KaleidoscopeLattice::Rhombic => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                |
                KaleidoscopeSymmetry::Cm // h & v
                |
                KaleidoscopeSymmetry::Pgg
                |
                KaleidoscopeSymmetry::Cmm
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
        };
        LabelBind{
            label: String::from("size a:"),
            color,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_axis_a"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );

        // not a good solution???
        match
            color
        {
            None => {
                ui
                    .label(
                        "active",
                    );
            },
            Some(color) => {
                ui
                    .label(
                        RichText::new("inactive")
                            .color(
                                color,
                            ),
                    );
            },
        };
        ui
            .add(
                DragValue::new(
                    &mut tex_axis_a,
                )
                    .fixed_decimals(
                        KALEIDOSCOPE_AXIS_DECIMALS,
                    )
                    .range(
                        KALEIDOSCOPE_AXIS_RANGE,
                    )
                    .speed(
                        KALEIDOSCOPE_AXIS_SPEED,
                    ),
            );

        tex_axis_a
    }

    pub fn change_midi(
        tex_axis_a: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            KALEIDOSCOPE_AXIS_RANGE.end() / 2.0,
            speed,
            tex_axis_a,
            KALEIDOSCOPE_AXIS_RANGE,
            KALEIDOSCOPE_AXIS_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexAxisB {}

impl FilterTexAxisB {
    pub fn show(
        mut tex_axis_b: f32,
        tex_lattice: KaleidoscopeLattice,
        tex_symmetry: KaleidoscopeSymmetry,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        let color: Option<Color32> = match
            tex_lattice
        {
            KaleidoscopeLattice::Square => Some(
                KALEIDOSCOPE_COLOR_INACTIVE,
            ),
            KaleidoscopeLattice::Oblique => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
            KaleidoscopeLattice::Hexagonal => Some(
                KALEIDOSCOPE_COLOR_INACTIVE,
            ),
            KaleidoscopeLattice::Rectangular => match
                tex_symmetry
            {
                KaleidoscopeSymmetry::P1
                |
                KaleidoscopeSymmetry::P2
                |
                KaleidoscopeSymmetry::Pg // h & v
                // | KaleidoscopeSymmetry::Pgm
                |
                KaleidoscopeSymmetry::Pmg
                |
                KaleidoscopeSymmetry::Pm // h & v
                |
                KaleidoscopeSymmetry::Pmm
                => None,
                _
                => Some(
                    KALEIDOSCOPE_COLOR_INACTIVE,
                ),
            },
            KaleidoscopeLattice::Rhombic => Some(
                KALEIDOSCOPE_COLOR_INACTIVE,
            ),
        };
        LabelBind{
            label: String::from("size b:"),
            color,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_axis_b"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        // not a good solution???
        match
            color
        {
            None => {
                ui
                    .label(
                        "active",
                    );
            },
            Some(color) => {
                ui
                    .label(
                        RichText::new(
                            "inactive",
                        )
                            .color(
                                color,
                            ),
                    );
            },
        };
        ui
            .add(
                DragValue::new(
                    &mut tex_axis_b,
                )
                    .fixed_decimals(
                        KALEIDOSCOPE_AXIS_DECIMALS,
                    )
                    .range(
                        KALEIDOSCOPE_AXIS_RANGE,
                    )
                    .speed(
                        KALEIDOSCOPE_AXIS_SPEED,
                    ),
            );

        tex_axis_b
    }

    pub fn change_midi(
        tex_axis_b: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            KALEIDOSCOPE_AXIS_RANGE.end() / 2.0,
            speed,
            tex_axis_b,
            KALEIDOSCOPE_AXIS_RANGE,
            KALEIDOSCOPE_AXIS_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexPositionX {}

impl FilterTexPositionX {
    pub fn show(
        mut tex_position_x: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("move h:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_position_x"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut tex_position_x,
                )
                    .fixed_decimals(
                        FILTER_MOVE_DECIMALS,
                    )
                    .range(
                        FILTER_MOVE_RANGE_MESH,
                    )
                    .speed(
                        FILTER_MOVE_SPEED,
                    ),
            );

        tex_position_x
    }

    pub fn change_midi(
        tex_position_x: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            FILTER_MOVE_OFFSET_MESH,
            speed,
            tex_position_x,
            FILTER_MOVE_RANGE_MESH,
            FILTER_MOVE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexPositionY {}

impl FilterTexPositionY {
    pub fn show(
        mut tex_position_y: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("move v:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_position_y"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut tex_position_y,
                )
                    .fixed_decimals(
                        FILTER_MOVE_DECIMALS,
                    )
                    .range(
                        FILTER_MOVE_RANGE_MESH,
                    )
                    .speed(
                        FILTER_MOVE_SPEED,
                    ),
            );

        tex_position_y
    }

    pub fn change_midi(
        tex_position_y: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            FILTER_MOVE_OFFSET_MESH,
            speed,
            tex_position_y,
            FILTER_MOVE_RANGE_MESH,
            FILTER_MOVE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexRotate {}

impl FilterTexRotate {
    pub fn show(
        mut tex_rotate: bool,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> bool {
        LabelBind{
            label: String::from("rotate:"),
            color: None,
            bind: BindValues {
                class: BindClass::Button,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_rotate"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .checkbox(
                &mut tex_rotate,
                "",
            );

        tex_rotate
    }

    pub fn change_midi(
        tex_rotate: bool,
    ) -> bool {
        !tex_rotate
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexRotationPreset {}

impl FilterTexRotationPreset {
    pub fn show(
        mut tex_rotation_preset: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("angle:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_rotation_preset"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        // Change value when rotating??? No, because it is already part of shape???
        ui
            .add(
                DragValue::new(
                    &mut tex_rotation_preset,
                )
                    .fixed_decimals(
                        MANDALA_ANGLE_DECIMALS,
                    )
                    .range(
                        MANDALA_ANGLE_RANGE,
                    )
                    .speed(
                        MANDALA_ANGLE_SPEED,
                    ),
            );
        // ui.label("°");

        tex_rotation_preset
    }

    pub fn change_midi(
        tex_rotation_preset: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            MANDALA_ANGLE_OFFSET,
            speed,
            tex_rotation_preset,
            MANDALA_ANGLE_RANGE,
            MANDALA_ANGLE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexRotationSpeed {}

impl FilterTexRotationSpeed {
    pub fn show(
        mut tex_rotation_speed: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("speed:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_rotation_speed"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut tex_rotation_speed,
                )
                    .fixed_decimals(
                        MANDALA_ROTATION_SPEED_DECIMALS,
                    )
                    .range(
                        MANDALA_ROTATION_SPEED_RANGE,
                    )
                    .speed(
                        MANDALA_ROTATION_SPEED_SPEED,
                    ),
            );

        tex_rotation_speed
    }

    pub fn change_midi(
        tex_rotation_speed: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_speed_0_speed(
            controller_class,
            midi_event,
            speed,
            tex_rotation_speed,
            MANDALA_ROTATION_SPEED_RANGE,
            MANDALA_ROTATION_SPEED_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTexSize {}

impl FilterTexSize {
    pub fn show(
        mut tex_size: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("size:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("tex_size"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut tex_size,
                )
                    .fixed_decimals(
                        MANDALA_SIZE_DECIMALS,
                    )
                    .range(
                        MANDALA_SIZE_RANGE,
                    )
                    .speed(
                        MANDALA_SIZE_SPEED,
                    ),
            );

        tex_size
    }

    pub fn change_midi(
        tex_size: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            MANDALA_SIZE_OFFSET,
            speed,
            tex_size,
            MANDALA_SIZE_RANGE,
            MANDALA_SIZE_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTimeHold {}

impl FilterTimeHold {
    pub fn show(
        mut time_hold: u32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> u32 {
        LabelBind{
            label: String::from("hold:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("time_hold"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut time_hold,
                )
                    .range(
                        FADER_TIME_RANGE,
                    )
                    .speed(
                        FADER_TIME_SPEED,
                    )
                    .custom_formatter(
                        |n, _| {
                            // broken???
                            let milis = n % 1000.0;
                            let n = n as i32 / 1000;
                            let mins = (n / 60) % 60;
                            let secs = n % 60;
                            format!("{mins:02}:{secs:02}.{milis:03}")
                        }
                    ),
            );
        ui
            .label(
                "m:s.ms",
            );

        time_hold
    }

    pub fn change_midi(
        time_hold: u32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> u32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            FADER_TIME_OFFSET,
            speed,
            time_hold as f32,
            FADER_TIME_RANGE,
            FADER_TIME_SPEED,
        ) as u32
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct FilterTimeMix {}

impl FilterTimeMix {
    pub fn show(
        mut time_mix: u32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> u32 {
        LabelBind{
            label: String::from("mix:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Filter,
                node_id,
                key: String::from("time_mix"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
        );
        ui
            .add(
                DragValue::new(
                    &mut time_mix,
                )
                    .range(
                        FADER_TIME_RANGE,
                    )
                    .speed(
                        FADER_TIME_SPEED,
                    )
                    .custom_formatter(
                        |n, _| {
                            let milis = n % 1000.0;
                            let n = n as i32 / 1000;
                            let mins = (n / 60) % 60;
                            let secs = n % 60;
                            format!("{mins:02}:{secs:02}.{milis:03}")
                        },
                    )
            );
        ui
            .label(
                "m:s.ms",
            );

        time_mix
    }

    pub fn change_midi(
        time_mix: u32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> u32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            FADER_TIME_OFFSET,
            speed,
            time_mix as f32,
            FADER_TIME_RANGE,
            FADER_TIME_SPEED,
        ) as u32
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
