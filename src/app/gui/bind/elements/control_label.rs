
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        gui::bind::{
            label_bind::LabelBind,
            BindClass,
            BindValues,
        },
    },
    constants::{
        TRIGGER_PRECISION_DECIMALS,
        TRIGGER_PRECISION_RANGE,
        TRIGGER_PRECISION_SPEED,
    },
    impls::midir::{
        value_classes::{
            float_range_offset_range,
            natural_step_range,
        },
        MidiControllerClass,
        MidiMessage,
        MidiNode,
    },
    nodes::NodeFamily,
};
use egui_winit_vulkano::egui::{
    DragValue,
    Ui,
};
use std::{
    cell::RefCell,
    ops::RangeInclusive,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct ControlPrecision {}

impl ControlPrecision {
    pub fn show(
        mut precision: u32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> u32 {
        LabelBind{
            label: String::from("precision:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Control,
                node_id,
                key: String::from("precision"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut precision,
                )
                    .fixed_decimals(
                        TRIGGER_PRECISION_DECIMALS,
                    )
                    .range(
                        TRIGGER_PRECISION_RANGE,
                    )
                    .speed(
                        TRIGGER_PRECISION_SPEED,
                    ),
            );

        precision
    }

    pub fn change_midi(
        precision: u32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> u32 {
        natural_step_range(
            controller_class,
            midi_event,
            speed,
            precision,
            TRIGGER_PRECISION_RANGE,
        )
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct ControlValue {}

impl ControlValue {
    pub fn show(
        mut value: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("value:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Control,
                node_id,
                key: String::from("value"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );
        ui
            .add(
                DragValue::new(
                    &mut value,
                )
                    .fixed_decimals(
                        3,
                    )
                    .range(
                        RangeInclusive::new(
                            -10000.0,
                            10000.0,
                        ),
                    )
                    .speed(
                        0.01,
                    ),
            );

        value
    }

    pub fn change_midi(
        value: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            0.0, // offset
            speed,
            value,
            RangeInclusive::new(
                -6.4, // -1000.0,
                6.3, // 1000.0,
            ),
            0.01,
        )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
