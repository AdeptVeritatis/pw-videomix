
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        gui::bind::{
            label_bind::LabelBind,
            BindClass,
            BindValues,
        },
    },
    constants::{
        NODE_FPS_RANGE,
        NODE_FPS_SPEED,
        STACK_FPS_OFFSET,
    },
    impls::midir::{
        value_classes::float_range_offset_range,
        MidiControllerClass,
        MidiMessage,
        MidiNode,
    },
    nodes::{
        NodeFamily,
        draw_node_fps,
    },
};
use egui_winit_vulkano::egui::Ui;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct SourceFPS {}

impl SourceFPS {
    pub fn show(
        fps: f32,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> f32 {
        LabelBind{
            label: String::from("fps:"),
            color: None,
            bind: BindValues {
                class: BindClass::Value,
                node_family: NodeFamily::Source,
                node_id,
                key: String::from("fps"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                ui,
            );

        draw_node_fps(
            fps,
            ui,
        )
    }

    pub fn change_midi(
        fps: f32,
        controller_class: MidiControllerClass,
        midi_event: MidiMessage,
        speed: f32,
    ) -> f32 {
        float_range_offset_range(
            controller_class,
            midi_event,
            STACK_FPS_OFFSET,
            speed,
            fps,
            NODE_FPS_RANGE,
            NODE_FPS_SPEED,
        )
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
