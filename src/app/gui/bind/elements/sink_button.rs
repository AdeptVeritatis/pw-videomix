
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        gui::bind::{
            button_bind::ButtonBind,
            BindClass,
            BindValues,
        },
    },
    impls::midir::{
        // MidiClass,
        // MidiControllerClass,
        MidiNode,
    },
    nodes::NodeFamily,
};
use egui_winit_vulkano::egui::{
    Button,
    Response,
    Sense,
    Ui,
};
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// not a LabelBind but a ButtonBind!!!

#[derive(Debug, Clone)]
pub struct SinkRecordVideo {}

impl SinkRecordVideo {
    pub fn show(
        label: String,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        node_id: String,
        ui: &mut Ui,
    ) -> Response {
        let response: Response = ui
            .add(
                Button::new(
                    label
                        .clone(),
                )
                    .sense(
                        Sense::click(),
                    ),
            );
        ButtonBind{
            label: label
                .clone(),
            name: String::from("record video"),
            bind: BindValues {
                class: BindClass::Button,
                node_family: NodeFamily::Sink,
                node_id,
                key: String::from("record_video"),
                location: vec!(
                    String::from("values"),
                ),
            },
        }
            .show(
                map_control_to_node,
                midi_node_rc,
                response,
            )
    }

    // pub fn change_midi(
    //
    //     controller_class: MidiControllerClass,
    //     midi_event: MidiMessage,
    //     speed: f32,
    // ) -> f32 {
    // }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
