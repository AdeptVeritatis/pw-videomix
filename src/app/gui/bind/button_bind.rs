
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        gui::bind::BindValues,
    },
    impls::midir::MidiNode,
};
use egui_winit_vulkano::egui::Response;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

/// Button with a context menu to bind to a control node or a MIDI channel.
#[derive(Debug, Clone)]
pub struct ButtonBind {
    // Text shown on the button.
    pub label: String,
    // Name shown in the bind context menu.
    pub name: String,
    // // Color of the text.
    // pub color: Option<Color32>,

    pub bind: BindValues,
}

impl ButtonBind {
    pub fn show(
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        response: Response,
    ) -> Response {
        self
            .bind
            .show_context_menu(
                map_control_to_node,
                midi_node_rc,
                self
                    .name
                    .clone(),
                response,
            )
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
