
use crate::{
    app::{
        data::app_nodes::ControlToNode,
        gui::bind::BindValues,
    },
    impls::midir::MidiNode,
};
use egui_winit_vulkano::egui::{
    Color32,
    Label,
    RichText,
    Sense,
    Ui,
};
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

/// Label with a context menu to bind a value to a control node or a MIDI channel.
#[derive(Debug, Clone)]
pub struct LabelBind {
    // Text shown as the label.
    pub label: String,
    // Color of the label.
    pub color: Option<Color32>,

    pub bind: BindValues,
}

impl LabelBind {
    pub fn show(
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        ui: &mut Ui,
    ) {
        let response = ui
            .add(
                match
                    self
                        .color
                {
                    None => {
                        Label::new(
                            self
                                .label
                                .clone(),
                        )
                    },
                    Some(color) => {
                        Label::new(
                            RichText::new(
                                self
                                    .label
                                    .clone(),
                            )
                                .color(
                                    color,
                                ),
                        )
                    },
                }
                    .sense(
                        Sense::click(),
                    ),
            );
        self
            .bind
            .show_context_menu(
                map_control_to_node,
                midi_node_rc,
                self
                    .label
                    .clone(),
                response,
            );
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
