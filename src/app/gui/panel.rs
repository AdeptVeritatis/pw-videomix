pub mod panel_add;
pub mod panel_node;

use egui_winit_vulkano::egui::{
    RichText,
    Ui,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn sized_text(
    ui: &mut Ui,
    text: impl Into<String>,
    size: f32,
) {
    ui
        .label(
            RichText::new(
                text
            )
            .size(
                size
            ),
        );
}
