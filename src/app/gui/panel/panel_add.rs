
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            panel::sized_text,
            options::GuiOptions,
        },
    },
    constants::{
        PANEL_ADD_SPACING_BUTTONS,
        PANEL_ADD_SPACING_TITLE,
        PANEL_ADD_WIDTH,
        PANEL_NODE_SPACING_SMALL,
        TEXT_CATEGORY_SIZE,
    },
    impls::files::{
        TextureMessage,
        open_file_dialog,
    },
    nodes::{
        control::ControlNode,
        filter::FilterNode,
        sinks::SinkNode,
        sources::SourceNode,
    },
};
#[cfg(unix)]
use crate::impls::pipewire::PipewireStream;
use egui_winit_vulkano::{
    egui::{
        containers::panel::Side,
        Align,
        Context,
        Id,
        Layout,
        ScrollArea,
        SidePanel,
    },
    RenderResources,
};
#[cfg(unix)]
use pipewire::core::Core;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
    sync::mpsc,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn create_panel_add(
    context: Context,
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    map_control_to_node: Rc<RefCell<ControlToNode>>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    #[cfg(unix)]
    map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
    #[cfg(unix)]
    pw_core: Core,
    resources: RenderResources,
    sender_texture: mpsc::Sender<TextureMessage>,
) {
    SidePanel::new(
        Side::Left,
        Id::new("Add")
    )
    .default_width(PANEL_ADD_WIDTH)
    .show(&context, move |ui_panel| {
        ScrollArea::vertical()
            .show(ui_panel, move |ui_scroll| {
                ui_scroll.vertical_centered_justified(|ui_inner| {
                    ui_inner.add_space(PANEL_ADD_SPACING_TITLE);
                    ui_inner.label("add new");
            // Input:
                    ui_inner.add_space(PANEL_ADD_SPACING_TITLE);
                    ui_inner.separator();
                    sized_text(ui_inner, "Input:", TEXT_CATEGORY_SIZE);
                    ui_inner.separator();
                    if ui_inner.button("File(s)").clicked() {
                        open_file_dialog(
                            Rc::clone(&map_source_to_node),
                            resources.clone(),
                            sender_texture.clone(),
                        );
                    };
                    // #[cfg(feature = "pipewire")]???
                    #[cfg(unix)]
                    if ui_inner
                        .button("PipeWire")
                        .clicked()
                    {
                        SourceNode::pw_input(
                            event_loop_proxy.clone(),
                            Rc::clone(&map_node_to_stream),
                            Rc::clone(&map_source_to_node),
                            pw_core.clone(),
                            resources.clone(),
                        );
                    };
                    // #[cfg(feature = "gstreamer")]
                    // if ui_inner.button("PipeWire (gst)").clicked() {
                    //     SourceNode::pw_input_gst(
                    //         Rc::clone(&map_source_to_node),
                    //         // resources.clone(),
                    //     );
                    // };
                    // if ui_inner.button("Screencast").clicked() {};
            // Filter:
                    ui_inner.add_space(PANEL_ADD_SPACING_BUTTONS);
                    ui_inner.separator();
                    sized_text(ui_inner, "Filter:", TEXT_CATEGORY_SIZE);
                    ui_inner.separator();
                // Mixer:
                    if ui_inner.button("Fader").clicked() {
                        FilterNode::fader(
                            Rc::clone(&map_filter_to_node),
                            resources.clone(),
                        );
                    };
                    if ui_inner.button("Mixer").clicked() {
                        FilterNode::mixer(
                            Rc::clone(&map_filter_to_node),
                            resources.clone(),
                        );
                    };
                // Tiling:
                    // nearly the same but prefer small to not be the same with next space
                    // ui_inner.add_space(PANEL_ADD_SPACING_BUTTONS);
                    ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    if ui_inner.button("Kaleidoscope").clicked() {
                        FilterNode::kaleidoscope(
                            Rc::clone(&map_filter_to_node),
                            resources.clone(),
                        );
                    };
                    if ui_inner.button("Mandala").clicked() {
                        FilterNode::mandala(
                            Rc::clone(&map_filter_to_node),
                            resources.clone(),
                        );
                    };
                // Colors:
                    ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    if ui_inner.button("Color Mixer").clicked() {
                        FilterNode::color_mixer(
                            Rc::clone(&map_filter_to_node),
                            resources.clone(),
                        );
                    };
                    if ui_inner.button("Color Rotator").clicked() {
                        FilterNode::color_rotator(
                            Rc::clone(&map_filter_to_node),
                            resources.clone(),
                        );
                    };
            // Output:
                    ui_inner.add_space(PANEL_ADD_SPACING_BUTTONS);
                    ui_inner.separator();
                    sized_text(ui_inner, "Output:", TEXT_CATEGORY_SIZE);
                    ui_inner
                        .separator();
                    if ui_inner.button("Monitor").clicked() {
                        SinkNode::monitor(
                            Rc::clone(&map_sink_to_node),
                            resources.clone(),
                        );
                        // MonitorNode::new(Rc::clone(&map_monitor_to_node));
                    };
                    if ui_inner.button("Window").clicked() {
                        // Create new video output window:
                        // Send switch to event_loop:
                        event_loop_proxy.send_event(GuiEvent::WindowCreate).ok();
                        // event_create_window_rc.replace(true);
                    };

                    ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    if ui_inner
                        .button("Picture File")
                        // .button("Snapshot")
                        .clicked()
                    {
                        SinkNode::snapshot(
                            Rc::clone(&map_sink_to_node),
                            // resources,
                        );
                    };
                    #[cfg(feature = "gstreamer")]
                    if
                        ui_inner
                            .button("Video File")
                            // .button("Encoder")
                            .clicked()
                    {
                        // Create new video output file:
                        SinkNode::encoder(
                            Rc::clone(&map_sink_to_node),
                            // resources.clone(),
                        );
                    };

                    #[cfg(unix)]
                    ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    #[cfg(unix)]
                    if ui_inner
                        .button("PipeWire")
                        .clicked()
                    {
                        SinkNode::pw_output(
                            event_loop_proxy.clone(),
                            Rc::clone(&map_node_to_stream),
                            Rc::clone(&map_sink_to_node),
                            pw_core.clone(),
                            resources.clone(),
                        );
                    };
                    // #[cfg(feature = "gstreamer")]
                    // if
                    //     ui_inner
                    //         .button("PipeWire (gst)")
                    //         .clicked()
                    // {
                    //     // Create new PipeWire output source:
                    //     SinkNode::pw_output_gst(
                    //         Rc::clone(&map_sink_to_node),
                    //         // resources.clone(),
                    //     );
                    // };
            // Control:
                    ui_inner.add_space(PANEL_ADD_SPACING_BUTTONS);
                    ui_inner.separator();
                    sized_text(ui_inner, "Control:", TEXT_CATEGORY_SIZE);
                    ui_inner.separator();
                // FunctionGenerator:
                    if ui_inner.button("Function Generator").clicked() {
                        ControlNode::function_generator(
                            map_control_to_node
                                .clone(),
                            resources
                                .clone(),
                        );
                    };
                // Number:
                    if ui_inner.button("Number").clicked() {
                        ControlNode::number(
                            map_control_to_node
                                .clone(),
                            resources
                                .clone(),
                        );
                    };
                // Trigger:
                    if ui_inner.button("Trigger").clicked() {
                        ControlNode::trigger(
                            map_control_to_node
                                .clone(),
                            resources
                                .clone(),
                        );
                    };
                // Trigonometry:
                    if ui_inner.button("Trigonometry").clicked() {
                        ControlNode::trigonometry(
                            map_control_to_node
                                .clone(),
                            resources
                                .clone(),
                        );
                    };

            // Close add panel:
                    ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    ui_inner.separator();
                    ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    ui_inner.with_layout(Layout::right_to_left(Align::Min), |ui_align| {
                        if ui_align.button("close panel").clicked() {
                            let options: GuiOptions = options_gui.borrow().to_owned();
                            options_gui.replace(
                                GuiOptions {
                                    panel_add_show: false,
                                    ..options
                                }
                            );
                        }
                    });
                });
            });
    });
}
