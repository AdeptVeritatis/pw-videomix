
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::{
            gui_events_node::{
                NodeEvent,
                NodeEventClass,
            },
            GuiEvent,
        },
        gui::{
            panel::sized_text,
            options::GuiOptions,
        },
    },
    constants::{
        PANEL_NODE_SPACING_SMALL,
        PANEL_NODE_SPACING_TITLE,
        TEXT_CATEGORY_SIZE,
    },
    impls::midir::MidiNode,
    nodes::{
        control::ControlNode,
        filter::FilterNode,
        sinks::SinkNode,
        sources::SourceNode,
        NodeFamily,
    },
};
#[cfg(unix)]
use crate::impls::pipewire::PipewireCamera;
use egui_winit_vulkano::egui::{
    Align,
    Context,
    Id,
    Layout,
    ScrollArea,
    SidePanel,
    Ui,
};
#[cfg(feature = "gstreamer")]
use egui_winit_vulkano::RenderResources;

use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn draw_node_panel_header(
    node_class: String,
    node_name: String,
    ui: &mut Ui,
) {
    ui
        .add_space(
            PANEL_NODE_SPACING_SMALL,
        );
    sized_text(
        ui,
        node_name,
        TEXT_CATEGORY_SIZE,
    );
    ui
        .add_space(
            PANEL_NODE_SPACING_SMALL,
        );
    ui
        .label(
            node_class,
        );
    ui
        .add_space(
            PANEL_NODE_SPACING_SMALL,
        );
    ui
        .separator();
}

// ----------------------------------------------------------------------------

pub fn create_panel_node(
    context: Context,
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    map_control_to_node: Rc<RefCell<ControlToNode>>,
    #[cfg(unix)]
    map_id_to_camera: Rc<RefCell<HashMap<u32, PipewireCamera>>>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    midi_node_rc: Rc<RefCell<MidiNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
    #[cfg(feature = "gstreamer")]
    // #[cfg(unix)]
    resources: RenderResources,
) {
    let selected_node: String = match
        options_gui
            .borrow()
            .selected_node
            .clone()
    {
        None => String::new(),
        Some(node) => node,
    };
    SidePanel::new(
        options_gui.borrow().panel_node_side.to_owned(),
        Id::new("NodeValues"),
    )
    .show(&context, move |ui_panel| {
        ScrollArea::vertical()
            .show(ui_panel, move |ui_scroll| {
                ui_scroll.vertical_centered_justified(|ui_inner| {
                    ui_inner.add_space(PANEL_NODE_SPACING_TITLE);
                    ui_inner.label("set node values");
                    ui_inner.add_space(PANEL_NODE_SPACING_TITLE);
                    ui_inner.separator();
                // Node:
                    // ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    // sized_text(ui_inner, selected_node.clone(), TEXT_CATEGORY_SIZE);
                    // ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                // Values - control:
                    // use function!!!
                    let control_to_node: ControlToNode = map_control_to_node.borrow().to_owned();
                    let control_node: ControlNode = match
                        control_to_node
                            .map
                            .get(
                                &selected_node,
                            )
                    {
                        None => ControlNode::None,
                        Some(control_node) => {
                            match
                                control_node
                                    .to_owned()
                            {
                                ControlNode::None => ControlNode::None,
                                ControlNode::FunctionGenerator(mut node) => {
                                    draw_node_panel_header(
                                        control_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    ControlNode::FunctionGenerator(
                                        node,
                                    )
                                },
                                ControlNode::Number(mut node) => {
                                    draw_node_panel_header(
                                        control_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    ControlNode::Number(
                                        node,
                                    )
                                },
                                ControlNode::Trigger(mut node) => {
                                    draw_node_panel_header(
                                        control_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    ControlNode::Trigger(
                                        node,
                                    )
                                },
                                ControlNode::Trigonometry(mut node) => {
                                    draw_node_panel_header(
                                        control_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            // midi_node_rc
                                            //     .clone(),
                                            ui_inner,
                                        );
                                    ControlNode::Trigonometry(
                                        node,
                                    )
                                },
                            }
                        },
                    };
                    match
                        control_node
                    {
                        ControlNode::None => (),
                        control_node => {
                            map_control_to_node
                                .borrow_mut()
                                .map
                                .insert(
                                    selected_node.clone(),
                                    control_node,
                                );
                        },
                    };
                // Values - filter:
                    // use function!!!
                    let hashmap_filter_to_node = map_filter_to_node.borrow().map.to_owned();
                    let filter_node: FilterNode = match
                        hashmap_filter_to_node
                            .get(&selected_node)
                    {
                        None => FilterNode::None,
                        Some(filter_node) => {
                            match
                                filter_node
                                    .to_owned()
                            {
                                FilterNode::None => FilterNode::None,
                                FilterNode::ColorMixer(mut node) => {
                                    draw_node_panel_header(
                                        filter_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    FilterNode::ColorMixer(
                                        node,
                                    )
                                },
                                FilterNode::ColorRotator(mut node) => {
                                    draw_node_panel_header(
                                        filter_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    FilterNode::ColorRotator(
                                        node,
                                    )
                                },
                                FilterNode::Fader(mut node) => {
                                    draw_node_panel_header(
                                        filter_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    FilterNode::Fader(
                                        node,
                                    )
                                },
                                FilterNode::Kaleidoscope(mut node) => {
                                    draw_node_panel_header(
                                        filter_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    FilterNode::Kaleidoscope(
                                        node,
                                    )
                                },
                                FilterNode::Mandala(mut node) => {
                                    draw_node_panel_header(
                                        filter_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    FilterNode::Mandala(
                                        node,
                                    )
                                },
                                FilterNode::Mixer(mut node) => {
                                    draw_node_panel_header(
                                        filter_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    FilterNode::Mixer(
                                        node,
                                    )
                                },
                            }
                        },
                    };
                    match
                        filter_node
                    {
                        FilterNode::None => (),
                        filter_node => {
                            map_filter_to_node
                                .borrow_mut()
                                .map
                                .insert(
                                    selected_node.clone(),
                                    filter_node,
                                );
                        },
                    };
                // Values - sinks:
                    // use function!!!
                    let hashmap_sinks_to_node = map_sink_to_node.borrow().map.to_owned();
                    let sink_node: SinkNode = match
                        hashmap_sinks_to_node
                            .get(&selected_node)
                    {
                        None => SinkNode::None,
                        Some(sink_node) => {
                            match
                                sink_node
                                    .to_owned()
                            {
                                SinkNode::None => SinkNode::None,
                                #[cfg(feature = "gstreamer")]
                                // #[cfg(unix)]
                                SinkNode::Encoder(mut node) => {
                                    draw_node_panel_header(
                                        sink_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            resources,
                                            ui_inner,
                                        );
                                    SinkNode::Encoder(
                                        node,
                                    )
                                },
                                SinkNode::Snapshot(mut node) => {
                                    draw_node_panel_header(
                                        sink_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    SinkNode::Snapshot(
                                        node,
                                    )
                                },
                                sink_node => sink_node,
                            }
                        },
                    };
                    match
                        sink_node
                    {
                        SinkNode::None => (),
                        sink_node => {
                            map_sink_to_node
                                .borrow_mut()
                                .map
                                .insert(
                                    selected_node
                                        .clone(),
                                    sink_node,
                                );
                        },
                    };
                // Values - sources:
                    // use function!!!
                    let hashmap_source_to_node = map_source_to_node.borrow().map.to_owned();
                    let source_node: SourceNode = match
                        hashmap_source_to_node
                            .get(&selected_node)
                    {
                        None => SourceNode::None,
                        Some(source_node) => {
                            match
                                source_node
                                    .to_owned()
                            {
                                SourceNode::None => SourceNode::None,
                                SourceNode::Picture(mut node) => {
                                    draw_node_panel_header(
                                        source_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            ui_inner,
                                        );
                                    SourceNode::Picture(
                                        node,
                                    )
                                },
                                #[cfg(unix)]
                                SourceNode::PipewireInput(mut node) => {
                                    draw_node_panel_header(
                                        source_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_id_to_camera
                                                .clone(),
                                            ui_inner,
                                        );
                                    SourceNode::PipewireInput(
                                        node,
                                    )
                                },
                                SourceNode::Stack(mut node) => {
                                    draw_node_panel_header(
                                        source_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            map_control_to_node
                                                .clone(),
                                            midi_node_rc
                                                .clone(),
                                            ui_inner,
                                        );
                                    SourceNode::Stack(
                                        node,
                                    )
                                },
                                #[cfg(feature = "gstreamer")]
                                SourceNode::Video(mut node) => {
                                    draw_node_panel_header(
                                        source_node
                                            .to_string(),
                                        node
                                            .name
                                            .clone(),
                                        ui_inner,
                                    );
                                    node
                                        .draw_panel(
                                            // midi_node_rc.clone(),
                                            ui_inner,
                                        );
                                    SourceNode::Video(
                                        node,
                                    )
                                },
                            }
                        },
                    };
                    match
                        source_node
                    {
                        SourceNode::None => (),
                        source_node => {
                            map_source_to_node
                                .borrow_mut()
                                .map
                                .insert(
                                    selected_node,
                                    source_node,
                                );
                        },
                    }
                // Close side panel:
                    // Widgets should use their own space.
                    // ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    ui_inner.separator();
                    ui_inner.add_space(PANEL_NODE_SPACING_SMALL);
                    ui_inner.with_layout(Layout::right_to_left(Align::Min), |ui_align| {
                        if ui_align.button("close panel").clicked() {
                            event_loop_proxy
                                .send_event(
                                    GuiEvent::ChangeNode(
                                        NodeEvent {
                                            id: String::new(),
                                            event_class: NodeEventClass::Deselect,
                                            family: NodeFamily::None,
                                        }
                                    )
                                )
                                .ok();
                        }
                    });
                });
            });
    });
}
