
use crate::impls::midir::MidiControllerSpeedClass;
use egui_winit_vulkano::egui::{
    containers::panel::{
        Side,
        // TopBottomPanel,
    },
    // Context,
    Ui,
    Visuals,
};
use std::{
    cell::RefCell,
    path::PathBuf,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct ChangeNodeName {
    pub node_id: String,
    pub node_name: String,
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct GuiOptions {
    // Rename nodes.
    pub change_name: Option<ChangeNodeName>,

    // Debug output in the background of the main panel.
    pub debug_output: bool,

    // Set size of node header.
    pub node_header_full: bool,

    // Set visibility of add panel.
    pub panel_add_show: bool,

    // Set visibility of hint to click node for panel.
    pub panel_node_hint: bool,

    // Set side of node panel.
    pub panel_node_side: Side,

    // Set visibility of node panel.
    pub panel_node_show: bool,

    // Set path to load/save project files.
    pub project_path_load: Option<PathBuf>,
    pub project_path_save: Option<PathBuf>,

    // Selected node.
    pub selected_node: Option<String>,

    // Fast speed of controller knobs.
    pub speed_class: MidiControllerSpeedClass,
    pub speed_fast: bool,

    // Set dark / light theme mode.
    pub visuals: Visuals,

}

impl Default for GuiOptions {
    fn default() -> Self {
        Self {
            change_name:
                None,
            debug_output:
                false,
            node_header_full:
                true,
            panel_add_show:
                true,
            panel_node_hint:
                true,
            panel_node_side:
                Side::Right,
            panel_node_show:
                false,
            project_path_load:
                None,
            project_path_save:
                None,
            selected_node:
                None,
            speed_class:
                MidiControllerSpeedClass::Push,
            speed_fast:
                false,
            visuals:
                Visuals::light(),
        }
    }
}

impl GuiOptions {
    /// Set visibility of debug output.
    pub fn debug_output(
        self,
        ui: &mut Ui,
    ) -> Self {
        let mut debug_output: bool = self.debug_output;

        let (button_debug, new_debug_output): (String, bool) = match
            debug_output
        {
            true => (String::from("Debug Output - hide"), false),
            false => (String::from("Debug Output - show"), true),
        };
        if ui.button(button_debug).clicked() {
            debug_output = new_debug_output;
            ui.close_menu();
        };
        Self {
            debug_output,
            ..self
        }
    }

    /// Set size of node header.
    pub fn node_header_full(
        self,
        ui: &mut Ui,
    ) -> Self {
        let mut full: bool = self.node_header_full;

        let (button_panel, new_full): (String, bool) = match
            full
        {
            true => (String::from("Header - small"), false),
            false => (String::from("Header - large"), true),
        };
        if ui.button(button_panel).clicked() {
            full = new_full;
            ui.close_menu();
        };
        Self {
            node_header_full: full,
            ..self
        }
    }

    /// Set visibility of add panel.
    pub fn panel_add_show(
        self,
        ui: &mut Ui,
    ) -> Self {
        let mut show: bool = self.panel_add_show;

        let (button_panel, new_show): (String, bool) = match
            show
        {
            true => (String::from("Add Panel - close"), false),
            false => (String::from("Add Panel - show"), true),
        };
        if ui.button(button_panel).clicked() {
            show = new_show;
            ui.close_menu();
        };
        Self {
            panel_add_show: show,
            ..self
        }
    }

    /// Set visibility of hint to click node for panel.
    pub fn panel_node_hint(
        self,
        ui: &mut Ui,
    ) -> Self {
        let mut hint: bool = self.panel_node_hint;

        let (button_panel, new_hint): (String, bool) = match
            hint
        {
            true => (String::from("Click Hint - hide"), false),
            false => (String::from("Click Hint - show"), true),
        };
        if ui.button(button_panel).clicked() {
            hint = new_hint;
            ui.close_menu();
        };
        Self {
            panel_node_hint: hint,
            ..self
        }
    }

    /// Set side of node panel.
    pub fn panel_node_side(
        self,
        ui: &mut Ui,
    ) -> Self {
        let mut side: Side = self.panel_node_side;

        let (button_panel, new_side): (String, Side) = match
            side
        {
            Side::Right => (String::from("Node Panel - left"), Side::Left),
            Side::Left => (String::from("Node Panel - right"), Side::Right),
        };
        if ui.button(button_panel).clicked() {
            side = new_side;
            ui.close_menu();
        };
        Self {
            panel_node_side: side,
            ..self
        }
    }

    /// Set visibility of node panel.
    pub fn panel_node_show(
        self,
        options_gui: Rc<RefCell<Self>>,
        ui: &mut Ui,
    ) -> Self {
        let mut show: bool = self.panel_node_show;

        let (button_panel, new_show): (String, bool) = match
            show
        {
            true => (String::from("Node Panel - close"), false),
            false => (String::from("Node Panel - show"), true), // should never be shown, but is!!!
        };
        if ui.button(button_panel).clicked() {
            if !new_show {
                options_gui
                    .borrow_mut()
                    .selected_node
                    .take();
            }
            show = new_show;
            ui.close_menu();
        };
        Self {
            panel_node_show: show,
            ..self
        }
    }

    /// Set dark / light theme mode.
    pub fn visuals(
        self,
        ui: &mut Ui,
    ) -> Self {
        let mut visuals: Visuals = self.visuals;

        let (button_text, new_visuals): (String, Visuals) = match
            visuals.dark_mode
        {
            true => (String::from("Style - light mode"), Visuals::light()),
            false => (String::from("Style - dark mode"), Visuals::dark()),
        };
        if ui.button(button_text).clicked() {
            visuals = new_visuals;
            ui.close_menu();
        };
        Self {
            visuals,
            ..self
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
