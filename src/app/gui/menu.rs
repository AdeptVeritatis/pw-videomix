
use crate::{
    app::{
        events::{
            gui_events_midi::{
                MidiEvent,
                MidiEventClass,
            },
            gui_events_storage::{
                StorageEvent,
                StorageEventClass,
            },
            GuiEvent,
        },
        gui::{
            bind::{
                midi_bind::MidiBind,
                BindClass,
                BindValues,
            },
            options::GuiOptions,
        },
    },
    constants::{
        MENU_SPACING,
        PANEL_NODE_SPACING_SMALL,
        PROJECT_DIRECTORY,
    },
    impls::midir::{
        MidiControllerSpeedClass,
        MidiNode,
    },
    nodes::NodeFamily,
};
use egui_winit_vulkano::egui::{
    containers::panel::TopBottomPanel,
    Color32,
    Context,
    RichText,
    Ui,
};
use midir::{
    Ignore,
    MidiInput,
    MidiInputPort,
};
use rfd::FileDialog;
use std::{
    cell::RefCell,
    rc::Rc,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn draw_menu(
    context_egui: Context,
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    midi_node_rc: Rc<RefCell<MidiNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
) {
    TopBottomPanel::top(
        "Menu",
    )
        .show(
            &context_egui,
            move |ui| {
                ui
                    .horizontal(
                        |ui_menu| {
                            draw_menu_settings(
                                options_gui
                                    .clone(),
                                ui_menu,
                            );
                            draw_menu_project(
                                event_loop_proxy
                                    .clone(),
                                options_gui
                                    .clone(),
                                ui_menu,
                            );
                            draw_menu_midi(
                                event_loop_proxy,
                                midi_node_rc,
                                options_gui,
                                ui_menu,

                            );
                        }
                    );
            },
        );
}

pub fn draw_menu_settings(
    options_gui: Rc<RefCell<GuiOptions>>,
    ui: &mut Ui,
) {
    ui.menu_button("Settings", |ui_settings| {
        ui_settings.menu_button("App", |ui_style| {
            let mut gui_options: GuiOptions = options_gui.borrow().to_owned();

            // Dark / light mode:
            gui_options = gui_options.visuals(ui_style);

            ui_style.separator();

            // Visibility of debug output:
            gui_options = gui_options.debug_output(ui_style);

            options_gui.replace(gui_options);
        });
        ui_settings.menu_button("Panels", |ui_style| {
            let mut gui_options: GuiOptions = options_gui.borrow().to_owned();

            // Visibility of add panel:
            gui_options = gui_options.panel_add_show(ui_style);

            ui_style.separator();

            // Visibility of node panel:
            if options_gui.borrow().selected_node.is_some() {
                gui_options = gui_options
                    .panel_node_show(
                        Rc::clone(&options_gui),
                        ui_style,
                    );

            }

            // ui_style.separator();

            // Node panel side:
            gui_options = gui_options
                .panel_node_side(ui_style);

            options_gui.replace(gui_options);
        });
        ui_settings.menu_button("Nodes", |ui_style| {
            let mut gui_options: GuiOptions = options_gui.borrow().to_owned();

            // Size of node header:
            gui_options = gui_options
                .node_header_full(ui_style);

            ui_style.separator();

            // Visibility of hint to click node for panel:
            gui_options = gui_options
                .panel_node_hint(ui_style);

            options_gui.replace(gui_options);
        });
        // if ui_settings.button("Close menu").clicked() {
        //     ui_settings.close_menu();
        // };
    });
}

pub fn
    draw_menu_project
(
    event_loop_proxy:
        EventLoopProxy<GuiEvent>,
    options_gui: Rc<RefCell<GuiOptions>>,
    ui:
        &mut Ui,
) {
    ui
        .menu_button(
            "Project",
            // "Session",
            |ui_storage| {
                ui_storage
                    .vertical_centered_justified(
                        |ui_centered| {
                    // Load:
                            ui_centered
                                .add_space(
                                    PANEL_NODE_SPACING_SMALL,
                                );

                            if
                                ui_centered
                                    .button(
                                        "Load  (ADD)",
                                    )
                                    .clicked()
                            {
                                match
                                    FileDialog::new()
                                        .set_title(
                                            "Select Project File - pw-videomix",
                                        )
                                        .add_filter(
                                            "projects (json)",
                                            &[
                                                "json", "JSON",
                                            ],
                                        )
                                        .set_directory(
                                            PROJECT_DIRECTORY,
                                        )
                                        .pick_file()
                                {
                                    None => (),
                                    Some(path_buf) => {
                                        println!("-> read from: {path_buf:?}");

                                        options_gui
                                            .borrow_mut()
                                            .project_path_load
                                        =
                                            Some(path_buf);

                                        event_loop_proxy
                                            .send_event(
                                                GuiEvent::Storage(
                                                    StorageEvent {
                                                        event_class:
                                                            StorageEventClass::LoadAdd,
                                                    },
                                                ),
                                            )
                                            .ok();
                                    },
                                };

                                ui_centered
                                    .close_menu();
                            };
                            ui_centered
                                .add_space(
                                    PANEL_NODE_SPACING_SMALL,
                                );
                            if
                                ui_centered
                                    .button(
                                        "Load  (REPLACE)",
                                    )
                                    .clicked()
                            {
                                match
                                    FileDialog::new()
                                        .set_title(
                                            "Select Project File - pw-videomix",
                                        )
                                        .add_filter(
                                            "projects (json)",
                                            &[
                                                "json", "JSON",
                                            ],
                                        )
                                        .set_directory(
                                            PROJECT_DIRECTORY,
                                        )
                                        .pick_file()
                                {
                                    None => (),
                                    Some(path_buf) => {
                                        println!("-> read from: {path_buf:?}");

                                        options_gui
                                            .borrow_mut()
                                            .project_path_load
                                        =
                                            Some(path_buf);

                                        event_loop_proxy
                                            .send_event(
                                                GuiEvent::Storage(
                                                    StorageEvent {
                                                        event_class:
                                                            StorageEventClass::LoadReplace,
                                                    },
                                                ),
                                            )
                                            .ok();
                                    },
                                };

                                ui_centered
                                    .close_menu();
                            };

                            match
                                options_gui
                                    .borrow()
                                    .project_path_load
                                    .to_owned()
                            {
                                None => (),
                                Some(path_buf) => {
                                    ui_centered
                                        .add_space(
                                            PANEL_NODE_SPACING_SMALL,
                                        );

                                    ui_centered
                                        .small(
                                            match
                                                path_buf
                                                    .file_name()
                                            {
                                                None => {
                                                    RichText::new(
                                                        "not a file",
                                                    )
                                                        .color(
                                                            Color32::RED,
                                                        )
                                                },
                                                Some(file_name) => match
                                                    file_name
                                                        .to_str()
                                                {
                                                    None => RichText::new(
                                                        "filename not readable",
                                                    )
                                                        .color(
                                                            Color32::RED,
                                                        ),
                                                    Some(string) => RichText::new(
                                                        string,
                                                    ),
                                                },
                                            },
                                        );
                                },
                            };

                            ui_centered
                                .add_space(
                                    PANEL_NODE_SPACING_SMALL,
                                );
                            ui_centered
                                .separator();
                            ui_centered
                                .add_space(
                                    PANEL_NODE_SPACING_SMALL,
                                );
                    // Save:
                            if
                                ui_centered
                                    .button(
                                        "Save",
                                    )
                                    .clicked()
                            {
                                match
                                    FileDialog::new()
                                        .set_title(
                                            "Select Project File - pw-videomix",
                                        )
                                        .add_filter(
                                            "projects (json)",
                                            &[
                                                "json", "JSON",
                                            ],
                                        )
                                        .set_directory(
                                            PROJECT_DIRECTORY,
                                        )
                                        .save_file()
                                {
                                    None => (),
                                    Some(path_buf) => {
                                        println!("<- save to: {path_buf:?}");

                                        options_gui
                                            .borrow_mut()
                                            .project_path_save
                                        =
                                            Some(path_buf);

                                        event_loop_proxy
                                            .send_event(
                                                GuiEvent::Storage(
                                                    StorageEvent {
                                                        event_class:
                                                            StorageEventClass::Save,
                                                    },
                                                ),
                                            )
                                            .ok();
                                    },
                                };

                                ui_centered
                                    .close_menu();
                            };

                            ui_centered
                                .add_space(
                                    PANEL_NODE_SPACING_SMALL,
                                );
                            ui_centered
                                .separator();
                            ui_centered
                                .add_space(
                                    MENU_SPACING,
                                );

                    // Clear:
                            ui_centered
                                .vertical_centered(
                                    |ui_clear| {
                                        if
                                            ui_clear
                                                .button(
                                                    RichText::new(
                                                        "Clear",
                                                    )
                                                        .color(
                                                            Color32::RED,
                                                        ),
                                                )
                                                .clicked()
                                        {
                                            event_loop_proxy
                                                .send_event(
                                                    GuiEvent::Storage(
                                                        StorageEvent {
                                                            event_class:
                                                                StorageEventClass::Clear,
                                                        },
                                                    ),
                                                )
                                                .ok();
                                            ui_clear
                                                .close_menu();
                                        };
                                    },
                                 );
                            ui_centered
                                .add_space(
                                    MENU_SPACING,
                                );
                        }
                    );
            }
        );
}

pub fn draw_menu_midi(
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    midi_node_rc: Rc<RefCell<MidiNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
    ui: &mut Ui,
) {
    ui.menu_button("MIDI", |ui_midi| {
        let mut midi_node: MidiNode = midi_node_rc.borrow().to_owned();

        let mut enabled: bool = midi_node.enabled;
        let enable_check: bool = midi_node.enabled;
        let mut connection: Option<String> = midi_node.connection.clone();
        ui_midi
            .checkbox(
                &mut enabled,
                "Enable MIDI"
            );
        if
            enabled
            !=
            enable_check
        {
            println!(
                "ii MIDI {}",
                match
                    enabled
                {
                    true => "enabled",
                    false => {
                        // Disconnect from existing ports.
                        connection = None;
                        event_loop_proxy
                            .send_event(
                                GuiEvent::Midi(
                                    MidiEvent {
                                        event_class: MidiEventClass::Disconnect,
                                    }
                                ),
                            )
                            .ok();
                        "disabled"
                    },
                },
            );
        }
        if
            enabled
        {
            ui_midi
                .separator();
            ui_midi.vertical_centered(|ui_centered| {
                ui_centered.small(
                    RichText::new(
                        match
                            connection
                                .clone()
                        {
                            None => String::from("- not connected -"),
                            Some(port) => port,
                        }
                    )
                    .color(Color32::DARK_GRAY)
                );
            });
            ui_midi.menu_button("Connect", |ui_connect| {
                let mut midi_input = MidiInput::new("pw-videomix connect").unwrap();
                midi_input
                    .ignore(Ignore::None);

                let available_ports: Vec<MidiInputPort> = midi_input.ports();
                for
                    port
                in
                    available_ports
                {
                    let name: String = match
                        midi_input
                            .port_name(&port)
                    {
                        Err(_) => String::from("no name"),
                        Ok(name) => name,
                    };
                    if
                        ui_connect
                            .button(name.clone())
                            .clicked()
                    {
                        connection = Some(name.clone());
                        event_loop_proxy
                            .send_event(
                                GuiEvent::Midi(
                                    MidiEvent {
                                        event_class: MidiEventClass::Connect(name),
                                    }
                                ),
                            )
                            .ok();
                        ui_connect.close_menu();
                    };
                }
            });
            if
                ui_midi
                    .button("Disconnect")
                    .clicked()
            {
                // Disconnect midi device.
                connection = None;
                event_loop_proxy
                    .send_event(
                        GuiEvent::Midi(
                            MidiEvent {
                                event_class: MidiEventClass::Disconnect,
                            }
                        ),
                    )
                    .ok();
                ui_midi.close_menu();
            }
            // ui_midi
            //     .add_space(PANEL_NODE_SPACING_SMALL);
            ui_midi
                .separator();
            ui_midi
                .add_space(PANEL_NODE_SPACING_SMALL);
            ui_midi
                .vertical_centered(|ui_hint| {
                    ui_hint
                        .small("1. Click a node to open\nthe side panel.\n\n2. Right-click a LABEL\nof a value.");
                });
            ui_midi
                .add_space(PANEL_NODE_SPACING_SMALL);
            ui_midi
                .separator();
            // ui_midi
            //     .add_space(PANEL_NODE_SPACING_SMALL);
            ui_midi
                .menu_button(
                    "Fast knob",
                    |ui_fast_knob| {
                        let mut gui_options: GuiOptions = options_gui.borrow().to_owned();
                        if
                            ui_fast_knob
                                .button(
                                    // format!(
                                    //     "Fast knob: {}",
                                        match
                                            gui_options
                                                .speed_fast
                                        {
                                            false => "off -> on",
                                            true => "on -> off",
                                        },
                                    // )
                                )
                                .clicked()
                        {
                            gui_options.speed_fast = !gui_options
                                .speed_fast;
                            options_gui
                                .replace(
                                    gui_options
                                        .clone(),
                                );
                        };
                        if
                            ui_fast_knob
                                .button(
                                    // format!(
                                    //     "Fast knob: {}",
                                        match
                                            gui_options
                                                .speed_class
                                        {
                                            MidiControllerSpeedClass::Push => "push -> toggle",
                                            MidiControllerSpeedClass::Toggle => "toggle -> push",
                                        },
                                    // )
                                )
                                .clicked()
                        {
                            gui_options.speed_class = match
                                gui_options
                                    .speed_class
                            {
                                MidiControllerSpeedClass::Push => MidiControllerSpeedClass::Toggle,
                                MidiControllerSpeedClass::Toggle => MidiControllerSpeedClass::Push,
                            };
                            options_gui
                                .replace(
                                    gui_options,
                                );
                        };
                        if
                            ui_fast_knob
                                .button(
                                    "Bind button",
                                )
                                .clicked()
                        {
                            println!(
                                "ii bind button 'fast knob'",
                            );
                            midi_node.bind_enable = BindClass::Button;
                            midi_node.bind_value = Some(
                                MidiBind {
                                    controller_class: midi_node
                                        .controller_class,
                                    bind: BindValues {
                                        class: BindClass::Button,
                                        node_family: NodeFamily::None,
                                        node_id: String::from("midi"),
                                        key: String::from("fast_knob"),
                                        location: Vec::new(),
                                    },
                                }
                            );
                            ui_fast_knob
                                .close_menu();
                        };
                    }
                );
            // ui_midi
            //     .add_space(PANEL_NODE_SPACING_SMALL);
            // ui_midi
            //     .separator();
            // ui_midi
            //     .add_space(PANEL_NODE_SPACING_SMALL);

        }

        // where to put it exactly???
        midi_node = MidiNode {
            connection,
            enabled,
            ..midi_node
        };
        midi_node_rc
            .replace(
                midi_node,
            );

        // if ui_midi.button("Close menu").clicked() {ui_midi.close_menu()}
    });
}
