
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::options::GuiOptions,
    },
    impls::midir::{
        MidiConnections,
        MidiMessage,
        MidiNode,
        connect_midi,
    },
};
use std::{
    cell::RefCell,
    rc::Rc,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct MidiEvent {
    pub event_class: MidiEventClass,
}

impl MidiEvent {
    pub fn event_received(
        &self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        midi_connection_rc: Rc<RefCell<Option<MidiConnections>>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) {
        self
            .event_class
            .evaluate(
                event_loop_proxy,
                map_control_to_node,
                map_filter_to_node,
                map_sink_to_node,
                map_source_to_node,
                midi_connection_rc,
                midi_node_rc,
                options_gui,
            );
    }
}

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub enum MidiEventClass {
    Connect(String),
    Disconnect,
    Received(MidiMessage),
}

impl MidiEventClass {
    pub fn evaluate(
        &self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        midi_connection_rc: Rc<RefCell<Option<MidiConnections>>>,
        midi_node_rc: Rc<RefCell<MidiNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) {
        match
            self
        {
            Self::Connect(port_connect) => {
                midi_connection_rc
                    .replace(
                        connect_midi(
                            event_loop_proxy,
                            port_connect
                                .to_owned(),
                        )
                    );
            },
            Self::Disconnect => {
                match
                    midi_connection_rc
                        .replace(None)
                {
                    None => (),
                    Some(connection) => {
                        println!("ii disconnect from MIDI source");
                        connection
                            .input
                            .close();
                        // Switch off all LEDs!!!
                        connection
                            .output
                            .close();
                    },
                };
            },
            Self::Received(midi_event) => {
                let midi_node: MidiNode = midi_node_rc
                    .borrow()
                    .to_owned();
                midi_node_rc
                    .replace(
                        midi_node
                            .message_received(
                                map_control_to_node,
                                map_filter_to_node,
                                map_sink_to_node,
                                map_source_to_node,
                                midi_connection_rc,
                                midi_event
                                    .to_owned(),
                                options_gui,
                            )
                    );
            },
        };
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
