
use egui_winit_vulkano::RenderResources;
use std::sync::Arc;
use vulkano::{
    buffer::Subbuffer,
    command_buffer::{
        allocator::StandardCommandBufferAllocator,
        AutoCommandBufferBuilder,
        CommandBufferUsage,
        CopyBufferToImageInfo,
        PrimaryCommandBufferAbstract,
    },
    image::view::ImageView,
    sync::GpuFuture,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn event_pipewire_frame(
    command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
    image_storage: Arc<ImageView>,
    picture_buffer: Subbuffer<[u8]>,
    resources: RenderResources,
) {
    // println!("ii image: {:?}", image_storage.image());
    // println!("ii buffer size: {:?}", picture_buffer);
    let mut command_buffer_builder = AutoCommandBufferBuilder::primary(
        command_buffer_allocator,
        // resources.command_buffer_allocator,
        resources.queue.queue_family_index(),
        CommandBufferUsage::OneTimeSubmit,
    )
    .unwrap();
    command_buffer_builder
        // Clear the image buffer.
        // .clear_color_image(ClearColorImageInfo::image(image.clone()))
        // .unwrap()
        .copy_buffer_to_image(CopyBufferToImageInfo::buffer_image(
            picture_buffer,
            image_storage.image().clone(),
        ))
        .unwrap();

    let command_buffer = command_buffer_builder
        .build()
        .unwrap();
    let finished = command_buffer.execute(resources.queue).unwrap();
    let _future = finished.then_signal_fence_and_flush().unwrap();
}
