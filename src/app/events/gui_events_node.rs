
use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::options::GuiOptions,
    },
    nodes::{
        remove::remove_node,
        sinks::SinkNode,
        NodeFamily,
    },
};
#[cfg(unix)]
use crate::impls::pipewire::PipewireStream;
use egui_winit_vulkano::Gui;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};
use winit::{
    event_loop::EventLoopProxy,
    window::WindowId,
};

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct NodeEvent {
    pub id: String,
    pub event_class: NodeEventClass,
    pub family: NodeFamily,
}

impl NodeEvent {
    pub fn event_change_node(
        &self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        #[cfg(unix)]
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) {
        self
            .event_class
            .evaluate(
                event_loop_proxy,
                map_control_to_node,
                map_filter_to_node,
                #[cfg(unix)]
                map_node_to_stream,
                map_sink_to_node,
                map_source_to_node,
                map_window_to_gui,
                self
                    .family,
                self
                    .id
                    .clone(),
                options_gui,
            );
    }
}

// ----------------------------------------------------------------------------

#[derive(Clone, PartialEq)]
pub enum NodeEventClass {
    Deselect,
    None,
    Remove,
    Select,
}

impl NodeEventClass {
    fn evaluate(
        &self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        #[cfg(unix)]
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
        node_family: NodeFamily,
        node_id: String,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) {
        match
            self
        {
            Self::None => (),
            Self::Select => {
                options_gui
                    .borrow_mut()
                    .selected_node
                    .replace(
                        node_id,
                    );
            },
            Self::Deselect => {
                options_gui
                    .borrow_mut()
                    .selected_node
                    .take();
            },
            Self::Remove => {
                // Remove renderer from sink window.
                // move windows into function???
                match
                    node_family
                {
                    NodeFamily::Sink => {
                        match
                            map_sink_to_node
                                .borrow()
                                .map
                                .get(
                                    &node_id,
                                )
                        {
                            Some(SinkNode::Window(window_node)) => {
                                event_loop_proxy
                                    .send_event(
                                        GuiEvent::WindowRemove(
                                            window_node
                                                .window_id,
                                        )
                                    )
                                    .ok();
                            },
                            _ => (),
                        };
                    },
                    _ => (),
                };
                // Remove node.
                remove_node(
                    map_control_to_node,
                    map_filter_to_node,
                    #[cfg(unix)]
                    map_node_to_stream,
                    map_sink_to_node,
                    map_source_to_node,
                    map_window_to_gui,
                    node_family,
                    node_id,
                    options_gui,
                );
            },
        };
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
