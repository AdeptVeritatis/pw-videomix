
use crate::{
    app::data::app_nodes::SinkToNode,
    nodes::sinks::{
        window::{
            WindowNode,
            WindowValues,
        },
        SinkNode,
    },
};
use egui_winit_vulkano::egui::Vec2;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};
use vulkano_util::renderer::VulkanoWindowRenderer;
use winit::{
    event::{
        ElementState,
        KeyEvent,
    },
    keyboard::{
        KeyCode,
        PhysicalKey,
    },
    window::WindowId,
};

// ----------------------------------------------------------------------------
// Functions - WindowEvent:
// ----------------------------------------------------------------------------

pub fn event_window_key(
    input: KeyEvent,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
    window_id: WindowId,
) {
    match
        input.state
    {
        ElementState::Released => (),
        ElementState::Pressed => match
            input
                .physical_key
        {
            PhysicalKey::Unidentified(_) => (),
            PhysicalKey::Code(key_code) => match
                key_code
            {
                KeyCode::Escape => {
                    event_fullscreen_off(
                        map_sink_to_node
                            .clone(),
                        map_window_to_sink
                            .clone(),
                        window_id,
                    );
                },
                KeyCode::KeyB => {
                    println!("-- toggle borderless test");
                },
                KeyCode::KeyF => {
                    event_fullscreen_toggle(
                        map_sink_to_node
                            .clone(),
                        map_window_to_sink
                            .clone(),
                        window_id,
                    );
                },
                _ => (),
            },
        },
    };
}

// ----------------------------------------------------------------------------

pub fn event_window_resize(
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
    renderer: &mut VulkanoWindowRenderer,
    window_id: WindowId,
) {
// ) -> VulkanoWindowRenderer {
    renderer
        .resize();

    let
        (
            sink_id,
            sink_node,
        )
    :
        (
            String,
            SinkNode,
        )
    = match
        map_window_to_sink
            .borrow()
            .get(
                &window_id,
            )
    {
        None => panic!("WindowId unknown"),
        Some(sink_id) => match
            map_sink_to_node
                .borrow()
                .map
                .get(sink_id)
        {
            None => panic!("SinkNode not found"),
            Some(sink_node) => match
                sink_node
                    .to_owned()
            {
                SinkNode::Window(mut window_node) => {
                    window_node
                        .values
                        .size
                    = Vec2::from(
                        renderer
                            .window_size(),
                    );

                    (
                        sink_id
                            .to_owned(),
                        SinkNode::Window(
                            window_node,
                        ),
                    )
                },
                _ => panic!("SinkNode not a window"),
            },
        },
    };

    map_sink_to_node
        .borrow_mut()
        .map
        .insert(
            sink_id,
            sink_node,
        );
}

// ----------------------------------------------------------------------------
// Functions - WindowEvent (fullscreen):
// ----------------------------------------------------------------------------

fn event_fullscreen_off(
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
    window_id: WindowId,
) {
    let sink_id: String = match
        map_window_to_sink
            .borrow()
            .get(&window_id)
    {
        None => String::new(),
        Some(sink_id) => sink_id.to_owned(),
    };
    let sink_node: SinkNode = match
        map_sink_to_node
            .borrow()
            .map
            .get(&sink_id)
    {
        None => SinkNode::None,
        Some(sink_node) => {
            match
                sink_node
            {
                SinkNode::None => SinkNode::None,
                #[cfg(feature = "gstreamer")]
                SinkNode::Encoder(encoder_node) => SinkNode::Encoder(encoder_node.to_owned()),
                SinkNode::Monitor(monitor_node) => SinkNode::Monitor(monitor_node.to_owned()),
                #[cfg(unix)]
                SinkNode::PipewireOutput(pw_output_node) => SinkNode::PipewireOutput(pw_output_node.to_owned()),
                // #[cfg(feature = "gstreamer")]
                // SinkNode::PwOutputGst(pw_output_gst_node) => SinkNode::PwOutputGst(pw_output_gst_node.to_owned()),
                SinkNode::Snapshot(snapshot_node) => SinkNode::Snapshot(snapshot_node.to_owned()),
                SinkNode::Window(window_node) => {
                    let values: WindowValues = window_node.to_owned().values;
                    SinkNode::Window(
                        WindowNode {
                            values: WindowValues {
                                fullscreen: false,
                                ..values
                            },
                            ..window_node.to_owned()
                        },
                    )
                },
            }
        },
    };
    map_sink_to_node
        .borrow_mut()
        .map
        .insert(
            sink_id,
            sink_node,
        );
}

// ----------------------------------------------------------------------------

fn event_fullscreen_toggle(
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
    window_id: WindowId,
) {
    let sink_id: String = match
        map_window_to_sink
            .borrow()
            .get(&window_id)
    {
        None => String::new(),
        Some(sink_id) => sink_id.to_owned(),
    };
    let sink_node: SinkNode = match
        map_sink_to_node
            .borrow()
            .map
            .get(&sink_id)
    {
        None => SinkNode::None,
        Some(sink_node) => {
            match
                sink_node
            {
                SinkNode::None => SinkNode::None,
                #[cfg(feature = "gstreamer")]
                SinkNode::Encoder(encoder_node) => SinkNode::Encoder(encoder_node.to_owned()),
                SinkNode::Monitor(monitor_node) => SinkNode::Monitor(monitor_node.to_owned()),
                #[cfg(unix)]
                SinkNode::PipewireOutput(pw_output_node) => SinkNode::PipewireOutput(pw_output_node.to_owned()),
                // #[cfg(feature = "gstreamer")]
                // SinkNode::PwOutputGst(pw_output_gst_node) => SinkNode::PwOutputGst(pw_output_gst_node.to_owned()),
                SinkNode::Snapshot(snapshot_node) => SinkNode::Snapshot(snapshot_node.to_owned()),
                SinkNode::Window(window_node) => {
                    let values: WindowValues = window_node.to_owned().values;
                    let fullscreen: bool = !values.fullscreen;
                    println!(
                        "-- toggle fullscreen from \"{}\" ( {} )",
                        window_node.id,
                        match fullscreen {
                            true => "on",
                            false => "off",
                        },
                    );
                    SinkNode::Window(
                        WindowNode {
                            values: WindowValues {
                                fullscreen,
                                ..values
                            },
                            ..window_node.to_owned()
                        },
                    )
                },
            }
        },
    };
    map_sink_to_node
        .borrow_mut()
        .map
        .insert(
            sink_id,
            sink_node,
        );
}
