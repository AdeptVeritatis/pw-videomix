
use crate::{
    app::data::app_nodes::FilterToNode,
    nodes::{
        filter::{
            fader::{
                FaderNode,
                FaderValues,
            },
            mixer::{
                MixerNode,
                MixerValues,
            },
            FilterNode,
        },
        NodeLayer,
    },
};
use std::{
    cell::RefCell,
    collections::BTreeMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------
//  Functions:
// ----------------------------------------------------------------------------

pub fn event_layer_remove(
    filter_id: String,
    layer_id: String,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
) {
    let filter_node: Option<FilterNode> = match
        map_filter_to_node
            .borrow()
            .map
            .get(&filter_id)
    {
        None => None,
        Some(FilterNode::None) => None,
        // Single input filter:
        Some(FilterNode::ColorMixer(_)) => None,
        Some(FilterNode::ColorRotator(_)) => None,
        Some(FilterNode::Kaleidoscope(_)) => None,
        Some(FilterNode::Mandala(_)) => None,
        // Multi input filter:
        Some(FilterNode::Fader(fader_node)) => {
            let mut values = fader_node.values.clone();
            let mut sources: BTreeMap<String, NodeLayer> = values.sources.clone();
            sources
                .remove(&layer_id);
            values = FaderValues {
                sources,
                ..values
            };
            Some(FilterNode::Fader(
                FaderNode {
                    values,
                    ..fader_node.to_owned()
                }
            ))
        },
        Some(FilterNode::Mixer(mixer_node)) => {
            let mut values = mixer_node.values.clone();
            let mut sources: BTreeMap<String, NodeLayer> = values.sources.clone();

            sources
                .remove(&layer_id);

            // Create new mix buffer for fragment shader.???

            values = MixerValues {
                sources,
                ..values
            };
            Some(FilterNode::Mixer(
                MixerNode {
                    values,
                    ..mixer_node.to_owned()
                }
            ))
        },
    };
    match
        filter_node
    {
        None => (),
        Some(filter_node) => {
            // println!("<- remove connection \"{}\"", layer_id);
            map_filter_to_node
                .borrow_mut()
                .map
                .insert(
                    filter_id,
                    filter_node,
                );
        },
    }
}
