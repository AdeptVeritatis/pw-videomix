
use crate::{
    app::data::app_nodes::{
        FilterToNode,
        SourceToNode,
    },
    constants::IMAGE_FORMAT,
    impls::vulkano::scenes::render::{
        color_mixer_scene::ColorMixerScene,
        fader_scene::FaderScene,
        kaleidoscope_scene::KaleidoscopeScene,
        mandala_scene::MandalaScene,
        mixer_scene::MixerScene,
        stack_scene::StackScene,
    },
    nodes::{
        filter::{
            color_mixer::ColorMixerNode,
            color_rotator::ColorRotatorNode,
            fader::FaderNode,
            kaleidoscope::KaleidoscopeNode,
            mandala::MandalaNode,
            mixer::MixerNode,
            FilterNode,
        },
        sources::{
            picture_stack_in::StackNode,
            SourceNode,
        },
        NodeFamily,
    },
};
use egui_winit_vulkano::{
    egui::Vec2,
    RenderResources,
};
use std::{
    cell::RefCell,
    rc::Rc,
    sync::Arc,
};
use vulkano::{
    image::{
        view::ImageView,
        Image,
        ImageCreateInfo,
        ImageType,
        ImageUsage,
    },
    memory::allocator::AllocationCreateInfo,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn event_recreate_image(
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    node_family: NodeFamily,
    node_id: String,
    resources: RenderResources,
    size: Vec2,
) {
    // Filter and some sources have an output with a variable framebuffer.
    // Sinks only write to something else or they would be a filter.
    // println!("!! recreate image");
    let extent: [u32; 3] = [size.x as u32, size.y as u32, 1];
    let image: Arc<Image> = Image::new(
        resources.memory_allocator,
        ImageCreateInfo {
            image_type: ImageType::Dim2d,
            format: IMAGE_FORMAT,
            extent,
            usage: ImageUsage::TRANSFER_DST | ImageUsage::TRANSFER_SRC | ImageUsage::SAMPLED | ImageUsage::COLOR_ATTACHMENT,
            ..Default::default()
        },
        AllocationCreateInfo::default(),
    )
    .unwrap();
    match
        node_family
    {
        NodeFamily::Filter => {
            let filter_node: FilterNode = match
                map_filter_to_node
                    .borrow()
                    .map
                    .get(&node_id)
            {
                None => FilterNode::None,
                Some(filter_node) => match
                    filter_node
                {
                    FilterNode::None => FilterNode::None,
                    FilterNode::ColorMixer(color_mixer_node) => {
                        let scene = ColorMixerScene {
                            image_storage: ImageView::new_default(image).unwrap(),
                            ..color_mixer_node.scene.clone()
                        };
                        FilterNode::ColorMixer(
                            ColorMixerNode {
                                scene,
                                ..color_mixer_node.to_owned()
                            }
                        )
                    },
                    FilterNode::ColorRotator(color_rotator_node) => {
                        let scene = ColorMixerScene {
                            image_storage: ImageView::new_default(image).unwrap(),
                            ..color_rotator_node.scene.clone()
                        };
                        FilterNode::ColorRotator(
                            ColorRotatorNode {
                                scene,
                                ..color_rotator_node.to_owned()
                            }
                        )
                    },
                    FilterNode::Fader(fader_node) => {
                        let scene = FaderScene {
                            image_storage: ImageView::new_default(image).unwrap(),
                            ..fader_node.scene.clone()
                        };
                        FilterNode::Fader(
                            FaderNode {
                                scene,
                                ..fader_node.to_owned()
                            }
                        )
                    },
                    FilterNode::Kaleidoscope(kaleidoscope_node) => {
                        let scene = KaleidoscopeScene {
                            image_storage: ImageView::new_default(image).unwrap(),
                            ..kaleidoscope_node.scene.clone()
                        };
                        FilterNode::Kaleidoscope(
                            KaleidoscopeNode {
                                scene,
                                ..kaleidoscope_node.to_owned()
                            }
                        )
                    },
                    FilterNode::Mandala(mandala_node) => {
                        let scene = MandalaScene {
                            image_storage: ImageView::new_default(image).unwrap(),
                            ..mandala_node.scene.clone()
                        };
                        FilterNode::Mandala(
                            MandalaNode {
                                scene,
                                ..mandala_node.to_owned()
                            }
                        )
                    },
                    FilterNode::Mixer(mixer_node) => {
                        let scene = MixerScene {
                            image_storage: ImageView::new_default(image).unwrap(),
                            ..mixer_node.scene.clone()
                        };
                        FilterNode::Mixer(
                            MixerNode {
                                scene,
                                ..mixer_node.to_owned()
                            }
                        )
                    },
                },
            };
            map_filter_to_node
                .borrow_mut()
                .map
                .insert(
                    node_id,
                    filter_node,
                );
        },
        NodeFamily::Source => {
            let source_node: SourceNode = match
                map_source_to_node
                    .borrow()
                    .map
                    .get(&node_id)
            {
                None => SourceNode::None,
                Some(source_node) => match
                    source_node
                {
                    SourceNode::None => SourceNode::None,
                    SourceNode::Stack(stack_node) => {
                        let scene = StackScene {
                            image_storage: ImageView::new_default(image).unwrap(),
                            ..stack_node.scene.clone()
                        };
                        SourceNode::Stack(
                            StackNode {
                                scene,
                                ..stack_node.to_owned()
                            }
                        )
                    },
                    source_node => source_node.to_owned(),
                },
            };
            map_source_to_node
                .borrow_mut()
                .map
                .insert(
                    node_id,
                    source_node,
                );
        },
        _ => (),

    };
}
