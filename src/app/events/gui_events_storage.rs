
use crate::{
    app::{
        data::{
            app_nodes::{
                ControlToNode,
                FilterToNode,
                SinkToNode,
                SourceToNode,
            },
            StorageContainer,
        },
        events::{
            gui_events_node::NodeEventClass,
            GuiEvent,
            NodeEvent,
        },
        gui::options::GuiOptions,
    },
    impls::serde::{
        asset_loader::AssetLoaderImpl,
        StorageDeserializer,
    },
    nodes::{
        connections::{
            InputConnection,
            InputConnectionConstructor,
        },
        control::{
            function_generator::FunctionGeneratorNode,
            number::NumberNode,
            trigger::TriggerNode,
            trigonometry::TrigonometryNode,
            ControlNode,
        },
        filter::{
            color_mixer::{
                ColorMixerNode,
                ColorMixerValues,
            },
            color_rotator::{
                ColorRotatorNode,
                ColorRotatorValues,
            },
            fader::{
                FaderNode,
                // FaderValues,
            },
            kaleidoscope::{
                KaleidoscopeNode,
                KaleidoscopeValues,
            },
            mandala::{
                MandalaNode,
                MandalaValues,
            },
            mixer::{
                MixerNode,
                // MixerValues,
            },
            FilterNode,
        },
        sinks::SinkNode,
        // NodeClass,
        NodeFamily,
    },
};
#[cfg(unix)]
use crate::impls::pipewire::PipewireStream;
use egui_winit_vulkano::{
    // Gui,
    RenderResources,
};
use serde::de::DeserializeSeed;
use std::{
    cell::RefCell,
    collections::HashMap,
    fs,
    // path::PathBuf,
    rc::Rc,
};
use winit::{
    event_loop::EventLoopProxy,
    // window::WindowId,
};

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct StorageEvent {
    pub event_class: StorageEventClass,
}

impl StorageEvent {
    pub fn event_storage(
        &self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        #[cfg(unix)]
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        // map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
        // map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        render_resources: RenderResources,
    ) {
        self
            .event_class
            .evaluate(
                event_loop_proxy,
                map_control_to_node,
                map_filter_to_node,
                #[cfg(unix)]
                map_node_to_stream,
                map_sink_to_node,
                map_source_to_node,
                // map_window_to_gui,
                // map_window_to_sink,
                options_gui,
                render_resources,
            );
    }
}

// ----------------------------------------------------------------------------

#[derive(Clone, PartialEq)]
pub enum StorageEventClass {
    Clear,
    Save,
    LoadAdd,
    LoadReplace,
}

impl StorageEventClass {
    fn evaluate(
        &self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        #[cfg(unix)]
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        // _map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
        // _map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        render_resources: RenderResources,
    ) {
        match
            self
        {
            // Self::None => (),
            Self::Clear => {
                self
                    .clear(
                        event_loop_proxy,
                        map_control_to_node,
                        map_filter_to_node,
                        #[cfg(unix)]
                        map_node_to_stream,
                        map_sink_to_node,
                        map_source_to_node,
                        options_gui,
                    );
            },
            Self::LoadAdd
            |
            Self::LoadReplace
            => {
                self
                    .load(
                        map_control_to_node,
                        map_filter_to_node,
                        map_source_to_node,
                        options_gui,
                        render_resources,
                    );
            },
            Self::Save => {
                self
                    .save(
                        map_control_to_node,
                        map_filter_to_node,
                        map_sink_to_node,
                        map_source_to_node,
                        options_gui,
                    );
            },
        };
    }

}

// ----------------------------------------------------------------------------

impl StorageEventClass {
    fn
        clear
    (
        &self,
        event_loop_proxy: EventLoopProxy<GuiEvent>,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        #[cfg(unix)]
        map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) {
        map_control_to_node
            .replace(
                ControlToNode::default(),
            );
        map_filter_to_node
            .replace(
                FilterToNode::default(),
            );
        map_source_to_node
            .replace(
                SourceToNode::default(),
            );
        #[cfg(unix)]
        map_node_to_stream
            .replace(
                HashMap::new()
            );

        let
            hashmap_sink_to_node
        =
            map_sink_to_node
                .borrow()
                .map
                .to_owned();

        for
            (
                id,
                sink_node,
            )
        in
            hashmap_sink_to_node
        {
            match
                sink_node
            {
                SinkNode::Window(_) => {
                    event_loop_proxy
                        .send_event(
                            GuiEvent::ChangeNode(
                                NodeEvent {
                                    family: NodeFamily::Sink,
                                    id,
                                    event_class: NodeEventClass::Remove,
                                }
                            )
                        )
                        .ok();
                },
                _ => {
                    map_sink_to_node
                        .borrow_mut()
                        .map
                        .remove(
                            &id,
                        );
                },
            };
        };

        options_gui
            .borrow_mut()
            .project_path_load
        =
            None;
        options_gui
            .borrow_mut()
            .project_path_save
        =
            None;
    }
}

// ----------------------------------------------------------------------------

impl StorageEventClass {
    fn
        load
    (
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
        render_resources: RenderResources,
    ) {
        match
            options_gui
                .borrow()
                .project_path_load
                .to_owned()
        {
            None => println!("!! error: no path"),
            Some(path_buf) => match
                path_buf
                    .to_str()
            {
                None => println!("!! error reading path"),
                Some(file_path) => match
                    fs::read_to_string(
                        file_path,
                    )
                {
                    Err(error) => println!("!! error reading file: {error}"),
                    Ok(container) => {
                        let
                            input_connections
                        :
                            Rc<RefCell<Vec<InputConnectionConstructor>>>
                        =
                            Rc::new(
                                RefCell::new(
                                    Vec::new(),
                                ),
                            );

                        // Deserialize:
                        match {
                            let mut asset_loader: AssetLoaderImpl = AssetLoaderImpl{
                                input_connections:
                                    input_connections
                                        .clone(),
                                render_resources,
                            };
                            let mut deserializer = serde_json::Deserializer::new(
                                serde_json::de::StrRead::new(
                                    &container,
                                ),
                            );
                            StorageDeserializer {
                                asset_loader:
                                    &mut asset_loader,
                            }
                                .deserialize(
                                    &mut deserializer,
                                )
                        } {
                            Err(error) => println!("!! error deserializing: {error}"),
                            Ok(storage) => match
                                self
                            {
                                Self::LoadAdd => {
                                    self
                                        .load_add(
                                            map_control_to_node,
                                            map_filter_to_node
                                                .clone(),
                                            storage,
                                        );
                                },
                                Self::LoadReplace => {
                                    self
                                        .load_replace(
                                            map_control_to_node,
                                            map_filter_to_node
                                                .clone(),
                                            storage,
                                        );

                                    load_connect(
                                        input_connections,
                                        map_filter_to_node,
                                        map_source_to_node,
                                    );
                                },
                                _ => println!("!! error: not a load event"),
                            },
                        };

                        // load_connect(
                        //     input_connections,
                        //     map_filter_to_node,
                        //     map_source_to_node,
                        // );
                    },
                },
            },
        }
    }

    fn
        load_add
    (
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        storage: StorageContainer,
    ) {
        // println!("{storage}");
        const
            APPENDIX
        :
            &str
        =
            ".1";

    // Check if nodes already exist.
    // ControlNodes:
        let
            control_to_node
        :
            HashMap<String, ControlNode>
        =
            map_control_to_node
                .borrow()
                .map
                .to_owned();
        for
            (
                mut control_id,
                mut control_node,
            )
        in
            storage
                .map_control_to_node
                .map
        {
            while
                match
                    control_to_node
                        .contains_key(
                            &control_id,
                        )
                {
                    false => {
                        map_control_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                control_id
                                    .clone(),
                                control_node
                                    .clone(),
                            );

                        false
                    },
                    true => {
                        control_id += APPENDIX;
                        control_node = match
                            control_node
                        {
                            ControlNode::None => ControlNode::None,
                            ControlNode::FunctionGenerator(node) => {
                                ControlNode::FunctionGenerator(
                                    FunctionGeneratorNode {
                                        id:
                                            control_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                            ControlNode::Number(node) => {
                                ControlNode::Number(
                                    NumberNode {
                                        id:
                                            control_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                            ControlNode::Trigger(node) => {
                                ControlNode::Trigger(
                                    TriggerNode {
                                        id:
                                            control_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                            ControlNode::Trigonometry(node) => {
                                ControlNode::Trigonometry(
                                    TrigonometryNode {
                                        id:
                                            control_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                        };
                        true
                    },
                }
            {};
        };

    // FilterNodes:
        let
            filter_to_node
        :
            HashMap<String, FilterNode>
        =
            map_filter_to_node
                .borrow()
                .map
                .to_owned();
        for
            (
                mut filter_id,
                mut filter_node,
            )
        in
            storage
                .map_filter_to_node
                .map
        {
            while
                match
                    filter_to_node
                        .contains_key(
                            &filter_id,
                        )
                {
                    false => {
                        map_filter_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                filter_id
                                    .clone(),
                                filter_node
                                    .clone(),
                            );

                        false
                    },
                    true => {
                        // change filter_id in connections, too? what about source if this changes name too or not???
                        filter_id += APPENDIX;
                        filter_node = match
                            filter_node
                        {
                            FilterNode::None => FilterNode::None,
                            FilterNode::ColorMixer(node) => {
                                FilterNode::ColorMixer(
                                    ColorMixerNode {
                                        id:
                                            filter_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                            FilterNode::ColorRotator(node) => {
                                FilterNode::ColorRotator(
                                    ColorRotatorNode {
                                        id:
                                            filter_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                            FilterNode::Kaleidoscope(node) => {
                                FilterNode::Kaleidoscope(
                                    KaleidoscopeNode {
                                        id:
                                            filter_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                            FilterNode::Fader(node) => {
                                FilterNode::Fader(
                                    FaderNode {
                                        id:
                                            filter_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                            FilterNode::Mandala(node) => {
                                FilterNode::Mandala(
                                    MandalaNode {
                                        id:
                                            filter_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                            FilterNode::Mixer(node) => {
                                FilterNode::Mixer(
                                    MixerNode {
                                        id:
                                            filter_id
                                                .clone(),
                                        name:
                                            node
                                                .name
                                            +
                                            APPENDIX,
                                        ..node
                                    },
                                )
                            },
                        };
                        true
                    },
                }
            {};
        };

    // SinkNodes:
        // map_sink_to_node
        //     .replace(
        //         storage
        //             .map_sink_to_node,
        //     );
    // SourceNodes:
        // map_source_to_node
        //     .replace(
        //         storage
        //             .map_source_to_node,
        //     );
    }

    fn
        load_replace
    (
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        storage: StorageContainer,
    ) {
        // println!("{storage}");
        map_control_to_node
            .replace(
                storage
                    .map_control_to_node,
            );
        map_filter_to_node
            .replace(
                storage
                    .map_filter_to_node
                        .clone(),
            );
        // map_sink_to_node
        //     .replace(
        //         storage
        //             .map_sink_to_node,
        //     );
        // map_source_to_node
        //     .replace(
        //         storage
        //             .map_source_to_node,
        //     );
    }
}

// ----------------------------------------------------------------------------

impl StorageEventClass {
    fn
        save
    (
        &self,
        map_control_to_node: Rc<RefCell<ControlToNode>>,
        map_filter_to_node: Rc<RefCell<FilterToNode>>,
        map_sink_to_node: Rc<RefCell<SinkToNode>>,
        map_source_to_node: Rc<RefCell<SourceToNode>>,
        options_gui: Rc<RefCell<GuiOptions>>,
    ) {
        match
            serde_json::to_string_pretty(
                &StorageContainer {
                    map_control_to_node:
                        map_control_to_node
                            .borrow()
                            .to_owned(),
                    map_filter_to_node:
                        map_filter_to_node
                            .borrow()
                            .to_owned(),
                    map_sink_to_node:
                        map_sink_to_node
                            .borrow()
                            .to_owned(),
                    map_source_to_node:
                        map_source_to_node
                            .borrow()
                            .to_owned(),
                },
            )
        {
            Err(error) => println!("!! {error}"),
            Ok(container) => match
                options_gui
                    .borrow()
                    .project_path_save
                    .to_owned()
            {
                None => println!("!! error: no path"),
                Some(path_buf) => match
                    path_buf
                        .to_str()
                {
                    None => println!("!! error reading path"),
                    Some(file_path) => match
                        fs::write(
                            file_path,
                            container
                                .clone(),
                        )
                    {
                        Err(error) => println!("!! {error}"),
                        Ok(_) => println!("ii file written"),
                    },
                },
            },
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn
    load_connect
(
    input_connections:
        Rc<RefCell<Vec<InputConnectionConstructor>>>,
    map_filter_to_node:
        Rc<RefCell<FilterToNode>>,
    map_source_to_node:
        Rc<RefCell<SourceToNode>>,
) {
    for
        constructor
    in
        input_connections
            .borrow()
            .to_owned()
    {
        // println!("!! connection found: {}",constructor.node_id);

        match
            constructor
                .node_family
        {
            NodeFamily::None => (),
            NodeFamily::Control => (),
            NodeFamily::Source => (),
            NodeFamily::Filter => {
                let
                    filter_node
                =
                    match
                        map_filter_to_node
                            .borrow()
                            .map
                            .get(
                                &constructor
                                    .node_id
                            )
                    {
                        None => FilterNode::None,
                        Some(FilterNode::None) => FilterNode::None,
                        Some(FilterNode::ColorMixer(node)) => FilterNode::ColorMixer(
                            ColorMixerNode {
                                values: ColorMixerValues {
                                    source:
                                        match // only for now, until source nodes are added !!!
                                            constructor
                                                .node_values
                                                .source_family
                                        {
                                            NodeFamily::Filter => Some(
                                                InputConnection::new(
                                                    constructor
                                                        .clone(),
                                                    map_filter_to_node
                                                        .clone(),
                                                    map_source_to_node
                                                        .clone(),
                                                ),
                                            ),
                                            _ => None,
                                        },
                                    ..node
                                        .values
                                        .to_owned()
                                },
                                ..node
                                    .to_owned()
                            },
                        ),
                        Some(FilterNode::ColorRotator(node)) => FilterNode::ColorRotator(
                            ColorRotatorNode {
                                values: ColorRotatorValues {
                                    source:
                                        match // only for now, until source nodes are added !!!
                                            constructor
                                                .node_values
                                                .source_family
                                        {
                                            NodeFamily::Filter => Some(
                                                InputConnection::new(
                                                    constructor
                                                        .clone(),
                                                    map_filter_to_node
                                                        .clone(),
                                                    map_source_to_node
                                                        .clone(),
                                                ),
                                            ),
                                            _ => None,
                                        },
                                    ..node
                                        .values
                                        .to_owned()
                                },
                                ..node
                                    .to_owned()
                            },
                        ),
                        Some(FilterNode::Fader(node)) => FilterNode::Fader(
                            node
                                .to_owned(),
                        ),
                        Some(FilterNode::Kaleidoscope(node)) => FilterNode::Kaleidoscope(
                            KaleidoscopeNode {
                                values: KaleidoscopeValues {
                                    source:
                                        match // only for now, until source nodes are added !!!
                                            constructor
                                                .node_values
                                                .source_family
                                        {
                                            NodeFamily::Filter => Some(
                                                InputConnection::new(
                                                    constructor
                                                        .clone(),
                                                    map_filter_to_node
                                                        .clone(),
                                                    map_source_to_node
                                                        .clone(),
                                                ),
                                            ),
                                            _ => None,
                                        },
                                    ..node
                                        .values
                                        .to_owned()
                                },
                                ..node
                                    .to_owned()
                            },
                        ),
                        Some(FilterNode::Mandala(node)) => FilterNode::Mandala(
                            MandalaNode {
                                values: MandalaValues {
                                    source:
                                        match // only for now, until source nodes are added !!!
                                            constructor
                                                .node_values
                                                .source_family
                                        {
                                            NodeFamily::Filter => Some(
                                                InputConnection::new(
                                                    constructor
                                                        .clone(),
                                                    map_filter_to_node
                                                        .clone(),
                                                    map_source_to_node
                                                        .clone(),
                                                ),
                                            ),
                                            _ => None,
                                        },
                                    ..node
                                        .values
                                        .to_owned()
                                },
                                ..node
                                    .to_owned()
                            },
                        ),
                        Some(FilterNode::Mixer(node)) => FilterNode::Mixer(
                            node
                                .to_owned(),
                        ),
                    };
                match
                    filter_node
                {
                    FilterNode::None => (),
                    _ => {
                        map_filter_to_node
                            .borrow_mut()
                            .map
                            .insert(
                                constructor
                                    .node_id,
                                filter_node,
                            );
                    },
                };
            },
            NodeFamily::Sink => {}, // !!!
        };
    };
}
