
use crate::{
    app::data::app_nodes::SinkToNode,
    constants::{
        SWAPCHAIN_FORMAT,
        // WINDOW_SINK_HEIGHT,
        // WINDOW_SINK_WIDTH,
    },
    nodes::sinks::SinkNode,
};
use egui_winit_vulkano::{
    Gui,
    GuiConfig,
    RenderResources,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};
use vulkano_util::{
    // context::VulkanoContext,
    renderer::VulkanoWindowRenderer,
    // window::{
    //     VulkanoWindows,
    //     WindowDescriptor,
    // },
};
use winit::{
    event_loop::ActiveEventLoop,
    window::WindowId,
};

// ----------------------------------------------------------------------------
//  Functions:
// ----------------------------------------------------------------------------

pub fn event_window_create(
    // context: &VulkanoContext,
    event_loop: &ActiveEventLoop,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
    map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
    renderer: &mut VulkanoWindowRenderer,
    window_id: WindowId,
    // mut windows: VulkanoWindows,
) {
    // // Create sink window:
    // let window_id: WindowId = windows
    //     .create_window(
    //         event_loop,
    //         context,
    //         &WindowDescriptor {
    //             title: String::from("pw-videomix - sink window"),
    //             width: WINDOW_SINK_WIDTH,
    //             height: WINDOW_SINK_HEIGHT,
    //             resizable: true,
    //             ..WindowDescriptor::default()
    //         },
    //         |create_info| {
    //             create_info.image_format = SWAPCHAIN_FORMAT;
    //             create_info.min_image_count = create_info
    //                 .min_image_count
    //                 .max(
    //                     2,
    //                 );
    //         },
    //     );
    // let renderer: &mut VulkanoWindowRenderer = windows
    //     .get_renderer_mut(
    //         window_id,
    //     )
    //     .unwrap();

    create_window(
        event_loop,
        map_sink_to_node,
        map_window_to_gui,
        map_window_to_sink,
        renderer,
        window_id,
    );
}

fn create_window(
    event_loop: &ActiveEventLoop,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_window_to_gui: Rc<RefCell<HashMap<WindowId, Gui>>>,
    map_window_to_sink: Rc<RefCell<HashMap<WindowId, String>>>,
    renderer: &mut VulkanoWindowRenderer,
    window_id: WindowId,
) {
    // Check out again: vulkano_util/src/window.rs/VulkanoWindows/create_window()!!!
    // Create GUI:
    let gui_sink: Gui = Gui::new(
        event_loop,
        renderer
            .surface(),
        renderer
            .graphics_queue(),
        SWAPCHAIN_FORMAT,
        GuiConfig {
            // allow_srgb_render_target: true,
            ..Default::default()
        },
    );
    let resources: RenderResources = gui_sink
        .render_resources();
    // Create and store WindowNode, return sink_id of window. // better return name???
    let sink_id: String = SinkNode::window(
        map_sink_to_node,
        resources,
        window_id,
    );
    // Add to map for sink_id from window_id.
    map_window_to_sink
        .borrow_mut()
        .insert(
            window_id,
            sink_id
                .clone(),
        );
    // Save GUI:
    map_window_to_gui
        .borrow_mut()
        .insert(
            window_id,
            gui_sink,
        );

    // Set window title:
    renderer
        .window()
        .set_title(
            format!(
                "pw-videomix - {}",
                sink_id,
            )
            .as_str(),
        );
}
