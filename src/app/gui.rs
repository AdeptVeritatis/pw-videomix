// #![allow(clippy::collapsible_if)]
pub mod bind;
pub mod menu;
pub mod options;
pub mod panel;

use crate::{
    app::{
        data::app_nodes::{
            ControlToNode,
            FilterToNode,
            SinkToNode,
            SourceToNode,
        },
        events::GuiEvent,
        gui::{
            menu::draw_menu,
            options::GuiOptions,
            panel::{
                panel_add::create_panel_add,
                panel_node::create_panel_node,
            },
        },
    },
    impls::{
        files::TextureMessage,
        midir::MidiNode,
    },
    nodes::{
        control::draw_control_nodes,
        filter::draw_filter_nodes,
        sinks::draw_sink_nodes,
        sources::draw_source_nodes,
    },
};
#[cfg(unix)]
use crate::impls::pipewire::{
    PipewireCamera,
    PipewireStream,
};
use egui_winit_vulkano::{
    egui::{
        CentralPanel,
        Color32,
        Context,
        RichText,
        Shadow,
        ScrollArea,
        // TopBottomPanel,
        Visuals,
    },
    RenderResources,
};
#[cfg(unix)]
use pipewire::core::Core;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
    sync::{
        mpsc,
        Arc,
    },
};
use vulkano::{
    command_buffer::allocator::StandardCommandBufferAllocator,
    descriptor_set::allocator::StandardDescriptorSetAllocator,
};
use winit::event_loop::EventLoopProxy;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn layout_gui(
    avg_fps: f32,
    command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
    context: Context,
    descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
    event_loop_proxy: EventLoopProxy<GuiEvent>,
    map_control_to_node: Rc<RefCell<ControlToNode>>,
    #[cfg(unix)]
    map_id_to_camera: Rc<RefCell<HashMap<u32, PipewireCamera>>>,
    map_filter_to_node: Rc<RefCell<FilterToNode>>,
    #[cfg(unix)]
    map_node_to_stream: Rc<RefCell<HashMap<String, PipewireStream>>>,
    map_sink_to_node: Rc<RefCell<SinkToNode>>,
    map_source_to_node: Rc<RefCell<SourceToNode>>,
    midi_node_rc: Rc<RefCell<MidiNode>>,
    options_gui: Rc<RefCell<GuiOptions>>,
    #[cfg(unix)]
    pw_core: Core,
    resources: RenderResources,
    sender_texture: mpsc::Sender<TextureMessage>,
) {
    // println!("test_gui");
    context
        .set_visuals(
            Visuals {
                window_shadow: Shadow::NONE,
                ..options_gui
                    .borrow()
                    .visuals
                    .to_owned()
            },
        );
    // Menu:
    // println!("test_menu");
    draw_menu(
        context
            .clone(),
        event_loop_proxy
            .clone(),
        midi_node_rc
            .clone(),
        options_gui
            .clone(),
    );
    // Add panel:
    // println!("test_add_panel");
    if
        options_gui
            .borrow()
            .panel_add_show
            .to_owned()
    {
        create_panel_add(
            context
                .clone(),
            event_loop_proxy
                .clone(),
            map_control_to_node
                .clone(),
            map_filter_to_node
                .clone(),
            #[cfg(unix)]
            map_node_to_stream
                .clone(),
            map_sink_to_node
                .clone(),
            map_source_to_node
                .clone(),
            options_gui
                .clone(),
            #[cfg(unix)]
            pw_core
                .clone(),
            resources
                .clone(),
            sender_texture
                .clone(),
        );
    }
    // Node panel:
    // println!("test_node_panel");
    if
        options_gui
            .borrow()
            .selected_node
            .is_some()
    {
        create_panel_node(
            context
                .clone(),
            event_loop_proxy
                .clone(),
            map_control_to_node
                .clone(),
            #[cfg(unix)]
            map_id_to_camera
                .clone(),
            map_filter_to_node
                .clone(),
            map_sink_to_node
                .clone(),
            map_source_to_node
                .clone(),
            midi_node_rc
                .clone(),
            options_gui
                .clone(),
            #[cfg(feature = "gstreamer")]
            // #[cfg(unix)]
            resources
                .clone(),
        );
    };
    // Central panel:
    // println!("test_central_panel");
    CentralPanel::default()
        .show(
            &context,
            |ui_central| {
                // Both and for whole area???
                ScrollArea::vertical()
                    .id_salt("main panel")
                    .show(
                        ui_central,
                        |ui_area| {
                            ui_area
                                .small(
                                    RichText::new(
                                        format!(
                                            "fps: {avg_fps:.1}",
                                        )
                                    )
                                        .color(
                                            Color32::GRAY,
                                        ),
                                );
                            // Wayland does not allow dropped files.
                            // ui_area.vertical_centered(|ui_centered| {
                            //     ui_centered.label("drop files here");
                            //     ui_centered.add_space(30.0);
                            // });
                            #[cfg(unix)]
                            if
                                options_gui
                                    .borrow()
                                    .debug_output
                                &&
                                !map_id_to_camera
                                    .borrow()
                                    .is_empty()
                            {
                                ui_area
                                    .label(
                                        "PipeWire video sources: [test]",
                                    );
                                for
                                    camera
                                in
                                    map_id_to_camera
                                        .borrow()
                                        .values()
                                {
                                    ui_area
                                        .label(
                                            format!(
                                                "• {}",
                                                camera.description,
                                            ),
                                        );
                                };
                            };
                            // Draw ControlNodes:
                            draw_control_nodes(
                                context
                                    .clone(),
                                event_loop_proxy
                                    .clone(),
                                map_control_to_node
                                    .clone(),
                                options_gui
                                    .clone(),
                                ui_area,
                            );
                            // Draw SourceNodes:
                            draw_source_nodes(
                                context
                                    .clone(),
                                descriptor_set_allocator
                                    .clone(),
                                event_loop_proxy
                                    .clone(),
                                map_source_to_node
                                    .clone(),
                                options_gui
                                    .clone(),
                                ui_area,
                            );
                            // Draw FilterNodes:
                            draw_filter_nodes(
                                context
                                    .clone(),
                                event_loop_proxy
                                    .clone(),
                                map_filter_to_node
                                    .clone(),
                                map_source_to_node
                                    .clone(),
                                options_gui
                                    .clone(),
                                ui_area,
                            );
                            // Draw SinkNodes:
                            draw_sink_nodes(
                                command_buffer_allocator
                                    .clone(),
                                context
                                    .clone(),
                                descriptor_set_allocator
                                    .clone(),
                                event_loop_proxy
                                    .clone(),
                                map_filter_to_node
                                    .clone(),
                                map_sink_to_node
                                    .clone(),
                                map_source_to_node
                                    .clone(),
                                options_gui
                                    .clone(),
                                resources
                                    .clone(),
                                ui_area,
                            );
                        },
                    );
            },
        );
    // println!("test_gui_end");
}
