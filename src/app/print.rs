
use crate::nodes::sinks::SinkNode;
use vulkano::format::Format;
use vulkano_util::context::VulkanoContext;
use winit::event_loop::ActiveEventLoop;
#[cfg(unix)]
use winit::platform::{
    wayland::ActiveEventLoopExtWayland,
    x11::ActiveEventLoopExtX11,
};

// ----------------------------------------------------------------------------

pub const LOAD_IMAGE: &str = "load ColorImage for";
pub const LOAD_TEXTURE_FROM_IMAGE: &str = "load texture from image";
pub const TEXTURE_ALREADY_KNOWN: &str = "texture already known";

pub const STACK_IMAGE_RECEIVED: &str = "stack image received";
pub const WINDOW_CONNECT_RECEIVED: &str = "window connect received";

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub enum PrintClass {
    CreateSinkNode(SinkNode),
    NewLine,
    None,
    ShouldNotHappen,
}

impl PrintClass {
    // use an append function for additional strings???
    pub fn print(
        self,
    ) {
        match self {
            Self::CreateSinkNode(sink_node) => {
                println!("-> create SinkNode \"{}\" ( {sink_node} )", sink_node.to_owned().get_id());
            },
            Self::NewLine => {
                println!();
            },
            Self::None => (),
            Self::ShouldNotHappen => {
                println!("!! this should not happen");
            },
        }
    }

}

// ----------------------------------------------------------------------------

// print_to_error??? print_to_debug???

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn print_greeter() {
    println!("pw-videomix v{}", env!("CARGO_PKG_VERSION"),);
    println!();
    println!(".. starting");
}

pub fn print_info(
    context_vulkano: &VulkanoContext,
    event_loop: &ActiveEventLoop,
    swapchain_format: &Format,
) {
    println!(
        ".. device:            {}", //"ii " ".. " "II "
        context_vulkano
            .device_name(),
    );
    println!(
        ".. type:              {:?}",
        context_vulkano
            .device_type(),
    );
    match
        event_loop
            .primary_monitor()
    {
        None => (),
        Some(monitor) => match
            monitor
                .name()
        {
            None => (),
            Some(name) => {
                println!(
        ".. primary monitor:   {name}",
                );
            },
        },
    };
    if
        event_loop
            .available_monitors()
            .count()
        >
        1
    {
        println!(
        ".. available monitors:",
        );
        for
            monitor
        in
            event_loop
                .available_monitors()
        {
            match
                monitor
                    .name()
            {
                None => (),
                Some(name) => println!(
        "      {name}",
                ),
            };
        };
    };

    // println!("ii memory: {:?}", context_vulkano.max_memory());
    #[cfg(unix)]
    println!(
        ".. platform:          {}",
        match
            event_loop
                .is_wayland()
        {
            true => "Wayland",
            false => match
                event_loop
                    .is_x11()
            {
                true => "X11",
                false => "unknown",
            },
        },
    );
    println!(
        ".. swapchain format:  {swapchain_format:?}",
    );
    match
        event_loop
            .system_theme()
    {
        None => (),
        Some(theme) => {
            println!(
        ".. system theme:      {theme:?}",
            );
        },
    };
}
