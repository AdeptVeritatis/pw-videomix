pub mod gui_events_buffer;
pub mod gui_events_layer;
pub mod gui_events_midi;
pub mod gui_events_node;
#[cfg(unix)]
pub mod gui_events_pipewire;
pub mod gui_events_storage;
pub mod gui_events_window;
// pub mod mpsc_events_texture;
pub mod window_events;

use crate::{
    app::events::{
        gui_events_midi::MidiEvent,
        gui_events_node::NodeEvent,
        gui_events_storage::StorageEvent,
    },
    nodes::NodeFamily,
};
use egui_winit_vulkano::egui::Vec2;
#[cfg(unix)]
use vulkano::buffer::Subbuffer;
use winit::window::WindowId;

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub enum GuiEvent {
    // Buffer:
    RecreateImage((NodeFamily, String, Vec2)),

    // Files:
    // use file dropped event to not read files during redraw???

    // Layer:
    LayerRemove((String, String)),

    // Nodes:
    // where is trigger of deselect event?
    ChangeNode(NodeEvent),
    // function needed in gui_events_node.rs???
    SelectedNode,

    // Midi:
    Midi(MidiEvent),

    // Pipewire:
    #[cfg(unix)]
    PipewireFrame((Subbuffer<[u8]>, String)),

    // Storage save/load set:
    Storage(StorageEvent),

    // Sink window:
    WindowCreate,
    WindowRemove(WindowId),
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
