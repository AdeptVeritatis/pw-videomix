pub mod app_directories;
pub mod app_midi;
pub mod app_nodes;
pub mod app_options;
pub mod app_pipewire;
pub mod app_threads;
pub mod app_time;
pub mod app_vulkano;

use crate::app::data::app_nodes::{
    ControlToNode,
    FilterToNode,
    SinkToNode,
    SourceToNode,
};
use serde::{
    Serialize,
    Deserialize,
};

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Clone)]
pub struct
    StorageContainer
{
    pub map_control_to_node:
        ControlToNode,
    pub map_filter_to_node:
        FilterToNode,
    pub map_sink_to_node:
        SinkToNode,
    pub map_source_to_node:
        SourceToNode,
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
