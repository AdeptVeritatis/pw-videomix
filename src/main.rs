// ----------------------------------------------------------------------------
// some thoughts:
// ----------------------------------------------------------------------------

// priority - first source has 0, next gets 1, ... to sort render queue
// even render in one pass, uneven in next; or just a sorted queue???
// no sorting at all, just luck or no care about single frames???

// Port positions get updated in draw, not in update.
// Own source position gets updated in draw, too.

// ----------------------------------------------------------------------------

// #![allow(clippy::too_many_arguments, clippy::single_match)]
// #![allow(clippy::type_complexity)]

pub mod app;
pub mod constants;
pub mod impls;
pub mod nodes;

use crate::app::{
    events::GuiEvent,
    print::print_greeter,
    PwVideomixApp,
};
use winit::{
    error::EventLoopError,
    event_loop::EventLoop,
};

// ----------------------------------------------------------------------------

// #[cfg(not(wasm_platform))]
fn main() -> Result<(), EventLoopError> {

// Greeter:
    print_greeter();

// App:
    let mut app: PwVideomixApp = PwVideomixApp::default();

// Gstreamer:
    #[cfg(feature = "gstreamer")]
    match
        gstreamer::init()
    {
        Err(error) => println!("!! error: {error}"),
        Ok(_) => (),
    };

// Pipewire:
    #[cfg(unix)]
    pipewire::init();

// Winit event loop:
    let event_loop: EventLoop<GuiEvent> = match
        EventLoop::with_user_event()
            .build()
    {
        Err(error) => panic!("!! error: {error}"),
        Ok(event_loop) => event_loop,
    };
    app.proxy = Some(
        event_loop
            .create_proxy(),
    );

// Run main loop:
    event_loop
        .run_app(
            &mut app,
        )
}

// #[cfg(wasm_platform)]
// fn main() {
//     panic!("Example not supported on Wasm");
// }
