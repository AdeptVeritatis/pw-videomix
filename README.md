<div align="center">
    <h1>pw-videomix</h1>
<h4> A video mixer node to use creative / artistic filter. </h4>
<!-- <h4> A video mixer node to use creative / artistic filter for PipeWire. </h4> -->

![screenshot](assets/screenshot.png)

</div>


# Description
`pw-videomix` is a video filter node with different input and output options.

Inputs can be pictures and stacks of pictures.\
Filter, converter or mixer nodes can be added and connected in a graph ui.\
Outputs can be monitor nodes within the app. Or separate windows to show on a different display. Or single and timed snapshots to uncompressed PNG files. Or video files encoded with GStreamer as Mkv or WebM (experimental! Linux only.).

Not implemented yet are video inputs and PipeWire in- and output streams (for e.g. webcams or screencasts from apps with Wayland).

It is written in `Rust`.\
It is platform independent. The PipeWire part will only be available in Unix builds.

***

It is a WIP prototype or better an experimental toolkit, as I use this for my kaleidoscopic art, which constantly changes.\
You can use it for whatever you like, but I would not recomend it for live performances at the moment (with the exception of an appreciative crowd).

<!-- One unintended use case could be a handy PipeWire video sink, once PipeWire streams are implemented. -->

# Requirements

Vulkan capable graphics card.

PipeWire instance running. (Linux only.)

GStreamer and gst-plugins(?). (Linux only due to cross-compiling issues. May be fixed.)

MIDI device and drivers running. (optional)

# Installation
A compiled binary will only be available for new versions!\
As developement can be rapid, it is recommended to build from source.\
Debug builds are significantly slower! Use release builds for any tests requiring performance.

A compiled binary for Linux is available in [./target/release/](https://gitlab.freedesktop.org/AdeptVeritatis/pw-videomix/-/blob/main/target/release/pw-videomix).

Don't forget to make it executable, if necessary.

A compiled [pw-videomix.exe](https://gitlab.freedesktop.org/AdeptVeritatis/pw-videomix/-/blob/main/target/x86_64-pc-windows-gnu/release/pw-videomix.exe) for Windows is available, too.

## Building from source
To build `pw-videomix`, you will need to have `Rust` installed. The recommended way to install `Rust` is from the [official download page](https://www.rust-lang.org/tools/install), using `rustup`.\
For Linux just install `rustup` from the official repositories of your distribution using the package manager.

Clone the main branch.

```
git clone https://gitlab.freedesktop.org/AdeptVeritatis/pw-videomix.git/
cd pw-videomix
cargo build --release --features gstreamer
./target/release/pw-videomix
```

Once cloned, you just need to `git pull` from the directory to update the source code.\
You can also build and start it with `cargo run --release` afterwards.

## Cross-compiling from Linux to Windows:

```
...
rustup target list
rustup target add x86_64-pc-windows-gnu
cargo build --release --target x86_64-pc-windows-gnu
```

App is in `./target/x86_64-pc-windows-gnu/release/pw-videomix.exe`.

# Usage
## Getting started:
Start `pw-videomix`.

Drag files into the main window. (Doesn't work with Wayland or Windows.)\
You can drop multiple files at once.

Or use the `File` button of the `Input` section in the left panel.

Pictures will open as nodes.\
Multiple pictures will open as stack nodes.

Open a mandala filter and a window sink.

Connect the filter to the picture.\
Connect the window to the filter.

Set window to play.

Click the filter. A side panel with more options will show up.\
Change parameters on the filter.

Right click on a node for a context menu to remove it.

## Filter classes:
 | filter | description |
 |--- |---
 | art: | |
 | Color Mixer | mixes colors from a connected source |
 | Color Rotator | changes hue from a connected source |
 | Kaleidoscope | creates wallpaper tiles from a connected source |
 | Mandala | creates mandala from a connected source |
 | Rotator | rotates image from a connected source - tba |
 | connect: | |
 | Fader | slideshow of connected sources over time |
 | Mixer | mixes multiple connected sources constantly |

## MIDI input:
Connect your MIDI device to your computer.\
There is a `MIDI` option in the top menu bar of the app.\
`Enable` MIDI in the menu.\
Select the MIDI device, you want to `Connect` to.\
Select the knob mode in the drop down menu depending on your device.

At the moment, only a couple of values from the mandala nodes can be changed.\
Click on the MandalaNode to open the side panel.\
Variables in the upper block of the side panel:\
fold\
size\
speed

Right click `on the label` (NOT on the changeable value!) and click `Touch MIDI knob to select channel!`.\
The next knob you touch will be assigned to this value.

 | knob mode | description |
 |--- |---
 | rotary knob: | absolute position of the knob |
 | absolute | from 0 to 127 |
 | rotary encoder: | direction and speed of turning the knob |
 | | (ccw/left) -3, -2, -1, offset, +1, +2, +3 (cw/right) |
 | relative \[0\] | 125, 126, 127, 0, 1, 2, 3 |
 | relative \[16\] | 13, 14, 15, 16, 17, 18, 19 |
 | relative \[64\] | 61, 62, 63, 64, 65, 66, 67 |

I tried an Arturia MINILAB mkII with the rotary encoders set to 'Relative#1' in the respective 'MIDI Control Center' and the knob mode `relative [64]`.\
Also an Akai MIDImix in `absolute` mode with its many knobs and sliders.

# Known shortcomings
## Severe:
After changing the output size of a filter node, the next node in the filter chain doesn't update the size of its frame buffer.
You need to reconnect to update the frame buffer.
May be fixed soon.

## Wayland:
Files can not be dropped into the app.

In an early phase, the available and the preferred swapchain format didn't match sometimes. That was fixed.
But in case a similar error happens again, you can set the preferred swapchain format in `src/constants.rs` at `pub const SWAPCHAIN_FORMAT: Format =`\
After that, you need to build the app again.

## Handling:
My concept of frames is very fluid and still unspecified. It may always be like this.
Don't expect any stability and reliability for any timings.

Gui is still rudimentary. Drag-and-drop to connect nodes in the graph may be implemented later. Or be used to add, remove and sort source elements of e.g. a stack node.
The context menu could be used for more options and parameters.

## Performance:
Loading textures is partly done in threads.
It may take a while and can block the main window at certain times nonetheless.
This happens when opening pictures.
But once the textures are loaded, everything is really smooth.

Spawning threads by opening a huge amount of images may come to a limit very fast.(?)

Opening a lot of sink windows may result in heavy ressource use.

## GStreamer:
A way to decode and encode video files in a Rust project is the GStreamer crate.

Encoding videos is very experimental. Often frames get lost.\
Mkv with Theora works and gives a seemingly uncompressed video file with a good quality.\
WebM with VP8 codec works, gives a highly compressed video file, but the quality is bad.\
WebM with VP9 codec breaks after a couple of frames here. But it has an acceptable quality for its small file size.

You should not be able to overwrite files.

Cross-compilation needs to be fixed. At the moment, functionality will only be available on Linux for:\
Encoder\
There should be a solution to build `cargo build --release --target x86_64-pc-windows-gnu --features gstreamer`.
The ones I found seemed easy, but I didn't understand any of it.

## PipeWire:
Preparations should be done for most of the framework.
You can see availabe video sources listed in the panel background (if debug output is activated in the menu).
The (deactivated) button for a PipeWire input stream creates the stream object but doesn't connect it.
You can connect it with a graph manager (e.g. qpwgraph) to video sources. And disconnect it.

At the moment, file descriptors pointing to areas in memory are received from the stream.
[pw-capture](https://github.com/EHfive/pw-capture) shows a possible solution.
But as long as I don't know a _safe_ method to access this data in memory, I will focus on different parts of the app.
The situation may change soon or in a couple of months. Who knows. Maybe there will be a different solution.
The GStreamer pipeline is so flexible, it could be used to create the PipeWire in- and outputs in the meantime.

# Roadmap
Add more filter types (e.g. other tiling options).

Add PipeWire stream input.\
Add PipeWire source output.

Add video input.\
Add video converter node.

Add mandala preview with borders painted on the template image.
Add screen capture input with borders painted on the screen.

Error handling, bug fixes.

Maybe localization.

Maybe you have a nice idea or need something really urgent.

# Construction zone:
More filter, connection, cleanup, ...

# Support and contributing
As I am an amateur in publishing my software and this is a private project, I am not familiar with all the habits.

But I am trying to be open for your remarks and wishes. And the issues tracker.

Matrix channel: [#pw-videomix:tchncs.de](https://app.element.io/#/room/#pw-videomix:tchncs.de)

You can contact me on Mastodon [@AdeptVeritatis@social.tchncs.de](https://social.tchncs.de/@AdeptVeritatis).\
Or just open an issue. I'm fine with that, too.

# Naming
If you have any problem with any names or terms I used for whatever reason, please contact me.

I tried to find a suitable name for the program and I wanted to let room for others. So I kept it close to my previous project [pw-midimix](https://gitlab.freedesktop.org/AdeptVeritatis/pw-midimix). Other ideas were "pw-filter-mandala" or "pw-filter-video" or "mandala-mixer" or ...
But it is supposed to be a video mixer node with different input, filter and output options.

## Filter requests
If you have an idea about new creative possibilities, don't hesitate to ask.

## Redistribution
This is free and open source software. As long as you adhere to the license, you can do whatever you want with it.

You don't like the name? Clone it and rename it!

You don't like, how I handle the project? Clone it and continue as you like!

You like the idea but want to implement it differently? Copy the idea!

You want to see a Flatpak repository of this? Do it yourself!

You want to sell this app? Stick to the license (especially the redistribution of modifications)!\
Maybe you want to hire someone to take care of the code.

# My motivation
Please read [./docs/diary.txt](https://gitlab.freedesktop.org/AdeptVeritatis/pw-videomix/-/blob/main/docs/diary.txt).

Everything personal is there.

# Acknowledgments
Thanks to:

Masahiro Kahata for inspring me to write my own graphics software.

[egui](https://crates.io/crates/egui) and the [examples](https://github.com/emilk/egui/tree/master/examples)

[egui_winit_vulkano](https://crates.io/crates/egui_winit_vulkano) and the [examples](https://github.com/hakolao/egui_winit_vulkano/tree/master/examples)

[PipeWire](https://pipewire.org/)

[pipewire-rs](https://crates.io/crates/pipewire) and the [docs](https://pipewire.pages.freedesktop.org/pipewire-rs/pipewire/)

[pw-viz](https://github.com/Ax9D/pw-viz) again for your code example. It helped me to find an entrance into GUIs.

[rfd](https://crates.io/crates/rfd)

[vulkano-rs](https://crates.io/crates/vulkano) and the [examples](https://github.com/vulkano-rs/vulkano/tree/master/examples)

[winit](https://crates.io/crates/winit) and the [examples](https://github.com/rust-windowing/winit/tree/master/examples)

[The Rust Programming Language](https://doc.rust-lang.org/book/title-page.html) and the [docs](https://doc.rust-lang.org/stable/std/index.html)

and more

# Project status
Still motivated.
